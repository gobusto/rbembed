/**
@file
@brief The program start point.

This file simply contains the `main()` function called when the program begins.
*/

#include "engine/cqSystem.h"

/**
@brief The program entry point.

The program begins here.

@param argc Length of the argv array.
@param argv Array of command-line program arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  if (sysInit(argc, argv)) { sysMainLoop(); }
  sysQuit();
  return 0;
}
