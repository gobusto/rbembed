Compiling Conqore 2 from source on Windows
==========================================

Conqore 2 is officially supported on both 32-bit and 64-bit Windows, and should
compile on all versions from Windows XP onwards - unless one of the third-party
development libraries listed below stops supporting older versions, of course.

What you need
-------------

In addition to [Conqore itself](https://gitlab.com/gobusto/rbembed/tree/master),
you'll also need to download the following third-party development libraries:

+ [SDL2](http://libsdl.org) (MinGW version)
+ [SDL2 mixer](https://www.libsdl.org/projects/SDL_mixer) (MinGW version)
+ [Ruby](http://rubyinstaller.org) (RubyInstaller, _not_ Development Kit)
+ [Open Dynamics Engine](https://bitbucket.org/odedevs/ode/downloads)

You'll also need [Code::Blocks](http://www.codeblocks.org/downloads) with MinGW
to actually compile anything!

*NOTE: You may need to install 7zip to extract the SDL and ODE .tar.gz files.*

Setting up your development machine
-----------------------------------

Before you can compile Conqore, you'll need to install the Code::Blocks IDE and
unzip the various development libraries.

### Install Code::Blocks (currently version 16.01)

Go the the Code::Blocks downloads page and click "Download the binary release".
The file should be called `codeblocks-16.01mingw-setup.exe` (or similar).

Once you've downloaded the installer, run it - you shouldn't need to change any
options, so just click Next until you're done.

### Unzip SDL (currently version 2.0.5)

Unzip `SDL2-devel-2.0.5-mingw.tar.gz` to `C:\`, so that you end up with:

+ `C:\SDL2-2.0.5\i686-w64-mingw32\include\`
+ `C:\SDL2-2.0.5\i686-w64-mingw32\lib\`
+ `C:\SDL2-2.0.5\x86_64-w64-mingw32\include\`
+ `C:\SDL2-2.0.5\x86_64-w64-mingw32\lib\`
+ ...some other stuff that we don't care about right now...

### Unzip SDL Mixer (currently version 2.0.1)

Unzip `SDL2_mixer-devel-2.0.1-mingw.tar.gz` to `C:\`, so that you end up with:

+ `C:\SDL2_mixer-2.0.1\i686-w64-mingw32\include\`
+ `C:\SDL2_mixer-2.0.1\i686-w64-mingw32\lib\`
+ `C:\SDL2_mixer-2.0.1\x86_64-w64-mingw32\include\`
+ `C:\SDL2_mixer-2.0.1\x86_64-w64-mingw32\lib\`
+ ...some other stuff that we don't care about right now...

### Install Ruby (currently version 2.3.3)

Run `rubyinstaller-2.3.3.exe`; this installs to `C:\`, so that you end up with:

+ `C:\Ruby23\include\`
+ `C:\Ruby23\lib\`
+ ...some other stuff that we don't care about right now...

### Compile ODE (currently version 0.14)

Extract `ode-0.14.tar.gz` to `C:\`, so that you end up with:

+ `C:\ode-0.14\include\`
+ `C:\ode-0.14\build\`
+ ...some other stuff that we don't care about right now...

Using the command prompt, enter the following:

    cd C:\ode-0.14\build
    premake4.exe codeblocks --enable-double-precision --with-trimesh=gimpact --enable-libccd

You should now have a file called `ode.cbp` in `C:\ode-0.14\build\codeblocks\`.
Open it in Code::Blocks, select `ReleaseDoubleLib` from the drop-down menu, and
finally click `Build > Rebuild` in the menu bar to compile ODE.

Assuming the build finishes without errors, there should now be a `lib/` folder
in `C:\ode-0.14` containing `ReleaseDoubleLib/libode_double.a`. If you run into
problems, download a newer (or older) version of ODE and try again.

Include the various `include/` and `lib/` directories within Code::Blocks
-------------------------------------------------------------------------

In Code::Blocks, go to `Settings > Compiler...` and click `Search Directories`.
The paths you'll need to specify here vary slightly depending on whether you're
compiling a 32-bit or 64-bit executable.

### 32-bit systems

*NOTE: This also works on 64-bit systems, but creates 32-bit executables.*

Add the following to the `Compiler` sub-tab:

+ `C:\SDL2-2.0.5\i686-w64-mingw32\include`
+ `C:\SDL2-2.0.5\i686-w64-mingw32\include\SDL2`
+ `C:\SDL2_mixer-2.0.1\i686-w64-mingw32\include`
+ `C:\Ruby23\include\ruby-2.3.0`
+ `C:\Ruby23\include\ruby-2.3.0\i386-mingw32`
+ `C:\ode-0.14\include`

Add the following to the `Linker` sub-tab:

+ `C:\SDL2-2.0.5\i686-w64-mingw32\lib`
+ `C:\SDL2_mixer-2.0.1\i686-w64-mingw32\lib`
+ `C:\Ruby23\lib`
+ `C:\ode-0.14\lib\ReleaseDoubleLib`

### 64-bit systems

*NOTE: If you run into problems here, try compiling a 32-bit version instead.*

Add the following to the `Compiler` sub-tab:

+ `C:\SDL2-2.0.5\x86_64-w64-mingw32\include`
+ `C:\SDL2-2.0.5\x86_64-w64-mingw32\include\SDL2`
+ `C:\SDL2_mixer-2.0.1\x86_64-w64-mingw32\include`
+ `C:\Ruby23-x64\include\ruby-2.3.0`
+ `C:\Ruby23-x64\include\ruby-2.3.0\x64-mingw32`
+ `C:\ode-0.14\include`

Add the following to the `Linker` sub-tab:

+ `C:\SDL2-2.0.5\x86_64-w64-mingw32\lib`
+ `C:\SDL2_mixer-2.0.1\x86_64-w64-mingw32\lib`
+ `C:\Ruby23-x64\lib`
+ `C:\ode-0.14\lib\ReleaseDoubleLib`

Compile Conqore 2
-----------------

Open the `conqore2.cbp` project file in Code::Blocks, select `Win32` from the
drop-down menu, and finally click `Build > Rebuild` in the menu bar. If you did
everything correctly, this should compile with zero errors.

If you want to compile it without a built-in renderer, select the `Win32 (DLL)`
option instead; just remember to compile one of the "external renderer" project
files so that the `.exe` can load the relevant .DLL/.SO/.DYLIB when it starts!

Miscellaneous
-------------

The required third-party DLL files are included somewhere within the folders of
SDL, SDL Mixer, and Ruby. Copy them into the directory containing the Conqore 2
`.exe` file and you should be good to go!
