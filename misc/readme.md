Building ODE
============

**Update:** Newer versions of ODE seem to work fine - in fact, they work better
than before! It's now much harder to push stuff through walls, and defining the
`dSINGLE`/`dDOUBLE` constant is no longer necessary. However, there is an extra
step necessary when building: Before `./configure`, use `./bootstrap`. This may
be all you need to do; if you get any errors, check the ODE `INSTALL.TXT` file,
install any missing stuff (such as autotools, libtool, etc.) and try again.

The ODE source code is no longer hosted at SourceForge; new versions can now be
downloaded from <https://bitbucket.org/odedevs/ode/downloads> instead.

### Original version:

Conqore2 was built around ODE v0.12 with GIMPACT trimesh support enabled. v0.13
causes various problems, so you should probably stick with 0.12 instead. Debian
users should extract `ode-0.12.tar.bz2` and then enter the following in bash:

    ./configure --enable-double-precision --with-trimesh=gimpact --enable-libccd
    make
    make install

Note that you might need to be root (i.e. use `su` or `sudo`) for the last step.
Also note that cylinder/capsule collision won't work without `libccd` enabled.

For Windows, I generally use `premake4.exe` to generate a Code::Blocks project;
check the `INSTALL.TXT` included with ODE for details.

NOTE: Win32 builds should be single-precision, Debian builds double-precision.
