Conqore 2
=========

Conqore 2 is a free and open-source game engine written in C.

It's not really "finished" yet, but it's complete enough to do some interesting
things. Features include:

+ Realistic physics via the [Open Dynamics Engine](http://ode.org).
+ Scripting using the [Ruby](http://ruby-lang.org) programming language.
+ Music and basic "3D" audio effects (panning and distance-based volume).
+ Support for a variety of model, image, and audio file formats.
+ Input abstraction (Keyboard/Mouse/Joypad); scripts don't worry about details.
+ Cross-platform: Runs on Windows, GNU/Linux, and MacOS using SDL and OpenGL.
+ A graphical renderer can be "compiled in" **or** loaded from a DLL at runtime.

TODO
----

+ Tidy up the code and document stuff with [Doxygen](http://www.doxygen.nl).
+ Fix the remaining problems with ragdolls (mass, shape, and orientation).
+ Support skeletal animation (MS3D actors can currently be static or ragdolls).
+ Figure out how save-games (including physics state/Ruby state) should work.

Maybe TODO
----------

+ Improve lighting support; currently, only static point lights are implemented.
  + Pre-calculate "power-of-effect" for static lights, so RGB values can change?
+ Add support for "one-shot" sounds (for conversations, etc).
  + Maybe do this by having "temporary" sounds, loaded on a per-map basis?
+ Add support for non-OpenGL renderers such as DirectX or software-based output.
+ Add support for "advanced" real-time audio effects: reverb, equalisation, etc.
+ Maybe add explicit (i.e. not actor-model-based) support for particle effects?
+ Maybe split the `cqSystem_SDL2.c` code into general and SDL-specific parts?

Creating maps/entities/scripts/etc.
-----------------------------------

I've not written much here yet - have a look at the various XML files in the
`bin/` directory, and the `readme.md` file in `bin/scripts/` for more info.

If you need textures/models/sounds, try [OpenGameArt](http://opengameart.org).

FAQ
---

### Why Conqore "2"? Where's the first one?

I abandoned [Conqore 1](https://sites.google.com/site/gobusto/projects/conqore)
years ago. It used [Python 2](http://python.org) instead of Ruby, but it wasn't
integrated properly and the general design of the core code wasn't particularly
well-thought-out. Conqore 2.0 has the same basic goals as the original project,
but shares none of the original code - it was written completely from scratch.

### Is it possible to make 2D games?

The "sprite" system  allows you to do this - though it's probably a better idea
to use a purpose-built 2D game engine instead.

### Is there any support for network play?

Not in the core engine (yet?), but it should be possible to do something with a
Ruby-side network connection manager.

### What 3D mesh formats are supported?

See the [LoadGameMesh](http://gitlab.com/gobusto/loadgamemesh) documentation; I
regularly re-sync the code in Conqore with it.

### What image formats are supported?

See the [stb_image](https://github.com/nothings/stb/) documentation. To replace
stb_image with something else, simply replace `engine/bitmap/bitmap_stb.c` with
something else - such as `bitmap_DevIL.c` - and change any affected libs in the
project settings.

### What audio formats are supported?

See the [SDL mixer](http://www.libsdl.org/projects/SDL_mixer) documentation. It
is possible to switch `engine/cqAudio_SDLmixer2.c` with something else (as with
stb_image, above), though there's quite a bit more work involved.

### Can I replace the other API-specific files with my own versions?

Yes! Conqore 2 has been designed to have a clean separation of "interface" (i.e
the structures or functions in the header files) and "implementation" (the `.c`
or `.cpp` files that implement those functions). If you want to replace the ODE
physics code with [Bullet](http://bulletphysics.org), for example, you can just
replace the `engine/cqPhysics_ODE.c` file with a `cqPhysics_Bullet.cpp` version
and recompile. This modularity ensures that any changes to one sub-system don't
require further amendments elsewhere.

### Is this related to [Spinel](https://github.com/cadwallion/spinel) at all?

No, but you should totally check out Andrew Nordman's presentation at Ruby Conf
12 on Youtube: <http://www.youtube.com/watch?v=H5_Kid3hpRs>

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license, which allows you to use or modify it for any purpose including paid-for
and closed-source projects. All I ask in return is that proper attribution is
given (i.e. don't remove my name from the copyright text in the source code, and
perhaps include me on the "credits" screen.
