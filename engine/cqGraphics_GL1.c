/**
@file
@brief An OpenGL 1.x implementation of the functions defined in cqGraphics.h

This code should be able to run on just about anything, but doesn't include any
fancy effects, such as "real" lighting or shaders. A more fully-featured OpenGL
2.x (or later) implementation will probably be provided in the future.

Note that the lighting calculation (performed when maps are first loaded) could
take quite a while for complex maps/slow CPUs.
*/

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqGraphics.h"
#include "cqPhysics.h"
#include "cqWorld.h"
#include "bitmap/bitmap.h"
#include "xml/unicode.h"

/* NOTE: For "fast" lighting, an RGB value is calculated/cached per-vertex. */
#if 1
  #define GFX_FAST_LIGHTING /**< @brief Calculate fast/less-correct lighting. */
#endif

/**
@brief [INTERNAL] Stores any image which may be used by sprites.

NOTE: This is currently used for both (A) the internal STORAGE of textures and
(B) a way for sprites to REFERENCE those textures. In other words, sprites will
use this structure for read-only purposes, whilst the internal image storage
will actually load/free textures stored in the texture_id attribute. This may
be split into two distinct structures in the future...

@todo Include the original image dimensions here, for later reference? Only
needed by the "storage" structure, since sprites will define their own size.
*/

struct _image_s
{

  GLuint texture_id;  /**< @brief about */

};

typedef struct _image_s image_s;  /**< @brief Nicer name for struct _image_s */

/**
@brief about

writeme
*/

struct _material_s
{
  long num_items;   /**< @brief about */
  GLuint *diffuse;  /**< @brief about */
};

typedef struct _material_s material_s;  /**< @brief about */

/* Global objects and variables. */

const GLfloat gfx_ambient_light[4] = { 0, 0, 0, 1 };

GLfloat gfx_width  = 640; /**< @brief about */
GLfloat gfx_height = 480; /**< @brief about */

wordtree_s *gfx_image_set = NULL; /**< @brief about */

GLuint      gfx_world_dlist = 0; /**< @brief about */
material_s *gfx_world_mtrl = NULL; /**< @brief about */

void (*gfxSwapBuffers)(void) = NULL;  /**< @brief about */

PHYS_ACTOR_IS_RAGDOLL((*physActorIsRagdoll_func));  /**< @brief about */
PHYS_ACTOR_GET_POSITION((*physActorGetPosition_func));  /**< @brief about */
PHYS_ACTOR_GET_QUATERNION((*physActorGetQuaternion_func));  /**< @brief about */
ACTOR_UPDATE_MESH((*actorUpdateMesh_func));  /**< @brief about */

/*
[PUBLIC] Get the "name" of the renderer being used.
*/

const char *gfxRendererName(void) { return "Basic OpenGL 1.x Renderer"; }

/*
[PUBLIC] Get the API version of the renderer being used.
*/

const char *gfxApiVersion(void) { return "Conqore2_2017-04-14"; }

/**
@brief <b>[INTERNAL]</b> Load an image file as an OpenGL texture.

writeme

@param file_name The filename to be loaded.
@param flags about
@return A valid GLuint texture ID on success or zero on failure.
*/

static GLuint gfxLoadTexture(const char *file_name, v2d_flag_t flags)
{

  bmp_image_s *image = NULL;
  GLuint gl_texture = 0;
  GLenum format;
  GLint param;

  /* Attempt to load the image file. */

  image = bmpLoadFile(file_name);
  if (!image) { return 0; }

  switch (image->bpp)
  {
    case 32: format = GL_RGBA;            break;    /* RGB with alpha.       */
    case 24: format = GL_RGB;             break;    /* RGB only.             */
    case 16: format = GL_LUMINANCE_ALPHA; break;    /* Greyscale with alpha. */
    case 8:  format = GL_LUMINANCE;       break;    /* Greyscale only.       */
    default: bmpFree(image);              return 0; /* Unknown pixel format. */
  }

  /* Create an OpenGL texture and copy the pixel data into it. */

  glGenTextures(1, &gl_texture);
  glBindTexture(GL_TEXTURE_2D, gl_texture);

  gluBuild2DMipmaps(GL_TEXTURE_2D, image->bpp / 8, image->w, image->h, format, GL_UNSIGNED_BYTE, image->data);

  param = flags & IMAGE_FLAG_NEAREST ? GL_NEAREST : GL_LINEAR_MIPMAP_LINEAR;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);

  param = flags & IMAGE_FLAG_CLAMP_X ? GL_CLAMP : GL_REPEAT;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, param);

  param = flags & IMAGE_FLAG_CLAMP_Y ? GL_CLAMP : GL_REPEAT;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, param);

  /* Free the bitmap data structure and return the OpenGL texture. */

  bmpFree(image);

  return gl_texture;

}

/**
@brief [INTERNAL] about

writeme

@param material about
@return writeme
*/

static material_s *gfxMaterialDelete(material_s *material)
{

  long i = 0;

  if (!material) { return NULL; }

  if (material->diffuse)
  {
    for (i = 0; i < material->num_items; i++) { glDeleteTextures(1, &material->diffuse[i]); }
    free(material->diffuse);
  }

  free(material);

  return NULL;

}

/**
@brief [INTERNAL] about

writeme

@param mesh about
@return writeme
*/

static material_s *gfxMaterialCreate(mesh_s *mesh)
{

  material_s *material = NULL;
  long i = 0;

  if      (!mesh                    ) { return NULL; }
  else if (!mesh->mtrl              ) { return NULL; }
  else if (mesh->mtrl->num_items < 1) { return NULL; }

  material = (material_s*)malloc(sizeof(material_s));
  if (!material) { return NULL; }
  memset(material, 0, sizeof(material_s));

  material->num_items = mesh->mtrl->num_items;
  material->diffuse = (GLuint*)malloc(sizeof(GLuint) * material->num_items);

  if (!material->diffuse)
  {
    gfxMaterialDelete(material);
    return NULL;
  }

  for (i = 0; i < mesh->mtrl->num_items; ++i)
  {
    material->diffuse[i] = gfxLoadTexture(mesh->mtrl->item[i]->diffusemap, 0);
    if (!material->diffuse[i]) { fprintf(stderr, "[WARNING] Could not load texture %s\n", mesh->mtrl->item[i]->diffusemap); }
  }

  return material;

}

/**
@brief [INTERNAL] Render a string of UTF-8 text using a 16x16 glyph font image.

The font image must be organised so that the first 6 rows contain the printable
ASCII characters (i.e from Space at ASCII code 32 to Delete at ASCII code 127),
with the next 4 rows containing the JIS X 0201 half-width katakana characters.
The final 6 rows should be used to store the Unicode "Latin-1" character range.

There are a couple of special cases: The first character of the 7th row (where
the half-width katakana characters begin) is the Unicode "Unknown Glyph" image.
Additionally, the first charater on the 11th row (where the Latin-1 characters
begin) is the Euro symbol. It's also worth noting that the ASCII tab character
is mapped to the space character glyph, so it won't be displayed as an Unknown
Character.

@todo Allow custom code range mappings to be specified for better international
support.

@param text The string of UTF-8 text to be drawn.
@return The number of glyphs drawn.
*/

static int gfxDrawText(const char *text)
{

  const double GLYPH_SIZE = 1.0 / 16.0;

  long utf = 0;  /* The Unicode value of a particular character. */
  unsigned char use = 0;  /* The 8-bit mapping of that Unicode character. */
  double x, y;
  int size, count;

  if (!text) { return 0; }

  glBegin(GL_QUADS);

  for (count = 0; text[0] != 0; count++)
  {

    /* Attempt to read a (multi-byte) UTF-8 character from the text buffer. */

    size = utfDecodeUTF8(text, &utf);
    if (size < 1) { break; }
    text += size;

    /* Map the Unicode value to one of the 256 available character glyphs. */

    if      (utf >= 0x0020 && utf <= 0x007F) { use = 0   + (utf - 0x0020); }
    else if (utf >= 0xFF61 && utf <= 0xFF9F) { use = 96  + (utf - 0xFF60); }
    else if (utf >= 0x00A1 && utf <= 0x00FF) { use = 160 + (utf - 0x00A0); }
    else if (utf == 0x20AC                 ) { use = 160; }  /* Euro sign. */
    else if (utf == 0x000B                 ) { use = 0;   }  /* ASCII tab. */
    else                                     { use = 96;  }  /* Any other. */

    /* Draw whichever glyph the unicode character has been mapped to. */

    x =                       GLYPH_SIZE * (use % 16);
    y = (1.0 - GLYPH_SIZE) - (GLYPH_SIZE * (use / 16));

    glTexCoord2f (x,              y + GLYPH_SIZE); glVertex2f(count - 0.5, +0.5);
    glTexCoord2f (x + GLYPH_SIZE, y + GLYPH_SIZE); glVertex2f(count + 0.5, +0.5);
    glTexCoord2f (x + GLYPH_SIZE, y             ); glVertex2f(count + 0.5, -0.5);
    glTexCoord2f (x,              y             ); glVertex2f(count - 0.5, -0.5);

  }

  glEnd();

  return count;

}

/**
@brief [INTERNAL] about

@param mesh about
@param vertex_id about
@param norm about
*/

static void gfxColorAtPoint(const mesh_s *mesh, long vertex_id, const vector3_t norm, const lamp_s *lamp)
{
  const float *xyz = &mesh->vertex[0][vertex_id*3];

  #ifdef GFX_FAST_LIGHTING
    float *rgb = &mesh->final_vertex[vertex_id*3];
    if (rgb[0] >= 0) { glColor3f(rgb[0], rgb[1], rgb[2]); return; }
    if (norm) { /* Silence "unused parameter" warning. */ }
  #else
    float rgb[3];
  #endif

  rgb[0] = gfx_ambient_light[0];
  rgb[1] = gfx_ambient_light[1];
  rgb[2] = gfx_ambient_light[2];

  for (; lamp; lamp = lampGetNext(lamp))
  {
    vector3_t src;
    vector3_t dir;
    float distance;
    long g, t;
    v2d_bool_t okay;

    /* The lamp origin may be given as doubles; convert it into floats: */
    src[0] = lamp->origin[0];
    src[1] = lamp->origin[1];
    src[2] = lamp->origin[2];

    /* Calculate the relative direction/distance to the lamp: */
    vecSubtract(dir, src, xyz);
    distance = vecLength(3, dir);
    vecNormalize(3, dir);

    if (distance >= lamp->radius) { continue; }
    #ifndef GFX_FAST_LIGHTING
      if (vecDotProduct(norm, dir) < 0) { continue; }
    #endif

    for (okay = V2D_TRUE, g = 0; okay && g < mesh->num_groups; ++g)
    {
      for (t = 0; okay && mesh->group[g] && t < mesh->group[g]->num_tris; ++t)
      {
        const float *v0 = &mesh->vertex[0][mesh->group[g]->tri[t]->v_id[0]*3];
        const float *v1 = &mesh->vertex[0][mesh->group[g]->tri[t]->v_id[1]*3];
        const float *v2 = &mesh->vertex[0][mesh->group[g]->tri[t]->v_id[2]*3];
        vector3_t tmp;
        float d;
        int i;

        /* Ignore any triangles that are too far away: */
        for (i = 0; i < 3; ++i)
          if (xyz[i] < src[i])
          {
            if ((v0[i] < xyz[i] && v1[i] < xyz[i] && v2[i] < xyz[i]) ||
                (v0[i] > src[i] && v1[i] > src[i] && v2[i] > src[i])) { break; }
          }
          else
          {
            if ((v0[i] < src[i] && v1[i] < src[i] && v2[i] < src[i]) ||
                (v0[i] > xyz[i] && v1[i] > xyz[i] && v2[i] > xyz[i])) { break; }
          }
        if (i < 3) { continue; }

        /* Ignore any triangles with vertices very close to this one: */
        vecSubtract(tmp, xyz, v0);
        if (tmp[0] > -0.01 && tmp[0] < 0.01 &&
            tmp[1] > -0.01 && tmp[1] < 0.01 &&
            tmp[2] > -0.01 && tmp[2] < 0.01) { continue; }
        vecSubtract(tmp, xyz, v1);
        if (tmp[0] > -0.01 && tmp[0] < 0.01 &&
            tmp[1] > -0.01 && tmp[1] < 0.01 &&
            tmp[2] > -0.01 && tmp[2] < 0.01) { continue; }
        vecSubtract(tmp, xyz, v2);
        if (tmp[0] > -0.01 && tmp[0] < 0.01 &&
            tmp[1] > -0.01 && tmp[1] < 0.01 &&
            tmp[2] > -0.01 && tmp[2] < 0.01) { continue; }

        d = rayHitsTriangle(xyz, dir, v0, v1, v2);
        if (d > 0 && d < distance) { okay = V2D_FALSE; }
      }
    }

    /* If nothing gets in the way of the lamp, add its RGB to the total: */
    if (okay)
    {
      float power = 1.0 / (1.0 + (1.0/lamp->radius)*distance*distance);
      #ifndef GFX_FAST_LIGHTING
        power *= vecDotProduct(dir, norm);
      #endif
      rgb[0] += lamp->rgb[0] * power;
      rgb[1] += lamp->rgb[1] * power;
      rgb[2] += lamp->rgb[2] * power;
    }
  }

  if (rgb[0] > 1.0) { rgb[0] = 1.0; }
  if (rgb[1] > 1.0) { rgb[1] = 1.0; }
  if (rgb[2] > 1.0) { rgb[2] = 1.0; }
  glColor3f(rgb[0], rgb[1], rgb[2]);
}

/**
@brief [INTERNAL] about

writeme

@param actor about
@param mesh about
@param texture_set about
*/

static void gfxDrawMesh(actor_s *actor, const mesh_s *mesh, const material_s *texture_set, const lamp_s *lamp)
{
  const GLenum LIGHTS[8] = {
    GL_LIGHT0, GL_LIGHT1, GL_LIGHT2, GL_LIGHT3,
    GL_LIGHT4, GL_LIGHT5, GL_LIGHT6, GL_LIGHT7
  };

  long g, t, v;

  v2d_real_t offset_u, offset_v;

  if (!mesh) { return; }

  /* If this mesh belongs to an actor, update any skeletal mesh information. */
  if (actor)
  {
    const v2d_bool_t physics_transforms_on_the_cpu = physActorIsRagdoll_func(actor);

    vector3_t origin;
    int i;

    if (actorUpdateMesh_func(actor, physics_transforms_on_the_cpu) != 0) { return; }

    physActorGetPosition_func(actor, 0, origin);

    /* Set up lighting: */
    for (i = 0; lamp && i < 8; lamp = lampGetNext(lamp))
    {
      float difference[3];
      difference[0] = lamp->origin[0] - origin[0];
      difference[1] = lamp->origin[1] - origin[1];
      difference[2] = lamp->origin[2] - origin[2];

      if (vecLength(3, difference) < lamp->radius)
      {
        GLfloat lightvec[4];

        glEnable(LIGHTS[i]);

        lightvec[0] = lamp->origin[0];
        lightvec[1] = lamp->origin[1];
        lightvec[2] = lamp->origin[2];
        lightvec[3] = 1.00;
        glLightfv(LIGHTS[i], GL_POSITION, lightvec);

        lightvec[0] = lamp->rgb[0];
        lightvec[1] = lamp->rgb[1];
        lightvec[2] = lamp->rgb[2];
        lightvec[3] = 1.0;
        glLightfv(LIGHTS[i], GL_DIFFUSE, lightvec);

        glLightf(LIGHTS[i], GL_QUADRATIC_ATTENUATION, 1/lamp->radius);

        ++i;
      }
    }

    /*
    WORKAROUND FOR `OpenGL 2.1 Mesa 13.0.5 (Renderer: Mesa DRI Intel(R) 945G)`
    If `GL_LIGHTING` is enabled, but no `GL_LIGHT`s are, everything goes blue!
    (Not sure if this is a real Mesa bug or something specific to my machine.)
    */
    # if 1
      if (i == 0)
      {
        glEnable(LIGHTS[i]);
        glLightfv(LIGHTS[i], GL_DIFFUSE, gfx_ambient_light);
        glLightf(LIGHTS[i], GL_QUADRATIC_ATTENUATION, 1);
        ++i;
      }
    #endif

    /* Disable any unused lights: */
    for (; i < 8; ++i) { glDisable(LIGHTS[i]); }

    glPushMatrix();

    if (!physics_transforms_on_the_cpu)
    {
      quaternion_t q;
      physActorGetQuaternion_func(actor, 0, q);
      glTranslatef(origin[0], origin[1], origin[2]);
      glRotatef(2 * acos(q[QUAT_W]) * RAD2DEG, q[QUAT_X], q[QUAT_Y], q[QUAT_Z]);
    }

    offset_u = actor->texture_offset[0];
    offset_v = actor->texture_offset[1];
  }
  else
  {
    offset_u = 0;
    offset_v = 0;
  }

  /* Loop through each sub-mesh. */
  for (g = 0; g < mesh->num_groups; ++g)
  {
    mtrl_s *skin = NULL;

    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    /* Set the material. */

    if (texture_set && group->mat_id >= 0 && group->mat_id < texture_set->num_items)
    { glBindTexture(GL_TEXTURE_2D, texture_set->diffuse[group->mat_id]); }
    else
    { glBindTexture(GL_TEXTURE_2D, 0); }

    if (mesh->mtrl && group->mat_id >= 0 && group->mat_id < mesh->mtrl->num_items)
    { skin = mesh->mtrl->item[group->mat_id]; }
    else
    { skin = NULL; }

    if (actor && skin && skin->alpha < 1.0)
    {
      glDisable(GL_LIGHTING);
      glEnable(GL_BLEND);
      glColor4f(1, 1, 1, skin->alpha);
    }

    /* Begin drawing the sub-mesh. */
    glBegin(GL_TRIANGLES);

    for (t = 0; t < group->num_tris; ++t)
    {
      const mesh_tri_s *tri = group->tri[t];

      for (v = 0; v < 3; ++v)
      {
        vector3_t norm;

        if (actor && group->final_normal)
        {
          norm[0] = group->final_normal[t*3*3 + v*3 + 0];
          norm[1] = group->final_normal[t*3*3 + v*3 + 1];
          norm[2] = group->final_normal[t*3*3 + v*3 + 2];
        }
        else if (mesh->normal)
        {
          norm[0] = mesh->normal[0][tri->n_id[v]*3 + 0];
          norm[1] = mesh->normal[0][tri->n_id[v]*3 + 1];
          norm[2] = mesh->normal[0][tri->n_id[v]*3 + 2];
        }
        else
        {
          norm[0] = 0;
          norm[1] = 0;
          norm[2] = 1;
        }

        if (mesh->texcoord)
          glTexCoord2f(
            mesh->texcoord[tri->t_id[v]*2 + 0] + offset_u,
            mesh->texcoord[tri->t_id[v]*2 + 1] + offset_v
          );
        else
          glTexCoord2f(0, 0);

        if (actor)
          glNormal3f(norm[0], norm[1], norm[2]);
        else
          gfxColorAtPoint(mesh, group->tri[t]->v_id[v], norm, lamp);

        if (actor && mesh->final_vertex)
          glVertex3f(
            mesh->final_vertex[tri->v_id[v]*3 + 0],
            mesh->final_vertex[tri->v_id[v]*3 + 1],
            mesh->final_vertex[tri->v_id[v]*3 + 2]
          );
        else
          glVertex3f(
            mesh->vertex[0][tri->v_id[v]*3 + 0],
            mesh->vertex[0][tri->v_id[v]*3 + 1],
            mesh->vertex[0][tri->v_id[v]*3 + 2]
          );

      }

    }

    glEnd();

    if (actor && skin && skin->alpha < 1.0)
    {
      glEnable(GL_LIGHTING);
      glDisable(GL_BLEND);
    }
  }

  /* Restore the matrix state, if necessary. */
  if (actor) { glPopMatrix(); }
}

/*
[PUBLIC] Initialise the graphics sub-system.
*/

const char *gfxInit(int argc, char **argv, const render_init_s *render_init)
{
  gfxQuit();

  if (argc || argv) { /* Silence "unused parameter" warnings. */ }

  if (!render_init) { return "No renderer initialisation data provided."; }

  gfx_width = render_init->w;
  gfx_height = render_init->h;

  if (gfx_width < 1 || gfx_height < 1) { return "Invalid width/height value."; }

  gfxSwapBuffers = render_init->swap_func;
  physActorIsRagdoll_func = render_init->ragdoll_func;
  physActorGetPosition_func = render_init->position_func;
  physActorGetQuaternion_func = render_init->quaternion_func;
  actorUpdateMesh_func = render_init->mesh_func;

  if (!physActorIsRagdoll_func) { return "No ragdoll function provided."; }
  if (!physActorGetPosition_func) { return "No position function provided."; }
  if (!physActorGetQuaternion_func) { return "No quaternion function provided."; }
  if (!actorUpdateMesh_func) { return "No mesh-update function provided."; }

  fprintf(stderr, "[INIT] OpenGL %s\n", glGetString(GL_VERSION));
  fprintf(stderr, "\tRenderer: %s\n", glGetString(GL_RENDERER));
  fprintf(stderr, "\tVendor: %s\n", glGetString(GL_VENDOR));

  glHint(GL_FOG_HINT,                    GL_NICEST);
  glHint(GL_LINE_SMOOTH_HINT,            GL_NICEST);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glHint(GL_POINT_SMOOTH_HINT,           GL_NICEST);
  glHint(GL_POLYGON_SMOOTH_HINT,         GL_NICEST);

  glEnable(GL_NORMALIZE);
  glEnable(GL_TEXTURE_2D);

  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CW);

  glAlphaFunc(GL_GREATER, 0);
  glEnable(GL_ALPHA_TEST);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT0, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT1, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT2, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT3, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT4, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT5, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT6, GL_AMBIENT, gfx_ambient_light);
  glLightfv(GL_LIGHT7, GL_AMBIENT, gfx_ambient_light);

  if (!bmpInit()) { return "bmpInit() failed."; }

  gfx_image_set = treeCreate();
  if (!gfx_image_set) { return "Could not initialise sprite image set."; }

  /* Ensure that both buffers are clear before anything is drawn: */
  glClear(GL_COLOR_BUFFER_BIT);
  if (gfxSwapBuffers) { gfxSwapBuffers(); }
  glClear(GL_COLOR_BUFFER_BIT);

  return NULL;
}

/**
@brief about

writeme

@param name about
@param data about
@param user about
@return writeme
*/

static void *gfxQuit_callback(const char *name, void *data, void *user)
{
  image_s *image = (image_s*)data;
  if (user || name) { }
  glDeleteTextures(1, &image->texture_id);
  free(image);
  return NULL;
}

/*
[PUBLIC] Shut down the graphics sub-system and free any allocated memory.
*/

void gfxQuit(void)
{
  gfxWorldDelete();
  gfx_image_set = treeDelete(gfx_image_set, gfxQuit_callback, NULL);
  bmpQuit();

  gfxSwapBuffers = NULL;
  physActorIsRagdoll_func = NULL;
  physActorGetPosition_func = NULL;
  physActorGetQuaternion_func = NULL;
  actorUpdateMesh_func  = NULL;
}

/*
[PUBLIC] writeme
*/

#include <time.h>

int gfxWorldCreate(const char *file_name, const lamp_s *lamp)
{
  mesh_s *mesh = NULL;
  time_t start;

  gfxWorldDelete();

  mesh = meshLoadFile(file_name);
  if (!mesh) { return -1; }

  /* Optimisation: Use the vertex buffer to cache RGB values for lighting. */
  #ifdef GFX_FAST_LIGHTING
    if (meshCreateBuffers(mesh))
    {
      long v;
      for (v = 0; v < mesh->num_verts; ++v) { mesh->final_vertex[v*3] = -1.0; }
    }
    else
    {
      meshDelete(mesh);
      return 1;
    }
  #endif

  gfx_world_mtrl = gfxMaterialCreate(mesh);

  /* Create world display list. */
  gfx_world_dlist = glGenLists(1);
  glNewList(gfx_world_dlist, GL_COMPILE);

  fprintf(stderr, "[INFO] Calculating map lighting... ");
  start = time(NULL);
  gfxDrawMesh(NULL, mesh, gfx_world_mtrl, lamp);
  fprintf(stderr, "%ld seconds\n", (long)difftime(time(NULL), start));

  glEndList();

  meshDelete(mesh);
  return 0;
}

/*
[PUBLIC] writeme
*/

void gfxWorldDelete(void)
{
  gfx_world_mtrl = gfxMaterialDelete(gfx_world_mtrl);
  glDeleteLists(gfx_world_dlist, 1);
  gfx_world_dlist = 0;
}

/*
[PUBLIC] writeme
*/

int gfxClassCreate(class_s *klass)
{
  gfxClassDelete(klass);
  if (klass) { klass->material = gfxMaterialCreate(klass->mesh); }
  return 0;
}

/*
[PUBLIC] writeme
*/

void gfxClassDelete(class_s *klass)
{
  if (klass) { klass->material = gfxMaterialDelete(klass->material); }
}

/*
[PUBLIC] about
*/

int gfxSpriteCreate(sprite_s *sprite)
{

  gfxSpriteDelete(sprite);

  sprite->image = (image_s*)malloc(sizeof(image_s));
  if (!sprite->image) { return -1; }
  memset(sprite->image, 0, sizeof(image_s));

  return 0;

}

/*
[PUBLIC] about
*/

void gfxSpriteDelete(sprite_s *sprite)
{

  if      (!sprite       ) { return; }
  else if (!sprite->image) { return; }

  free(sprite->image);
  sprite->image = NULL;

}

/*
[PUBLIC] about
*/

int gfxCacheImage(const char *name, const char *file_name, v2d_flag_t flags)
{

  image_s *image = NULL;
  void    *old   = NULL;

  if (!name || !file_name) { return -1; }

  image = (image_s*)malloc(sizeof(image_s));
  if (!image) { return -2; }
  memset(image, 0, sizeof(image_s));

  image->texture_id = gfxLoadTexture(file_name, flags);
  if (!image->texture_id) { fprintf(stderr, "[WARNING] Couldn't load %s\n", file_name); }

  if (!treeInsert(gfx_image_set, name, image, &old))
  {
    fprintf(stderr, "[WARNING] Couldn't store image %s\n", name);
    glDeleteTextures(1, &image->texture_id);
    free(image);
  }

  if (old)
  {
    image = (image_s*)old;
    glDeleteTextures(1, &image->texture_id);
    free(image);
  }

  return 0;

}

/*
[PUBLIC] about
*/

int gfxSpriteSetImage(sprite_s *sprite, const char *image)
{

  image_s *data = NULL;

  if      (!sprite       ) { return -1; }
  else if (!sprite->image) { return -2; }

  data = (image_s*)treeSearch(gfx_image_set, image);

  sprite->image->texture_id = data ? data->texture_id : 0;

  return data ? 0 : -3;

}

/**
@brief [INTERNAL] about

writeme

@param virtual_height about
*/

static void gfxSetProjection2D(float virtual_height)
{

  float virtual_width = (gfx_width / gfx_height) * virtual_height;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glScalef(2.0 / virtual_width, 2.0 / virtual_height, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

}

/**
[INTERNAL] about

writeme

@param name about
@param data about
@param user about
@return writeme
*/

static void *gfxDrawSprite(const char *name, void *data, void *user)
{

  sprite_s *sprite = (sprite_s*)data;

  GLfloat matrix4x4[4*4];

  if (name || user) { /* Silence "unused parameter" warnings.  */ }

  glPushMatrix();

  /* Upgrade the 3-row/2-col V2D sprite matrix into a 4x4 OpenGL matrix. */

  memset(matrix4x4, 0, sizeof(GLfloat) * 4*4);
  matrix4x4[(4 * 0) + 0] = sprite->matrix[0][0];
  matrix4x4[(4 * 0) + 1] = sprite->matrix[0][1];
  matrix4x4[(4 * 1) + 0] = sprite->matrix[1][0];
  matrix4x4[(4 * 1) + 1] = sprite->matrix[1][1];
  matrix4x4[(4 * 3) + 0] = sprite->matrix[2][0];
  matrix4x4[(4 * 3) + 1] = sprite->matrix[2][1];
  matrix4x4[(4 * 3) + 3] = 1.0;
  glLoadMatrixf(matrix4x4);

  glBindTexture(GL_TEXTURE_2D, sprite->image->texture_id);
  glColor4ub(sprite->rgba[0], sprite->rgba[1], sprite->rgba[2], sprite->rgba[3]);

  if (!sprite->text)
  {
    glBegin(GL_QUADS);
    glTexCoord2f(sprite->xywh[0] + 0,               sprite->xywh[1] + sprite->xywh[3]); glVertex2f(-0.5, +0.5);
    glTexCoord2f(sprite->xywh[0] + sprite->xywh[2], sprite->xywh[1] + sprite->xywh[3]); glVertex2f(+0.5, +0.5);
    glTexCoord2f(sprite->xywh[0] + sprite->xywh[2], sprite->xywh[1] + 0              ); glVertex2f(+0.5, -0.5);
    glTexCoord2f(sprite->xywh[0] + 0,               sprite->xywh[1] + 0              ); glVertex2f(-0.5, -0.5);
    glEnd();
  }
  else { gfxDrawText(sprite->text); }

  glPopMatrix();

  return data;

}

/*
[PUBLIC] Draw everything in the game world.
*/

void gfxDrawGame(const camera_s *camera, actor_s *actor, wordtree_s *sprite_set, const lamp_s *lamp)
{
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  /* Prepare to render 3D. */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(camera ? camera->fov / 2 : 55, gfx_width / gfx_height, 0.05, 2000);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_DEPTH_TEST);

  /* Position camera. */
  glRotatef(-90, 1, 0, 0);
  glRotatef(+90, 0, 0, 1);

  if (camera)
  {
    glRotatef(-2 * acos(camera->quat[QUAT_W]) * RAD2DEG, camera->quat[QUAT_X], camera->quat[QUAT_Y], camera->quat[QUAT_Z]);
    glTranslatef(-camera->xyz[0], -camera->xyz[1], -camera->xyz[2]);
  }

  /* Draw world. */
  glCallList(gfx_world_dlist);

  /* Draw actors. */
  glEnable(GL_LIGHTING);
  for (; actor; actor = actorGetNext(actor))
    gfxDrawMesh(actor, actor->klass->mesh, actor->klass->material, lamp);
  glDisable(GL_LIGHTING);

  /* DEBUG: Draw lights. */
  #if 0
  {
    const STEP_SIZE = 10;
    float r, a;

    glDisable(GL_TEXTURE_2D);
    for (; lamp; lamp = lampGetNext(lamp))
    {
      glColor3f(lamp->rgb[0], lamp->rgb[1], lamp->rgb[2]);
      for (r = 0; r < 180; r += STEP_SIZE)
      {
        glPushMatrix();
        glTranslatef(lamp->origin[0], lamp->origin[1], lamp->origin[2]);
        glRotatef(r * RAD2DEG, 0, 0, 1);
        for (a = 0; a < 360; a += STEP_SIZE)
        {
          glRotatef(STEP_SIZE, 0, 1, 0);
          glBegin(GL_LINES);
          glVertex3f(0, 0, 0);
          glVertex3f(lamp->radius, 0, 0);
          glEnd();
        }
        glPopMatrix();
      }
    }
    glEnable(GL_TEXTURE_2D);
  }
  #endif

  /* Draw sprites. */
  gfxSetProjection2D(480);
  glDisable(GL_DEPTH_TEST);

  glEnable(GL_BLEND);
  treeEachItem(sprite_set, gfxDrawSprite, NULL);
  glDisable(GL_BLEND);

  if (gfxSwapBuffers) { gfxSwapBuffers(); }
}
