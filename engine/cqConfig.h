/**
@file
@brief Provides functions for reading and writing configuration files.

Note that user input configuration is handled separately, by cqInput.h
*/

#ifndef __CQ2_CONFIG_H__
#define __CQ2_CONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"

/* Constants: */

#define CONFIG_MAX_LANGCODE 32  /**< @brief Maximum "language code" length. */

#define CFG_FLAG_FULLSCREEN 0x01  /**< @brief The game should be full-screen. */
#define CFG_FLAG_LOCKMOUSE  0x02  /**< @brief Mouse cursor is hidden/grabbed. */
#define CFG_FLAG_USEAUDIO   0x04  /**< @brief Enable music and sound effects. */
#define CFG_FLAG_UNUSED3    0x08  /**< @brief Not used yet. */
#define CFG_FLAG_UNUSED4    0x10  /**< @brief Not used yet. */
#define CFG_FLAG_UNUSED5    0x20  /**< @brief Not used yet. */
#define CFG_FLAG_UNUSED6    0x40  /**< @brief Not used yet. */
#define CFG_FLAG_UNUSED7    0x80  /**< @brief Not used yet. */

/**
@brief This structure represents a "configuration" for the game engine.

This is usually read from/written to an external configuration file.
*/

typedef struct
{

  char language[CONFIG_MAX_LANGCODE]; /**< @brief The current "locale" code. */

  int video_w;  /**< @brief The expected display width in pixels. */
  int video_h;  /**< @brief The expected display height in pixels. */

  int audio_channels; /**< @brief The number of audio channels to allocate. */

  float mouse_sensitivity;  /**< @brief Mouse movement "scaling" value. */

  v2d_flag_t flags; /**< @brief Various boolean flags; see `CFG_FLAG_*`. */

} config_s;

/**
@brief Initialise a configuration structure to hard-coded "default" values.

This is usually done prior to (maybe) loading an external configuration file.

@param config The configuration structure to initialise.
@return Zero on success, or non-zero on failure.
*/

int cfgInit(config_s *config);

/**
@brief Load configuration settings from an external file.

@param config The configuration structure to update.
@param file_name The name of (and path to) the configuration file to load.
@return Zero on success, or non-zero on failure.
*/

int cfgLoad(config_s *config, const char *file_name);

/**
@brief Save configuration settings to an external file.

@param config The configuration structure to be written.
@param file_name The name of (and path to) the configuration file to write to.
@return Zero on success, or non-zero on failure.
*/

int cfgSave(config_s *config, const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_CONFIG_H__ */
