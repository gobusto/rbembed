/**
@file
@brief Defines a generic API for interacting with physics engines.

This could be a realistic physics engine, such as ODE or Bullet, or something
much simpler, such as a grid of AABB blocks.
*/

#ifndef __CQ2_PHYSICS_H__
#define __CQ2_PHYSICS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cqActor.h"
#include "bits/type.h"

/* Actor *class* flags; these do not vary for specific actor instances: */

#define PHYS_FLAG_STATIC    0x01  /**< @brief about */
#define PHYS_FLAG_NOSPIN    0x02  /**< @brief about */
#define PHYS_FLAG_RESERVED2 0x04  /**< @brief about */
#define PHYS_FLAG_RESERVED3 0x08  /**< @brief about */
#define PHYS_FLAG_RESERVED4 0x10  /**< @brief about */
#define PHYS_FLAG_RESERVED5 0x20  /**< @brief about */
#define PHYS_FLAG_RESERVED6 0x40  /**< @brief about */
#define PHYS_FLAG_RESERVED7 0x80  /**< @brief about */

/* Function signatures, for easy sharing with renderer DLLs: */

#define PHYS_ACTOR_IS_RAGDOLL(x) v2d_bool_t x(const actor_s *actor)
#define PHYS_ACTOR_GET_POSITION(x) int x(const actor_s *actor, int id, float *vec3)
#define PHYS_ACTOR_GET_QUATERNION(x) int x(const actor_s *actor, int id, quaternion_t quaternion)

typedef unsigned char phys_flag_t;  /**< @brief about */

/**
@brief about

writeme
*/

typedef enum
{

  SHAPE_BOX,      /**< @brief about */
  SHAPE_SPHERE,   /**< @brief about */
  SHAPE_CAPSULE,  /**< @brief about */
  SHAPE_CYLINDER, /**< @brief about */
  SHAPE_TRIMESH   /**< @brief about */

} shape_t;

/**
@brief [INTERNAL] about

writeme
*/

typedef struct
{

  size_t num_items; /**< @brief about */
  float *point;     /**< @brief about */
  actor_s **actor;  /**< @brief about */

} hitscan_s;

/**
@brief about

writeme

@return writeme
*/

int physInit(void);

/**
@brief about

writeme
*/

void physQuit(void);

/**
@brief about

writeme
*/

void physUpdate(void);

/**
@brief about

writeme

@param file_name writeme
@return writeme
*/

int physWorldCreate(const char *file_name);

/**
@brief about

writeme
*/

void physWorldDelete(void);

/**
@brief about

writeme

@param klass writeme
@param shape writeme
@param kilos writeme
@param flags writeme
@return writeme
*/

int physClassCreate(class_s *klass, shape_t shape, float kilos, phys_flag_t flags);

/**
@brief about

writeme

@param klass writeme
*/

void physClassDelete(class_s *klass);

/**
@brief about

writeme

@param actor writeme
@return writeme
*/

int physActorCreate(actor_s *actor);

/**
@brief about

writeme

@param actor writeme
*/

void physActorDelete(actor_s *actor);

/**
@brief about

writeme

@param actor about
@return writeme
*/

v2d_bool_t physActorGetGravity(const actor_s *actor);

/**
@brief about

writeme

@param actor about
@param enabled about
@return writeme
*/

v2d_bool_t physActorSetGravity(actor_s *actor, v2d_bool_t enabled);

/**
@brief about

writeme

@param actor about
@return writeme
*/

v2d_bool_t physActorIsRagdoll(const actor_s *actor);

/**
@brief about

writeme

@param actor about
@return writeme
*/

v2d_bool_t physActorCanBecomeRagdoll(const actor_s *actor);

/**
@brief about

writeme

@param actor about
@return writeme
*/

v2d_bool_t physActorBecomeRagdoll(actor_s *actor);

/**
@brief about

writeme

@param actor writeme
@param id writeme
@param vec3 writeme
@return writeme
*/

int physActorGetPosition(const actor_s *actor, int id, float *vec3);

/**
@brief about

writeme

@param actor writeme
@param x writeme
@param y writeme
@param z writeme
@return writeme
*/

int physActorSetPosition(actor_s *actor, float x, float y, float z);

/**
@brief about

writeme

@param actor writeme
@param id about
@param vec3 writeme
@return writeme
*/

int physActorGetVelocity(const actor_s *actor, int id, float *vec3);

/**
@brief about

writeme

@param actor writeme
@param id about
@param x writeme
@param y writeme
@param z writeme
@return writeme
*/

int physActorSetVelocity(actor_s *actor, int id, float x, float y, float z);

/**
@brief about

writeme

@param actor writeme
@param id about
@param x writeme
@param y writeme
@param z writeme
@return writeme
*/

int physActorAddForce(actor_s *actor, int id, float x, float y, float z);

/**
@brief about

writeme

@param actor writeme
@param id about
@param quaternion writeme
@return writeme
*/

int physActorGetQuaternion(const actor_s *actor, int id, quaternion_t quaternion);

/**
@brief about

writeme

@param actor writeme
@param w about
@param x about
@param y about
@param z about
@return writeme
*/

int physActorSetQuaternion(actor_s *actor, float w, float x, float y, float z);

/**
@brief about

writeme

@param px about
@param py about
@param pz about
@param dx about
@param dy about
@param dz about
@return writeme
*/

hitscan_s *physHitscanCreate(float px, float py, float pz, float dx, float dy, float dz);

/**
@brief about

writeme

@param hitscan about
*/

void physHitscanDelete(hitscan_s *hitscan);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_PHYSICS_H__ */
