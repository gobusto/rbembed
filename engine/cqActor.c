/**
@file
@brief Provides structures and functions for handling in-game actor instances.

Actors are based on classes, which are defined by the cqClass.h and cqClass.c
files. AI and physics are handled by cqScript.h and cqPhysics.h respectively.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqActor.h"
#include "cqPhysics.h"
#include "cqScript.h"
#include "cqSystem.h"
#include "cqWorld.h"

/*
[PUBLIC] Free a previously created actor from memory.
*/

actor_s *actorDelete(actor_s *actor)
{

  actor_s *next = actorGetNext(actor);

  if (!actor) { return NULL; }

  /* Free the actor script instance and physics data. */

  scriptActorDelete(actor);
  physActorDelete(actor);

  /* Free the main actor structure, and return the next one in the list. */

  free(v2dLinkDelete(actor->link));
  return next;

}

/*
[PUBLIC] Create a new actor.
*/

actor_s *actorCreate(const char *class_name, v2d_real_t x, v2d_real_t y, v2d_real_t z, v2d_real_t angle)
{

  actor_s *actor = NULL;
  class_s *klass = NULL;

  quaternion_t wxyz;

  v2d_chain_s *chain = worldGetActorChain();
  if (!chain) { return NULL; }

  /* Get the actor class. */

  klass = classFind(class_name);

  if (!klass)
  {
    fprintf(stderr, "[WARNING] Unknown class: %s\n", class_name ? class_name : "");
    return NULL;
  }

  /* Allocate memory for the new actor structure. */

  actor = (actor_s*)malloc(sizeof(actor_s));

  if (!actor)
  {
    fprintf(stderr, "[WARNING] malloc() failed for actor: %s\n", class_name);
    return NULL;
  }

  /* Initialise the actor structure. */

  memset(actor, 0, sizeof(actor_s));

  actor->klass = klass;

  actor->anim_speed = 1.0;

  /* Initialise actor physics. */

  if (physActorCreate(actor) != 0)
  {
    fprintf(stderr, "[WARNING] Couldn't initialise physics: %s\n", class_name);
    actorDelete(actor);
    return NULL;
  }

  quatFromAngle(wxyz, QUAT_Z, angle);

  physActorSetPosition(actor, x, y, z);
  physActorSetQuaternion(actor, wxyz[0], wxyz[1], wxyz[2], wxyz[3]);

  /* Create a Ruby/Python/Lua actor to go with it, and tie them together. */

  if (scriptActorCreate(class_name, actor, angle) != 0)
  {
    fprintf(stderr, "[WARNING] Couldn't initialise script: %s\n", class_name);
  }

  /* If the actor has no AI, make it a ragdoll by default (if possible). */

  if (!actor->script) { physActorBecomeRagdoll(actor); }

  /* Return the newly created actor structure. */

  actor->link = v2dLinkCreate(actor);
  if (!actor->link) { return actorDelete(actor); }
  v2dLinkInsert(chain, NULL, actor->link);

  return actor;

}

/*
[PUBLIC] Update the state of an actor.
*/

int actorUpdate(actor_s *actor)
{

  if (!actor) { return 0; }

  /* Update the actor animation state. */

  if (actor->animation)
  {

    actor->anim_tween += (actor->animation->fps / (1000.0 / CQ2_TICK_DELAY)) * actor->anim_speed;

    for (; actor->anim_tween >= 1.0; actor->anim_tween -= 1.0)
    {

      actor->anim_frame = actor->animation->frame_start + actor->anim_index;
      actor->anim_index++;

      if (actor->anim_loops)
      {
        while (actor->anim_index >= actor->animation->frame_count)
        {      actor->anim_index -= actor->animation->frame_count; }
      }
      else if (actor->anim_index > actor->animation->frame_count - 1)
      {        actor->anim_index = actor->animation->frame_count - 1; }

    }

  }

  /* Call the actor script update function and return the result. */

  return scriptCall(actor, EVENT_THINK, NULL, NULL);

}

/*
[PUBLIC] about
*/

int actorSetAnimation(actor_s *actor, const char *anim_name, v2d_bool_t loop)
{

  if (!actor) { return -1; }

  actor->anim_loops = loop;
  actor->anim_index = 0;
  actor->anim_tween = 0.0;
  actor->animation = animSetFind(actor->klass->anim, anim_name);

  return actor->animation ? 0 : -2;

}

/*
[PUBLIC]
*/

v2d_bool_t actorSetAnimationSpeed(actor_s *actor, v2d_real_t speed)
{
  if (!actor || speed <= 0.0) { return V2D_FALSE; }
  actor->anim_speed = speed;
  return V2D_TRUE;
}

/*
[PUBLIC]
*/

v2d_bool_t actorAnimationIsPlaying(actor_s *actor)
{

  if      (!actor           ) { return V2D_FALSE; }
  else if (!actor->animation) { return V2D_FALSE; }
  else if (actor->anim_loops) { return V2D_TRUE;  }
  else if (actor->anim_index != actor->animation->frame_count - 1) { return V2D_TRUE; }

  return actor->anim_frame != actor->animation->frame_start + actor->animation->frame_count - 1;

}

/*
[PUBLIC] about
*/

int actorUpdateMesh(actor_s *actor, v2d_bool_t include_physics_transformations)
{

  long frame_a, frame_b, g, t, p;

  if      (!actor             ) { return -1; }
  else if (!actor->klass->mesh) { return -2; }

  /*
  PERFORMANCE OPTIMISATION:
  -------------------------
  If this actor is not animated (and we do not need to do physics transforms),
  then we may as well skip this step and just render the basic mesh as it is.
  Of course, if buffers have already been allocated (by another actor of the
  same class) then we will still need to go through the whole process in order
  to overwrite whatever data they left in them, since that's what will be used.
  */

  if (!actor->animation && !include_physics_transformations &&
      !actor->klass->mesh->final_vertex) { return 0; }

  /* Work out the current animation frame + interpolation to the next. */

  frame_a = actor->anim_frame;
  frame_b = actor->animation ? actor->animation->frame_start + actor->anim_index : frame_a;

  if (frame_a < 0 || frame_a >= actor->klass->mesh->num_frames) { frame_a = 0; }
  if (frame_b < 0 || frame_b >= actor->klass->mesh->num_frames) { frame_b = 0; }

  /* Ensure that "working" buffers have been allocated for this mesh. */

  if (!meshCreateBuffers(actor->klass->mesh)) { return 666; }

  for (g = 0; g < actor->klass->mesh->num_groups; ++g)
  {

    mesh_group_s *group = actor->klass->mesh->group[g];
    if (!group) { continue; }

    for (t = 0; t < group->num_tris; ++t)
    {

      for (p = 0; p < 3; ++p)
      {

        const long v = group->tri[t]->v_id[p];
        const long n = group->tri[t]->n_id[p];

        quaternion_t vertex;
        quaternion_t normal;

        /* Get the initial vertex/normal position. */

        vertex[QUAT_W] = 0;
        vertex[QUAT_X] = actor->klass->mesh->vertex[frame_a][(v*3) + 0] + ((actor->klass->mesh->vertex[frame_b][(v*3) + 0] - actor->klass->mesh->vertex[frame_a][(v*3) + 0]) * actor->anim_tween);
        vertex[QUAT_Y] = actor->klass->mesh->vertex[frame_a][(v*3) + 1] + ((actor->klass->mesh->vertex[frame_b][(v*3) + 1] - actor->klass->mesh->vertex[frame_a][(v*3) + 1]) * actor->anim_tween);
        vertex[QUAT_Z] = actor->klass->mesh->vertex[frame_a][(v*3) + 2] + ((actor->klass->mesh->vertex[frame_b][(v*3) + 2] - actor->klass->mesh->vertex[frame_a][(v*3) + 2]) * actor->anim_tween);

        normal[QUAT_W] = 0;
        normal[QUAT_X] = actor->klass->mesh->normal[frame_a][(n*3) + 0] + ((actor->klass->mesh->normal[frame_b][(v*3) + 0] - actor->klass->mesh->normal[frame_a][(v*3) + 0]) * actor->anim_tween);
        normal[QUAT_Y] = actor->klass->mesh->normal[frame_a][(n*3) + 1] + ((actor->klass->mesh->normal[frame_b][(v*3) + 1] - actor->klass->mesh->normal[frame_a][(v*3) + 1]) * actor->anim_tween);
        normal[QUAT_Z] = actor->klass->mesh->normal[frame_a][(n*3) + 2] + ((actor->klass->mesh->normal[frame_b][(v*3) + 2] - actor->klass->mesh->normal[frame_a][(v*3) + 2]) * actor->anim_tween);

        /* Transform vertices/normals based on physics data. (NOTE: Slow!) */

        if (include_physics_transformations)
        {

          vector3_t central_point;
          vector3_t phys_position;
          quaternion_t phys_rotation;
          quaternion_t tmpv;
          quaternion_t tmpn;

          long body_id = 0;

          /* Get the position/rotation of the associated physics object (if any). */

          if (actor->klass->mesh->vert_tag) { body_id = actor->klass->mesh->vert_tag[v]; }
          else                              { body_id = 0;                               }

          if (physActorGetPosition(actor, body_id, phys_position) != 0)
          {

            body_id = 0;

            physActorGetPosition  (actor, 0, phys_position);
            physActorGetQuaternion(actor, 0, phys_rotation);

          } else { physActorGetQuaternion(actor, body_id, phys_rotation); }

          /* Rotate the vertex position based on the physics quaternion data. */

          if (physActorIsRagdoll(actor))
               { meshGetTagCenter(actor->klass->mesh, body_id, central_point); }
          else { meshGetCenter   (actor->klass->mesh,          central_point); }

          vertex[QUAT_X] -= central_point[0];
          vertex[QUAT_Y] -= central_point[1];
          vertex[QUAT_Z] -= central_point[2];

          quatMultiply(tmpv, phys_rotation, vertex);
          quatMultiply(tmpn, phys_rotation, normal);
          quatInvert(phys_rotation);
          quatMultiply(vertex, tmpv, phys_rotation);
          quatMultiply(normal, tmpn, phys_rotation);

          vertex[QUAT_X] += phys_position[0];
          vertex[QUAT_Y] += phys_position[1];
          vertex[QUAT_Z] += phys_position[2];

        }

        /* Store the transformed vertex/normal vectors. */

        actor->klass->mesh->final_vertex[(v*3)+0] = vertex[QUAT_X];
        actor->klass->mesh->final_vertex[(v*3)+1] = vertex[QUAT_Y];
        actor->klass->mesh->final_vertex[(v*3)+2] = vertex[QUAT_Z];

        group->final_normal[(t*3*3) + (p*3) + 0] = normal[QUAT_X];
        group->final_normal[(t*3*3) + (p*3) + 1] = normal[QUAT_Y];
        group->final_normal[(t*3*3) + (p*3) + 2] = normal[QUAT_Z];

      }

    }

  }

  /* Report success. */

  return 0;

}
