/**
@file
@brief Defines a generic API for interacting with scripting languages.

Examples of possible scripting languages include Python, Ruby, and Lua. Details
about the language used are hidden away within the corresponding *.c file. This
header file is intended to be generic, so that switching from Python to Ruby is
as simple as replacing the cqScript_Python3.c file with cqScript_Ruby2.c and
re-compiling the program, with no changes required elsewhere in the project.
*/

#ifndef __CQ2_SCRIPT_H__
#define __CQ2_SCRIPT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cqActor.h"
#include "bits/type.h"

/**
@brief Defines possible "events" for script objects to react to.

This will most likely be expanded in the future.
*/

typedef enum
{

  EVENT_MAPCHANGE,  /**< @brief Called when the game changes maps.            */
  EVENT_MESSAGE,    /**< @brief Generic "Key" + "Value" message.              */
  EVENT_COLLIDE,    /**< @brief Called when an actor collides with something. */
  EVENT_THINK       /**< @brief Called once per game-loop for each actor.     */

} event_t;

/**
@brief Initialise the script interpreter.

Call this function before doing anything else.

@param argc Number of command-line arguments, passed through from main().
@param argv Array of command-line arguments, passed through from main().
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int scriptInit(int argc, char **argv);

/**
@brief Shut down the script interpreter.

Call this function when the program shuts down.
*/

void scriptQuit(void);

/**
@brief Load a Python/Ruby/Lua class definition from a file.

The name of the Python/Ruby/Lua class is expected to be the same as the C class
that calls this function. For example, if a C class called "Shotgun" calls this
function with a filename value of "PlayerWeapon.py", the "PlayerWeapon.py" file
is expected to define a Python class called "Shotgun", so that the C and Python
class names can be used interchangeably.

Coincidentally, this is also how Lua scripts work in the Crysis engine:

http://sdk.crydev.net/display/SDKDOC5/Structure+of+a+Script+Entity

@param file_name The name of the file to be loaded.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int scriptClassCreate(const char *file_name);

/**
@brief Free a previously created Python/Ruby/Lua class definition from memory.

This will generally be called by the corresponding C class when it is freed,
which is why it is important for the C and Python/Ruby/Lua class names to be
the same - freeing a C class called "BonusItem" is expected to delete the
Python/Ruby/Lua "BonusItem" class as well.

@param class_name The name of the class to be freed.
*/

void scriptClassDelete(const char *class_name);

/**
@brief Create a Python/Ruby/Lua actor object to complement a C actor structure.

The type of Python/Ruby/Lua object created depends on the name of the C actor's
parent class name. For example, if actor->klass->name is "EnemyDog", the object
created by Python/Ruby/Lua will be an instance of a Python/Ruby/Lua "EnemyDog"
class.

@param class_name The name of the class to be created.
@param actor The C actor to create a Python/Ruby/Lua actor for.
@param angle The initial angle of the actor.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int scriptActorCreate(const char *class_name, actor_s *actor, v2d_real_t angle);

/**
@brief Free a previously created Python/Ruby/Lua actor object from memory.

In general, this will only get called when the corresponding C actor structure
is being deleted.

@param actor The C actor that the Python/Ruby/Lua actor is associated with.
*/

void scriptActorDelete(actor_s *actor);

/**
@brief Call an "event response" method for a Python/Ruby/Lua actor.

This function is used to inform Python/Ruby/Lua actor objects that something in
the game world has happened to them, so that they can respond to it. The event
might be a collision with another actor, for example, in which case the actor's
onCollision() method would be called, so that it could decide how to react. If
the other actor was a bullet, the script might react by decreasing its "health"
value.

@param actor The C actor that the Python/Ruby/Lua actor is associated with.
@param event_type The type of event that the actor should respond to.
@param data_a Varies depending on the type of event to be handled.
@param data_b Varies depending on the type of event to be handled.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int scriptCall(actor_s *actor, event_t event_type, void *data_a, void *data_b);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_SCRIPT_H__ */
