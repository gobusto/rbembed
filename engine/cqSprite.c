/**
@file
@brief Provides structures and functions for handling 2D "sprite" graphics.

These functions are mainly useful for creating in-game HUDs or menus.
*/

#include <stdlib.h>
#include <string.h>
#include "cqGraphics.h"
#include "cqSprite.h"
#include "xml/xml.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/* Global objects and variables. */

wordtree_s *sprite_dict = NULL; /**< @brief Stores all sprite structures. */

/*
[PUBLIC] Load images into the graphics subsystem so sprites can use them.
*/

int spriteCacheImageSet(const char *file_name, const char *language)
{

  xml_s *document = NULL;
  xml_s *root_tag = NULL;
  xml_s *this_tag = NULL;

  /* Load the image definitions XML file. */

  document = xmlLoadFile(file_name);
  if (!document) { return -1; }

  root_tag = xmlFindName(document->node, "ImageSet", 0);
  if (!root_tag)
  {
    xmlDelete(document);
    return -2;
  }

  /* Loop through each image in the set. */

  for (this_tag = root_tag->node; this_tag; this_tag = this_tag->next)
  {

    const char *name = NULL;
    xml_s *option = NULL;
    xml_s *attrib = NULL;

    if (this_tag->name[0] == '#') { continue; }

    if (strcmp(this_tag->name, "Image") != 0)
    {
      fprintf(stderr, "[WARNING] Unknown tag type: %s\n", this_tag->name);
      continue;
    }

    /* Get the name (i.e. unique ID) used to identify this image. */

    attrib = xmlFindName(this_tag->attr, "name", 0);
    if (!attrib)
    {
      fprintf(stderr, "[WARNING] Tag with no 'name' attribute; ignoring...\n");
      continue;
    }
    name = attrib->text;

    /* Loop through each possible option sub-tag within this image tag. */

    for (option = this_tag->node; option; option = option->next)
    {

      const char *path = NULL;
      v2d_flag_t flags = 0;

      if (option->name[0] == '#') { continue; }

      if (strcmp(option->name, "File") != 0)
      {
        fprintf(stderr, "[WARNING] Unknown tag type: %s\n", option->name);
        continue;
      }

      /* NOTE: If no locale is given, assume that this item is ALWAYS used. */

      attrib = xmlFindName(option->attr, "locale", 0);
      if (attrib)
      {
        if      (!language                          ) { continue; }
        else if (strcmp(attrib->text, language) != 0) { continue; }
      }

      /* Get the file name of the image. */

      attrib = xmlFindName(option->attr, "path", 0);
      path = attrib ? attrib->text : "";

      /* Get various flags. */

      attrib = xmlFindName(option->attr, "smoothing", 0);
      if (attrib)
      {
        if (strcmp(attrib->text, "nearest") == 0) { flags |= IMAGE_FLAG_NEAREST; }
      }

      attrib = xmlFindName(option->attr, "clamp", 0);
      if (attrib)
      {
        if (strcmp(attrib->text, "true") == 0)
        { flags |= IMAGE_FLAG_CLAMP_X | IMAGE_FLAG_CLAMP_Y; }
      }

      attrib = xmlFindName(option->attr, "clampx", 0);
      if (attrib)
      {
        if (strcmp(attrib->text, "true") == 0) { flags |= IMAGE_FLAG_CLAMP_X; }
      }

      attrib = xmlFindName(option->attr, "clampy", 0);
      if (attrib)
      {
        if (strcmp(attrib->text, "true") == 0) { flags |= IMAGE_FLAG_CLAMP_Y; }
      }

      /* Load the image. */

      gfxCacheImage(name, path, flags);

    }

  }

  xmlDelete(document);

  /* Report success. */

  return 0;

}

/**
@brief [INTERNAL] Free a sprite from memory.

This is called internally to handle the actual deletion process.

@param name The name of the sprite.
@param data The actual sprite structure to be deleted.
@param user Not used.
@return Always returns NULL.
*/

static void *spriteDelete_Internal(const char *name, void *data, void *user)
{

  sprite_s *sprite = (sprite_s*)data;

  if (user || name) { /* Silence "unused parameter" warnings. */ }

  if (sprite)
  {
    gfxSpriteDelete(sprite);
    if (sprite->text) { free(sprite->text); }
    free(sprite);
  }

  return NULL;

}

/*
[PUBLIC] Initialise the sprite management subsystem.
*/

int spriteInit(void)
{
  spriteQuit();
  return (sprite_dict = treeCreate()) ? 0 : -1;
}

/*
[PUBLIC] Shut down the sprite management subsystem.
*/

void spriteQuit(void) { sprite_dict = treeDelete(sprite_dict, spriteDelete_Internal, NULL); }

/*
[PUBLIC] Get a pointer to the internal "sprite set" structure.
*/

wordtree_s *spriteGetData(void) { return sprite_dict; }

/*
[PUBLIC] Create a new sprite.
*/

sprite_s *spriteCreate(const char *name, v2d_real_t x, v2d_real_t y, v2d_real_t w, v2d_real_t h)
{

  sprite_s *sprite = NULL;
  void     *old    = NULL;

  sprite = (sprite_s*)malloc(sizeof(sprite_s));
  if (!sprite) { return NULL; }
  memset(sprite, 0, sizeof(sprite_s));

  if (gfxSpriteCreate(sprite) != 0)
  {
    spriteDelete_Internal(name, sprite, NULL);
    return NULL;
  }

  v2dMatrixReset(sprite->matrix);
  v2dMatrixScale(sprite->matrix, w, h);
  v2dMatrixPosition(sprite->matrix, x, y);

  sprite->xywh[2] = 1.0;
  sprite->xywh[3] = 1.0;

  sprite->rgba[0] = 255;
  sprite->rgba[1] = 255;
  sprite->rgba[2] = 255;
  sprite->rgba[3] = 255;

  if (!treeInsert(sprite_dict, name, sprite, &old))
  {
    spriteDelete_Internal(name, sprite, NULL);
    return NULL;
  }

  if (old) { spriteDelete_Internal(name, (sprite_s*)old, NULL); }

  return sprite;

}

/*
[PUBLIC] Destroy a previously-created sprite.
*/

void spriteDelete(const char *name)
{

  void *old = NULL;
  treeInsert(sprite_dict, name, NULL, &old);
  spriteDelete_Internal(name, old, NULL);

}

/*
[PUBLIC] Find a specific sprite by name.
*/

sprite_s *spriteFind(const char *name) { return (sprite_s*)treeSearch(sprite_dict, name); }

/*
[PUBLIC] Set or change the text string of a sprite structure.
*/

int spriteSetText(sprite_s *sprite, const char *text)
{

  if (!sprite) { return -1; }

  if (sprite->text) { free(sprite->text); }

  if (text)
  {

    sprite->text = (char*)malloc(strlen(text) + 1);
    if (!sprite->text) { return -2; }
    strcpy(sprite->text, text);

  } else { sprite->text = NULL; }

  return 0;

}
