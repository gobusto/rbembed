/**
@file
@brief Provides functions for loading and managing game map files.

The functions in this file call out to other parts of the engine (such as actor
logic, physics, and scripting), so it serves as a "manager" for the game state.
*/

#ifndef __CQ2_WORLD_H__
#define __CQ2_WORLD_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cqActor.h"
#include "cqLamp.h"
#include "bits/type.h"

/**
@brief Specify a new map file to be loaded at the next possible opportunity.

@param file_name The name of (and path to) the file to load.
@param force_actor_deletion If true, *force* any current actors to be deleted.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

v2d_bool_t worldLoadMap(const char *file_name, v2d_bool_t force_actor_deletion);

/**
@brief Free any currently-existing map data.

This deletes all actors, and calls gfxWorldDelete() and gfxPhysDelete() too.
*/

void worldDestroy(void);

/**
@brief Save the world state to a file.

@param file_name The name of (and path to) the file to save.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

v2d_bool_t worldSave(const char *file_name);

/**
@brief Load the world state from a file.

@param file_name The name of (and path to) the file to load.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

v2d_bool_t worldRestore(const char *file_name);

/**
@brief Update the state of all objects in the world.

This calls actor AI scripts, and updates the state of any physics objects.

@return `V2D_TRUE` if the map changes, or `V2D_FALSE` if it stays the same.
*/

v2d_bool_t worldUpdate(void);

/**
@brief Get the first actor structure in the linked list of world actors.

@return The first actor in the linked list on success, or NULL if none exists.
*/

actor_s *worldGetActor(void);

/**
@brief Get the first lamp structure in the linked list of world lamps.

@return The first lamp in the linked list on success, or NULL if none exists.
*/

lamp_s *worldGetLamp(void);

/**
@brief Get the "chain" structure used to store all actors in the world.

This is used internally by actorCreate() and shouldn't be used anywhere else.

@return The chain used to store actors on success, or NULL on failure.
*/

v2d_chain_s *worldGetActorChain(void);

/**
@brief Get the "chain" structure used to store all lamps in the world.

This is used internally by lampCreate() and shouldn't be used anywhere else.

@return The chain used to store lamps on success, or NULL on failure.
*/

v2d_chain_s *worldGetLampChain(void);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_WORLD_H__ */
