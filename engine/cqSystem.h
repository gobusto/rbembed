/**
@file
@brief Handles high-level engine functionality, such a start-up and shut-down.

The functions in this file are called by the main() function to start, run, and
shut-down the engine.
*/

#ifndef __CQ2_SYSTEM_H__
#define __CQ2_SYSTEM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"

/* Constants: */
#define CQ2_TICK_DELAY 16 /**< @brief Delay (in milliseconds) between ticks. */

/**
@brief Initialise the engine and subsystems.

This function should be called when the program first starts.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t sysInit(int argc, char **argv);

/**
@brief Shut down the engine, and free any currently allocated memory.

This function should be called when the program terminates.
*/

void sysQuit(void);

/**
@brief This function handles the main progam loop.

Once called, this function will continue to run until a termination event is
received.
*/

void sysMainLoop(void);

/**
@brief Instruct `sysMainLoop()` to end at the next possible opportunity.
*/

void sysEndProgram(void);

/**
@brief Get the current state of a (named) input.

@param name The name of the input "slot" to check, such as "jump" or "run".
@return A positive value, usually (but not always) in the range of zero-to-one.
*/

v2d_real_t sysGetInput(const char *name);

/**
@brief Ask a joypad to vibrate for a given length of time.

Note: This might be changed in the future, for various reasons:

+ There's no way to correlate joypads to "users" due to the abstracted mappings.
+ There's no way to have non-joypad deviced vibrate.
+ There's no way to have more "advanced" effects, other than a basic rumble.

@param joypad_id The ID of the joypad that should vibrate.
@param strength The "strength" of the vibration, from zero to one.
@param length The "length" of the vibration in milliseconds.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

v2d_bool_t sysPadRumble(int joypad_id, float strength, v2d_ui32_t length);

/**
@brief Update the position and angle of the 3D camera.

@param x The X position of the camera.
@param y The Y position of the camera.
@param z The Z position of the camera.
@param qw The W component of the rotation quaternion.
@param qx The X component of the rotation quaternion.
@param qy The Y component of the rotation quaternion.
@param qz The Z component of the rotation quaternion.
@param fov The camera field-of-view angle (in DEGREES, rather than radians).
*/

void sysSetCamera(float x, float y, float z, float qw, float qx, float qy, float qz, float fov);

/**
@brief Get the currently-active "language" from the configuration settings.

@param out The output buffer to copy the language string to.
@param max_size The maximum length of the output buffer.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

v2d_bool_t sysGetLocale(char *out, long max_size);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_SYSTEM_H__ */
