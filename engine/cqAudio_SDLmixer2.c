/**
@file
@brief about

This file implements the function defined in cqAudio.h using SDL2 mixer.
*/

#include <SDL2/SDL_mixer.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "cqAudio.h"
#include "xml/xml.h"
#include "lgm/anim/util/wordtree.h"
#include "lgm/anim/util/quaternion.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/* Channel flags. */

#define SND_FLAG_POSITIONAL 0x01  /**< @brief about */
#define SND_FLAG_UNUSED1    0x02  /**< @brief about */
#define SND_FLAG_UNUSED2    0x04  /**< @brief about */
#define SND_FLAG_UNUSED3    0x08  /**< @brief about */
#define SND_FLAG_UNUSED4    0x10  /**< @brief about */
#define SND_FLAG_UNUSED5    0x20  /**< @brief about */
#define SND_FLAG_UNUSED6    0x40  /**< @brief about */
#define SND_FLAG_UNUSED7    0x80  /**< @brief about */

/**
@brief [INTERNAL] about

writeme
*/

typedef struct
{

  v2d_si32_t guid;  /**< @brief about */

  v2d_flag_t flags; /**< @brief Various flags that affect this channel. */

  float xyzr[4];  /**< @brief The XYZ position and radius of this 3D sound. */

} snd_channel_s;

/* Global objects and variables. */

v2d_bool_t snd_initialised = V2D_FALSE; /**< @brief about */

Mix_Music  *snd_music = NULL; /**< @brief about */
wordtree_s *snd_sound = NULL; /**< @brief about */

v2d_ui8_t snd_num_channels = 0;           /**< @brief about */
snd_channel_s **snd_channel = NULL; /**< @brief about */

float snd_ear_xyza[4]; /**< @brief about */

/**
@brief [INTERNAL] Create a GUID for a specific sound playback event.

@todo Explain the logic behind this here...

@param id A sound channel number, from 0 to 255 inclusive.
@return A 32-bit GUID, which may be used to reference the channel later on.
*/

#define sndEncodeChannelGUID(id) (((id & 0xFF) << 16) + (clock() & 0xFFFF))

/**
@brief [INTERNAL] Extract the channel number value from a GUID.

@todo Again, explain the logic here, and how it relates to the encoder above...

@param guid A previously generated 32-bit GUID value.
@return The channel associated with the provided GUID.
*/

#define sndDecodeChannelGUID(guid) (guid >> 16)

/**
@brief [INTERNAL] about

writeme

@param name about
@param file_name about
@return writeme
*/

static int sndCacheEffect(const char *name, const char *file_name)
{

  Mix_Chunk *snd = NULL;
  void      *old = NULL;

  if (!name || !file_name) { return -1; }

  snd = Mix_LoadWAV(file_name);
  if (!snd) { fprintf(stderr, "[WARNING] Couldn't load %s\n", file_name); }

  if (!treeInsert(snd_sound, name, snd, &old))
  {
    fprintf(stderr, "[WARNING] Couldn't store sound %s\n", name);
    Mix_FreeChunk(snd);
  }

  if (old) { Mix_FreeChunk((Mix_Chunk*)old); }

  return 0;

}

/*
[PUBLIC] about
*/

int sndCacheEffectSet(const char *file_name, const char *language)
{

  xml_s *document = NULL;
  xml_s *root_tag = NULL;
  xml_s *this_tag = NULL;

  /* Load the sound definitions XML file. */

  document = xmlLoadFile(file_name);
  if (!document) { return -1; }

  root_tag = xmlFindName(document->node, "SoundSet", 0);
  if (!root_tag)
  {
    xmlDelete(document);
    return -2;
  }

  /* Loop through each sound in the set. */

  for (this_tag = root_tag->node; this_tag; this_tag = this_tag->next)
  {

    const char *name = NULL;
    xml_s *option = NULL;
    xml_s *attrib = NULL;

    if (this_tag->name[0] == '#') { continue; }

    if (strcmp(this_tag->name, "Sound") != 0)
    {
      fprintf(stderr, "[WARNING] Unknown tag type: %s\n", this_tag->name);
      continue;
    }

    /* Get the name (i.e. unique ID) used to identify this sound. */

    attrib = xmlFindName(this_tag->attr, "name", 0);
    if (!attrib)
    {
      fprintf(stderr, "[WARNING] Tag with no 'name' attribute; ignoring...\n");
      continue;
    }
    name = attrib->text;

    /* Loop through each possible option sub-tag within this sound tag. */

    for (option = this_tag->node; option; option = option->next)
    {

      const char *path = NULL;

      if (option->name[0] == '#') { continue; }

      if (strcmp(option->name, "File") != 0)
      {
        fprintf(stderr, "[WARNING] Unknown tag type: %s\n", option->name);
        continue;
      }

      /* NOTE: If no locale is given, assume that this item is ALWAYS used. */

      attrib = xmlFindName(option->attr, "locale", 0);
      if (attrib)
      {
        if      (!language                          ) { continue; }
        else if (strcmp(attrib->text, language) != 0) { continue; }
      }

      /* Get the file name of the image. */

      attrib = xmlFindName(option->attr, "path", 0);
      path = attrib ? attrib->text : "";

      /* Load the image. */

      sndCacheEffect(name, path);

    }

  }

  xmlDelete(document);

  /* Report success. */

  return 0;

}

/*
[PUBLIC] Initialise the audio subsystem.
*/

int sndInit(v2d_ui8_t num_channels)
{
  int i;

  sndQuit();

  /* Check parameters. */
  if (num_channels < 1) { return -1; }

  /* Initialise the listener position. */
  snd_ear_xyza[0] = 0.0;
  snd_ear_xyza[1] = 0.0;
  snd_ear_xyza[2] = 0.0;
  snd_ear_xyza[3] = 0.0;

  /* Initialise the sound effects dictionary. */
  snd_sound = treeCreate();
  if (!snd_sound) { return -2; }

  /* Initialise SDL mixer. */
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0) { return -2; }
  snd_num_channels = Mix_AllocateChannels(num_channels);

  /* Log some debug info. */
  {
    const SDL_version *dll;
    SDL_version ver;
    int frequency;
    int channels;
    Uint16 format;

    dll = Mix_Linked_Version();
    SDL_MIXER_VERSION(&ver);
    Mix_QuerySpec(&frequency, &format, &channels);

    fprintf(stderr, "[INIT] SDL Mixer %d.%d.%d\n", dll->major, dll->minor, dll->patch);
    fprintf(stderr, "\tOriginally compiled with: v%d.%d.%d\n", ver.major, ver.minor, ver.patch);
    fprintf(stderr, "\tFrequency: %dHz\n", frequency);
    fprintf(stderr, "\tChannels: %d\n", channels);
    fprintf(stderr, "\tFormat: ");
    switch (format)
    {
      case AUDIO_U8:     fprintf(stderr, "U8");      break;
      case AUDIO_S8:     fprintf(stderr, "S8");      break;
      case AUDIO_U16LSB: fprintf(stderr, "U16-LSB"); break;
      case AUDIO_S16LSB: fprintf(stderr, "S16-LSB"); break;
      case AUDIO_U16MSB: fprintf(stderr, "U16-MSB"); break;
      case AUDIO_S16MSB: fprintf(stderr, "S16-MSB"); break;
      default:           fprintf(stderr, "Unknown"); break;
    }
    fprintf(stderr, "\n");
  }

  /* Allocate channels. Note that this might not match the requested value! */

  snd_channel = (snd_channel_s**)malloc(sizeof(snd_channel_s*) * snd_num_channels);
  if (!snd_channel) { return -3; }
  memset(snd_channel, 0, sizeof(snd_channel_s*) * snd_num_channels);

  for (i = 0; i < snd_num_channels; i++)
  {
    snd_channel[i] = (snd_channel_s*)malloc(sizeof(snd_channel_s));
    if (!snd_channel[i]) { return -4; }
    memset(snd_channel[i], 0, sizeof(snd_channel_s));
  }

  /* Report success. */

  snd_initialised = V2D_TRUE;

  return 0;

}

/**
@brief about

writeme

@param name about
@param data about
@param user about
@return writeme
*/

static void *sndQuit_callback(const char *name, void *data, void *user)
{
  if (user || name) { }
  Mix_FreeChunk((Mix_Chunk*)data);
  return NULL;
}

/*
[PUBLIC] Shut down the audio subsystem.
*/

void sndQuit(void)
{

  int i = 0;

  /* Stop all music and sound effects BEFORE freeing them from memory. */

  if (snd_initialised)
  {
    sndStopMusic();
    Mix_HaltChannel(-1);
  }

  /* Free all sound effects. */

  snd_sound = treeDelete(snd_sound, sndQuit_callback, NULL);

  /* Free the audio channel data structures. */

  if (snd_channel)
  {

    for (i = 0; i < snd_num_channels; i++)
    {
      if (snd_channel[i]) { free(snd_channel[i]); }
    }

    free(snd_channel);
    snd_channel = NULL;

  }

  /* Shut down the audio subsystem. */

  if (snd_initialised) { Mix_CloseAudio(); }

  snd_initialised = V2D_FALSE;

}

/*
[PUBLIC] Play (and optionally loop) a sound effect.
*/

v2d_si32_t sndPlayEffect(const char *sound, int num_loops, v2d_bool_t is_3D,
                        float x, float y, float z, float radius)
{

  void *data = NULL;
  int id = 0;

  /* Ensure that the audio subsystem is ready, and that the radius is valid. */

  if      (!snd_initialised      ) { return -1; }
  else if (is_3D && radius <= 0.0) { return -2; }

  /* Try to find the sound requested. */

  data = treeSearch(snd_sound, sound);
  if (!data) { return -3; }

  /* Attempt to allocate an audio channel, so that the sound can be played. */

  id = Mix_PlayChannel(-1, (Mix_Chunk*)data, num_loops);
  if (id < 0 || id >= snd_num_channels) { return -4; }
  memset(snd_channel[id], 0, sizeof(snd_channel_s));

  /* Initialise the sound channel attributes structure. */

  snd_channel[id]->guid = sndEncodeChannelGUID(id);

  if (is_3D)
  {
    snd_channel[id]->flags = SND_FLAG_POSITIONAL;
    snd_channel[id]->xyzr[0] = x;
    snd_channel[id]->xyzr[1] = y;
    snd_channel[id]->xyzr[2] = z;
    snd_channel[id]->xyzr[3] = radius;
  }

  /* Return the GUID so that this sound playback instance can be referenced. */

  return snd_channel[id]->guid;

}

/*
[PUBLIC] Stop a sound from playing.
*/

v2d_bool_t sndStopSound(v2d_si32_t guid)
{

  const v2d_si32_t id = sndDecodeChannelGUID(guid);

  /* Ensure that the audio subsystem is ready, and that the GUID is valid. */
  if (!snd_initialised || id < 0 || id >= snd_num_channels) { return V2D_FALSE; }

  /* If the channel was reused after the GUID was given, don't do anything. */
  if (snd_channel[id]->guid != guid) { return V2D_FALSE; }

  /* The channel is still using this GUID, so it's okay to stop it. */
  Mix_HaltChannel(id);

  /* The GUID is no longer valid, so set the channel to use -1 instead. */
  snd_channel[id]->guid = -1;

  /* Report success. */
  return V2D_TRUE;

}

/*
[PUBLIC] Determine if a sound effect is still playing.
*/

v2d_bool_t sndSoundPlaying(v2d_si32_t guid)
{

  const v2d_si32_t id = sndDecodeChannelGUID(guid);

  /* Ensure that the audio subsystem is ready, and that the GUID is valid. */
  if (!snd_initialised || id < 0 || id >= snd_num_channels) { return V2D_FALSE; }

  /* If the channel was reused after the GUID was given, it's "not playing". */
  if (snd_channel[id]->guid != guid) { return V2D_FALSE; }

  /* If the channel is still using this GUID, then it's okay to check it. */
  return Mix_Playing(id) ? V2D_TRUE : V2D_FALSE;

}

/*
[PUBLIC] Reposition a 3D sound.
*/

v2d_bool_t sndSoundSetPosition(v2d_si32_t guid, float x, float y, float z)
{

  const v2d_si32_t id = sndDecodeChannelGUID(guid);

  /* Ensure that the audio subsystem is ready, and that the GUID is valid. */
  if (!snd_initialised || id < 0 || id >= snd_num_channels) { return V2D_FALSE; }

  /* If the channel was reused after the GUID was given, don't do anything. */
  if (snd_channel[id]->guid != guid) { return V2D_FALSE; }

  snd_channel[id]->xyzr[0] = x;
  snd_channel[id]->xyzr[1] = y;
  snd_channel[id]->xyzr[2] = z;

  /* Report success. */
  return V2D_TRUE;

}

/*
[PUBLIC] Play (and loop) a music track.
*/

int sndPlayMusic(const char *file_name, int num_loops)
{

  if (!snd_initialised) { return 1; }

  sndStopMusic();

  if (!file_name) { return -1; }
  snd_music = Mix_LoadMUS(file_name);
  if (!snd_music) { return -2; }
  if (Mix_PlayMusic(snd_music, num_loops) != 0) { return -3; }

  return 0;

}

/*
[PUBLIC] Stop any currently playing music.
*/

void sndStopMusic(void)
{

  if (!snd_initialised) { return; }

  Mix_HaltMusic();
  Mix_FreeMusic(snd_music);
  snd_music = NULL;

}

/*
[PUBLIC] Returns non-zero if music is currently playing.
*/

int sndMusicPlaying(void) { return snd_music != NULL; }

/*
[PUBLIC] about
*/

void sndSetListener(float x, float y, float z, float angle)
{

  snd_ear_xyza[0] = x;
  snd_ear_xyza[1] = y;
  snd_ear_xyza[2] = z;
  snd_ear_xyza[3] = angle;

}

/*
[PUBLIC] about
*/

void sndUpdate(void)
{

  float xyz[3];
  float angle, distance;
  int i = 0;

  if (!snd_initialised) { return; }

  for (i = 0; i < snd_num_channels; i++)
  {

    snd_channel_s *chan = snd_channel[i];

    if (chan->flags & SND_FLAG_POSITIONAL)
    {

      /* Get the angle of this channel relative to the angle of the listener. */

      angle = atan2(chan->xyzr[1] - snd_ear_xyza[1], chan->xyzr[0] - snd_ear_xyza[0]) - snd_ear_xyza[3];
      angle *= -RAD2DEG;

      while (angle <  0  ) { angle += 360.0; }
      while (angle >= 360) { angle -= 360.0; }

      /* Get the distance between this 3D sound and the listener. */

      xyz[0] = chan->xyzr[0] - snd_ear_xyza[0];
      xyz[1] = chan->xyzr[1] - snd_ear_xyza[1];
      xyz[2] = chan->xyzr[2] - snd_ear_xyza[2];

      distance = vecLength(3, xyz) / (chan->xyzr[3] / 255.0);

      if      (distance < 0.0  ) { distance = 0.0;   }
      else if (distance > 255.0) { distance = 255.0; }

    }
    else
    {
      angle = 0;
      distance = 0;
    }

    /* Set the position of the sound. */

    Mix_SetPosition(i, (Sint16)angle, (Uint8)distance);

  }

}
