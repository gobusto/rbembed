/**
@file
@brief Provides functions for reading and writing input configuration files.

This file allows "physical" inputs - such as mouse movement, joystick axes, and
keyboard keys - to be mapped to "virtual" input slots. Multiple physical inputs
can be mapped to a single virtual slot; a First Person Shooter game might allow
the player to use either a joypad thumbstick or a mouse to turn the camera, for
example. The game engine itself doesn't care which kind of device is being used
to populate an input slot - it only needs to know the value that the input slot
currently contains, so that it can update the player character's position, etc.

Each "virtual" input slot is expected to store values of zero or more - zero is
used to represent "no button pressed", with values of 0.3 or 1.0 indicating the
total "pressure" being applied by all gamepad axes/keyboard keys/etc. mapped to
the slot. Note that slots aren't limited to a maximum value of 1.0, since quick
mouse movement will often generate higher values. Capping mouse-movement speeds
would cause problems (for example, in an FPS where accurate aiming is required)
and so restrictions on values over 1.0 are something that the game engine needs
to handle itself. As a side effect, this also means that mapping two buttons to
the same slot will result in a value of 2.0 if they are both pressed at once.

As mentioned above, slots cannot contain negative values. They're like buttons,
rather than axes, for two reasons: (A) Keyboard keys and joystick/mouse buttons
cannot report negative values, and (B) a physical axis value of -1.0 can offset
a physical button value of 1.0, resulting in a combined input value of zero. To
avoid this issue, physical axes are split into "positive" and "negative" pairs,
with positive values passed through normally, and negative axes inverted first.

This will commonly result in there being two "virtual" input slots - turn_right
and turn_left, for example - with one "half" of the axis mapped to each, but it
is possible to map the negative half to "shoot" and the positive half to "use",
if required...
*/

#include <stdlib.h>
#include <string.h>
#include "cqInput.h"
#include "xml/xml.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/**
@brief [INTERNAL] Create a new (blank) input mapping set.

This is used internally by inputLoad() but may be used elsewhere in the future.

@param count The number of input mappings to allocate memory for.
@return A new input set structure on success, or NULL on failure.
*/

static input_set_s *inputCreate(int count)
{

  input_set_s *input = NULL;
  int error, i;

  if (count < 0) { return NULL; }

  /* Create a basic input set structure. */

  input = (input_set_s*)malloc(sizeof(input_set_s));
  if (!input) { return NULL; }
  memset(input, 0, sizeof(input_set_s));

  /* Allocate an input slot "dictionary" and a list of device mappings. */

  input->slot = treeCreate();
  input->num_items = count;
  input->item = (input_map_s**)malloc(sizeof(input_map_s*) * count);

  if (input->slot && input->item)
  {
    for (error = 0, i = 0; i < count; ++i)
    {
      input->item[i] = (input_map_s*)malloc(sizeof(input_map_s));
      if (input->item[i]) { memset(input->item[i], 0, sizeof(input_map_s)); }
      else                { error = 1;                                      }
    }
  } else { error = 1; }

  /* Return the result, or NULL if something went wrong. */

  return error ? inputFree(input) : input;

}

/*
[PUBLIC] Create a new input mapping set structure from a configuration file.
*/

input_set_s *inputLoad(const char *file_name)
{

  input_set_s *input_set = NULL;

  xml_s *document = NULL;
  xml_s *root_tag = NULL;
  xml_s *this_tag = NULL;

  int pass, i;

  /* Load the XML configuration file. */

  document = xmlLoadFile(file_name);
  if (!document) { return NULL; }

  root_tag = xmlFindName(document->node, "InputConfig", 0);
  if (!root_tag)
  {
    xmlDelete(document);
    return NULL;
  }

  /* Cycle through all tags within this XML file. */

  for (pass = 0; pass < 2; ++pass)
  {

    for (i = 0, this_tag = root_tag->node; this_tag; this_tag = this_tag->next)
    {

      /* Determine which kind of tag this is. */

      if (strcmp(this_tag->name, "Keyboard") == 0)
      {
        if (input_set)
        {
          input_set->item[i]->device_type = INPUT_DEVICE_KEYBOARD;
          input_set->item[i]->input_type = INPUT_TYPE_BUTTON;
        }
      }
      else if (strcmp(this_tag->name, "Mouse") == 0)
      {
        if (input_set) { input_set->item[i]->device_type = INPUT_DEVICE_MOUSE; }
      }
      else if (strcmp(this_tag->name, "Joypad") == 0)
      {
        if (input_set) { input_set->item[i]->device_type = INPUT_DEVICE_JOYPAD; }
      }
      else
      {
        if (this_tag->name[0] != '#') /* Text or comment nodes are okay. */
        { fprintf(stderr, "[WARNING] Unknown tag: %s\n", this_tag->name); }
        continue;
      }

      /* On the second pass, use the attributes to update the configuration. */

      if (input_set)
      {

        xml_s *attrib = NULL;

        /* Get the device index. This only really applies to joypads... */

        attrib = xmlFindName(this_tag->attr, "device", 0);
        if (attrib)
        {
          input_set->item[i]->device_id = atoi(attrib->text);
        }

        /* Get the input type. Keyboards generally only have "button" types. */

        attrib = xmlFindName(this_tag->attr, "type", 0);
        if (attrib)
        {
          if (strcmp(attrib->text, "button") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_BUTTON;
          }
          else if (strcmp(attrib->text, "hatup") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_HAT;
            input_set->item[i]->flags = INPUT_FLAG_HATUP;
          }
          else if (strcmp(attrib->text, "hatdown") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_HAT;
            input_set->item[i]->flags = INPUT_FLAG_HATDOWN;
          }
          else if (strcmp(attrib->text, "hatleft") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_HAT;
            input_set->item[i]->flags = INPUT_FLAG_HATLEFT;
          }
          else if (strcmp(attrib->text, "hatright") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_HAT;
            input_set->item[i]->flags = INPUT_FLAG_HATRIGHT;
          }
          else if (strcmp(attrib->text, "axis-") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_AXIS;
            input_set->item[i]->flags = INPUT_FLAG_NEGATIVE;
          }
          else if (strcmp(attrib->text, "axis+") == 0)
          {
            input_set->item[i]->input_type = INPUT_TYPE_AXIS;
            input_set->item[i]->flags = 0;
          }
          else { fprintf(stderr, "[INPUT] Unknown type: %s\n", attrib->text); }
        }

        /* Determine WHICH axis/button/etc. this mapping applies to. */

        attrib = xmlFindName(this_tag->attr, "index", 0);
        if (attrib)
        {
          input_set->item[i]->input_id = atoi(attrib->text);
        }

        /* Determine which action this input is expected to trigger. */

        attrib = xmlFindName(this_tag->attr, "action", 0);
        if (attrib)
        {

          input_set->item[i]->slot = (float*)treeSearch(input_set->slot, attrib->text);
          if (!input_set->item[i]->slot)
          {

            input_set->item[i]->slot = (float*)malloc(sizeof(float));
            if (!input_set->item[i]->slot)
            {
              xmlDelete(document);
              return inputFree(input_set);
            }

            *input_set->item[i]->slot = 0.0;
            if (!treeInsert(input_set->slot, attrib->text, input_set->item[i]->slot, NULL))
            {
              free(input_set->item[i]->slot);
              xmlDelete(document);
              return inputFree(input_set);
            }

          }

        }

      }

      /* Increase the "mappings found" counter. */

      ++i;

    }

    /* At the end of the first pass, allocate a new input mapping structure. */

    if (pass == 0)
    {
      input_set = inputCreate(i);
      if (!input_set) { break; }
    }

  }

  /* Free the XML document and return the result. */

  xmlDelete(document);

  return input_set;

}

/*
[PUBLIC] Write an input mapping set structure to a configuration file.
*/

int inputSave(const char *file_name)
{

  if (!file_name) { return 1; }

  /* TODO */

  return 0;

}

/**
@brief [INTERNAL] writeme

about

@param name about
@param data about
@param user about
@return writeme
*/

static void *inputFree_callback(const char *name, void *data, void *user)
{
  if (user || name) { }
  free(data);
  return NULL;
}

/*
[PUBLIC] Destroy a previously-created input mapping set.
*/

input_set_s *inputFree(input_set_s *input_set)
{

  if (!input_set) { return NULL; }

  /* Free the list of device mappings. */

  if (input_set->item)
  {

    int i = 0;

    for (i = 0; i < input_set->num_items; i++)
    {
      if (input_set->item[i]) { free(input_set->item[i]); }
    }

    free(input_set->item);

  }

  /* Free the list of input slots. */

  treeDelete(input_set->slot, inputFree_callback, NULL);

  /* Free the main input set structure. */

  free(input_set);

  return NULL;

}
