/**
@file
@brief A "dummy" implementation of the functions in `cqGraphics.h`.

This doesn't actually draw anything; it's only really intended for debugging. It
may also be useful as a starting point for anyone intending to create a new .DLL
renderer from a (mostly) blank canvas.
*/

#include <stdio.h>
#include "cqGraphics.h"

/*
[PUBLIC] Get the "name" of the renderer being used.
*/

const char *gfxRendererName(void) { return "Dummy Renderer"; }

/*
[PUBLIC] Get the API version of the renderer being used.
*/

const char *gfxApiVersion(void) { return "Conqore2_2017-04-14"; }

/*
[PUBLIC] Initialise the graphics sub-system.
*/

const char *gfxInit(int argc, char **argv, const render_init_s *render_init)
{
  fprintf(stderr, "gfxInit()\n");
  if (argc || argv) { /* Silence "unused parameter" warnings. */ }
  if (!render_init) { return "render_init is NULL"; }
  return NULL;
}

/*
[PUBLIC] Shut down the graphics sub-system and free any allocated memory.
*/

void gfxQuit(void)
{
  fprintf(stderr, "gfxQuit()\n");
}

/*
[PUBLIC] Set up anything related to the game world when a new map is loaded.
*/

int gfxWorldCreate(const char *file_name, const lamp_s *lamp)
{
  fprintf(stderr, "gfxWorldCreate()\n");
  if (!file_name) { return -1; }
  if (lamp) { /* Silence "unused parameter" warnings. */ }
  return 0;
}

/*
[PUBLIC] Free any memory allocated by `gfxWorldCreate()`.
*/

void gfxWorldDelete(void)
{
  fprintf(stderr, "gfxWorldDelete()\n");
}

/*
[PUBLIC] Set up various things for a given actor class when it's first loaded.
*/

int gfxClassCreate(class_s *klass)
{
  fprintf(stderr, "gfxClassCreate()\n");
  if (!klass) { return -1; }
  return 0;
}

/*
[PUBLIC] Free any memory allocated by `gfxClassCreate()`.
*/

void gfxClassDelete(class_s *klass)
{
  fprintf(stderr, "gfxClassDelete()\n");
  if (!klass) { return; }
}

/*
[PUBLIC] Initialise a new 2D sprite instance.
*/

int gfxSpriteCreate(sprite_s *sprite)
{
  fprintf(stderr, "gfxSpriteCreate()\n");
  if (!sprite) { return -1; }
  return 0;
}

/*
[PUBLIC] Free any memory allocated by `gfxSpriteCreate()`.
*/

void gfxSpriteDelete(sprite_s *sprite)
{
  fprintf(stderr, "gfxSpriteDelete()\n");
  if (!sprite) { return; }
}

/*
[PUBLIC] Load a new image, and assign a name to it.
*/

int gfxCacheImage(const char *name, const char *file_name, v2d_flag_t flags)
{
  fprintf(stderr, "gfxCacheImage()\n");
  if (!name || !file_name) { return -1; }
  if (flags) { /* Silence "unused parameter" warnings. */ }
  return 0;
}

/*
[PUBLIC] Change the image used by a particular sprite instance.
*/

int gfxSpriteSetImage(sprite_s *sprite, const char *image)
{
  fprintf(stderr, "gfxSpriteSetImage()\n");
  if (!sprite || !image) { return -1; }
  return 0;
}

/*
[PUBLIC] Draw everything in the game world.
*/

void gfxDrawGame(const camera_s *camera, actor_s *actor, wordtree_s *sprite_set, const lamp_s *lamp)
{
  fprintf(stderr, "gfxDrawGame()\n");
  if (camera || actor || sprite_set || lamp) { /* Silence "unused parameter" warnings. */ }
}
