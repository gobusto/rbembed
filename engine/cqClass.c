/**
@file
@brief about

writeme
*/

#include <stdlib.h>
#include <string.h>
#include "cqClass.h"
#include "cqGraphics.h"
#include "cqPhysics.h"
#include "cqScript.h"
#include "xml/xml.h"
#include "lgm/anim/util/wordtree.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/* Global objects and variables. */

wordtree_s *cq_class = NULL; /**< @brief about */

/**
@brief [INTERNAL] writeme

about

@param name about
@param script about
@param mesh about
@param skin about
@param anim about
@param shape about
@param weight about
@param sx about
@param sy about
@param sz about
@param flags about
@return writeme
*/

static class_s *classCreate(
  const char *name,
  const char *script,
  const char *mesh,
  const char *skin,
  const char *anim,
  shape_t shape,
  float weight,
  float sx,
  float sy,
  float sz,
  phys_flag_t flags
)
{
  class_s *klass = NULL;
  float offset[3];

  if (!name) { return NULL; }

  /* Create a new class structure. */
  klass = (class_s*)malloc(sizeof(class_s));
  if (!klass) { return NULL; }
  memset(klass, 0, sizeof(class_s));

  /* Store the name of this class, so that actors can report their type. */
  klass->name = (char*)malloc(strlen(name) + 1);
  if (!klass->name)
  {
    free(klass);
    return NULL;
  }
  strcpy(klass->name, name);

  /* Load the mesh file. */
  if (mesh)
  {

    klass->mesh = meshLoadFile(mesh);
    meshGetCenter(klass->mesh, offset);
    meshTranslate(klass->mesh, -offset[0], -offset[1], -offset[2]);
    meshScale(klass->mesh, sx, sy, sz);

    if (klass->mesh)
    {

      if (skin)
      {

        /*
        This allows the "built-in" material group of a mesh to be replaced with
        a specific .mtl or .skin file. This is for Open Arena-style .MD3 files,
        which have their internal names are set to "Material" (because a single
        player model can have multiple skins in that game). Of course, any .MD3
        files explicitly stating "myskin.tga" or similar will work as they are.

        Problem: This only works properly if the materials in the "replacement"
        file are in the right order; groups reference their materials by index.
        The Right Thing To Do would be to handle this whilst the model is being
        loaded, so that we get the NAME of the material for each group as well.
        */

        mtrlFreeGroup(klass->mesh->mtrl);
        klass->mesh->mtrl = mtrlLoadFile(skin);

      }

      if (gfxClassCreate(klass) != 0)
      { fprintf(stderr, "[WARNING] Failed to load mesh materials: %s\n", mesh); }

    } else { fprintf(stderr, "[WARNING] Failed to load mesh: %s\n", mesh); }

    /* Load animation data. TODO: Fall back to using the internal mesh anims? */
    if (anim)
    {
      klass->anim = animSetCreate();
      if (animLoadFile(klass->anim, "default", anim) < 1) { fprintf(stderr, "[WARNING] Failed to load animations: %s\n", anim); }
    }
    else { klass->anim = animFromMesh(klass->mesh, 30); }

    /* Initialise the physics data. */
    if (physClassCreate(klass, shape, weight, flags) != 0)
    { fprintf(stderr, "[WARNING] Failed to initialise physics: %s\n", script); }
  }

  /* Load the Ruby/Python/Lua class definition (for scripting). */
  if (scriptClassCreate(script) != 0)
  { fprintf(stderr, "[WARNING] Failed to load script: %s\n", script); }

  /* Return the result. */
  return klass;
}

/**
@brief [INTERNAL] writeme

about

@param name about
@param data about
@param user about
@return about
*/

static void *classDelete(const char *name, void *data, void *user)
{
  class_s *klass = (class_s*)data;

  if (user || name) { }

  if (!klass) { return NULL; }

  scriptClassDelete(name);
  physClassDelete(klass);
  gfxClassDelete(klass);
  animSetDelete(klass->anim);
  meshDelete(klass->mesh);

  free(klass->name);
  free(klass);
  return NULL;
}

/*
[PUBLIC] about
*/

int classLoad(const char *file_name)
{

  xml_s *document = NULL;
  xml_s *root_tag = NULL;
  xml_s *this_tag = NULL;

  /* If a class dictionary dosn't already exist, create one. */

  if (!cq_class)
  {
    cq_class = treeCreate();
    if (!cq_class) { return 1; }
  }

  /* Load the class definitions file. */

  document = xmlLoadFile(file_name);
  if (!document) { return -1; }

  root_tag = xmlFindName(document->node, "ClassSet", 0);
  if (!root_tag)
  {
    xmlDelete(document);
    return -2;
  }

  for (this_tag = root_tag->node; this_tag; this_tag = this_tag->next)
  {

    class_s *klass = NULL;

    const char *name = NULL;
    const char *mesh = NULL;
    const char *skin = NULL;
    const char *anim = NULL;
    const char *code = NULL;

    shape_t shape = SHAPE_BOX;
    float kilos = 0;
    float scale_x = 1.0;
    float scale_y = 1.0;
    float scale_z = 1.0;
    phys_flag_t flags = 0;

    xml_s *attrib = NULL;

    if (this_tag->name[0] == '#') { continue; }

    if (strcmp(this_tag->name, "Class") != 0)
    {
      fprintf(stderr, "[WARNING] Unknown tag type: %s\n", this_tag->name);
      continue;
    }

    attrib = xmlFindName(this_tag->attr, "name", 0);
    name = attrib ? attrib->text : "";

    attrib = xmlFindName(this_tag->attr, "script", 0);
    code = attrib ? attrib->text : NULL;

    attrib = xmlFindName(this_tag->attr, "mesh", 0);
    mesh = attrib ? attrib->text : NULL;

    attrib = xmlFindName(this_tag->attr, "material", 0);
    skin = attrib ? attrib->text : NULL;

    attrib = xmlFindName(this_tag->attr, "animation", 0);
    anim = attrib ? attrib->text : NULL;

    attrib = xmlFindName(this_tag->attr, "shape", 0);
    shape = SHAPE_BOX;  /* <-- Used as a default. */

    if (attrib)
    {
      if (attrib->text)
      {
        if      (!strcmp(attrib->text, "sphere"  )) { shape = SHAPE_SPHERE;   }
        else if (!strcmp(attrib->text, "capsule" )) { shape = SHAPE_CAPSULE;  }
        else if (!strcmp(attrib->text, "cylinder")) { shape = SHAPE_CYLINDER; }
        else if (!strcmp(attrib->text, "trimesh" )) { shape = SHAPE_TRIMESH; }
        else if (strcmp(attrib->text, "box") != 0)
        { fprintf(stderr, "[WARNING] %s: Unknown shape %s\n", name, attrib->text); }
      }
    }

    attrib = xmlFindName(this_tag->attr, "weight", 0);
    kilos = attrib ? atof(attrib->text) : 0;

    /* Actor scaling - this can either be overall, or per-axis. */

    attrib = xmlFindName(this_tag->attr, "scale", 0);

    scale_x = attrib ? atof(attrib->text) : 1.0;
    scale_y = attrib ? atof(attrib->text) : 1.0;
    scale_z = attrib ? atof(attrib->text) : 1.0;

    attrib = xmlFindName(this_tag->attr, "xscale", 0);
    if (attrib) { scale_x = atof(attrib->text); }

    attrib = xmlFindName(this_tag->attr, "yscale", 0);
    if (attrib) { scale_y = atof(attrib->text); }

    attrib = xmlFindName(this_tag->attr, "zscale", 0);
    if (attrib) { scale_z = atof(attrib->text); }

    /* Check for flags. These are assumed to be "false" if not present. */

    flags = 0;

    attrib = xmlFindName(this_tag->attr, "static", 0);
    if (attrib)
    {
      if (strcmp(attrib->text, "true") == 0) { flags |= PHYS_FLAG_STATIC; }
    }

    attrib = xmlFindName(this_tag->attr, "nospin", 0);
    if (attrib)
    {
      if (strcmp(attrib->text, "true") == 0) { flags |= PHYS_FLAG_NOSPIN; }
    }

    /* Attempt to create the new class. */

    klass = classCreate(name, code, mesh, skin, anim, shape, kilos, scale_x, scale_y, scale_z, flags);

    if (klass)
    {

      void *old = NULL;

      if (!treeInsert(cq_class, name, klass, &old))
      {
        fprintf(stderr, "[WARNING] Failed to store class: %s\n", name);
        classDelete(name, klass, NULL);
      }

      if (old) { classDelete(name, old, NULL); }

    } else { fprintf(stderr, "[WARNING] Failed to create class: %s\n", name); }

  }

  xmlDelete(document);

  /* Report success. */

  return 0;

}

/*
[PUBLIC] about
*/

void classQuit(void) { cq_class = treeDelete(cq_class, classDelete, NULL); }

/*
[PUBLIC] about
*/

class_s *classFind(const char *name) { return (class_s*)treeSearch(cq_class, name); }
