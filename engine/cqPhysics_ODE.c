/**
@file
@brief An ODE-based implementation of the functions defined in cqPhysics.h

For details on how to use ODE, see the manual on the official wiki page:
<http://ode-wiki.org/wiki/index.php?title=Manual:_All>

As suggested by the wiki page above, measurements are based around the metric
system: Lengths are in metres, masses in kilograms, and timesteps in seconds.

This code was written for ODE 0.12 with GIMPACT trimesh support. v0.13 causes
'undefined reference' errors, and OPCODE trimesh collision causes all kind of
problems with things falling through walls. Either single or double-precision
can be used (though single seems to work better on Win32 for some reason).
*/

#ifndef dSINGLE
  #ifndef dDOUBLE
    #define dDOUBLE /**< @brief Newer versions of ODE _should_ define this. */
  #endif
#endif

#include <ode/ode.h>
#include <stdlib.h>
#include "cqPhysics.h"
#include "cqScript.h"
#include "cqSystem.h"  /* <-- This is where CQ2_TICK_DELAY is defined. */
#include "cqWorld.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/* Constants and macros. */

#define MAX_CONTACTS 8 /**< @brief about */

/* Flags used internally for specific actor instances: */

#define PHYS_FLAG_ODE_ISRAGDOLL 0x01  /**< @brief Set by ragdollified actors. */
#define PHYS_FLAG_ODE_RESERVED1 0x02  /**< @brief Not used yet.               */
#define PHYS_FLAG_ODE_RESERVED2 0x04  /**< @brief Not used yet.               */
#define PHYS_FLAG_ODE_RESERVED3 0x08  /**< @brief Not used yet.               */

#define PHYS_FLAG_ODE_DORAGDOLL 0x10  /**< @brief Become a ragdoll ASAP.      */
#define PHYS_FLAG_ODE_DOSETGEOM 0x20  /**< @brief Set collision space ASAP.   */
#define PHYS_FLAG_ODE_DOUNUSED2 0x40  /**< @brief Not used yet.               */
#define PHYS_FLAG_ODE_DOUNUSED3 0x80  /**< @brief Not used yet.               */

/**
@brief [INTERNAL] Triangle mesh shape structure.

This structure is used to contain everything ODE needs for a trimesh shape.
*/

typedef struct
{

  dTriMeshDataID data;  /**< @brief about */

  int num_vertices;     /**< @brief about */
  float *vertex;        /**< @brief about */

  int num_triangles;    /**< @brief about */
  int *triangle;        /**< @brief about */

} trimesh_s;

/**
@brief [INTERNAL] An ODE-specific implementation of the "_physics_s" structure.

@todo This needs to include information about how ragdolls should work...
*/

struct _physics_s
{

  shape_t     shape;  /**< @brief about */
  float       kilos;  /**< @brief about */
  phys_flag_t flags;  /**< @brief about */

  trimesh_s *trimesh; /**< @brief about */

};

typedef struct _physics_s physics_s;  /**< @brief A nicer name for it. */

/**
@brief [INTERNAL] An ODE-specific implementation of the "_ragdoll_s" structure.

writeme
*/

struct _ragdoll_s
{
  v2d_flag_t flags; /**< @brief about */

  int num_bodies;   /**< @brief about */
  int num_geoms;    /**< @brief about */
  int num_joints;   /**< @brief about */

  dBodyID  *body;   /**< @brief about */
  dGeomID  *geom;   /**< @brief about */
  dJointID *joint;  /**< @brief about */
};

typedef struct _ragdoll_s ragdoll_s;  /**< @brief A nicer name for it. */

/* Global objects and variables. */

dWorldID phys_world = 0;  /**< @brief writeme */
dSpaceID phys_space = 0;  /**< @brief writeme */
dJointGroupID phys_contacts = 0;  /**< @brief writeme */
v2d_bool_t phys_locked = V2D_FALSE; /**< @brief writeme */

dGeomID phys_map_geom = 0;          /**< @brief writeme */
trimesh_s *phys_map_trimesh = NULL; /**< @brief writeme */

/**
@brief [INTERNAL] about

writeme

@param trimesh about
@return writeme
*/

static trimesh_s *physTrimeshDelete(trimesh_s *trimesh)
{

  if (!trimesh) { return NULL; }

  if (trimesh->data) { dGeomTriMeshDataDestroy(trimesh->data); }
  if (trimesh->vertex) { free(trimesh->vertex); }
  if (trimesh->triangle) { free(trimesh->triangle); }

  free(trimesh);
  return NULL;

}

/**
@brief [INTERNAL] about

@todo Allow a sub-mesh to be used, as this would be useful for ragdoll meshes
which will need to be broken down into individual parts.

@param mesh about
@return writeme
*/

static trimesh_s *physTrimeshCreate(mesh_s *mesh)
{

  trimesh_s *trimesh = NULL;
  long g, i, t;

  if (!mesh) { return NULL; }

  trimesh = (trimesh_s*)malloc(sizeof(trimesh_s));
  if (!trimesh) { return NULL; }
  memset(trimesh, 0, sizeof(trimesh_s));

  trimesh->num_vertices = mesh->num_verts;

  for (g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g]) { trimesh->num_triangles += mesh->group[g]->num_tris; }
  }

  trimesh->data = dGeomTriMeshDataCreate();
  trimesh->vertex =   (float*)malloc(sizeof(float) * trimesh->num_vertices  * 3);
  trimesh->triangle = (int*  )malloc(sizeof(int  ) * trimesh->num_triangles * 3);

  if (!trimesh->data || !trimesh->vertex || !trimesh->triangle)
  { return physTrimeshDelete(trimesh); }

  for (i = 0; i < mesh->num_verts; i++)
  {
    trimesh->vertex[(i*3)+0] = mesh->vertex[0][(i*3)+0];
    trimesh->vertex[(i*3)+1] = mesh->vertex[0][(i*3)+1];
    trimesh->vertex[(i*3)+2] = mesh->vertex[0][(i*3)+2];
  }

  /* Note that the vertex indices are reversed... */

  for (i = 0, g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g])
    {
      for (t = 0; t < mesh->group[g]->num_tris; t++, i++)
      {
        trimesh->triangle[(i*3)+0] = mesh->group[g]->tri[t]->v_id[2];
        trimesh->triangle[(i*3)+1] = mesh->group[g]->tri[t]->v_id[1];
        trimesh->triangle[(i*3)+2] = mesh->group[g]->tri[t]->v_id[0];
      }
    }
  }

  /* NOTE: GIMPACT trimesh support relies on this exact size/stride setup... */

  dGeomTriMeshDataBuildSingle(trimesh->data,
                              trimesh->vertex,  sizeof(float)*3, trimesh->num_vertices,
                              trimesh->triangle, trimesh->num_triangles * 3, sizeof(int)*3);

  return trimesh;

}

/**
@brief [INTERNAL] about

writeme

@param ragdoll writeme
@return about
*/

static ragdoll_s *physRagdollDelete(ragdoll_s *ragdoll)
{

  int i = 0;

  if (!ragdoll) { return NULL; }

  /* Free joints. */

  if (ragdoll->joint)
  {

    for (i = 0; i < ragdoll->num_joints; i++)
    {
      if (ragdoll->joint[i]) { dJointDestroy(ragdoll->joint[i]); }
    }

    free(ragdoll->joint);

  }

  /* Free geoms. */

  if (ragdoll->geom)
  {

    for (i = 0; i < ragdoll->num_geoms; i++)
    {
      if (ragdoll->geom[i]) { dGeomDestroy(ragdoll->geom[i]); }
    }

    free(ragdoll->geom);

  }

  /* Free bodies. */

  if (ragdoll->body)
  {

    for (i = 0; i < ragdoll->num_bodies; i++)
    {
      if (ragdoll->body[i]) { dBodyDestroy(ragdoll->body[i]); }
    }

    free(ragdoll->body);

  }

  /* Free the main structure. */

  free(ragdoll);
  return NULL;

}

/**
@brief [INTERNAL] about

writeme

@param num_bodies about
@param num_geoms about
@param num_joints about
@return writeme
*/

static ragdoll_s *physRagdollCreate(int num_bodies, int num_geoms, int num_joints)
{

  ragdoll_s *ragdoll = NULL;

  if (num_bodies < 0 || num_geoms < 0 || num_joints < 0) { return NULL; }

  ragdoll = (ragdoll_s*)malloc(sizeof(ragdoll_s));
  if (!ragdoll) { return NULL; }
  memset(ragdoll, 0, sizeof(ragdoll_s));

  ragdoll->num_bodies = num_bodies;
  ragdoll->num_geoms  = num_geoms;
  ragdoll->num_joints = num_joints;

  if (num_bodies)
  {
    ragdoll->body = (dBodyID*)malloc(sizeof(dBodyID) * num_bodies);
    if (!ragdoll->body) { return physRagdollDelete(ragdoll); }
    memset(ragdoll->body, 0, sizeof(dBodyID) * num_bodies);
  }

  if (num_geoms)
  {
    ragdoll->geom = (dGeomID*)malloc(sizeof(dGeomID) * num_geoms);
    if (!ragdoll->geom) { return physRagdollDelete(ragdoll); }
    memset(ragdoll->geom, 0, sizeof(dGeomID) * num_geoms);
  }

  if (num_joints)
  {
    ragdoll->joint = (dJointID*)malloc(sizeof(dJointID) * num_joints);
    if (!ragdoll->joint) { return physRagdollDelete(ragdoll); }
    memset(ragdoll->joint, 0, sizeof(dJointID) * num_joints);
  }

  return ragdoll;

}

/*
[PUBLIC] about
*/

int physInit(void)
{

  physQuit();

  dInitODE();

  phys_world = dWorldCreate();
  phys_space = dSimpleSpaceCreate(0);
  phys_contacts = dJointGroupCreate(0);

  dWorldSetGravity(phys_world, 0, 0, -9.8);
  dWorldSetCFM(phys_world, 0.0001);
  dWorldSetERP(phys_world, 0.5);

  fprintf(stderr, "[INIT] ODE %s\n", dODE_VERSION);
  fprintf(stderr, "\tConfig: %s\n", dGetConfiguration());

  return 0;

}

/*
[PUBLIC] about
*/

void physQuit(void)
{

  if (!phys_world && !phys_space) { return; }

  physWorldDelete();

  dJointGroupDestroy(phys_contacts);
  dWorldDestroy(phys_world);
  dSpaceDestroy(phys_space);

  dCloseODE();

}

/**
@brief [INTERNAL] about

writeme

@param data about
@param geom_a about
@param geom_b about
*/

static void physCollide(void *data, dGeomID geom_a, dGeomID geom_b)
{

  dContactGeom contact_geom[MAX_CONTACTS];
  v2d_real_t   contact_data[1 + (MAX_CONTACTS * 3)];

  actor_s *actor_a = NULL;
  actor_s *actor_b = NULL;
  dBodyID body_a = 0;
  dBodyID body_b = 0;
  int num_contacts, i;

  if (data) { /* Silence "unused variable" warnings. */ }

  /* Determine if the two geoms are intersecting. */

  num_contacts = dCollide(geom_a, geom_b, MAX_CONTACTS, contact_geom, sizeof(dContactGeom));

  if (num_contacts < 1) { return; }

  /* Create a list of "generic" contact points to pass to scripts. */

  contact_data[0] = num_contacts;

  for (i = 0; i < num_contacts; i++)
  {
    contact_data[1 + (i*3)+0] = contact_geom[i].pos[0];
    contact_data[1 + (i*3)+1] = contact_geom[i].pos[1];
    contact_data[1 + (i*3)+2] = contact_geom[i].pos[2];
  }

  /* If the geoms are associated with actors, check if they SHOULD collide. */

  actor_a = (actor_s*)dGeomGetData(geom_a);
  actor_b = (actor_s*)dGeomGetData(geom_b);

  if (actor_a != actor_b)
  {

    int result_a = actor_a ? scriptCall(actor_a, EVENT_COLLIDE, actor_b, contact_data) : 0;
    int result_b = actor_b ? scriptCall(actor_b, EVENT_COLLIDE, actor_a, contact_data) : 0;

    if (result_a || result_b) { return; }

  }

  /* If neither geom is associated with a body, the stop here. */

  body_a = dGeomGetBody(geom_a);
  body_b = dGeomGetBody(geom_b);

  if (!body_a && !body_b) { return; }

  /* Otherwise, create a set of "contact" joints to push the objects apart. */

  for (i = 0; i < num_contacts; i++)
  {

    dContact contact;
    dJointID joint;

    memset(&contact, 0, sizeof(dContact));
    contact.geom = contact_geom[i];
    contact.surface.mu = 40;

    joint = dJointCreateContact(phys_world, phys_contacts, &contact);
    dJointAttach(joint, body_a, body_b);

  }

}

/*
[PUBLIC] about
*/

void physUpdate(void)
{
  const dQuaternion ode_identity_quaternion = { 1.0, 0.0, 0.0, 0.0 };

  actor_s *actor = NULL;

  /* NOTE: We can't update the collision space durin collision testing... */
  phys_locked = V2D_TRUE;
  dSpaceCollide(phys_space, NULL, physCollide);
  phys_locked = V2D_FALSE;

  /* Update the position of everything in the world / reset collisions: */
  dWorldQuickStep(phys_world, CQ2_TICK_DELAY / 1000.0);
  dJointGroupEmpty(phys_contacts);

  /* Handle any special flags for the individual actors: */
  for (actor = worldGetActor(); actor; actor = actorGetNext(actor))
  {
    if (!actor->physics) { continue; }

    if (!physActorIsRagdoll(actor))
    {
      /* If an actor is not supposed to spin, reset them back to "upright": */
      if (actor->klass->physics->flags & PHYS_FLAG_NOSPIN)
      {
        dGeomSetQuaternion(actor->physics->geom[0], ode_identity_quaternion);
        if (actor->physics->body[0]) {
          dBodySetTorque(actor->physics->body[0], 0, 0, 0);
          dBodySetAngularVel(actor->physics->body[0], 0, 0, 0);
        }
      }
      /* If an actor tried to ragdoll-ify during the lock, ragdoll-ify now: */
      if (actor->physics->flags & PHYS_FLAG_ODE_DORAGDOLL)
      {
      /* NOTE: This creates a NEW physics actor i.e. resets `flags` to zero: */
        physActorBecomeRagdoll(actor);
      }
    }

    /* If an actor was spawned during the lock, enable collision now: */
    if (actor->physics->flags & PHYS_FLAG_ODE_DOSETGEOM)
    {
      int i;
      for (i = 0; i < actor->physics->num_geoms; ++i)
      { dSpaceAdd(phys_space, actor->physics->geom[i]); }
      actor->physics->flags -= PHYS_FLAG_ODE_DOSETGEOM;
    }

  }

}

/*
[PUBLIC] about
*/

int physWorldCreate(const char *file_name)
{
  mesh_s *mesh = NULL;

  physWorldDelete();

  mesh = meshLoadFile(file_name);
  phys_map_trimesh = physTrimeshCreate(mesh);
  meshDelete(mesh);

  if (!phys_map_trimesh) { return -1; }

  phys_map_geom = dCreateTriMesh(phys_space, phys_map_trimesh->data, NULL, NULL, NULL);

  return 0;
}

/*
[PUBLIC] about
*/

void physWorldDelete(void)
{
  if (phys_map_geom) { dGeomDestroy(phys_map_geom); }
  phys_map_geom = 0;
  phys_map_trimesh = physTrimeshDelete(phys_map_trimesh);
}

/*
[PUBLIC] about
*/

int physClassCreate(class_s *klass, shape_t shape, float kilos, phys_flag_t flags)
{

  if (!phys_world && !phys_space) { return -1; }
  else if (!klass)                { return -2; }

  /* Free any old data first. */

  physClassDelete(klass);

  /* Allocate memory for a new physics object. */

  klass->physics = (physics_s*)malloc(sizeof(physics_s));
  if (!klass->physics) { return -1; }
  memset(klass->physics, 0, sizeof(physics_s));

  /* Initialise the new physics structure. */

  klass->physics->shape = shape;
  klass->physics->kilos = kilos > 0 ? kilos : 0.001;  /* Mass can't be zero. */
  klass->physics->flags = flags;

  /* If the class uses trimesh collision, create a trimesh data structure. */

  if (klass->physics->shape == SHAPE_TRIMESH)
  {

    klass->physics->trimesh = physTrimeshCreate(klass->mesh);

    if (!klass->physics->trimesh)
    {
      physClassDelete(klass);
      return -2;
    }

  }

  /* Report success. */

  return 0;

}

/*
[PUBLIC] about
*/

void physClassDelete(class_s *klass)
{

  if      (!klass         ) { return; }
  else if (!klass->physics) { return; }

  physTrimeshDelete(klass->physics->trimesh);

  free(klass->physics);

}

/*
[PUBLIC] about
*/

int physActorCreate(actor_s *actor)
{
  dSpaceID space = phys_locked ? NULL : phys_space;

  dMass mass;
  float x, y, z;

  if (!phys_world && !phys_space) { return -1; }
  else if (!actor)                { return -2; }

  /* Free any old data first. */
  physActorDelete(actor);

  /* If this actor class does not have any physics data, ignore it. */
  if (!actor->klass->physics) { return 0; }

  /* Actors have 1 body + 1 geom by default - unless they're "static": */
  if (actor->klass->physics->flags & PHYS_FLAG_STATIC)
    actor->physics = physRagdollCreate(0, 1, 0);
  else
    actor->physics = physRagdollCreate(1, 1, 0);
  if (!actor->physics) { return -3; }

  if (phys_locked) { actor->physics->flags |= PHYS_FLAG_ODE_DOSETGEOM; }

  /* writeme */
  if (actor->klass->mesh)
  {
    x = actor->klass->mesh->xyzmax[0] - actor->klass->mesh->xyzmin[0];
    y = actor->klass->mesh->xyzmax[1] - actor->klass->mesh->xyzmin[1];
    z = actor->klass->mesh->xyzmax[2] - actor->klass->mesh->xyzmin[2];
  }
  else
  {
    x = 1;
    y = 1;
    z = 1;
  }

  switch (actor->klass->physics->shape)
  {
    case SHAPE_SPHERE:
      if (z < x || z < y) { z = x > y ? x : y; }
      actor->physics->geom[0] = dCreateSphere(space, z / 2.0);
      dMassSetSphereTotal(&mass, actor->klass->physics->kilos, z / 2.0);
    break;

    case SHAPE_CAPSULE:
      /* TODO: This should align the capsule along the LONGEST axis, not Z. */
      x = x > y ? x : y;
      actor->physics->geom[0] = dCreateCapsule(space, x / 2.0, z - x);
      dMassSetCapsuleTotal(&mass, actor->klass->physics->kilos, 3, x / 2.0, z - x);
    break;

    case SHAPE_CYLINDER:
      /* TODO: This should align the cylinder along the LONGEST axis, not Z. */
      x = x > y ? x : y;
      actor->physics->geom[0] = dCreateCylinder(space, x / 2.0, z);
      dMassSetCylinderTotal(&mass, actor->klass->physics->kilos, 3, x / 2.0, z);
    break;

    case SHAPE_TRIMESH:
      /*
      NOTE: Using dMassSetTrimeshTotal() results in an error message + crash...
      "ODE INTERNAL ERROR 2: The centre of mass must be at the origin."
      */
      actor->physics->geom[0] = dCreateTriMesh(space, actor->klass->physics->trimesh->data, NULL, NULL, NULL);
      /* dMassSetTrimeshTotal(&mass, actor->klass->physics->kilos, actor->physics->geom); */
      dMassSetBoxTotal(&mass, actor->klass->physics->kilos, x, y, z);
    break;

    case SHAPE_BOX: default:
      actor->physics->geom[0] = dCreateBox(space, x, y, z);
      dMassSetBoxTotal(&mass, actor->klass->physics->kilos, x, y, z);
    break;
  }

  dGeomSetData(actor->physics->geom[0], actor);

  /* Create a body to go with the geom. Immovable actors only have a geom. */
  if (actor->physics->num_bodies > 0)
  {
    actor->physics->body[0] = dBodyCreate(phys_world);
    dBodySetMass(actor->physics->body[0], &mass);
    dGeomSetBody(actor->physics->geom[0], actor->physics->body[0]);
  }

  /* Report success. */
  return 0;
}

/*
[PUBLIC] about
*/

void physActorDelete(actor_s *actor)
{
  if (actor) { actor->physics = physRagdollDelete(actor->physics); }
}

/*
[PUBLIC] about.
*/

v2d_bool_t physActorGetGravity(const actor_s *actor)
{
  if      (!actor                         ) { return V2D_FALSE; }
  else if (!actor->physics                ) { return V2D_FALSE; }
  else if (actor->physics->num_bodies <= 0) { return V2D_FALSE; }

  return dBodyGetGravityMode(actor->physics->body[0]);
}

/*
[PUBLIC] about.
*/

v2d_bool_t physActorSetGravity(actor_s *actor, v2d_bool_t enabled)
{

  int i;

  if      (!actor         ) { return V2D_FALSE; }
  else if (!actor->physics) { return V2D_FALSE; }

  for (i = 0; i < actor->physics->num_bodies; ++i)
  { dBodySetGravityMode(actor->physics->body[i], enabled); }

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t physActorIsRagdoll(const actor_s *actor)
{
  if (!actor) { return V2D_FALSE; }
  return actor->physics->flags & PHYS_FLAG_ODE_ISRAGDOLL;
}

/*
[PUBLIC] about
*/

v2d_bool_t physActorCanBecomeRagdoll(const actor_s *actor)
{
  if      (!actor                       ) { return V2D_FALSE; }
  else if (!actor->klass->mesh          ) { return V2D_FALSE; }
  else if (!actor->klass->mesh->vert_tag) { return V2D_FALSE; }
  return !physActorIsRagdoll(actor);
}

/*
[PUBLIC] about
*/

v2d_bool_t physActorBecomeRagdoll(actor_s *actor)
{

  v2d_bool_t   gravity_enabled;
  float        old_position[3];
  quaternion_t old_rotation;
  float        vel[3];

  vector3_t xyz;

  dMass mass;

  int num_joints, i;

  if (!physActorCanBecomeRagdoll(actor)) { return V2D_FALSE; }

  if (phys_locked)
  {
    actor->physics->flags |= PHYS_FLAG_ODE_DORAGDOLL;
    return V2D_TRUE;
  }

  /* Get the current physical state. (TODO: Also get angular velocity...?) */

  gravity_enabled = physActorGetGravity(actor);
  physActorGetPosition  (actor, 0, old_position);
  physActorGetQuaternion(actor, 0, old_rotation);
  physActorGetVelocity  (actor, 0, vel         );

  physActorDelete(actor);

  /* Calculate the number of JOINTS that are needed. */

  for (num_joints = 0, i = 0; i < actor->klass->mesh->tag_list->num_tags; i++)
  {
    if (actor->klass->mesh->tag_list->tag[i]->parent >= 0) { num_joints++; }
  }

  i = actor->klass->mesh->tag_list->num_tags;

  actor->physics = physRagdollCreate(i, i, num_joints);
  if (!actor->physics) { return V2D_FALSE; }
  actor->physics->flags |= PHYS_FLAG_ODE_ISRAGDOLL;

  /* Create each body/geom pair. */

  for (i = 0; i < actor->physics->num_bodies; i++)
  {

    /* TODO: Use trimeshes (for better accuracy) and set the mass correctly. */

    xyz[0] = actor->klass->mesh->tag_list->tag[i]->xyzmax[0] - actor->klass->mesh->tag_list->tag[i]->xyzmin[0];
    xyz[1] = actor->klass->mesh->tag_list->tag[i]->xyzmax[1] - actor->klass->mesh->tag_list->tag[i]->xyzmin[1];
    xyz[2] = actor->klass->mesh->tag_list->tag[i]->xyzmax[2] - actor->klass->mesh->tag_list->tag[i]->xyzmin[2];

    if (xyz[0] <= 0) { xyz[0] = 0.1; }
    if (xyz[1] <= 0) { xyz[1] = 0.1; }
    if (xyz[2] <= 0) { xyz[2] = 0.1; }

    actor->physics->geom[i] = dCreateBox(phys_space, xyz[0], xyz[1], xyz[2]);
    dMassSetBoxTotal(&mass, 0.1, xyz[0], xyz[1], xyz[2]);

    dGeomSetData(actor->physics->geom[i], actor);

    /* Create a body to go with the geom. */

    actor->physics->body[i] = dBodyCreate(phys_world);
    dBodySetMass(actor->physics->body[i], &mass);

    if (!gravity_enabled) { dBodySetGravityMode(actor->physics->body[i], 0); }

    dGeomSetBody(actor->physics->geom[i], actor->physics->body[i]);

    /* Position the new body/geom pair. TODO: Use quaternion rotation here! */

    meshGetTagCenter(actor->klass->mesh, i, xyz);

    xyz[0] += old_position[0];
    xyz[1] += old_position[1];
    xyz[2] += old_position[2];

    dBodySetPosition (actor->physics->body[i], xyz[0], xyz[1], xyz[2]);
    dBodySetLinearVel(actor->physics->body[i], vel[0], vel[1], vel[2]);

  }

  /* Create joints. TODO: Use quaternion rotation here! */

  for (num_joints = 0, i = 0; i < actor->klass->mesh->tag_list->num_tags; i++)
  {

    if (actor->klass->mesh->tag_list->tag[i]->parent < 0) { continue; }

    actor->physics->joint[num_joints] = dJointCreateBall(phys_world, 0);

    dJointAttach(actor->physics->joint[num_joints],
        actor->physics->body[i],
        actor->physics->body[actor->klass->mesh->tag_list->tag[i]->parent]);

    dJointSetBallAnchor(actor->physics->joint[num_joints],
        actor->klass->mesh->tag_list->tag[i]->position[0] + old_position[0],
        actor->klass->mesh->tag_list->tag[i]->position[1] + old_position[1],
        actor->klass->mesh->tag_list->tag[i]->position[2] + old_position[2]);

    num_joints++;

  }

  return V2D_TRUE;
}

/*
[PUBLIC] about
*/

int physActorGetPosition(const actor_s *actor, int id, float *vec3)
{

  const dReal *ode_data = NULL;

  /* Initialise the output vector, so that a [0,0,0] value is used on error. */

  if (vec3) { memset(vec3, 0, sizeof(float) * 3); }

  /* Check the parameters. */

  if      (!actor || !vec3) { return -1; }
  else if (!actor->physics) { return -2; }

  if      (id < 0 || id >= actor->physics->num_geoms) { return -3; }
  else if (actor->physics->geom[id] == 0            ) { return -4; }

  /* Get the position. Don't memcpy() here - dReal could be float OR double. */

  ode_data = dGeomGetPosition(actor->physics->geom[id]);

  vec3[0] = ode_data[0];
  vec3[1] = ode_data[1];
  vec3[2] = ode_data[2];

  return 0;

}

/*
[PUBLIC] about
*/

int physActorSetPosition(actor_s *actor, float x, float y, float z)
{

  float origin[3];
  float xyz[3];
  int i = 0;

  if      (!actor         ) { return -1; }
  else if (!actor->physics) { return -2; }

  /* Move all geoms and offset them relative to the reference position. */

  physActorGetPosition(actor, 0, origin);

  for (i = 0; i < actor->physics->num_geoms; i++)
  {

    if (physActorGetPosition(actor, i, xyz) == 0)
    {

      xyz[0] += x - origin[0];
      xyz[1] += y - origin[1];
      xyz[2] += z - origin[2];

      dGeomSetPosition(actor->physics->geom[i], xyz[0], xyz[1], xyz[2]);

    }

  }

  /* Report success. */

  return 0;

}

/*
[PUBLIC] about
*/

int physActorGetVelocity(const actor_s *actor, int id, float *vec3)
{
  const dReal *ode_data = NULL;

  /* Initialise the output vector, so that a [0,0,0] value is used on error. */

  if (vec3) { memset(vec3, 0, sizeof(float) * 3); }

  /* Check the parameters. */

  if      (!actor || !vec3) { return -1; }
  else if (!actor->physics) { return -2; }

  if      (id < 0 || id >= actor->physics->num_bodies) { return -3; }
  else if (actor->physics->body[id] == 0             ) { return -4; }

  /* Get the velocity. Don't memcpy() here - dReal could be float OR double. */

  ode_data = dBodyGetLinearVel(actor->physics->body[id]);

  vec3[0] = ode_data[0];
  vec3[1] = ode_data[1];
  vec3[2] = ode_data[2];

  return 0;
}

/*
[PUBLIC] about
*/

int physActorSetVelocity(actor_s *actor, int id, float x, float y, float z)
{
  if      (!actor         ) { return -1; }
  else if (!actor->physics) { return -2; }

  if      (id < 0 || id >= actor->physics->num_bodies) { return -3; }
  else if (actor->physics->body[id] == 0             ) { return -4; }

  dBodySetLinearVel(actor->physics->body[id], x, y, z);

  return 0;
}

/*
[PUBLIC] about
*/

int physActorAddForce(actor_s *actor, int id, float x, float y, float z)
{
  if      (!actor         ) { return -1; }
  else if (!actor->physics) { return -2; }

  if      (id < 0 || id >= actor->physics->num_bodies) { return -3; }
  else if (actor->physics->body[id] == 0             ) { return -4; }

  dBodyAddForce(actor->physics->body[id], x, y, z);

  return 0;
}

/*
[PUBLIC] about
*/

int physActorGetQuaternion(const actor_s *actor, int id, quaternion_t quaternion)
{

  dQuaternion ode_quaternion;

  /* Initialise the output so that a [1,0,0,0] value is used on error. */

  quatIdentity(quaternion);

  /* Check the parameters. */

  if      (!actor || !quaternion) { return -1; }
  else if (!actor->physics      ) { return -2; }

  if      (id < 0 || id >= actor->physics->num_geoms) { return -3; }
  else if (actor->physics->geom[id] == 0            ) { return -4; }

  /* Get the rotation. Don't memcpy() here - dReal could be float OR double. */

  if (actor->klass->physics->flags & PHYS_FLAG_NOSPIN && !physActorIsRagdoll(actor))
       { dGeomGetOffsetQuaternion(actor->physics->geom[id], ode_quaternion); }
  else { dGeomGetQuaternion      (actor->physics->geom[id], ode_quaternion); }

  quaternion[0] = ode_quaternion[0];
  quaternion[1] = ode_quaternion[1];
  quaternion[2] = ode_quaternion[2];
  quaternion[3] = ode_quaternion[3];

  return 0;

}

/*
[PUBLIC] about
*/

int physActorSetQuaternion(actor_s *actor, float w, float x, float y, float z)
{

  dQuaternion new_quat;

  if      (!actor         ) { return -1; }
  else if (!actor->physics) { return -2; }

  /* TODO, MAYBE: Allow quaternions to be set for ragdoll actors...? */

  if (physActorIsRagdoll(actor)) { return 1; }

  new_quat[0] = w;
  new_quat[1] = x;
  new_quat[2] = y;
  new_quat[3] = z;

  if (actor->klass->physics->flags & PHYS_FLAG_NOSPIN && !physActorIsRagdoll(actor))
       { dGeomSetOffsetQuaternion(actor->physics->geom[0], new_quat); }
  else { dGeomSetQuaternion(      actor->physics->geom[0], new_quat); }

  return 0;

}

/**
@brief [INTERNAL] about

writeme

@param data about
@param ray about
@param other about
*/

static void physRayCollide(void *data, dGeomID ray, dGeomID other)
{

  hitscan_s *hitscan = (hitscan_s*)data;
  dContactGeom contact;

  if (dCollide(ray, other, 1, &contact, sizeof(dContactGeom)))
  {

    void *tmp;

    tmp = realloc(hitscan->actor, (hitscan->num_items + 1) * sizeof(actor_s*));
    if (tmp) { hitscan->actor = (actor_s**)tmp; } else { return; }

    tmp = realloc(hitscan->point, (hitscan->num_items + 1) * sizeof(float) * 3);
    if (tmp) { hitscan->point = (float*)tmp; } else { return; }

    hitscan->actor[hitscan->num_items] = (actor_s*)dGeomGetData(other);
    hitscan->point[(hitscan->num_items * 3) + 0] = contact.pos[0];
    hitscan->point[(hitscan->num_items * 3) + 1] = contact.pos[1];
    hitscan->point[(hitscan->num_items * 3) + 2] = contact.pos[2];

    ++hitscan->num_items;

  }

}

/*
[PUBLIC] about
*/

hitscan_s *physHitscanCreate(float px, float py, float pz, float dx, float dy, float dz)
{

  hitscan_s *hitscan = NULL;
  dGeomID ray;
  float dxyz[3];

  dxyz[0] = dx;
  dxyz[1] = dy;
  dxyz[2] = dz;

  ray = dCreateRay(phys_space, vecLength(3, dxyz));
  if (!ray) { return NULL; }
  dGeomRaySet(ray, px, py, pz, dx, dy, dz);
  dGeomRaySetClosestHit(ray, V2D_TRUE);

  hitscan = (hitscan_s*)malloc(sizeof(hitscan_s));
  if (hitscan)
  {
    memset(hitscan, 0, sizeof(hitscan_s));
    dSpaceCollide2(ray, (dGeomID)phys_space, hitscan, physRayCollide);
  }

  dGeomDestroy(ray);
  return hitscan;

}

/*
[PUBLIC] about
*/

void physHitscanDelete(hitscan_s *hitscan)
{

  if (!hitscan) { return; }

  free(hitscan->actor);
  free(hitscan->point);
  free(hitscan);

}
