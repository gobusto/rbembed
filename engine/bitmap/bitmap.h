/**
@file
@brief Provides utilities for loading image files in a non-API-specific way.

Copyright (C) 2013-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CQ2_BITMAP_H__
#define __CQ2_BITMAP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Constants: */
#define BMP_FALSE 0 /**< @brief Constant "false" value. */
#define BMP_TRUE  1 /**< @brief Constant "true" value. */

/* General types used to represent images: */
typedef unsigned long bmp_size_t; /**< @brief Stores width/height values. */
typedef unsigned char bmp_bpp_t;  /**< @brief Stores bits-per-pixel values. */
typedef unsigned char bmp_byte_t; /**< @brief Stores individual R/G/B/A data. */
typedef unsigned char bmp_bool_t; /**< @brief Stores various boolean values. */

/**
@brief This structure represents a single image.

Possible bits-per-pixel values are:

+ 8 (Grey)
+ 16 (Interleaved Grey + Opacity)
+ 24 (Interleaved RGB)
+ 32 (Interleaved RGBA)

Note that all images are represented as 8-bits-per-channel, with no padding for
per-row alignment.
*/

typedef struct
{

  bmp_size_t w;     /**< @brief The width of the image in pixels. */
  bmp_size_t h;     /**< @brief The height of the image in pixels. */
  bmp_bpp_t  bpp;   /**< @brief The number of bits per pixel. */
  bmp_byte_t *data; /**< @brief A raw data buffer representing pixel values. */

} bmp_image_s;

/**
@brief Initialise anything that may need doing prior to loading images.

@return `BMP_TRUE` on success, or `BMP_FALSE` on failure.
*/

bmp_bool_t bmpInit(void);

/**
@brief Clean up anything that may have been previously set up by `bmpInit()`.
*/

void bmpQuit(void);

/**
@brief Load an image file into memory.

@param file_name The name of (and path to) the image file to be loaded.
@return A new image structure on success, or `NULL` on failure.
*/

bmp_image_s *bmpLoadFile(const char *file_name);

/**
@brief Create a new (blank) image structure.

@param w The width of the image in pixels.
@param h The height of the image in pixels.
@param bpp The number of bits per pixel.
@return A new image structure on success, or `NULL` on failure.
*/

bmp_image_s *bmpCreate(bmp_size_t w, bmp_size_t h, bmp_bpp_t bpp);

/**
@brief Free a previously-created image structure from memory.

@param image The image structure to be deleted.
*/

void bmpFree(bmp_image_s *image);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_BITMAP_H__ */
