/**
@file
@brief An stb_image.h implementation of the API functions defined in bitmap.h

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/*
[PUBLIC] Initialise anything that may need doing prior to loading images.
*/

bmp_bool_t bmpInit(void)
{
  stbi_set_flip_vertically_on_load(BMP_TRUE);
  return BMP_TRUE;
}

/*
[PUBLIC] Clean up anything that may have been previously set up by `bmpInit()`.
*/

void bmpQuit(void) { }

/*
[PUBLIC] Load an image file into memory.
*/

bmp_image_s *bmpLoadFile(const char *file_name)
{
  bmp_image_s *image = NULL;
  stbi_uc *data;
  int x, y, channels;

  if (!stbi_info(file_name, &x, &y, &channels)) { return NULL; }

  data = stbi_load(file_name, &x, &y, &channels, channels);
  if (data) { image = bmpCreate(x, y, channels * 8); }
  if (image) { memcpy(image->data, data, x * y * channels); }
  stbi_image_free(data);

  return image;
}
