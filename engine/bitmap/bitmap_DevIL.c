/**
@file
@brief A DevIL implementation of the API-specific functions defined in bitmap.h

Copyright (C) 2013-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <IL/il.h>
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/* Global objects and variables. */
bmp_bool_t _bmp_is_init = BMP_FALSE;  /**< @brief Are we initialised yet? */

/*
[PUBLIC] Initialise anything that may need doing prior to loading images.
*/

bmp_bool_t bmpInit(void)
{
  bmpQuit();

  ilInit();

  fprintf(stderr, "[INIT] DevIL\n");
  fprintf(stderr, "\tVersion: %s\n", ilGetString(IL_VERSION_NUM));

  ilEnable(IL_ORIGIN_SET);
  ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

  ilEnable(IL_TYPE_SET);
  ilTypeFunc(IL_UNSIGNED_BYTE);

  _bmp_is_init = BMP_TRUE;

  return BMP_TRUE;
}

/*
[PUBLIC] Clean up anything that may have been previously set up by `bmpInit()`.
*/

void bmpQuit(void)
{
  if (_bmp_is_init) { ilShutDown(); }
  _bmp_is_init = BMP_FALSE;
}

/*
[PUBLIC] Load an image file into memory.
*/

bmp_image_s *bmpLoadFile(const char *file_name)
{
  bmp_image_s *image = NULL;
  bmp_bpp_t bpp = 0;

  ILuint il_data = 0;
  ILenum format;

  if (!_bmp_is_init) { return NULL; }

  /* Attempt to load the image file as a DevIL image. */
  ilGenImages(1, &il_data);
  ilBindImage(il_data);

  if (!ilLoadImage(file_name))
  {
    ilDeleteImages(1, &il_data);
    return NULL;
  }

  /* NOTE: DevIL lacks the luminance-with-alpha type that OpenGL has... */
  bpp = ilGetInteger(IL_IMAGE_BITS_PER_PIXEL);
  switch (bpp)
  {
    case 32: format = IL_RGBA;            break;        /* RGB with alpha.  */
    case 24: format = IL_RGB;             break;        /* RGB only.        */
    case 16: format = IL_RGBA; bpp = 32;  break;        /* Grey with alpha. */
    case 8:  format = IL_LUMINANCE;       break;        /* Greyscale only.  */
    default: ilDeleteImages(1, &il_data); return NULL;  /* Unknown format.  */
  }

  /* Create a non-API-specific version of the image data. */
  image = bmpCreate(ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), bpp);
  if (image) { ilCopyPixels(0, 0, 0, image->w, image->h, 1, format, IL_UNSIGNED_BYTE, image->data); }

  /* Free the DevIL version of the image from memory and return the result. */
  ilDeleteImages(1, &il_data);
  return image;
}
