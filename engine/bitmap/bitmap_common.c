/**
@file
@brief Implements the non-API-specific parts of bitmap.h

Copyright (C) 2013-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/*
[PUBLIC] Create a new (blank) image structure.
*/

bmp_image_s *bmpCreate(bmp_size_t w, bmp_size_t h, bmp_bpp_t bpp)
{
  bmp_image_s *image = NULL;

  if (w < 1 || h < 1) { return NULL; }
  if (bpp != 8 && bpp != 16 && bpp != 24 && bpp != 32) { return NULL; }

  image = (bmp_image_s*)malloc(sizeof(bmp_image_s));
  if (!image) { return NULL; }
  memset(image, 0, sizeof(bmp_image_s));

  image->w = w;
  image->h = h;
  image->bpp = bpp;

  image->data = (bmp_byte_t*)malloc(sizeof(bmp_byte_t) * w * h * bpp / 8);

  if (!image->data)
  {
    free(image);
    return NULL;
  }

  return image;
}

/*
[PUBLIC] Free a previously-created image structure from memory.
*/

void bmpFree(bmp_image_s *image)
{
  if (!image) { return; }
  if (image->data) { free(image->data); }
  free(image);
}
