/**
@file
@brief Handles high-level engine functionality, such a start-up and shut-down.

This file implements the function defined in cqSystem.h using SDL2
*/

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqActor.h"
#include "cqAudio.h"
#include "cqClass.h"
#include "cqConfig.h"
#include "cqGraphics.h"
#include "cqInput.h"
#include "cqPhysics.h"
#include "cqScript.h"
#include "cqSprite.h"
#include "cqSystem.h"
#include "cqWorld.h"

/* Constants: */
#define SYS_JOY_DEADZONE 0.25f  /**< @brief Absolute joypad axis "deadzone". */

/* Global objects and variables: */

SDL_Window *sys_window = NULL;    /**< @brief The SDL2 window handle. */
SDL_GLContext sys_context = NULL; /**< @brief The OpenGL rendering context. */

camera_s sys_camera;  /**< @brief The current positioning of the 3D camera. */

config_s sys_config;  /**< @brief The currently-loaded game configuration. */

input_set_s *input_config = NULL; /**< @brief User-defined input mappings. */

int sys_mouse_i = 0;  /**< @brief The current mouse wheel X axis value. */
int sys_mouse_j = 0;  /**< @brief The current mouse wheel Y axis value. */

int num_joypads = 0;  /**< @brief The number of currently-active joypads. */
SDL_Joystick **sys_joypad_set = NULL; /**< @brief A list of active joypads. */
SDL_Haptic   **sys_haptic_set = NULL; /**< @brief Joypad "vibration" stuff. */

SDL_bool sys_end_game = SDL_FALSE;  /**< @brief End-game-ASAP flag. */

/**
@brief An abstracted wrapper for `SDL_GL_SwapWindow()`, used by the renderer.
*/

static void sysSwapBuffers(void) { SDL_GL_SwapWindow(sys_window); }

/**
@brief A wrapper function to handle command-line parameters for media files.

Additional classes, images, and sounds can be specified at the command line, so
this function handles these "additional" files in addition to the "basic" ones.

@param argc Length of the argv array.
@param argv Array of command-line program arguments.
@param base The "default" file, used even when no additional ones are required.
@param param The command-line text used to specify additional files: `--sounds`
@param lang The language code to use when loading things.
@param func The function used to actually load these files.
*/

static void sysLoadMedia(int argc, char **argv, const char *base, const char *param, const char *lang, int (*func)(const char*, const char*))
{
  const char *file_name;
  int i;

  fprintf(stderr, "[INFO] Loading %s:\n", param);
  for (i = 0; i < argc; ++i)
  {
    if      (i == 0)                      { file_name = base;      }
    else if (strcmp(argv[i], param) == 0) { file_name = argv[++i]; }
    else                                  { continue;              }

    fprintf(stderr, "\tLoading %s...\n", file_name);
    if (func(file_name, lang)) { fprintf(stderr, "\t\tFailed!\n"); }
  }
}

/**
@brief A simple wrapper for `classLoad()` that takes an additional parameter.

This is necessary because `classLoad()` doesn't have a `language` parameter.

@param file_name The name of the file to be loaded, as per `classLoad()`.
@param language Not used.
@return See `classLoad()`.
*/

static int sysLoadClass(const char *file_name, const char *language)
{
  if (language) { /* Silence "unused parameter" warning. */ }
  return classLoad(file_name);
}

/*
[PUBLIC] Initialise the engine and subsystems.
*/

v2d_bool_t sysInit(int argc, char **argv)
{
  const char *start_map = "maps/start.xml";

  const char *error;
  render_init_s render_init;
  Uint32 video_flags;
  int i;

  /* Load config file. */
  cfgInit(&sys_config);
  if (cfgLoad(&sys_config, "config.xml") != 0)
  {
    fprintf(stderr, "[WARNING] Could not load config file.\n");
  }

  /* Initialise SDL. */
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    fprintf(stderr, "[ERROR] Could not initialise SDL.\n");
    return V2D_FALSE;
  }

  /* Handle general command-line parameters: */
  for (i = 1; i < argc; ++i)
  {
    if (strcmp(argv[i], "--stderr") == 0)
    {
      if (!freopen(argv[++i], "w", stderr))
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Redirect failed", "Could not redirect stderr", NULL);
    }
    else if (strcmp(argv[i], "--map") == 0) { start_map = argv[++i]; }
  }

  /* Debug info: */
  {
    SDL_version ver;
    SDL_GetVersion(&ver);
    fprintf(stderr, "[INIT] SDL %d.%d.%d\n", ver.major, ver.minor, ver.patch);
    SDL_VERSION(&ver);
    fprintf(stderr, "\tOriginally compiled with: v%d.%d.%d\n", ver.major, ver.minor, ver.patch);
  }

  /* Create a window. */

  SDL_GL_SetAttribute (SDL_GL_RED_SIZE,   8);
  SDL_GL_SetAttribute (SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute (SDL_GL_BLUE_SIZE,  8);
  SDL_GL_SetAttribute (SDL_GL_ALPHA_SIZE, 8);

  SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE, 24);
/*  SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8); */

  SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, SDL_TRUE);
/*  SDL_GL_SetAttribute (SDL_GL_SWAP_CONTROL, SDL_TRUE); */

  video_flags = SDL_WINDOW_OPENGL;
  if (sys_config.flags & CFG_FLAG_FULLSCREEN) { video_flags |= SDL_WINDOW_FULLSCREEN; }

  sys_window = SDL_CreateWindow("Conqore 2",
      SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
      sys_config.video_w, sys_config.video_h, video_flags);

  if (!sys_window)
  {
    fprintf(stderr, "[ERROR] Could not create window.\n");
    return V2D_FALSE;
  }

  sys_context = SDL_GL_CreateContext(sys_window);
  if (!sys_context)
  {
    fprintf(stderr, "[ERROR] Could not create window.\n");
    return V2D_FALSE;
  }

  /* Initialise the graphics engine: */

  fprintf(stderr, "[DEBUG] Renderer: %s\n", gfxRendererName());

  memset(&render_init, 0, sizeof(render_init_s));
  render_init.w = sys_config.video_w;
  render_init.h = sys_config.video_h;
  render_init.swap_func = sysSwapBuffers;
  render_init.ragdoll_func = physActorIsRagdoll;
  render_init.position_func = physActorGetPosition;
  render_init.quaternion_func = physActorGetQuaternion;
  render_init.mesh_func = actorUpdateMesh;

  error = gfxInit(argc, argv, &render_init);
  if (error)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Graphics Error", error, NULL);
    return V2D_FALSE;
  }

  /* Load sprite images. */
  sysLoadMedia(argc, argv, "base/imageset.xml", "--images", sys_config.language, spriteCacheImageSet);

  /* Initialise joypads. */

  num_joypads = SDL_NumJoysticks();

  if (num_joypads > 0)
  {

    sys_joypad_set = (SDL_Joystick**)malloc(sizeof(SDL_Joystick*) * num_joypads);

    if (sys_joypad_set)
    {

      sys_haptic_set = (SDL_Haptic**)malloc(sizeof(SDL_Haptic*) * num_joypads);

      for (i = 0; i < num_joypads; i++)
      {

        sys_joypad_set[i] = SDL_JoystickOpen(i);

        if (sys_haptic_set)
        {
          sys_haptic_set[i] = SDL_HapticOpenFromJoystick(sys_joypad_set[i]);
          SDL_HapticRumbleInit(sys_haptic_set[i]);
        }

      }

    } else { num_joypads = 0; }

  }

  /* Load input config. */
  input_config = inputLoad("input.xml");
  if (!input_config) { fprintf(stderr, "[WARNING] Couldn't load input config.\n"); }

  /* Initialise audio subsystem. If this fails, continue anyway. */
  if (sys_config.flags & CFG_FLAG_USEAUDIO)
  {
    if (sndInit(sys_config.audio_channels) == 0)
      sysLoadMedia(argc, argv, "base/soundset.xml", "--sounds", sys_config.language, sndCacheEffectSet);
    else
      fprintf(stderr, "[WARNING] Couldn't initialise audio subsystem.\n");
  }

  /* Initialise the physics engine. */
  if (physInit() != 0)
  {
    fprintf(stderr, "[ERROR] Could not initialise physics subsystem.\n");
    return V2D_FALSE;
  }

  /* Initialise the sprite manager. */
  if (spriteInit() != 0)
  {
    fprintf(stderr, "[ERROR] Couldn't initialise sprite manager subsystem.\n");
    return V2D_FALSE;
  }

  /* Initialise scripting. This must be done before classes are loaded. */
  if (scriptInit(argc, argv) != 0)
  {
    fprintf(stderr, "[ERROR] Could not initialise actor script subsystem.\n");
    return V2D_FALSE;
  }

  /* Load actor classes and initialise their scripts. */
  sysLoadMedia(argc, argv, "base/classset.xml", "--actors", NULL, sysLoadClass);

  /* Initialise camera. */
  memset(&sys_camera, 0, sizeof(camera_s));
  quatIdentity(sys_camera.quat);
  sys_camera.fov = 110;

  /* Load the initial map. */
  worldLoadMap(start_map, V2D_TRUE);

  /* Success. */
  return V2D_TRUE;
}

/*
[PUBLIC] Shut down the engine, and free any currently allocated memory.
*/

void sysQuit(void)
{
  worldDestroy();
  classQuit();
  scriptQuit();
  spriteQuit();
  physQuit();
  sndQuit();

  input_config = inputFree(input_config);

  if (sys_joypad_set)
  {
    int i;

    for (i = 0; i < num_joypads; i++) { SDL_JoystickClose(sys_joypad_set[i]); }

    free(sys_joypad_set);
    sys_joypad_set = NULL;
  }

  if (sys_haptic_set)
  {
    int i;

    for (i = 0; i < num_joypads; i++) { SDL_HapticClose(sys_haptic_set[i]); }

    free(sys_haptic_set);
    sys_haptic_set = NULL;
  }

  num_joypads = 0;

  gfxQuit();

  SDL_GL_DeleteContext(sys_context);
  sys_context = NULL;

  SDL_DestroyWindow(sys_window);
  sys_window = NULL;

  SDL_Quit();

  sys_end_game = SDL_FALSE;
}

/*
[PUBLIC] Instruct `sysMainLoop()` to end at the next possible opportunity.
*/

void sysEndProgram(void) { sys_end_game = SDL_TRUE; }

/**
@brief Process window events.

@return `SDL_FALSE` if the game loop should end, or `SDL_TRUE` to keep going.
*/

static SDL_bool sysPollEvents(void)
{
  SDL_Event msg;
  sys_mouse_i = 0;
  sys_mouse_j = 0;

  while (SDL_PollEvent(&msg))
  {
    if (msg.type == SDL_MOUSEWHEEL)
    {
      sys_mouse_i += msg.wheel.x;
      sys_mouse_j += msg.wheel.y;
    }
    else if (msg.type == SDL_QUIT) { return SDL_FALSE; }
  }

  return SDL_TRUE;
}

/**
@brief Reset the state of a given (named) input to zero.

@param name The name of the input "slot".
@param data The current value of that input "slot".
@param user Not used.
@return The new value of the input "slot" - which will always be zero here.
*/

static void *sysResetInput(const char *name, void *data, void *user)
{
  if (name || user) { /* Ignored - only the actual value matters here. */ }
  *((float*)data) = 0;
  return data;
}

/**
@brief Update the current "user input" state.

Note that mouse wheel events are handled by `sysPollEvents()` instead.

@return `SDL_FALSE` if the game loop should end, or `SDL_TRUE` to keep going.
*/

static SDL_bool sysCheckInput(void)
{
  input_map_s *item = NULL;

  float value = 0;
  int i;

  const Uint8 *key = NULL;
  Uint8 hat_flags = 0;
  Uint8 mouse_flags = 0;
  int x, y, numkeys;

  if (!input_config) { return SDL_TRUE; }

  key = SDL_GetKeyboardState(&numkeys);
  mouse_flags = SDL_GetMouseState(&x, &y);

  if (sys_config.flags & CFG_FLAG_LOCKMOUSE)
  {
    SDL_WarpMouseInWindow(sys_window, sys_config.video_w / 2, sys_config.video_h / 2);
    x -= sys_config.video_w / 2; y -= sys_config.video_h / 2;
  }
  else { x = 0; y = 0; }

  SDL_ShowCursor(sys_config.flags & CFG_FLAG_LOCKMOUSE ? SDL_FALSE : SDL_TRUE);

  treeEachItem(input_config->slot, sysResetInput, NULL);

  for (i = 0; i < input_config->num_items; i++)
  {

    item = input_config->item[i];

    value = 0;

    switch (item->device_type)
    {

      case INPUT_DEVICE_KEYBOARD:
        if (!key) { break; }
        if (item->device_id < 0 || item->device_id >= 1) { break; }
        if (item->input_type != INPUT_TYPE_BUTTON) { break; }
        if (item->input_id < 0 || item->input_id >= numkeys) { break; }
        value = key[item->input_id];
      break;

      case INPUT_DEVICE_MOUSE:

        if (item->device_id < 0 || item->device_id >= 1) { break; }

        if (item->input_type == INPUT_TYPE_AXIS)
        {
          if      (item->input_id == 0) { value = x * sys_config.mouse_sensitivity; }
          else if (item->input_id == 1) { value = y * sys_config.mouse_sensitivity; }
          else if (item->input_id == 2) { value = sys_mouse_i;  /* Wheel X. */ }
          else if (item->input_id == 3) { value = sys_mouse_j;  /* Wheel Y. */ }
        }

        /* NOTE: SDL_BUTTON starts from 1, not zero! */

        if (item->input_type == INPUT_TYPE_BUTTON)
        { value = (mouse_flags & SDL_BUTTON(item->input_id + 1)) != 0; }

      break;

      case INPUT_DEVICE_JOYPAD:

        if (item->device_id < 0 || item->device_id >= num_joypads) { break; }

        switch (item->input_type)
        {

          case INPUT_TYPE_HAT:
            hat_flags = SDL_JoystickGetHat(sys_joypad_set[item->device_id], item->input_id);
            value =
              (hat_flags & SDL_HAT_UP    && item->flags & INPUT_FLAG_HATUP  ) ||
              (hat_flags & SDL_HAT_DOWN  && item->flags & INPUT_FLAG_HATDOWN) ||
              (hat_flags & SDL_HAT_LEFT  && item->flags & INPUT_FLAG_HATLEFT) ||
              (hat_flags & SDL_HAT_RIGHT && item->flags & INPUT_FLAG_HATRIGHT);
          break;

          case INPUT_TYPE_AXIS:
            value = SDL_JoystickGetAxis(sys_joypad_set[item->device_id], item->input_id) / 32768.0f;
            if (value > 0 - SYS_JOY_DEADZONE && value < 0 + SYS_JOY_DEADZONE) { value = 0; }
          break;

          case INPUT_TYPE_BUTTON:
            value = SDL_JoystickGetButton(sys_joypad_set[item->device_id], item->input_id);
          break;

          case INPUT_TYPE_UNKNOWN: default: break;

        }

      break;

      case INPUT_DEVICE_UNKNOWN: default: break;

    }

    if (item->flags & INPUT_FLAG_NEGATIVE) { value = 0 - value; }
    if (value > 0) { *item->slot += value; }

  }

  /* Keep going (do not end the program). */
  return SDL_TRUE;
}

/*
[PUBLIC] This function handles the main progam loop.
*/

void sysMainLoop(void)
{
  Uint32 timer = 0;

  while (!sys_end_game)
  {
    v2d_bool_t updated = V2D_FALSE;

    /* Logic update loop. This may run zero or more times per game loop. */
    while (timer < SDL_GetTicks())
    {
      v2d_bool_t map_has_changed;

      if (!sysPollEvents()) { return; }
      if (!sysCheckInput()) { return; }
      map_has_changed = worldUpdate();
      sndUpdate();

      if (map_has_changed) { SDL_SetWindowTitle(sys_window, "Conqore 2"); }

      timer = (map_has_changed ? SDL_GetTicks() : timer) + CQ2_TICK_DELAY;
      updated = V2D_TRUE;
    }

    /* Determine whether to update the display or not. */
    if (updated)
      gfxDrawGame(&sys_camera, worldGetActor(), spriteGetData(), worldGetLamp());
    else if (!sysPollEvents())
      return;

    /* Thread sleep. */
    SDL_Delay(1);
  }
}

/*
[PUBLIC] Get the current state of a (named) input.
*/

v2d_real_t sysGetInput(const char *name)
{
  float *data = NULL;
  if (input_config) { data = (float*)treeSearch(input_config->slot, name); }
  return data ? data[0] : 0;
}

/*
[PUBLIC] Ask a joypad to vibrate for a given length of time.
*/

v2d_bool_t sysPadRumble(int joypad_id, float strength, v2d_ui32_t length)
{
  if (joypad_id < 0 || joypad_id >= num_joypads || !sys_haptic_set) { return V2D_FALSE; }
  return SDL_HapticRumblePlay(sys_haptic_set[joypad_id], strength, length) == 0;
}

/*
[PUBLIC] Update the position and angle of the 3D camera.
*/

void sysSetCamera(float x, float y, float z, float qw, float qx, float qy, float qz, float fov)
{
  sys_camera.xyz[0] = x;
  sys_camera.xyz[1] = y;
  sys_camera.xyz[2] = z;
  sys_camera.quat[QUAT_W] = qw;
  sys_camera.quat[QUAT_X] = qx;
  sys_camera.quat[QUAT_Y] = qy;
  sys_camera.quat[QUAT_Z] = qz;
  sys_camera.fov = fov;
}

/*
[PUBLIC] Get the currently-active "language" from the configuration settings.
*/

v2d_bool_t sysGetLocale(char *out, long max_size)
{
  if (!out || max_size < 2) { return V2D_FALSE; }
  memset(out, 0, max_size);
  strncpy(out, sys_config.language, max_size-1);
  return V2D_TRUE;
}
