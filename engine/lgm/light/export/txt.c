/**
@file
@brief Handles lights stored in a custom, plain-text format.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Export a set of lights to a .TXT file.
*/

int lightSaveTXT(light_set_s *light_set, const char *file_name)
{
  FILE *out = NULL;
  long i;

  if (!light_set || !file_name)  { return -1; }
  out = fopen(file_name, "wb");
  if (!out) { return -1; }

  for (i = 0; i < light_set->num_items; ++i)
  {
    const light_s *light = light_set->item[i];

    if (light->flags & LIGHT_FLAG_SPOT)
      fprintf(out, "spot %f %f %f %f %f %f %f %f %f %f %f\n",
        light->origin[0], light->origin[1], light->origin[2],
        light->target[0], light->target[1], light->target[2],
        light->radius, light->angle,
        light->rgb[0], light->rgb[1], light->rgb[2]
      );

    else if (light->flags & LIGHT_FLAG_SUN)
      fprintf(out, "sun %f %f %f %f %f %f\n",
        light->target[0], light->target[1], light->target[2],
        light->rgb[0], light->rgb[1], light->rgb[2]
      );

    else
      fprintf(out, "point %f %f %f %f %f %f %f\n",
        light->origin[0], light->origin[1], light->origin[2],
        light->radius,
        light->rgb[0], light->rgb[1], light->rgb[2]
      );

  }

  return fclose(out);
}
