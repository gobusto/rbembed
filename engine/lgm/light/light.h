/**
@file
@brief Provides utilities for loading and saving 3D "lights" files.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_LIGHT_H__
#define __LGM_LIGHT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "../shared/typedefs.h"

/* Flags. If a light is not a spotlight or a sun, it's a basic point light. */

#define LIGHT_FLAG_NOTRANSITION 0x01  /**< @brief Don't "smooth" RGB changes. */
#define LIGHT_FLAG_SPOT         0x02  /**< @brief This light is a spot-light. */
#define LIGHT_FLAG_SUN          0x04  /**< @brief This light is a "sun lamp". */
#define LIGHT_FLAG_RESERVED3    0x08  /**< @brief Not used. */
#define LIGHT_FLAG_RESERVED4    0x10  /**< @brief Not used. */
#define LIGHT_FLAG_RESERVED5    0x20  /**< @brief Not used. */
#define LIGHT_FLAG_RESERVED6    0x40  /**< @brief Not used. */
#define LIGHT_FLAG_RESERVED7    0x80  /**< @brief Not used. */

typedef unsigned char light_flags_t;  /**< @brief Light flags datatype. */

/**
@brief Enumerates possible light export formats.
*/

typedef enum
{

  LIGHT_FORMAT_TXT /**< @brief Conqore .TXT */

} light_format_t;

/**
@brief This structure describes a single "light" object.

Lights may optionally be associated with joint IDs, so that skeletally-animated
meshes can have light sources attached to them. For example, a person holding a
torch could define a "BeamSource" joint, allowing a light source to move as the
hand holding the torch changes position.
*/

typedef struct
{

  light_flags_t flags;  /**< @brief Various flags to modify light behavior. */
  float angle;      /**< @brief Spotlights: Half-angle of cone, in degrees. */
  float radius;     /**< @brief Light fade-off range. Does not affect suns. */

  long joint_id;    /**< @brief Optional joint ID - negative ID = no joint. */
  float origin[3];  /**< @brief The basic XYZ position of the light source. */
  float target[3];  /**< @brief The directional target for spotlights/suns. */

  float fps;            /**< @brief The RGB cycle speed in frames-per-second. */
  lgm_count_t num_rgb;  /**< @brief The number of elements in the RGB array.  */
  float *rgb;           /**< @brief The Red/Green/Blue values for this light. */

} light_s;

/**
@brief This structure stores a set of lights.

This is used to load a file containing multiple lights into a single structure.
*/

typedef struct
{

  lgm_count_t num_items; /**< @brief Number of items in the set. */
  light_s **item; /**< @brief Array of individual lights. */

} light_set_s;

/* ========================================================================= */
/* IMPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Load a light set from a text string.

This function only attempts to handle plain-text formats.

@param text The text buffer to parse.
@return A valid light set on success, or NULL on failure.
*/

light_set_s *lightLoadText(const char *text);

/**
@brief Load a light set from a binary data buffer.

This function does not attempt to handle plain-text formats.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid light set on success, or NULL on failure.
*/

light_set_s *lightLoadData(size_t length, const unsigned char *data);

/**
@brief Load a light set from a file.

@param file_name The file name to load.
@return A valid light set on success, or NULL on failure.
*/

light_set_s *lightLoadFile(const char *file_name);

/* ========================================================================= */
/* EXPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Export a set of lights to a file.

@param light_set The light set to be exported.
@param file_name The name (and path) of the output file.
@param format The file format to export as.
@return Zero on success, or non-zero on failure.
*/

int lightSaveFile(light_set_s *light_set, const char *file_name, light_format_t format);

/* ========================================================================= */
/* OTHER FUNCTIONS                                                           */
/* ========================================================================= */

/**
@brief Create a new light set structure.

Note that this doesn't create any actual lights - it just allocates an array to
store them in.

@param num_items The number of light structures to allocate memory for.
@return A new light set structure on success, or NULL on failure.
*/

light_set_s *lightSetCreate(lgm_count_t num_items);

/**
@brief Delete a previously-created light set structure.

This also deletes any lights within the set.

@param light_set The light set structure to be deleted.
@return Always returns NULL.
*/

light_set_s *lightSetDelete(light_set_s *light_set);

/**
@brief Create a new light structure.

@param x The X position of the light.
@param y The Y position of the light.
@param z The Z position of the light.
@param radius The effective distance of the light.
@param num_rgb The number of RGB "animation" values to allocate memory for.
@return A new light structure on success, or NULL on failure.
*/

light_s *lightCreate(float x, float y, float z, float radius, lgm_count_t num_rgb);

/**
@brief Delete a previously-created light structure.

@param light The light structure to be deleted.
@return Always returns NULL.
*/

light_s *lightDelete(light_s *light);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_LIGHT_H__ */
