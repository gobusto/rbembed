/**
@file
@brief Handles lights stored in a custom, plain-text format.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/**
@brief Internal structure used to store the parser state.
*/

typedef struct
{

  long        count;      /**< @brief Line counter.   */
  light_set_s *light_set; /**< @brief Light set data. */

} txt_parse_s;

/**
@brief Per-line callback, used to actually parse the data.

@param line The current line of text.
@param data A pointer to the light set.
@return Zero on success, or non-zero on failure.
*/

static int lightLoadTXT_line(const char *line, void *data)
{
  txt_parse_s *state = (txt_parse_s*)data;
  float x, y, z, i, j, k, radius, angle, r, g, b;
  light_flags_t flags;

  if (sscanf(line, "spot %f %f %f %f %f %f %f %f %f %f %f", &x, &y, &z, &i, &j, &k, &radius, &angle, &r, &g, &b) == 11)
  { flags = LIGHT_FLAG_SPOT; }
  else if (sscanf(line, "sun %f %f %f %f %f %f", &i, &j, &k, &r, &g, &b) == 6)
  { flags = LIGHT_FLAG_SUN; x = 0; y = 0; z = 0; radius = 0; angle = 0; }
  else if (sscanf(line, "point %f %f %f %f %f %f %f", &x, &y, &z, &radius, &r, &g, &b) == 7)
  { flags = 0; i = 0; j = 0; k = 0; angle = 0; }
  else
  {
    fprintf(stderr, "[ERROR] Unknown TXT light format: %s\n", line);
    return -2;
  }

  /* We only actually load the data on the second pass... */
  if (state->light_set)
  {
    state->light_set->item[state->count] = lightCreate(x, y, z, radius, 1);
    if (!state->light_set->item[state->count]) { return -1; }
    state->light_set->item[state->count]->flags = flags;
    /* Animated lights aren't supported yet, so only one RGB value is used: */
    state->light_set->item[state->count]->rgb[0] = r;
    state->light_set->item[state->count]->rgb[1] = g;
    state->light_set->item[state->count]->rgb[2] = b;
    /* Suns/Spotlights need to specify a direction: */
    state->light_set->item[state->count]->target[0] = i;
    state->light_set->item[state->count]->target[1] = j;
    state->light_set->item[state->count]->target[2] = k;
    /* Spotlights need to specify a beam-width angle: */
    state->light_set->item[state->count]->angle = angle;
  }

  /* ...the first pass just figures out how many lights are needed: */
  ++state->count;

  return 0;
}

/*
[PUBLIC] Load a set of lights from a .TXT file.
*/

light_set_s *lightLoadTXT(const char *text)
{
  txt_parse_s state;

  /* Count the number of lights: */
  memset(&state, 0, sizeof(txt_parse_s));
  if (txtEachLine(text, "#", lightLoadTXT_line, &state) != 0) { return NULL; }

  /* Actually load the light data: */
  state.light_set = lightSetCreate(state.count);
  state.count = 0;
  if (state.light_set && txtEachLine(text, "#", lightLoadTXT_line, &state) != 0)
    return lightSetDelete(state.light_set);
  return state.light_set;
}
