/**
@file
@brief Functions and structures used when importing various things.

Copyright (C) 2012-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_IMPORT_H__
#define __LGM_IMPORT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "typedefs.h"

/**
@brief Read a little-endian UINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetUI16_LE(x) ((unsigned short)((x) ? \
  ((unsigned short)(x)[0] << 0) + \
  ((unsigned short)(x)[1] << 8) : 0))

/**
@brief Read a big-endian UINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetUI16_BE(x) ((unsigned short)((x) ? \
  ((unsigned short)(x)[1] << 0) + \
  ((unsigned short)(x)[0] << 8) : 0))

/**
@brief Read a little-endian SINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetSI16_LE(x) ((signed short)((x) ? \
  ((signed short)(x)[0] << 0) + \
  ((signed short)(x)[1] << 8) : 0))

/**
@brief Read a big-endian SINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetSI16_BE(x) ((signed short)((x) ? \
  ((signed short)(x)[1] << 0) + \
  ((signed short)(x)[0] << 8) : 0))

/**
@brief Read a little-endian UINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetUI32_LE(x) ((unsigned long)((x) ? \
  ((unsigned long)(x)[0] << 0 ) + \
  ((unsigned long)(x)[1] << 8 ) + \
  ((unsigned long)(x)[2] << 16) + \
  ((unsigned long)(x)[3] << 24) : 0))

/**
@brief Read a big-endian UINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetUI32_BE(x) ((unsigned long)((x) ? \
  ((unsigned long)(x)[3] << 0 ) + \
  ((unsigned long)(x)[2] << 8 ) + \
  ((unsigned long)(x)[1] << 16) + \
  ((unsigned long)(x)[0] << 24) : 0))

/**
@brief Read a little-endian SINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetSI32_LE(x) ((signed long)((x) ? \
  ((signed long)(x)[0] << 0 ) + \
  ((signed long)(x)[1] << 8 ) + \
  ((signed long)(x)[2] << 16) + \
  ((signed char)(x)[3] << 24) : 0))

/**
@brief Read a big-endian SINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define binGetSI32_BE(x) ((signed long)((x) ? \
  ((signed long)(x)[3] << 0 ) + \
  ((signed long)(x)[2] << 8 ) + \
  ((signed long)(x)[1] << 16) + \
  ((signed char)(x)[0] << 24) : 0))

/**
@brief Break a string of text into individual lines and handle each in turn.

This function changes tabs into spaces and removes indentation.

@param text The multi-line text string to be processed.
@param comment Comment string, such as "#" or "//". Should be NULL if not used.
@param user_func A user-defined callback function for each line.
@param user_data Optional user-supplied data, passed to the callback function.
@return Zero on success, or non-zero on error.
*/

int txtEachLine(
  const char *text,
  const char *comment,
  int (*user_func)(const char*, void*),
  void *user_data
);

/**
@brief Load the contents of a file into a malloc()'d buffer.

The allocated buffer is actually one byte longer than the "length" value, as an
extra NULL-terminator byte is added on to the end of the data. This is so that
a plain-text file can be safely treated as a C-style string.

@param file_name The name (and path) of the file to be loaded into memory.
@param length Optional "buffer length" output value pointer.
@return A malloc()'d buffer on success, or NULL on failure.
*/

unsigned char *txtLoadFile(const char *file_name, long *length);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_IMPORT_H__ */
