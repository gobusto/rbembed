/**
@file
@brief Handles Wavefront .MTL files.

<http://paulbourke.net/dataformats/mtl/>

<https://en.wikipedia.org/wiki/Material_Template_Library>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/**
@brief Material file "parse-state" structure.

This is used to maintain the parser state between each call to mtrlReadLine(),
as using static variables would cause problems for multi-threaded applications.
*/

typedef struct
{

  long          count;  /**< @brief Stores the index of the current material. */
  mtrl_group_s *group;  /**< @brief Material group; possibly a NULL pointer.  */

} mtrl_parse_s;

/**
@brief Per-line callback function for MTL files.

@todo Don't assume that the text is followed by a space character - it could be
a tab or something instead. Also, it could possibly be followed by several more
whitespace characters, so don't include those when copying the filename string.

@todo Support Ka/Kd/Ks/Ns here. Note that Ka/Kd/Ks may have one or three values
depending on whether or not separate RGB values are needed. There are also more
variations, such as `Ka xyz x y z` or `Ka spectral file.rfl factor`.

@param line The current line of text.
@param data The parse state object being used.
@return Zero on success, or non-zero on error.
*/

static int mtrlReadLine(const char *line, void *data)
{
  mtrl_parse_s *state = (mtrl_parse_s*)data;
  mtrl_s *mtrl = NULL;

  if (!state) { return -1; }

  if (strncmp(line, "newmtl ", 7) == 0) { ++state->count; }

  /* No materials list is allocated on the first pass, so stop here: */
  if (state->group)
    mtrl = state->group->item[state->count];
  else
    return 0;

  if (strncmp(line, "newmtl ", 7) == 0)
    strncpy(mtrl->name, &line[7], MTRL_MAXSTRLEN);
  else if (strncmp(line, "d ",  2) == 0)
    mtrl->alpha = 0 + atof(&line[2]);
  else if (strncmp(line, "Tr ", 3) == 0)
    mtrl->alpha = 1 - atof(&line[3]);
  else if (strncmp(line, "map_Kd ", 7) == 0)
    strncpy(mtrl->diffusemap, &line[7], MTRL_MAXSTRLEN);
  else if (strncmp(line, "map_d ", 6) == 0)
    strncpy(mtrl->alphamap, &line[6], MTRL_MAXSTRLEN);
  else if (strncmp(line, "bump ", 5) == 0)
    strncpy(mtrl->bumpmap, &line[5], MTRL_MAXSTRLEN);
  else if (strncmp(line, "map_bump ", 9) == 0)
    strncpy(mtrl->bumpmap, &line[9], MTRL_MAXSTRLEN);
  else if (strncmp(line, "custom_properties ", 18) == 0)
    strncpy(mtrl->custom_properties, &line[18], MTRL_MAXSTRLEN);

  return 0;
}

/*
[PUBLIC] Load a material group from a Wavefrton .mtl file.
*/

mtrl_group_s *mtrlLoadMTL(const char *text)
{
  mtrl_parse_s state;

  /* First, determine how many materials to allocate memory for. */
  memset(&state, 0, sizeof(mtrl_parse_s));
  txtEachLine(text, "#", mtrlReadLine, &state);

  /* Next, create the materials list and populate it. */
  state.group = mtrlCreateGroup(state.count);
  state.count = -1;
  txtEachLine(text, "#", mtrlReadLine, &state);

  return state.group;
}
