/**
@file
@brief Handles Quake III .SKIN files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a material group from a Quake III .skin file.
*/

mtrl_group_s *mtrlLoadSKIN(const char *text)
{
  mtrl_group_s *group = NULL;
  int pass = 0;

  if (!text) { return NULL; }

  /* Pass 1 counts the number of materials. Pass 2 copies them into a group. */

  for (pass = 0; pass < 2; ++pass)
  {
    long num_lines, col;
    size_t s, i;

    for (num_lines = 0, col = 0, s = 0, i = 0; ; ++i)
    {
      if (text[i] == ',')
      {
        /* Blank names (or commas in the filename column) aren't allowed. */
        if (s == i || col != 0) { return NULL; }

        /* Get the material name. */
        if (group)
        {
          size_t maxlen = (i-s < MTRL_MAXSTRLEN) ? i-s : MTRL_MAXSTRLEN;
          strncpy(group->item[num_lines]->name, &text[s], maxlen);
        }

        s = i+1; ++col; /* Move on to the texture filename column. */
      }
      else if (text[i] == '\n' || text[i] == '\r' || text[i] == 0)
      {
        if (col == 0)
        {
          /* The first column should end with a comma, not an end-of-line... */
          if (s < i) { return NULL; }
          /* ...though it's possible that a file might end with a newline... */
          if (text[i] == 0) { break; }
          /* ...and blank lines (or \r\n sequences on Windows) are okay too. */
          ++s; continue;
        }
        else if (s == i) { return NULL; /* Blank filenames aren't allowed. */ }

        /* Get the texture filename. */
        if (group)
        {
          size_t maxlen = (i-s < MTRL_MAXSTRLEN) ? i-s : MTRL_MAXSTRLEN;
          strncpy(group->item[num_lines]->diffusemap, &text[s], maxlen);
        }

        s = i+1; --col; /* Move back to the material name column. */

        /* Update the material count; stop looping if this is the last line. */
        ++num_lines;
        if (text[i] == 0) { break; }
      }
      else if (!isprint(text[i])) { return NULL; }
    }

    /* At the end of the first pass, allocate a material group structure. */
    if (pass == 0)
    {
      group = mtrlCreateGroup(num_lines);
      if (!group) { return NULL; }
    }
  }

  /* Return the result. */
  return group;
}
