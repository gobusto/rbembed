/**
@file
@brief Functions used internally to load materials.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_IMPORT_MTRL_H__
#define __LGM_IMPORT_MTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../mtrl.h"
#include "../../shared/import.h"

/**
@brief Load a material group from a Wavefrton .mtl file.

@param text The text buffer to parse.
@return A valid material group on success, or NULL on failure.
*/

mtrl_group_s *mtrlLoadMTL(const char *text);

/**
@brief Load a material group from a Quake III .skin file.

@param text The text buffer to parse.
@return A valid material group on success, or NULL on failure.
*/

mtrl_group_s *mtrlLoadSKIN(const char *text);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_IMPORT_MTRL_H__ */
