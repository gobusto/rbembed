/**
@file
@brief Provides utilities for loading and saving material description files.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_MTRL_H__
#define __LGM_MTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../shared/typedefs.h"

/* Define constant values. */

#define MTRL_MAXSTRLEN 128  /**< @brief Maximum static string length. */

/**
@brief Enumerates possible material export formats.
*/

typedef enum
{

  MTRL_FORMAT_MTL, /**< @brief Wavefront .MTL  */
  MTRL_FORMAT_SKIN /**< @brief Quake III .SKIN */

} mtrl_format_t;

/**
@brief Defines a single material within a group of materials.

@todo Allocate string lengths dynamically?
*/

typedef struct
{

  char name[MTRL_MAXSTRLEN];        /**< @brief Material name. */

  /* General material colour values: */
  float ambient[3];   /**< @brief Equivalent to OpenGL `GL_AMBIENT`.   */
  float diffuse[3];   /**< @brief Equivalent to OpenGL `GL_DIFFUSE`.   */
  float specular[3];  /**< @brief Equivalent to OpenGL `GL_SPECULAR`.  */
  float emission[3];  /**< @brief Equivalent to OpenGL `GL_EMISSION`.  */
  float shininess;    /**< @brief Equivalent to OpenGL `GL_SHININESS`. */
  float alpha;        /**< @brief Opacity value; 1.0 is fully opaque.  */

  /* Texture file names: */
  char diffusemap[MTRL_MAXSTRLEN];  /**< @brief Diffuse texture filename.  */
  char bumpmap[MTRL_MAXSTRLEN];     /**< @brief Bump-map texture filename. */
  char alphamap[MTRL_MAXSTRLEN];    /**< @brief Transparency texture file. */

  /* Extra bits and pieces: */
  char custom_properties[MTRL_MAXSTRLEN]; /**< @brief Custom data filename. */

} mtrl_s;

/**
@brief Stores a group of materials.

This is necessary because most mesh formats allow multiple materials to be used
for a single model file.
*/

typedef struct
{

  lgm_count_t num_items;  /**< @brief Number of items in this group. */
  mtrl_s **item;          /**< @brief Array of material structures. */

} mtrl_group_s;

/* ========================================================================= */
/* IMPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Load a material group from a text string.

@param text The text buffer to parse.
@return A valid material group on success, or NULL on failure.
*/

mtrl_group_s *mtrlLoadText(const char *text);

/**
@brief Load a material group from a file.

This is just a convenient wrapper for mtrlLoadText().

@param file_name The file name to load.
@return A valid material group on success, or NULL on failure.
*/

mtrl_group_s *mtrlLoadFile(const char *file_name);

/* ========================================================================= */
/* EXPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Export a material group as a file.

@param group The material group to be exported.
@param file_name The file name to write to.
@param format The file format to export as.
@return Zero on success or non-zero on failure.
*/

int mtrlSaveFile(const mtrl_group_s *group, const char *file_name, mtrl_format_t format);

/* ========================================================================= */
/* OTHER FUNCTIONS                                                           */
/* ========================================================================= */

/**
@brief Create a new material group.

@param num_items The number of materials within the group.
@return A new material group structure on success, or NULL on failure.
*/

mtrl_group_s *mtrlCreateGroup(lgm_count_t num_items);

/**
@brief Free a previously created material group.

@param group The material group to be freed.
@return Always returns NULL.
*/

mtrl_group_s *mtrlFreeGroup(mtrl_group_s *group);

/**
@brief Search for a material by name.

If multiple materials have the same name, the first one found is returned.

@param group The material group to search.
@param name The material name to search for.
@return A valid index if the material is found, otherwise returns -1.
*/

lgm_index_t mtrlFind(const mtrl_group_s *group, const char *name);

/**
@brief Replace any DOS/Windows-style backslash path separators.

Windows allows either forward- or back-slash characters to be used in paths,
but UNIX-like systems expect forward-slashes to be used in all cases.

@param group The list of materials to be amended.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t mtrlUnixPaths(mtrl_group_s *group);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_MTRL_H__ */
