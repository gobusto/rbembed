/**
@file
@brief Handles Quake III .SKIN files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "export.h"

/*
[PUBLIC] Save a material group to a Quake III .skin file.
*/

int mtrlSaveSKIN(const mtrl_group_s *group, const char *file_name)
{
  FILE *out = NULL;
  long i;

  if (!group || !file_name) { return -1; }
  out = fopen(file_name, "wb");
  if (!out) { return 1; }

  for (i = 0; i < group->num_items; ++i)
  {
    fprintf(out, "%s,%s%s",
        group->item[i]->name[0]       ? group->item[i]->name       : "null",
        group->item[i]->diffusemap[0] ? group->item[i]->diffusemap : "null",
        i + 1 < group->num_items      ? "\n"                       : "");
  }

  fclose(out);
  return 0;
}
