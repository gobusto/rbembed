/**
@file
@brief Handles Wavefront .MTL files.

<http://paulbourke.net/dataformats/mtl/>

<https://en.wikipedia.org/wiki/Material_Template_Library>

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Save a material group to a Wavefront .MTL file.
*/

int mtrlSaveMTL(const mtrl_group_s *group, const char *file_name)
{
  FILE *out = NULL;
  long i;

  if (!group || !file_name) { return -1; }
  out = fopen (file_name, "wb");
  if (!out) { return 1; }

  for (i = 0; i < group->num_items; ++i)
  {
    mtrl_s *m = group->item[i];

    fprintf(out, "newmtl %s\n", m->name[0] ? m->name : "null");

    fprintf(out, "\tKa %f %f %f\n", m->ambient[0], m->ambient[1], m->ambient[2]);
    fprintf(out, "\tKd %f %f %f\n", m->diffuse[0], m->diffuse[1], m->diffuse[2]);
    fprintf(out, "\tKs %f %f %f\n", m->specular[0], m->specular[1], m->specular[2]);
    /* Maybe TODO: Can .MTL files support `m->emission[3]` somehow...? */
    fprintf(out, "\tNs %f\n", m->shininess);
    fprintf(out, "\td %f\n", m->alpha);

    if (m->diffusemap[0]) fprintf(out, "\tmap_Kd %s\n", m->diffusemap);
    if (m->alphamap[0])   fprintf(out, "\tmap_d %s\n",  m->alphamap);
    if (m->bumpmap[0])    fprintf(out, "\tbump %s\n",   m->bumpmap);

    /* NOTE: This is not an "official" MTL property... */
    if (m->custom_properties[0]) fprintf(out, "\tcustom_properties %s\n", m->custom_properties);
  }

  fclose(out);
  return 0;
}
