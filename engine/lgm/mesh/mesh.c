/**
@file
@brief Various "support" functions, not directly related to import/export.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mesh.h"

/*
[PUBLIC] Allocate a new, blank mesh-mesh structure.
*/

mesh_s *meshCreate(lgm_count_t frm, lgm_count_t vrt, lgm_count_t nrm, lgm_count_t tex, lgm_count_t grp)
{
  mesh_s *mesh = NULL;
  lgm_index_t i;

  if (frm < 1 || vrt < 3 || nrm < 0 || tex < 0 || grp < 1) { return NULL; }

  mesh = (mesh_s*)malloc(sizeof(mesh_s));
  if (!mesh) { return NULL; }
  memset(mesh, 0, sizeof(mesh_s));

  mesh->num_frames = frm;
  mesh->num_verts  = vrt;
  mesh->num_norms  = nrm;
  mesh->num_coords = tex;
  mesh->num_groups = grp;

  mesh->name = (char**)malloc(sizeof(char*) * frm);
  if (!mesh->name) { return meshDelete(mesh); }
  memset(mesh->name, 0, sizeof(char*) * frm);

  mesh->vertex = (float**)malloc(sizeof(float*) * frm);
  if (!mesh->vertex) { return meshDelete(mesh); }
  memset(mesh->vertex, 0, sizeof(float*) * frm);

  for (i = 0; i < frm; ++i)
  {
    mesh->vertex[i] = (float*)malloc(sizeof(float) * vrt * 3);
    if (!mesh->vertex[i]) { return meshDelete(mesh); }
    memset(mesh->vertex[i], 0, sizeof(float) * vrt * 3);
  }

  if (mesh->num_norms > 0)
  {
    mesh->normal = (float**)malloc(sizeof(float*) * frm);
    if (!mesh->normal) { return meshDelete(mesh); }
    memset(mesh->normal, 0, sizeof(float*) * frm);

    for (i = 0; i < frm; ++i)
    {
      mesh->normal[i] = (float*)malloc(sizeof(float) * nrm * 3);
      if (!mesh->normal[i]) { return meshDelete(mesh); }
      memset(mesh->normal[i], 0, sizeof(float) * nrm * 3);
    }
  }

  if (mesh->num_coords > 0)
  {
    mesh->texcoord = (float*)malloc(sizeof(float) * tex * 2);
    if (!mesh->texcoord) { return meshDelete(mesh); }
    memset(mesh->texcoord, 0, sizeof(float) * tex * 2);
  }

  mesh->group = (mesh_group_s**)malloc(sizeof(mesh_group_s*) * grp);
  if (!mesh->group) { return meshDelete(mesh); }
  memset(mesh->group, 0, sizeof(mesh_group_s*) * grp);

  return mesh;
}

/*
[PUBLIC] Free a previously created mesh from memory.
*/

mesh_s *meshDelete(mesh_s *mesh)
{
  lgm_index_t i;

  if (!mesh) { return NULL; }

  /* Note: This must be done BEFORE freeing any groups/etc: */
  meshDeleteBuffers(mesh);

  lightSetDelete(mesh->light);
  mtrlFreeGroup(mesh->mtrl);

  if (mesh->name)
  {
    for (i = 0; i < mesh->num_frames; ++i) { free(mesh->name[i]); }
    free(mesh->name);
  }

  if (mesh->vertex)
  {
    for (i = 0; i < mesh->num_frames; ++i) { free(mesh->vertex[i]); }
    free(mesh->vertex);
  }

  if (mesh->normal)
  {
    for (i = 0; i < mesh->num_frames; ++i) { free(mesh->normal[i]); }
    free(mesh->normal);
  }

  free(mesh->texcoord);

  if (mesh->group)
  {
    for (i = 0; i < mesh->num_groups; ++i) { meshGroupDelete(mesh->group[i]); }
    free(mesh->group);
  }

  meshTagSetDelete(mesh->tag_list);
  free(mesh->vert_tag);

  free(mesh);
  return NULL;
}

/*
[PUBLIC] Allocate a new, blank triangle group.
*/

mesh_group_s *meshGroupCreate(const char *name, lgm_index_t mat_id, lgm_count_t num_tris)
{
  mesh_group_s *grp = NULL;
  lgm_index_t i;

  if (num_tris < 1) { return NULL; }

  grp = (mesh_group_s*)malloc(sizeof(mesh_group_s));
  if (!grp) { return NULL; }
  memset(grp, 0, sizeof(mesh_group_s));

  grp->mat_id = mat_id;
  grp->num_tris = num_tris;

  if (name)
  {
    grp->name = (char*)malloc(strlen(name) + 1);
    if (!grp->name) { return meshGroupDelete(grp); }
    strcpy(grp->name, name);
  }

  grp->tri = (mesh_tri_s**)malloc(sizeof(mesh_tri_s*) * num_tris);
  if (!grp->tri) { return meshGroupDelete(grp); }
  memset(grp->tri, 0, sizeof(mesh_tri_s*) * num_tris);

  for (i = 0; i < num_tris; ++i)
  {
    grp->tri[i] = (mesh_tri_s*)malloc(sizeof(mesh_tri_s));
    if (!grp->tri[i]) { return meshGroupDelete(grp); }
    memset(grp->tri[i], 0, sizeof(mesh_tri_s));
  }

  return grp;
}

/*
[PUBLIC] Delete a previously-created mesh group.
*/

mesh_group_s *meshGroupDelete(mesh_group_s *group)
{
  if (!group) { return NULL; }

  if (group->tri)
  {
    lgm_index_t t;
    for (t = 0; t < group->num_tris; ++t) { free(group->tri[t]); }
    free(group->tri);
  }

  free(group->name);
  free(group);

  return NULL;
}

/*
[PUBLIC] Find the center-point of the AABB for given mesh animation frame.
*/

lgm_bool_t meshGetCenter(const mesh_s *mesh, float *out)
{
  if (!mesh) return LGM_FALSE;

  if (out)
  {
    out[0] = mesh->xyzmin[0] + ((mesh->xyzmax[0] - mesh->xyzmin[0]) / 2.0);
    out[1] = mesh->xyzmin[1] + ((mesh->xyzmax[1] - mesh->xyzmin[1]) / 2.0);
    out[2] = mesh->xyzmin[2] + ((mesh->xyzmax[2] - mesh->xyzmin[2]) / 2.0);
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Determine the center-point of the vertices associated with a tag.
*/

lgm_bool_t meshGetTagCenter(const mesh_s *mesh, lgm_index_t tag_id, float *out)
{
  if (!mesh || !mesh->tag_list || tag_id < 0) return LGM_FALSE;
  else if (tag_id >= mesh->tag_list->num_tags) return LGM_FALSE;

  if (out)
  {
    const mesh_tag_s *tag = mesh->tag_list->tag[tag_id];
    out[0] = tag->xyzmin[0] + ((tag->xyzmax[0] - tag->xyzmin[0]) / 2.0);
    out[1] = tag->xyzmin[1] + ((tag->xyzmax[1] - tag->xyzmin[1]) / 2.0);
    out[2] = tag->xyzmin[2] + ((tag->xyzmax[2] - tag->xyzmin[2]) / 2.0);
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Invert the normals of a mesh.
*/

lgm_bool_t meshInvertNormals(mesh_s *mesh)
{
  lgm_index_t f, n;

  if (!mesh || !mesh->normal) { return LGM_FALSE; }

  for (f = 0; f < mesh->num_frames; ++f)
  {
    for (n = 0; n < mesh->num_norms * 3; ++n) { mesh->normal[f][n] *= -1; }
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Reverse all faces in a mesh and invert the normal data.
*/

lgm_bool_t meshReverseFaces(mesh_s *mesh)
{
  lgm_index_t g, t, tmp;

  if (!mesh) { return LGM_FALSE; }

  for (g = 0; g < mesh->num_groups; ++g)
  {
    mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    for (t = 0; t < group->num_tris; ++t)
    {
      tmp                    = group->tri[t]->v_id[0];
      group->tri[t]->v_id[0] = group->tri[t]->v_id[2];
      group->tri[t]->v_id[2] = tmp;

      tmp                    = group->tri[t]->n_id[0];
      group->tri[t]->n_id[0] = group->tri[t]->n_id[2];
      group->tri[t]->n_id[2] = tmp;

      tmp                    = group->tri[t]->t_id[0];
      group->tri[t]->t_id[0] = group->tri[t]->t_id[2];
      group->tri[t]->t_id[2] = tmp;
    }
  }

  return meshInvertNormals(mesh);
}

/*
[PUBLIC] Scale a mesh to make it smaller or larger.
*/

lgm_bool_t meshScale(mesh_s *mesh, float x, float y, float z)
{
  lgm_index_t f, i;

  if (!mesh) { return LGM_FALSE; }

  mesh->xyzmin[0] *= x; mesh->xyzmax[0] *= x;
  mesh->xyzmin[1] *= y; mesh->xyzmax[1] *= y;
  mesh->xyzmin[2] *= z; mesh->xyzmax[2] *= z;

  for (f = 0; f < mesh->num_frames; ++f)
  {
    for (i = 0; i < mesh->num_verts; ++i)
    {
      mesh->vertex[f][(i * 3) + 0] *= x;
      mesh->vertex[f][(i * 3) + 1] *= y;
      mesh->vertex[f][(i * 3) + 2] *= z;
    }
  }

  if (mesh->tag_list)
  {
    for (i = 0; i < mesh->tag_list->num_tags; ++i)
    {
      mesh->tag_list->tag[i]->position[0] *= x;
      mesh->tag_list->tag[i]->position[1] *= y;
      mesh->tag_list->tag[i]->position[2] *= z;

      mesh->tag_list->tag[i]->xyzmin[0] *= x;
      mesh->tag_list->tag[i]->xyzmin[1] *= y;
      mesh->tag_list->tag[i]->xyzmin[2] *= z;

      mesh->tag_list->tag[i]->xyzmax[0] *= x;
      mesh->tag_list->tag[i]->xyzmax[1] *= y;
      mesh->tag_list->tag[i]->xyzmax[2] *= z;
    }
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Translate a mesh so that all vertices are offset along the X/Y/Z axes.
*/

lgm_bool_t meshTranslate(mesh_s *mesh, float x, float y, float z)
{
  lgm_index_t f, i;

  if (!mesh) { return LGM_FALSE; }

  mesh->xyzmin[0] += x; mesh->xyzmax[0] += x;
  mesh->xyzmin[1] += y; mesh->xyzmax[1] += y;
  mesh->xyzmin[2] += z; mesh->xyzmax[2] += z;

  for (f = 0; f < mesh->num_frames; ++f)
  {
    for (i = 0; i < mesh->num_verts; ++i)
    {
      mesh->vertex[f][(i * 3) + 0] += x;
      mesh->vertex[f][(i * 3) + 1] += y;
      mesh->vertex[f][(i * 3) + 2] += z;
    }
  }

  if (mesh->tag_list)
  {
    for (i = 0; i < mesh->tag_list->num_tags; ++i)
    {
      mesh->tag_list->tag[i]->position[0] += x;
      mesh->tag_list->tag[i]->position[1] += y;
      mesh->tag_list->tag[i]->position[2] += z;

      mesh->tag_list->tag[i]->xyzmin[0] += x;
      mesh->tag_list->tag[i]->xyzmin[1] += y;
      mesh->tag_list->tag[i]->xyzmin[2] += z;

      mesh->tag_list->tag[i]->xyzmax[0] += x;
      mesh->tag_list->tag[i]->xyzmax[1] += y;
      mesh->tag_list->tag[i]->xyzmax[2] += z;
    }
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Allocate "working" buffers for storing transformed vertices/normals.
*/

lgm_bool_t meshCreateBuffers(mesh_s *mesh)
{
  lgm_index_t g;

  if (!mesh) { return LGM_FALSE; }

  if (!mesh->final_vertex)
  {
    mesh->final_vertex = (float*)malloc(sizeof(float) * 3 * mesh->num_verts);
    if (!mesh->final_vertex) { return LGM_FALSE; }
    memset(mesh->final_vertex, 0, sizeof(float) * 3 * mesh->num_verts);
  }

  for (g = 0; g < mesh->num_groups; ++g)
  {
    mesh_group_s *group = mesh->group[g];
    size_t size_required;

    if (!group || group->num_tris < 1 || group->final_normal) { continue; }

    /* Allocate an XYZ position for each vertex of each triangle. */
    size_required = (sizeof(float) * 3) * (group->num_tris * 3);

    group->final_normal = (float*)malloc(size_required);
    if (!group->final_normal)
    {
      meshDeleteBuffers(mesh);
      return LGM_FALSE;
    }

    memset(group->final_normal, 0, size_required);
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Free any previously-allocated "working" buffers.
*/

lgm_bool_t meshDeleteBuffers(mesh_s *mesh)
{
  lgm_index_t g;

  if (!mesh) { return LGM_FALSE; }

  if (mesh->final_vertex)
  {
    free(mesh->final_vertex);
    mesh->final_vertex = NULL;
  }

  for (g = 0; g < mesh->num_groups; ++g)
  {
    if (mesh->group[g] && mesh->group[g]->final_normal)
    {
      free(mesh->group[g]->final_normal);
      mesh->group[g]->final_normal = NULL;
    }
  }

  return LGM_TRUE;
}

/*
[PUBLIC] Generate a collection of animation sequences from mesh frame names.
*/

wordtree_s *animFromMesh(const mesh_s *mesh, float fps)
{
  wordtree_s *anim_set = NULL;
  lgm_index_t start, f;

  /* Create a new wordtree structure to store the animation sequences. */
  if (!mesh || fps <= 0) { return NULL; }
  anim_set = treeCreate();
  if (!anim_set) { return NULL; }

  /* Loop through each frame of animation in the mesh. */
  for (start = 0, f = 0; f < mesh->num_frames; ++f)
  {
    char *name = NULL;
    size_t tmp = 0;

    /* Determine if a new animation sequence will begin on the NEXT frame. */
    if (f + 1 < mesh->num_frames)
    {
      size_t len[2];
      int suffix[2];
      long i;

      /* If this frame (or the next) has no name, just ignore it and move on. */
      if (!mesh->name[f+0] || !mesh->name[f+1]) { continue; }

      /* Get the length of the name WITHOUT any numeric suffix. */
      for (i = 0; i < 2; ++i)
      {
        const char *txt = mesh->name[f+i]; /* If this is "shoot24"... */
        for (len[i] = strlen(txt); len[i] && isdigit(txt[len[i]-1]); --len[i]){}
        suffix[i] = atoi(&txt[len[i]]); /* ...then this number will be "24". */
      }

      /* Determine whether this is a NEW sequence, or just the next frame... */
      if (len[0] == len[1] && suffix[0] + 1 == suffix[1])
      {
        if (strncmp(mesh->name[f], mesh->name[f+1], len[0]) == 0)
        { continue; /* This is just the next frame in the current sequence. */ }
      }
    }

    /* If this frame is the last one in the current sequence, store it. */
    tmp = mesh->name[start] ? strlen(mesh->name[start]) : 0;
    name = (char*)malloc(tmp + 1);

    if (name)
    {
      /*
      Frame names tend to be in a "run1" or "shoot01" format, so we need to get
      rid of any trailing digits. However, some files contain multiple versions
      of an animation, where "attack101", "attack201", and "attack301" are used
      to indicate the first frame of variation 1, 2, and 3 respectively - thus,
      we only want to strip away trailing '1' or '01' text in order to preserve
      the "attack1", "attack2", or "attack3" prefix text. Quake 2 has some good
      test cases for this - have a look through the various .MD2 files it uses.
      */

      strcpy(name, mesh->name[start] ? mesh->name[start] : "");
      if (name[tmp-1] == '0' || name[tmp-1] == '1')
      {
        if (!isdigit(name[tmp-2])) { name[tmp-1] = 0; /* Just trim the '1'. */ }
        else if (name[tmp-2]=='0') { name[tmp-2] = 0; /* Trim the '01' bit. */ }
      }

      /* Add this animation to the set, and free the temporary name buffer. */
      animCreate(anim_set, name, start, (f-start) + 1, 0, fps);
      free(name);
    }

    start = f + 1;  /* The next sequence will begin on the next frame... */
  }

  return anim_set;
}
