/**
@file
@brief Provides utilities for loading and saving 3D model files.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_MESH_H__
#define __LGM_MESH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "../light/light.h"
#include "../mtrl/mtrl.h"
#include "../anim/anim.h"

/**
@brief Enumerates possible mesh export formats.
*/

typedef enum
{

  MESH_FORMAT_OBJ,  /**< @brief Wavefront .OBJ                    */
  MESH_FORMAT_OFF,  /**< @brief Princeton University .OFF         */
  MESH_FORMAT_PLY,  /**< @brief Stanford University .PLY          */
  MESH_FORMAT_STL   /**< @brief StereoLithographic Triangles .STL */

} mesh_format_t;

/**
@brief Describes a single triangle within a mesh.

Note that the same vertex and normal indices are used for all animation frames
of a morph-animated mesh. The vertices may change positions between frames, but
the indices used to reference them do not.
*/

typedef struct
{

  lgm_index_t v_id[3]; /**< @brief Vertex index for each point. */
  lgm_index_t n_id[3]; /**< @brief Normal index for each point. */
  lgm_index_t t_id[3]; /**< @brief UV-map index for each point. */

} mesh_tri_s;

/**
@brief Describes a single group (or "object", or "surface") within a mesh file.

Each group has its own list of triangles, but the vertex, normal, and tex-coord
arrays are shared by all groups in the model.
*/

typedef struct
{

  char *name;         /**< @brief The (unique) name for this group. */
  lgm_index_t mat_id; /**< @brief Material ID used, or -1 for none. */

  lgm_count_t num_tris; /**< @brief The number of triangles in the group. */
  mesh_tri_s **tri;     /**< @brief An array of triangle data structures. */

  float *final_normal;  /**< @brief OPTIONAL: final_normal[num_tris * 3 * 3] */

} mesh_group_s;

/**
@brief The main mesh structure.

This is used to contain everything else.
*/

typedef struct
{

  light_set_s *light; /**< @brief A set of light sources used by this mesh. */
  mtrl_group_s *mtrl; /**< @brief A list of materials used by this mesh.    */

  lgm_count_t num_frames; /**< @brief Number of morph-based animation frames. */
  char **name;            /**< @brief Morph-frame name: `name[num_frames]`    */

  lgm_count_t num_verts;  /**< @brief The total number of vertices.     */
  float **vertex;         /**< @brief `vertex[num_frames][num_verts*3]` */

  lgm_count_t num_norms;  /**< @brief The total number of normals.      */
  float **normal;         /**< @brief `normal[num_frames][num_norms*3]` */

  lgm_count_t num_coords; /**< @brief The total number of UV-map coordinates. */
  float *texcoord;        /**< @brief `texcoord[num_coords*2]`                */

  lgm_count_t num_groups; /**< @brief The number of groups within this mesh. */
  mesh_group_s **group;   /**< @brief `group[num_groups]`                    */

  mesh_tag_set_s *tag_list; /**< @brief Default tag positions, if required.   */
  lgm_index_t *vert_tag;    /**< @brief Vertex-Tag IDs: `vert_tag[num_verts]` */

  float *final_vertex;  /**< @brief OPTIONAL: final_vertex[num_verts * 3] */

  vector3_t xyzmin; /**< @brief AABB data; defines the minimum XYZ position. */
  vector3_t xyzmax; /**< @brief AABB data; defines the maximum XYZ position. */

} mesh_s;

/* ========================================================================= */
/* IMPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Load a mesh file from a text string.

This function **only** attempts to handle plain-text formats, like OBJ or PLY.

@param text The text buffer to parse.
@return A valid mesh on success, or `NULL` on failure.
*/

mesh_s *meshLoadText(const char *text);

/**
@brief Load a mesh file from a binary data buffer.

This function **doesn't** attempt to handle plain-text formats like OBJ or PLY.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or `NULL` on failure.
*/

mesh_s *meshLoadData(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a file.

This is just a convenient wrapper for meshLoadData() and meshLoadText().

@param file_name The file name to load.
@return A valid mesh on success, or `NULL` on failure.
*/

mesh_s *meshLoadFile(const char *file_name);

/* ========================================================================= */
/* EXPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Export a single frame of a mesh to a file.

OBJ files will be exported without an associated .MTL file; use `meshSaveOBJ()`
if you also want to output an .MTL file and add a `mtllib` line to the `.OBJ`.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@param format The file format to export as.
@return Zero on success, or non-zero on error.
*/

int meshSaveFile(const mesh_s *mesh, lgm_index_t frame, const char *file_name, mesh_format_t format);

/**
@brief Export a mesh as a Wavefront OBJ-format file.

This can be used to also export the material data in MTL format.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param obj_file The OBJ file name to export to.
@param mtl_file The MTL file name to export to.
@return Zero on success, or non-zero on error.
*/

int meshSaveOBJ(const mesh_s *mesh, lgm_index_t frame, const char *obj_file, const char *mtl_file);

/* ========================================================================= */
/* OTHER FUNCTIONS                                                           */
/* ========================================================================= */

/**
@brief Create a new mesh structure.

@param frm The number of animation frames to allocate memory for.
@param vrt The number of vertices to allocate memory for.
@param nrm The number of normals to allocate memory for.
@param tex The number of texture coordinates to allocate memory for.
@param grp The number of groups to allocate memory for.
@return A new mesh structure on success, or `NULL` on failure.
*/

mesh_s *meshCreate(lgm_count_t frm, lgm_count_t vrt, lgm_count_t nrm, lgm_count_t tex, lgm_count_t grp);

/**
@brief Free a previously created mesh from memory.

@param mesh The mesh to be freed.
@return Always returns NULL.
*/

mesh_s *meshDelete(mesh_s *mesh);

/**
@brief Create a new triangle group.

@param name The name of this triangle group.
@param mat_id The material ID. Can be negative to indicate no material.
@param num_tris The number of triangles to allocate memory for.
@return A new mesh group structure on success, or `NULL` on failure.
*/

mesh_group_s *meshGroupCreate(const char *name, lgm_index_t mat_id, lgm_count_t num_tris);

/**
@brief Delete a previously-created mesh group.

@param group The group to be freed from memory.
@return Always returns `NULL`.
*/

mesh_group_s *meshGroupDelete(mesh_group_s *group);

/**
@brief Determine the center-point of the AABB for given mesh.

The result is based on the first "morph" frame in the mesh.

@param mesh The mesh to find the center point of.
@param out An output buffer to output the X, Y, and Z coordinates to.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshGetCenter(const mesh_s *mesh, float *out);

/**
@brief Determine the center-point of the vertices associated with a tag.

@param mesh The mesh to find the center point of.
@param tag_id The number of the tag to find the center point of.
@param out An output buffer to output the X, Y, and Z coordinates to.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshGetTagCenter(const mesh_s *mesh, lgm_index_t tag_id, float *out);

/**
@brief Invert the normals of a mesh.

@param mesh The mesh to be modified.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshInvertNormals(mesh_s *mesh);

/**
@brief Reverse all faces in a mesh and invert the normal data.

@param mesh The mesh to reverse the faces of.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshReverseFaces(mesh_s *mesh);

/**
@brief Scale a mesh to make it smaller or larger.

@param mesh The mesh to be resized.
@param x The X-scaling factor.
@param y The Y-scaling factor.
@param z The Z-scaling factor.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshScale(mesh_s *mesh, float x, float y, float z);

/**
@brief Translate a mesh so that all vertices are offset along the X/Y/Z axes.

@param mesh The mesh to be repositioned.
@param x The X-translation offset.
@param y The Y-translation offset.
@param z The Z-translation offset.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshTranslate(mesh_s *mesh, float x, float y, float z);

/**
@brief Allocate "working" buffers for storing transformed vertices/normals.

These are useful for storing "intermediate" positions for animated meshes.

@param mesh The mesh to allocate buffers for.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshCreateBuffers(mesh_s *mesh);

/**
@brief Free any previously-allocated "working" buffers.

@param mesh The mesh whose buffers are to be freed.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshDeleteBuffers(mesh_s *mesh);

/**
@brief Generate a collection of animation sequences based on mesh frame names.

This works by grouping consecutive frames with names like "idle01" and "idle02"
together into a single sequence, so it works well for Quake or Quake II models,
where this kind of frame-naming convention is used.

@param mesh The mesh file to inspect.
@param fps The frames-per-second rate to use.
@return A collection of animation sequences on success, or NULL on failure.
*/

wordtree_s *animFromMesh(const mesh_s *mesh, float fps);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_MESH_H__ */
