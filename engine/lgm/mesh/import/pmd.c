/**
@file
@brief Handles import of MikuMikuDance .PMD files.

<http://mikumikudance.wikia.com/wiki/MMD:Polygon_Model_Data>

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Constants. */

#define LGM_PMD_HEAD_SIZE 7   /**< @brief _writeme_ */
#define LGM_PMD_INFO_SIZE 276 /**< @brief _writeme_ */
#define LGM_PMD_VERT_SIZE 38  /**< @brief _writeme_ */
#define LGM_PMD_POLY_SIZE 6   /**< @brief _writeme_ */
#define LGM_PMD_MTRL_SIZE 70  /**< @brief _writeme_ */

/*
[PUBLIC] Load a mesh from a MikuMikuDance PMD data buffer.
*/

mesh_s *meshLoadPMD(size_t length, const unsigned char *data)
{
  mesh_s *mesh = NULL;
  float version;
  lgm_count_t num_verts, num_polys, num_mtrls, i, mtrl_poly_start;
  size_t offs_verts, offs_polys, offs_mtrls, offs_bones;

  offs_verts = LGM_PMD_HEAD_SIZE + LGM_PMD_INFO_SIZE;

  if (!data || length < offs_verts + 4) { return NULL; }
  memcpy(&version, &data[3], sizeof(float));
  if (memcmp(data, "Pmd", 3) || version < 1 || version > 1) { return NULL; }

  /* Get counts + check that the offsets make sense: */

  num_verts = binGetUI32_LE(&data[offs_verts]);
  offs_polys = offs_verts + 4 + (num_verts * LGM_PMD_VERT_SIZE);
  if (length < offs_polys + 4) { return NULL; }

  num_polys = binGetUI32_LE(&data[offs_polys]);
  if (num_polys % 3) { return NULL; } else { num_polys /= 3; }
  offs_mtrls = offs_polys + 4 + (num_polys * LGM_PMD_POLY_SIZE);
  if (length < offs_mtrls + 4) { return NULL; }

  num_mtrls = binGetUI32_LE(&data[offs_mtrls]);
  offs_bones = offs_mtrls + 4 + (num_mtrls * LGM_PMD_MTRL_SIZE);
  if (length < offs_bones + 2) { return NULL; }

  /* Start actually loading stuff: */

  mesh = meshCreate(1, num_verts, num_verts, num_verts, num_mtrls);
  if (!mesh) { return NULL; }
  mesh->mtrl = mtrlCreateGroup(num_mtrls);

  for (i = 0; i < num_verts; ++i)
  {
    const size_t offs = offs_verts + 4 + (i * LGM_PMD_VERT_SIZE);
    memcpy(&mesh->vertex[0][i * 3], &data[offs + 0 ], 4 * 3);
    memcpy(&mesh->normal[0][i * 3], &data[offs + 12], 4 * 3);
    memcpy(&mesh->texcoord[ i * 2], &data[offs + 24], 4 * 2);
    mesh->texcoord[(i * 2) + 1] *= -1;
  }

  for (mtrl_poly_start = 0, i = 0; i < num_mtrls; ++i)
  {
    const size_t offs = offs_mtrls + 4 + (i * LGM_PMD_MTRL_SIZE);
    lgm_count_t mtrl_poly_count, t;
    char name[20];

    /* Material 1 affects the first N faces, Material 2 affects the next N... */
    mtrl_poly_count = binGetUI32_LE(&data[offs + 46]);
    if (mtrl_poly_count % 3) { return meshDelete(mesh); }
    mtrl_poly_count /= 3;
    if (mtrl_poly_start + mtrl_poly_count > num_polys) { return meshDelete(mesh); }

    sprintf(name, "material%ld", i);
    mesh->group[i] = meshGroupCreate(name, i, mtrl_poly_count);
    if (!mesh->group[i]) { return meshDelete(mesh); }

    for (t = 0; t < mtrl_poly_count; ++t)
    {
      const size_t p = offs_polys + 4 + ((mtrl_poly_start + t) * LGM_PMD_POLY_SIZE);
      int v;
      for (v = 0; v < 3; ++v)
      {
        mesh->group[i]->tri[t]->v_id[v] = binGetUI16_LE(&data[p + (v * 2)]);
        mesh->group[i]->tri[t]->n_id[v] = mesh->group[i]->tri[t]->v_id[v];
        mesh->group[i]->tri[t]->t_id[v] = mesh->group[i]->tri[t]->v_id[v];
      }
    }

    /* Assuming we didn't run into a malloc failure, load the material data: */
    if (mesh->mtrl)
    {
      mtrl_s *mtrl = mesh->mtrl->item[i];
      size_t c;

      strcpy(mtrl->name, name);

      memcpy(mtrl->diffuse,    &data[offs + 0 ], sizeof(float) * 3);
      memcpy(&mtrl->alpha,     &data[offs + 12], sizeof(float) * 1);
      memcpy(&mtrl->shininess, &data[offs + 16], sizeof(float) * 1);
      memcpy(mtrl->specular,   &data[offs + 20], sizeof(float) * 3);
      memcpy(mtrl->ambient,    &data[offs + 32], sizeof(float) * 3);
      /* NOTE: Text is usually encoded as SHIFT-JIS, not UTF-8 or Latin-1... */
      memcpy(mtrl->diffusemap, &data[offs + 50], 20);

      /* If a sphere-map file exists, it's included here after an asterisk: */
      for (c = 0; c < 20; ++c)
      {
        /* TODO: In the future, maybe actually use it (as a separate field)? */
        if (mtrl->diffusemap[c] == '*') { mtrl->diffusemap[c] = 0; }
      }
    }

    mtrl_poly_start += mtrl_poly_count;
  }

  meshReverseFaces(mesh);
  meshInvertNormals(mesh);
  return mesh;
}
