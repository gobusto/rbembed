/**
@file
@brief Handles 3D Studio .3DS files.

<http://paulbourke.net/dataformats/3ds/>

<http://web.archive.org/web/20130508102329/http://www.jrepp.com/docs/3ds-0.1.htm>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* 3DS constants. */

#define ID_3DS_MAIN3DS       0x4D4D /**< @brief 3DS: Main chunk ID.           */
#define ID_3DS_EDIT3DS       0x3D3D /**< @brief 3DS: Mesh chunk ID.           */
#define ID_3DS_KEYF3DS       0xB000 /**< @brief 3DS: Animation chunk ID.      */

#define ID_3DS_EDIT_MATERIAL 0xAFFF /**< @brief 3DS: Material list chunk ID.  */
#define ID_3DS_EDIT_OBJECT   0x4000 /**< @brief 3DS: Object data chunk ID.    */

#define ID_3DS_MTRL_NAME     0xA000 /**< @brief 3DS: Material name chunk.     */
#define ID_3DS_MTRL_DIFFUSE  0xA200 /**< @brief 3DS: Material diffuse chunk.  */
#define ID_3DS_MTRL_FILENAME 0xA300 /**< @brief 3DS: Material filename chunk. */

#define ID_3DS_OBJECT_MESH   0x4100 /**< @brief 3DS: Trimesh data chunk ID.   */
#define ID_3DS_OBJECT_LIGHT  0x4600 /**< @brief 3DS: Light data chunk ID.     */

#define ID_3DS_MESH_VERTICES 0x4110 /**< @brief 3DS: Trimesh vertices chunk.  */
#define ID_3DS_MESH_TRIANGLE 0x4120 /**< @brief 3DS: Trimesh triangles chunk. */

#define ID_3DS_MESH_TEXCOORD 0x4140 /**< @brief 3DS: Trimesh texcoords chunk. */

#define ID_3DS_MESH_MATRIX   0x4160 /**< @brief 3DS: Trimesh matrix chunk.    */

#define ID_3DS_LIGHT_OFF     0x4620 /**< @brief 3DS: ?????                    */
#define ID_3DS_LIGHT_SPOT    0x4610 /**< @brief 3DS: ?????                    */
#define ID_3DS_LIGHT_UNKNOWN 0x465A /**< @brief 3DS: ?????                    */

/**
@brief [INTERNAL] The structure used for parsing text-based formats.

This is used to keep track of the current "parser state" through multiple calls
to per-line callback functions which would otherwise have to rely on statically
allocated variables. Since it is possible for a multi-threaded program to parse
multiple files at the same time, static variables are not an ideal solution.
*/

typedef struct
{

  unsigned long flags;  /**< @brief 3DS chunk ID. */

  mesh_s *mesh; /**< @brief The mesh being created/constructed. */

  long num_groups;  /**< @brief Number of groups [in total/read so far].    */
  long num_coords;  /**< @brief Number of texcoords [in total/read so far]. */
  long num_verts;   /**< @brief Number of vertices [in total/read so far].  */
  long num_tris;    /**< @brief Number of triangles [in total/read so far]. */
  long num_mtrls;   /**< @brief Number of materials [in total/read so far]. */

  long mat_id;      /**< @brief The current material index.      */
  long offs_vertex; /**< @brief The current vertex index offset. */

} parse_3ds_s;

/**
@brief [INTERNAL] Handle a 3DS chunk.

Work-in-progress.

@param state writeme.
@param length The total size of the data[] array.
@param data An array of raw byte values.
@return Zero on success, or non-zero on failure.
*/

static int meshLoad3DS_chunk(parse_3ds_s *state, unsigned long length, const unsigned char *data)
{

  unsigned long chunk_size, offset;
  int number_of_passes, pass;

  unsigned short num_items;

  if (!state || length < 6 || !data) { return 666; }

  /* Some chunks need two passes: One to count things, another to load them. */

  if (state->flags == ID_3DS_EDIT3DS) { number_of_passes = 2; }
  else                                { number_of_passes = 1; }

  for (pass = 0; pass < number_of_passes; pass++)
  {

    /* For the main Edit3DS chunk, reset the various counters on each pass. */

    if (state->flags == ID_3DS_EDIT3DS)
    {
      state->num_verts  = 0;
      state->num_coords = 0;
      state->num_groups = 0;
      state->num_mtrls  = 0;
    }

    /* Object chunks begin with a NULL-terminated name string. */

    if (state->flags == ID_3DS_EDIT_OBJECT)
    {

      for (offset = 0; data[offset]; offset++) { /* ... */ }
      offset++; /* <-- NULL terminator byte. */

    } else { offset = 0; }

    /* Loop through each sub-chunk within the current chunk. */

    for (; offset <= length - 6; offset += chunk_size)
    {

      chunk_size = binGetUI32_LE(&data[offset + 2]);
      if (chunk_size < 6 || chunk_size > length-offset) { return 666; }

      switch (binGetUI16_LE(&data[offset]))
      {

        /* The keyframe chunk contains animation data. */

        case ID_3DS_KEYF3DS:
          if (state->flags != ID_3DS_MAIN3DS) { return 666; }
        break;  /* Animation key-frame data is currently ignored. */

        /* The Edit3DS chunk contains the actual scene data, such as meshes. */

        case ID_3DS_EDIT3DS:
          if (state->flags != ID_3DS_MAIN3DS) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
        return meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]);

        /* The Material chunk contains a set of material definition chunks. */

        case ID_3DS_EDIT_MATERIAL:
          if (state->flags != ID_3DS_EDIT3DS) { return 666; }
          state->flags = ID_3DS_EDIT_MATERIAL;
          if (meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
          state->num_mtrls++;
        break;

        /* The Material name chunk contains just the name, and nothing else. */

        case ID_3DS_MTRL_NAME:
          if (state->flags != ID_3DS_EDIT_MATERIAL) { return 666; }
          if (state->mesh) { strncpy(state->mesh->mtrl->item[state->num_mtrls]->name, (const char*)&data[offset+6], MTRL_MAXSTRLEN); }
        break;

        /* The Diffuse chunk contains various diffuse material parameters. */

        case ID_3DS_MTRL_DIFFUSE:
          if (state->flags != ID_3DS_EDIT_MATERIAL) { return 666; }
          state->flags = ID_3DS_MTRL_DIFFUSE;
          if (meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT_MATERIAL;
        break;

        /* The filename chunk defines which texture file to use. */

        case ID_3DS_MTRL_FILENAME:
          if (state->flags != ID_3DS_MTRL_DIFFUSE) { return 666; }
          if (state->mesh) { strncpy(state->mesh->mtrl->item[state->num_mtrls]->diffusemap, (const char*)&data[offset+6], MTRL_MAXSTRLEN); }
        break;

        /* Object chunks may contain lights, cameras, or meshes. */

        case ID_3DS_EDIT_OBJECT:
          if (state->flags != ID_3DS_EDIT3DS) { return 666; }
          state->flags = ID_3DS_EDIT_OBJECT;
          if (meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
        break;

        /* Trimesh data defines an actual polygonal mesh object. */

        case ID_3DS_OBJECT_MESH:
          state->offs_vertex = state->num_verts;
          state->mat_id = -1;
          if (state->flags != ID_3DS_EDIT_OBJECT) { return 666; }
          state->flags = ID_3DS_OBJECT_MESH;
          if (meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT_OBJECT;
          if (state->mesh && state->num_groups > 0) { state->mesh->group[state->num_groups-1]->mat_id = state->mat_id; }
        break;

        /* Lights aren't loaded yet, but they will be in the future... */

        case ID_3DS_OBJECT_LIGHT:
          fprintf(stderr, "[3DS] FIXME: Ignoring light definition chunk...\n");
          if (state->flags != ID_3DS_EDIT_OBJECT) { return 666; }
          state->flags = ID_3DS_OBJECT_LIGHT;
          if (meshLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT_OBJECT;
          /* state->num_lights++; */
        break;

        /* writeme */

        case ID_3DS_MESH_MATRIX:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          /* if (state->mesh) { fprintf(stderr, "[3DS] FIXME: Matrix ignored!\n"); } */
        break;

        /* writeme */

        case ID_3DS_MESH_VERTICES:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          num_items = binGetUI16_LE(&data[offset+6]);
          if (state->mesh) { memcpy(&state->mesh->vertex[0][state->num_verts*3], &data[offset+8], sizeof(float) * num_items*3); }
          state->num_verts += num_items;
        break;

        /* writeme */

        case ID_3DS_MESH_TEXCOORD:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          num_items = binGetUI16_LE(&data[offset+6]);
          if (state->mesh) { memcpy(&state->mesh->texcoord[state->num_coords*2], &data[offset+8], sizeof(float) * num_items*2); }
          state->num_coords += num_items;
        break;

        /* writeme */

        case ID_3DS_MESH_TRIANGLE:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }

          if (state->mesh)
          {

            long i = 0;

            /* TODO: Use the actual group name here... */
            state->mesh->group[state->num_groups] = meshGroupCreate(NULL, -1, binGetUI16_LE(&data[offset+6]));
            if (!state->mesh->group[state->num_groups]) { return 666; }

            for (i = 0; i < state->mesh->group[state->num_groups]->num_tris; i++)
            {

              state->mesh->group[state->num_groups]->tri[i]->v_id[2] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+0)]);
              state->mesh->group[state->num_groups]->tri[i]->v_id[1] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+2)]);
              state->mesh->group[state->num_groups]->tri[i]->v_id[0] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+4)]);

              state->mesh->group[state->num_groups]->tri[i]->t_id[0] = state->mesh->group[state->num_groups]->tri[i]->v_id[0];
              state->mesh->group[state->num_groups]->tri[i]->t_id[1] = state->mesh->group[state->num_groups]->tri[i]->v_id[1];
              state->mesh->group[state->num_groups]->tri[i]->t_id[2] = state->mesh->group[state->num_groups]->tri[i]->v_id[2];

            }

          }

          state->num_groups++;

        break;

        /* Any other chunk types are ignored. */

        case 0xA010: case 0xA020: case 0xA030: /* Always ignored. */ break;

        default:
          /* if (state->mesh) { fprintf(stderr, "[3DS] Ignoring chunk: %04x\n", binGetUI16_LE(&data[offset])); } */
        break;

      }

    }

    /* If necessary, allocate memory at the end of the first pass. */

    if (state->flags == ID_3DS_EDIT3DS && pass == 0)
    {
      state->mesh = meshCreate(1, state->num_verts, 0, state->num_coords, state->num_groups);
      if (!state->mesh) { return 666; }
      state->mesh->mtrl = mtrlCreateGroup(state->num_mtrls);
      if (!state->mesh->mtrl && state->num_mtrls > 0) { return 666; }
    }

  }

  /* No problems found; report success. */

  return 0;

}

/*
[PUBLIC] Load a mesh from a 3D Studio .3DS data buffer.
*/

mesh_s *meshLoad3DS(size_t length, const unsigned char *data)
{

  parse_3ds_s state;
  unsigned long data_size = 0;

  if      (length <= 6 || !data)                  { return NULL; }
  else if (binGetUI16_LE(data) != ID_3DS_MAIN3DS) { return NULL; }

  data_size = binGetUI32_LE(&data[2]);
  if (data_size > (unsigned long)length) { return NULL; }

  memset(&state, 0, sizeof(parse_3ds_s));
  state.flags = ID_3DS_MAIN3DS;

  if (meshLoad3DS_chunk(&state, data_size-6, &data[6]) != 0)
  { state.mesh = meshDelete(state.mesh); }

  return state.mesh;

}
