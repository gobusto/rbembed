/**
@file
@brief Handles Misfit Model 3D .MM3D files.

<http://www.misfitcode.com/misfitmodel3d/olh_mm3dformat.html>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Constants */

#define MM3D_CHUNK_METADATA           0x1001  /**< @brief about */
#define MM3D_CHUNK_UNKNOWN            0x1002  /**< @brief about */
#define MM3D_CHUNK_GROUPS             0x0101  /**< @brief about */
#define MM3D_CHUNK_EMBEDDEDTEXTURES   0x0141  /**< @brief about */
#define MM3D_CHUNK_EXTERNALTEXTURES   0x0142  /**< @brief about */
#define MM3D_CHUNK_MATERIALS          0x0161  /**< @brief about */
#define MM3D_CHUNK_TEXPROJECTIONTRIS  0x016c  /**< @brief about */
#define MM3D_CHUNK_CANVASIMAGES       0x0191  /**< @brief about */
#define MM3D_CHUNK_SKELETALANIMS      0x0301  /**< @brief about */
#define MM3D_CHUNK_FRAMEANIMS         0x0321  /**< @brief about */
#define MM3D_CHUNK_FRAMEANIMPOINTS    0x0326  /**< @brief about */
#define MM3D_CHUNK_FRAMERELANIMS      0x0341  /**< @brief about */
#define MM3D_CHUNK_EOF                0x3fff  /**< @brief about */

#define MM3D_CHUNK_VERTICES           0x8001  /**< @brief about */
#define MM3D_CHUNK_TRIANGLES          0x8021  /**< @brief about */
#define MM3D_CHUNK_TRINORMALS         0x8026  /**< @brief about */
#define MM3D_CHUNK_JOINTS             0x8041  /**< @brief about */
#define MM3D_CHUNK_JOINTVERTS         0x8046  /**< @brief about */
#define MM3D_CHUNK_POINTS             0x8061  /**< @brief about */
#define MM3D_CHUNK_SMOOTHANGLES       0x8106  /**< @brief about */
#define MM3D_CHUNK_WEIGHTS            0x8146  /**< @brief about */
#define MM3D_CHUNK_TEXPROJECTIONS     0x8168  /**< @brief about */
#define MM3D_CHUNK_TEXCOORDS          0x8121  /**< @brief about */

/*
[PUBLIC] Load a mesh from a Misfit Model 3D .MM3D data buffer.
*/

mesh_s *meshLoadMM3D(size_t length, const unsigned char *data)
{
  mesh_s *mesh = NULL;

  if (length < 12 || !data) { return NULL; }

  /* TODO */

  return mesh;
}
