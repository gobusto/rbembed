/**
@file
@brief Handles Dark Engine .BIN files.

This code is based on my own reverse engineering, so may have a few bugs...

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* LGMD header structure. This comes after a "LGMD" 4cc and UI32 LE version. */

#define LGMD_HEAD_NAME      8   /**< @brief ASCII name string       (8 bytes) */
#define LGMD_HEAD_FLOAT_A   16  /**< @brief Unknown                 (Float32) */
#define LGMD_HEAD_FLOAT_B   20  /**< @brief Unknown                 (Float32) */
#define LGMD_HEAD_AABBXMAX  24  /**< @brief AABB - Maximum X        (Float32) */
#define LGMD_HEAD_AABBYMAX  28  /**< @brief AABB - Maximum Y        (Float32) */
#define LGMD_HEAD_AABBZMAX  32  /**< @brief AABB - Maximum Z        (Float32) */
#define LGMD_HEAD_AABBXMIN  36  /**< @brief AABB - Minimum X        (Float32) */
#define LGMD_HEAD_AABBYMIN  40  /**< @brief AABB - Minimum Y        (Float32) */
#define LGMD_HEAD_AABBZMIN  44  /**< @brief AABB - Minimum Z        (Float32) */
#define LGMD_HEAD_OFFSETX   48  /**< @brief Offset X                (Float32) */
#define LGMD_HEAD_OFFSETY   52  /**< @brief Offset Y                (Float32) */
#define LGMD_HEAD_OFFSETZ   56  /**< @brief Offset Z                (Float32) */

#define LGMD_HEAD_NUMPOLYS  60  /**< @brief Number of polygons      (UI16 LE) */
#define LGMD_HEAD_NUMVERTS  62  /**< @brief Number of vertices      (UI16 LE) */
#define LGMD_HEAD_NUM_XXXA  64  /**< @brief Unknown                 (UI16 LE) */
#define LGMD_HEAD_NUMSKINS  66  /**< @brief Number of textures      (UI8)     */
#define LGMD_HEAD_NUM_XXXB  67  /**< @brief Unknown                 (UI8)     */
#define LGMD_HEAD_NUM_XXXC  68  /**< @brief Unknown                 (UI8)     */
#define LGMD_HEAD_NUM_XXXD  69  /**< @brief Unknown                 (UI8)     */

#define LGMD_HEAD_OFFS_XXXA 70  /**< @brief Unknown                 (UI32 LE) */
#define LGMD_HEAD_OFFSSKINS 74  /**< @brief Offset to textures      (UI32 LE) */
#define LGMD_HEAD_OFFSUVMAP 78  /**< @brief Offset to texcoords     (UI32 LE) */
#define LGMD_HEAD_OFFS_XXXB 82  /**< @brief Unknown                 (UI32 LE) */
#define LGMD_HEAD_OFFSVERTS 86  /**< @brief Offset to vertices      (UI32 LE) */
#define LGMD_HEAD_OFFS_XXXC 90  /**< @brief Unknown                 (UI32 LE) */
#define LGMD_HEAD_OFFSNORMS 94  /**< @brief Offset to normals       (UI32 LE) */
#define LGMD_HEAD_OFFSPOLYS 98  /**< @brief Offset to polygons      (UI32 LE) */
#define LGMD_HEAD_OFFS_XXXD 102 /**< @brief Unknown                 (UI32 LE) */
#define LGMD_HEAD_OFFS_EOF  106 /**< @brief Length of file in bytes (UI32 LE) */

#define LGMD_SIZEOF_HEADER  110 /**< @brief Length of header in versions < 4. */

#define LGMD_HEAD_UNKNOWN_A 110 /**< @brief Unknown; v4 files only. (UI32 LE) */
#define LGMD_HEAD_UNKNOWN_B 114 /**< @brief Unknown; v4 files only. (UI32 LE) */
#define LGMD_HEAD_UNKNOWN_C 118 /**< @brief Unknown; v4 files only. (UI32 LE) */

/* LGMD texture structure. */

#define LGMD_SKIN_FILENAME  0   /**< @brief Texture filename string (16bytes) */
#define LGMD_SKIN_FLAGS     16  /**< @brief Unknown                 (UI8)     */
#define LGMD_SKIN_ID        17  /**< @brief Texture ID              (UI8)     */
#define LGMD_SKIN_UNKNOWN_A 18  /**< @brief Unknown                 (UI32 LE) */
#define LGMD_SKIN_UNKNOWN_B 22  /**< @brief Unknown                 (UI32 LE) */
#define LGMD_SIZEOF_SKIN    26  /**< @brief Size of LGMD skin structure. */

/* LGMD v2 polygon structure. Possibly also applies to version 1 files too. */

#define LGMD_POLYV2_FLAGS     0   /**< @brief Various flags.        (UI8)     */
#define LGMD_POLYV2_SKINID    1   /**< @brief Material index.       (UI8)     */
#define LGMD_POLYV2_RESERVED  2   /**< @brief Always seems to be 0. (UI8)     */
#define LGMD_POLYV2_NUMVERTS  3   /**< @brief Number of vertices.   (UI8)     */
#define LGMD_POLYV2_NORMALID  4   /**< @brief Normal index.         (UI16 LE) */
#define LGMD_POLYV2_UNKNOWN   6   /**< @brief Unknown.              (Float32) */
#define LGMD_POLYV2_DATASTART 10  /**< @brief Relative offset to the data.    */

/* LGMD v3/4 polygon structure. The size varies based on the flags/vertices. */

#define LGMD_POLYV3_ID        0   /**< @brief Polygon ID.           (UI16 LE) */
#define LGMD_POLYV3_SKINID    2   /**< @brief Material index.       (UI8)     */
#define LGMD_POLYV3_RESERVED  3   /**< @brief Always seems to be 0. (UI8)     */
#define LGMD_POLYV3_FLAGS     4   /**< @brief Various flags.        (UI8)     */
#define LGMD_POLYV3_NUMVERTS  5   /**< @brief Number of vertices.   (UI8)     */
#define LGMD_POLYV3_NORMALID  6   /**< @brief Normal index.         (UI16 LE) */
#define LGMD_POLYV3_UNKNOWN   8   /**< @brief Unknown.              (Float32) */
#define LGMD_POLYV3_DATASTART 12  /**< @brief Relative offset to the data.    */

/*
The rest of the polygon data varies based on (A) vertex count and (B) flags:

UI16 vertex_index[NUMVERTS]     Always present?
UI16 unknown_index[NUMVERTS]    Always present?
UI16 texcoord_index[NUMVERTS]   Only present with LGMD_POLYFLAG_HASUVMAP.
UI8  texture_id                 Only present in v4 (or later?) files.

The System Shock 2 "NATE.BIN" and "NATETEST.BIN" files contain examples without
UV maps. Thief II has 01.bin and 02.bin (which appear to have incorrect polygon
counts) and OPBOX.BIN (which is version 2 rather than the usual version 3).
*/

#define LGMD_POLYFLAG_UNKNOWN1 0x01 /**< @brief Always set to 1.            */
#define LGMD_POLYFLAG_HASUVMAP 0x02 /**< @brief Polygon has UV map indices. */
#define LGMD_POLYFLAG_UNKNOWN3 0x04 /**< @brief Always set to 0.            */
#define LGMD_POLYFLAG_UNKNOWN4 0x08 /**< @brief Always set to 1.            */
#define LGMD_POLYFLAG_UNKNOWN5 0x10 /**< @brief Always set to 1.            */
#define LGMD_POLYFLAG_NOSKINID 0x20 /**< @brief Polygon has no material.    */
#define LGMD_POLYFLAG_UNKNOWN7 0x40 /**< @brief Unknown.                    */
#define LGMD_POLYFLAG_UNKNOWN8 0x80 /**< @brief Always set to 0.            */

/**
@brief [INTERNAL] Load a Dark Engine LGMD-format mesh.

Thief + Thief II use versions 2, 3, and 4. System Shock 2 only seems to use v4.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

static mesh_s *meshLoadBIN_LGMD(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  unsigned long skin_offs,  num_skins;
  unsigned long poly_offs,  num_polys;
  unsigned long vert_offs,  num_verts;
  unsigned long coord_offs, num_coords;
  unsigned long num_groups, g;
  unsigned long version, offs, end_of_polys;

  /* Determine whether this is a known LGMD-format mesh file version. */

  if (length < LGMD_SIZEOF_HEADER || !data) { return NULL; }
  if (memcmp(data, "LGMD", 4) != 0) { return NULL; }
  version = binGetUI32_LE(&data[4]);
  if (version < 2 || version > 4) { return NULL; }

  /* Get various offsets/sizes. Note that num_coords is not given explicitly. */

  skin_offs = binGetUI32_LE(&data[LGMD_HEAD_OFFSSKINS]);
  num_skins = data[LGMD_HEAD_NUMSKINS];

  poly_offs = binGetUI32_LE(&data[LGMD_HEAD_OFFSPOLYS]);
  num_polys = binGetUI16_LE(&data[LGMD_HEAD_NUMPOLYS]);

  vert_offs = binGetUI32_LE(&data[LGMD_HEAD_OFFSVERTS]);
  num_verts = binGetUI16_LE(&data[LGMD_HEAD_NUMVERTS]);

  coord_offs = binGetUI32_LE(&data[LGMD_HEAD_OFFSUVMAP]);
  num_coords = (binGetUI32_LE(&data[LGMD_HEAD_OFFS_XXXB]) - coord_offs) / (4*2);

  /* Thief 2: 01.bin and 02.bin lie about the polygon count; they have less. */

  end_of_polys = binGetUI32_LE(&data[LGMD_HEAD_OFFS_XXXD]);

  /*
  System Shock 2: "NATETEST.BIN" contains one polygon, but zero materials. This
  is because the polygon has the NOSKINID flag set, so no materials are needed.
  */

  num_groups = num_skins > 0 ? num_skins : 1;

  /* Allocate a basic mesh structure and load in the vertex/texcoord data. */

  mesh = meshCreate(1, num_verts, 0, num_coords, num_groups);
  if (!mesh) { return NULL; }

  memcpy(mesh->vertex[0], &data[vert_offs], num_verts  * sizeof(float) * 3);
  memcpy(mesh->texcoord, &data[coord_offs], num_coords * sizeof(float) * 2);

  for (g = 0; g < num_coords; ++g) { mesh->texcoord[(g*2)+1] *= -1; }

  /* Load materials. TODO: Do versions < 3 look in "txt" rather than "txt16"? */

  mesh->mtrl = mtrlCreateGroup(num_skins);
  if (mesh->mtrl)
  {
    for (offs = skin_offs, g = 0; g < num_skins; ++g, offs += LGMD_SIZEOF_SKIN)
    {
      mtrl_s *skin = mesh->mtrl->item[g];
      strncpy(skin->name, (const char*)&data[offs + LGMD_SKIN_FILENAME], 16);
      sprintf(skin->diffusemap, "txt16/%s", skin->name);
    }
  }

  /* Load the polygon data. We want to group them together based on texture. */

  for (g = 0; g < num_groups; ++g)
  {

    int pass;

    for (pass = 0; pass < 2; ++pass)
    {

      unsigned long num_tris = 0;
      unsigned long p;

      for (offs = poly_offs, p = 0; p < num_polys; ++p)
      {

        unsigned long sizeof_poly;
        unsigned char skin_id, flags, local_verts, tmp;

        /* Some files lie about the polycount, so terminate early if needed. */

        if (offs >= end_of_polys)
        {
          if (pass == 0 && g == 0) { fprintf(stderr, "[LGMD] Expected %lu polygons, but only found %lu...\n", num_polys, p); }
          break;
        }
        else if (version >= 3)
        {
          flags       = data[offs + LGMD_POLYV3_FLAGS];
          local_verts = data[offs + LGMD_POLYV3_NUMVERTS];
        }
        else
        {
          flags       = data[offs + LGMD_POLYV2_FLAGS];
          local_verts = data[offs + LGMD_POLYV2_NUMVERTS];
        }

        /* Not sure what this byte is for, but it always seems to be zero... */

        if (version >= 3) { tmp = data[offs + LGMD_POLYV3_RESERVED]; }
        else              { tmp = data[offs + LGMD_POLYV2_RESERVED]; }

        if (pass == 0 && g == 0 && tmp != 0)
        { fprintf(stderr, "[LGMD] WARNING: Polygon %lu UINT8=%d\n", p, tmp); }

        /* Warn about unusual flag combinations for debugging purposes. */

        if (pass == 0 && g == 0 && flags != 27 && flags != 57 && flags != 89)
        { fprintf(stderr, "[LGMD] WARNING: Polygon %lu flags=%d\n", p, flags); }

        /*
        NOTE: The following flag bits appear to be set for EVERY polygon:

        LGMD_POLYFLAG_UNKNOWN1  00000001
        LGMD_POLYFLAG_UNKNOWN4  00001000
        LGMD_POLYFLAG_UNKNOWN5  00010000

        I suspect that one of them indicates that the vertex index list exists,
        with another one indicating that the "unknown" index list exists (after
        all, the UV-map list is only present if the right flag is set) but this
        is purely speculation, unless I can find some suitable test cases...
        */

        sizeof_poly = version < 3 ? LGMD_POLYV2_DATASTART:LGMD_POLYV3_DATASTART;
        sizeof_poly += local_verts * 2; /* Assume the "vertex" list exists. */
        if (flags & LGMD_POLYFLAG_HASUVMAP) { sizeof_poly += local_verts * 2; }
        sizeof_poly += local_verts * 2; /* Assume the "unknown" list exists. */
        if (version >= 4) { ++sizeof_poly; }  /* Extra material index value. */

        /*
        Determine which material this polygon uses. I think that the texture ID
        is supposed to be read as a SIGNED byte (so that 255 becomes -1), since
        polygons with the NOSKINID flag always seem to have a skin ID of 255...
        */

        tmp = data[offs+(version < 3 ? LGMD_POLYV2_SKINID:LGMD_POLYV3_SKINID)];

        if (flags & LGMD_POLYFLAG_NOSKINID)
        {
          if (pass == 0 && g == 0 && tmp != 255) { fprintf(stderr, "[LGMD] WARNING: Polygon %lu has no material, but the material index is set to %d rather than -1.\n", p, tmp); }
          skin_id = 0;  /* Just assume it should use the first material. */
        }
        else
        {

          for (skin_id = 0; skin_id < num_skins; ++skin_id)
          {
            unsigned long skin_data = skin_offs + (LGMD_SIZEOF_SKIN * skin_id);
            if (data[skin_data + LGMD_SKIN_ID] == tmp) { break; }
          }

          if (skin_id >= num_skins)
          {
            if (pass == 0 && g == 0) { fprintf(stderr, "[LGMD] WARNING: Polygon %lu uses an invalid material (%d/%lu).\n", p, tmp, num_skins); }
            skin_id = 0;  /* Just assume it should use the first texture. */
          }

        }

        /* Does this polygon belong to the current texture group? */

        if (skin_id == g)
        {

          mesh_group_s *group = mesh->group[g];

          if (group)
          {

            int i;

            for (i = 0; i < local_verts; ++i)
            {

              unsigned short vid = 0;
              unsigned short tid = 0;

              if (version >= 3) { vid = binGetUI16_LE(&data[offs + LGMD_POLYV3_DATASTART + (0 * local_verts*2) + (i*2)]); }
              else              { vid = binGetUI16_LE(&data[offs + LGMD_POLYV2_DATASTART + (0 * local_verts*2) + (i*2)]); }

              if (flags & LGMD_POLYFLAG_HASUVMAP)
              {
                if (version >= 3) { tid = binGetUI16_LE(&data[offs + LGMD_POLYV3_DATASTART + (2 * local_verts*2) + (i*2)]); }
                else              { tid = binGetUI16_LE(&data[offs + LGMD_POLYV2_DATASTART + (2 * local_verts*2) + (i*2)]); }
              }

              if (i >= 3)
              {

                group->tri[num_tris+i-2]->v_id[0] = group->tri[num_tris    ]->v_id[0];
                group->tri[num_tris+i-2]->v_id[1] = group->tri[num_tris+i-3]->v_id[2];
                group->tri[num_tris+i-2]->v_id[2] = vid;

                group->tri[num_tris+i-2]->t_id[0] = group->tri[num_tris    ]->t_id[0];
                group->tri[num_tris+i-2]->t_id[1] = group->tri[num_tris+i-3]->t_id[2];
                group->tri[num_tris+i-2]->t_id[2] = tid;

              }
              else
              {
                group->tri[num_tris]->v_id[i] = vid;
                group->tri[num_tris]->t_id[i] = tid;
              }

            }

          }

          num_tris += local_verts-2;

        }

        /* Move on to the next polygon. */

        offs += sizeof_poly;

      }

      /* At the end of the FIRST pass, create a group to store the triangles. */

      if (pass == 0 && num_tris > 0)
      {

        char name[MTRL_MAXSTRLEN];
        sprintf(name, "group%lu", g);

        mesh->group[g] = meshGroupCreate(name, g, num_tris);
        if (!mesh->group[g]) { return meshDelete(mesh); }

      }

    }

  }

  return mesh;

}

/* LGMM header structure. This comes after a "LGMM" 4cc and UI32 LE version. */

#define LGMM_HEAD_FLOAT_A   8   /**< @brief Always zero? Origin X?  (Float32) */
#define LGMM_HEAD_FLOAT_B   12  /**< @brief Always zero? Origin Y?  (Float32) */
#define LGMM_HEAD_FLOAT_C   16  /**< @brief Always zero? Origin Z?  (Float32) */

#define LGMM_HEAD_ZEROUI8   20  /**< @brief Always zero.            (UI8)     */
#define LGMM_HEAD_NUMLISTB  21  /**< @brief Item count for LISTB.   (UI8)     */
#define LGMM_HEAD_NUMSKINS  22  /**< @brief Number of materials.    (UI8)     */
#define LGMM_HEAD_NUMLISTA  23  /**< @brief Item count for LISTA.   (UI8)     */
#define LGMM_HEAD_NUMPOLYS  24  /**< @brief Number of triangles.    (UI16 LE) */
#define LGMM_HEAD_NUMVERTS  26  /**< @brief Number of vertices.     (UI16 LE) */
#define LGMM_HEAD_NUMLISTC  28  /**< @brief Item count for LISTC.   (UI16 LE) */
#define LGMM_HEAD_ZEROUI16  30  /**< @brief Always zero.            (UI16 LE) */

#define LGMM_HEAD_OFFSLISTA1 32 /**< @brief Offset to LISTA1.       (UI32 LE) */
#define LGMM_HEAD_OFFSLISTB  36 /**< @brief Offset to LISTB.        (UI32 LE) */
#define LGMM_HEAD_OFFSSKINS  40 /**< @brief Offset to materials.    (UI32 LE) */
#define LGMM_HEAD_OFFSLISTA2 44 /**< @brief Offset to LISTA2.       (UI32 LE) */
#define LGMM_HEAD_OFFSPOLYS  48 /**< @brief Offset to triangles.    (UI32 LE) */
#define LGMM_HEAD_OFFSNORMS  52 /**< @brief Offset to normals.      (UI32 LE) */
#define LGMM_HEAD_OFFSVERTS  56 /**< @brief Offset to vertices.     (UI32 LE) */
#define LGMM_HEAD_OFFSUVMAP  60 /**< @brief Offset to texcoords.    (UI32 LE) */
#define LGMM_HEAD_OFFSLISTC  64 /**< @brief Offset to LISTC.        (UI32 LE) */
#define LGMM_HEAD_ZEROUI32   68 /**< @brief Always zero.            (UI32 LE) */

#define LGMM_SIZEOF_HEADER  72  /**< @brief Length of LGMM header. */

/* LGMM v1 material structure. */

#define LGMM_SKINV1_FILENAME  0   /**< @brief Texture file name.    (24bytes) */
#define LGMM_SKINV1_FLAGS     24  /**< @brief Material flags.       (UI8)     */
#define LGMM_SKINV1_LISTA     25  /**< @brief LISTA item count.     (UI8)     */
#define LGMM_SKINV1_ID        26  /**< @brief Unique ID.            (UI16 LE) */
#define LGMM_SKINV1_NUMPOLYS  28  /**< @brief Number of triangles.  (UI16 LE) */
#define LGMM_SKINV1_POLYSKIP  30  /**< @brief Triangles to skip.    (UI16 LE) */
#define LGMM_SKINV1_NUMVERTS  32  /**< @brief Number of vertices.   (UI16 LE) */
#define LGMM_SKINV1_VERTSKIP  34  /**< @brief Vertices to skip.     (UI16 LE) */
#define LGMM_SKINV1_ZEROUI32  36  /**< @brief Always zero.          (UI32 LE) */
#define LGMM_SIZEOF_SKINV1    40  /**< @brief Size of LGMM v1 skin structure. */

/* LGMM v2 material structure. Identical, but with a larger filename field. */

#define LGMM_SKINV2_FILENAME  0   /**< @brief Texture file name.    (40bytes) */
#define LGMM_SKINV2_FLAGS     40  /**< @brief Material flags.       (UI8)     */
#define LGMM_SKINV2_LISTA     41  /**< @brief LISTA item count.     (UI8)     */
#define LGMM_SKINV2_ID        42  /**< @brief Unique ID.            (UI16 LE) */
#define LGMM_SKINV2_NUMPOLYS  44  /**< @brief Number of triangles.  (UI16 LE) */
#define LGMM_SKINV2_POLYSKIP  46  /**< @brief Triangles to skip.    (UI16 LE) */
#define LGMM_SKINV2_NUMVERTS  48  /**< @brief Number of vertices.   (UI16 LE) */
#define LGMM_SKINV2_VERTSKIP  50  /**< @brief Vertices to skip.     (UI16 LE) */
#define LGMM_SKINV2_ZEROUI32  52  /**< @brief Always zero.          (UI32 LE) */
#define LGMM_SIZEOF_SKINV2    56  /**< @brief Size of LGMM v2 skin structure. */

/* LGMM Material flags. */

#define LGMM_SKINFLAG_NOFILE   0x01  /**< @brief Filename is missing if set. */
#define LGMM_SKINFLAG_UNKNOWN2 0x02  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN3 0x04  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN4 0x08  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN5 0x10  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN6 0x20  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN7 0x40  /**< @brief Not used.                   */
#define LGMM_SKINFLAG_UNKNOWN8 0x80  /**< @brief Not used.                   */

/* LGMM polygon structure. */

#define LGMM_POLY_VERTEXA     0   /**< @brief Vertex index #1.      (UI16 LE) */
#define LGMM_POLY_VERTEXB     2   /**< @brief Vertex index #2.      (UI16 LE) */
#define LGMM_POLY_VERTEXC     4   /**< @brief Vertex index #3.      (UI16 LE) */
#define LGMM_POLY_SKINID      6   /**< @brief Material/Group ID.    (UI16 LE) */
#define LGMM_POLY_UNKNOWN     8   /**< @brief Unknown.              (Float32) */
#define LGMM_POLY_NORMALID    12  /**< @brief Normal index.         (UI16 LE) */
#define LGMM_POLY_RESERVED    14  /**< @brief Always -1.            (SI16 LE) */
#define LGMM_SIZEOF_POLY      16  /**< @brief Size of LGMM polygon structure. */

/**
@brief [INTERNAL] Load a Dark Engine LGMM-format mesh.

@todo The models are often messed up because the various parts haven't been set
to the right position; presumably there's skeleton and matrix data in here...?
Otherwise, it might be in the associated .CAL files.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

static mesh_s *meshLoadBIN_LGMM(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  unsigned long version, offs, g;
  unsigned long num_verts, vert_offs;
  unsigned long num_skins, skin_offs;
  unsigned long num_polys, poly_offs;
  unsigned long texcoord_offs;

  /* Determine whether this is a known LGMM-format mesh file version. */

  if (length < LGMM_SIZEOF_HEADER || !data) { return NULL; }
  if (memcmp(data, "LGMM", 4) != 0) { return NULL; }
  version = binGetUI32_LE(&data[4]);
  if (version != 1 && version != 2) { return NULL; }

  /* Get various counts and offsets. */

  num_skins = data[LGMM_HEAD_NUMSKINS];
  num_verts = binGetUI16_LE(&data[LGMM_HEAD_NUMVERTS]);
  num_polys = binGetUI16_LE(&data[LGMM_HEAD_NUMPOLYS]);
  poly_offs = binGetUI32_LE(&data[LGMM_HEAD_OFFSPOLYS]);

  /* DEBUG: The following data is unknown, but may be skeleton-related... */

  #if 0
  {

    unsigned long num_lista   = data[LGMM_HEAD_NUMLISTA];
    unsigned long num_listb   = data[LGMM_HEAD_NUMLISTB];
    unsigned long num_listc   = binGetUI16_LE(&data[LGMM_HEAD_NUMLISTC]);

    unsigned long offs_lista1 = binGetUI32_LE(&data[LGMM_HEAD_OFFSLISTA1]);
    unsigned long offs_lista2 = binGetUI32_LE(&data[LGMM_HEAD_OFFSLISTA2]);
    unsigned long offs_listb  = binGetUI32_LE(&data[LGMM_HEAD_OFFSLISTB]);
    unsigned long offs_listc  = binGetUI32_LE(&data[LGMM_HEAD_OFFSLISTC]);

    /* Output some useful info. */

    fprintf(stderr, "[LGMM] Total number of vertices: %lu\n", num_verts);
    fprintf(stderr, "[LGMM] Total number of polygons: %lu\n", num_polys);
    fprintf(stderr, "[LGMM] Total number of textures: %lu\n", num_skins);
    fprintf(stderr, "[LGMM] Number of items in LISTA: %lu\n", num_lista);
    fprintf(stderr, "[LGMM] Number of items in LISTB: %lu\n", num_listb);
    fprintf(stderr, "[LGMM] Number of items in LISTC: %lu\n", num_listc);

    /* LIST A: There are actually TWO sets of data for this... */

    for (g = 0; g < num_lista; ++g)
    {

      fprintf(stderr, "LIST A #%lu: ", g);

      /* The first is simply two consecutive lists of UI8 values. */

      fprintf(stderr, "(%d ->", data[offs_lista1 + g + 0        ]);
      fprintf(stderr, " %d)\n", data[offs_lista1 + g + num_lista]);

      /* The second stores 16 bytes of data for each item. */

      offs = offs_lista2 + (g*16);

      fprintf(stderr, "\tPolys count: %d\n", binGetUI16_LE(&data[offs+0]));
      fprintf(stderr, "\tPoly offset: %d\n", binGetUI16_LE(&data[offs+2]));
      fprintf(stderr, "\tVerts count: %d\n", binGetUI16_LE(&data[offs+4]));
      fprintf(stderr, "\tVert offset: %d\n", binGetUI16_LE(&data[offs+6]));
      fprintf(stderr, "\tUNKNOWNA U8: %d\n", data[offs+8]);
      fprintf(stderr, "\tUNKNOWNB U8: %d\n", data[offs+9]);
      fprintf(stderr, "\tUNKNOWNC U8: %d\n", data[offs+10]);
      fprintf(stderr, "\tUNKNOWND U8: %d\n", data[offs+11]);
      fprintf(stderr, "\tGroup index: %d\n", binGetUI16_LE(&data[offs+12]));
      fprintf(stderr, "\tListB index: %d\n", binGetUI16_LE(&data[offs+14]));

    }

    fprintf(stderr, "\n");

    /* LIST B: Contains 20 bytes for each item. */

    for (offs = offs_listb, g = 0; g < num_listb; ++g, offs += 20)
    {

      fprintf(stderr, "LIST B #%lu:\n", g);
      fprintf(stderr, "\tAlways zero: %lu\n", binGetUI32_LE(&data[offs+0]));
      fprintf(stderr, "\tIndex A...?: %d\n", data[offs+4]);
      fprintf(stderr, "\tFlags A...?: %d\n", data[offs+5]); /* Zero or one. */
      fprintf(stderr, "\tIndex B...?: %d\n", data[offs+6]);
      fprintf(stderr, "\tFlags B...?: %d\n", data[offs+7]); /* Zero or one. */
      fprintf(stderr, "\tAlways zero: %lu\n", binGetUI32_LE(&data[offs+8]));
      fprintf(stderr, "\tAlways zero: %lu\n", binGetUI32_LE(&data[offs+12]));
      fprintf(stderr, "\tAlways zero: %lu\n", binGetUI32_LE(&data[offs+16]));

    }

    fprintf(stderr, "\n");

    /* LIST C: Stores a single 32-bit float for each item; may be missing. */

    for (g = 0; g < num_listc; ++g)
    {
      float f;
      memcpy(&f, &data[offs_listc + (g*4)], 4);
      fprintf(stderr, "LIST C #%lu = %f\n", g, f);
    }

    fprintf(stderr, "\n");

  }
  #endif

  /* Allocate a basic mesh structure. */

  mesh = meshCreate(1, num_verts, 0, num_verts, num_skins);
  if (!mesh) { return NULL; }
  mesh->mtrl = mtrlCreateGroup(num_skins);

  /* Load vertices. */

  vert_offs = binGetUI32_LE(&data[LGMM_HEAD_OFFSVERTS]);
  memcpy(mesh->vertex[0], &data[vert_offs], num_verts * 3*4);

  /* Load texture coordinates. */

  texcoord_offs = binGetUI32_LE(&data[LGMM_HEAD_OFFSUVMAP]);
  for (offs = texcoord_offs, g = 0; g < num_verts; ++g, offs += 12)
  {
    memcpy(&mesh->texcoord[g*2], &data[offs], 4*2);
    mesh->texcoord[(g*2)+1] *= -1;
  }

  /* Load materials / triangle groups. */

  skin_offs = binGetUI32_LE(&data[LGMM_HEAD_OFFSSKINS]);
  for (offs = skin_offs, g = 0; g < num_skins; ++g)
  {

    unsigned long offs_p, p;

    /* Get the name (and triangle count) for this material group. */

    if (version < 2) { p = binGetUI16_LE(&data[offs + LGMM_SKINV1_NUMPOLYS]); }
    else             { p = binGetUI16_LE(&data[offs + LGMM_SKINV2_NUMPOLYS]); }

    mesh->group[g] = meshGroupCreate((const char*)&data[offs], g, p);
    if (!mesh->group[g]) { return meshDelete(mesh); }

    /* Get textures. LGMM v1 files often look in "txt" rather than "txt16". */

    if (mesh->mtrl)
    {
      strcpy(mesh->mtrl->item[g]->name, mesh->group[g]->name);
      sprintf(mesh->mtrl->item[g]->diffusemap, "txt16/%s", mesh->group[g]->name);
    }

    /* Get the triangle data for this group. */

    for (offs_p = poly_offs, p = 0; offs_p < poly_offs + (num_polys * LGMM_SIZEOF_POLY); offs_p += LGMM_SIZEOF_POLY)
    {

      int temp;

      if (binGetUI16_LE(&data[offs_p + LGMM_POLY_SKINID]) != g) { continue; }

      /* DEBUG: Do some error checking, just in case we get any surprises... */

      if (p >= (unsigned long)mesh->group[g]->num_tris)
      {
        fprintf(stderr, "[LGMM] Error: Found more triangles than expected.\n");
        continue;
      }

      temp = binGetSI16_LE(&data[offs_p + LGMM_POLY_RESERVED]);
      if (temp != -1)
      {
        fprintf(stderr, "[LGMM] Polygon %lu: Expected -1, got %d\n", p, temp);
        continue;
      }

      /* Get the vertex indices (in REVERSE order to change the winding). */

      mesh->group[g]->tri[p]->v_id[0] = binGetUI16_LE(&data[offs_p + LGMM_POLY_VERTEXC]);
      mesh->group[g]->tri[p]->v_id[1] = binGetUI16_LE(&data[offs_p + LGMM_POLY_VERTEXB]);
      mesh->group[g]->tri[p]->v_id[2] = binGetUI16_LE(&data[offs_p + LGMM_POLY_VERTEXA]);

      mesh->group[g]->tri[p]->t_id[0] = mesh->group[g]->tri[p]->v_id[0];
      mesh->group[g]->tri[p]->t_id[1] = mesh->group[g]->tri[p]->v_id[1];
      mesh->group[g]->tri[p]->t_id[2] = mesh->group[g]->tri[p]->v_id[2];

      /* Increase the per-group triangle counter. */

      ++p;

    }

    /* Move on to the next material group. */

    offs += version < 2 ? LGMM_SIZEOF_SKINV1 : LGMM_SIZEOF_SKINV2;

  }

  return mesh;

}

/*
[PUBLIC] Load a mesh from a Dark Engine .BIN data buffer.
*/

mesh_s *meshLoadBIN(size_t length, const unsigned char *data)
{
  mesh_s *mesh = meshLoadBIN_LGMD(length, data);
  if (!mesh) { mesh = meshLoadBIN_LGMM(length, data); }
  return mesh;
}
