/**
@file
@brief Handles Quake I .MDL files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a mesh from a Quake .MDL data buffer.
*/

mesh_s *meshLoadMDL(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;  /* The mesh object to be returned. */
  long         *exuv = NULL;  /* Extra texture coord index list. */

  float scale[3];             /* Vertex compression data: Scaling.     */
  float trans[3];             /* Vertex compression data: Translation. */

  long offs_frames = 0;
  long offs_faces  = 0;
  long offs_coords = 0;

  long num_verts     = 0;     /* Number of vertices per frame. */
  long num_faces     = 0;     /* Number of triangles.          */
  long num_ex_coords = 0;     /* Number of EXTRA texcoords.    */

  long skin_width  = 0;       /* Texture width  (pixels). */
  long skin_height = 0;       /* Texture height (pixels). */

  long tmp = 0;

  long i, j, k, a, b;

  /* Validate header. */

  if (!data || length < 84) { return NULL; }

  if (memcmp (data, "IDPO", 4) != 0) { return NULL; }
  if (binGetUI32_LE (&data[4])  != 6) { return NULL; }

  /* Get vertex and triangle counts. */

  num_verts = binGetUI32_LE (&data[60]);
  num_faces = binGetUI32_LE (&data[64]);

  /* Skip past the embedded textures + Work out the texcoord data offset. */

  tmp         = binGetUI32_LE (&data[48]);
  skin_width  = binGetUI32_LE (&data[52]);
  skin_height = binGetUI32_LE (&data[56]);

  offs_coords = 84;

  for (i = 0; i < tmp; i++)
  {

    offs_coords += 4;

    if (binGetUI32_LE (&data[offs_coords-4]))
    {

      /* Animated texture - get frame count. */

      j = binGetUI32_LE (&data[offs_coords]);

      offs_coords += (skin_width * skin_height * j) + (4 * j) + 4;

    } else { offs_coords += skin_width * skin_height; }

  }

  offs_faces  = offs_coords + (12 * num_verts);
  offs_frames = offs_faces  + (16 * num_faces);

  /* Create a lookup table for "dual purpose" texcoords. */

  exuv = (long*)malloc (sizeof(long) * num_verts);

  if (!exuv) { return NULL; }

  /* Determine how many "dual purpose" texcoords exist. */

  num_ex_coords = 0;

  for (i = 0; i < num_verts; i++)
  {

    if (binGetUI32_LE (&data[offs_coords + (i*12)]))
    {

      exuv[i] = num_verts + num_ex_coords;

      num_ex_coords++;

    } else { exuv[i] = i; }

  }

  /* Create the mesh, with no normal data and a single group. */

  mesh = meshCreate (binGetUI32_LE (&data[68]), num_verts, 0, num_verts + num_ex_coords, 1);

  if (!mesh)
  {

    free (exuv);

    return NULL;

  }

  /* Create the group. */

  mesh->group[0] = meshGroupCreate ("MDL", -1, num_faces);

  if (!mesh->group[0])
  {

    meshDelete (mesh);

    free (exuv);

    return NULL;

  }

  /* Copy texture coordinate data. */

  for (i = 0; i < num_verts; i++)
  {

    a = binGetUI32_LE (&data[offs_coords + (i*12) + 4]);
    b = binGetUI32_LE (&data[offs_coords + (i*12) + 8]);

    mesh->texcoord[(i*2)+0] =        (float)a / (float)skin_width;
    mesh->texcoord[(i*2)+1] = 1.0f - (float)b / (float)skin_height;

    if (binGetUI32_LE (&data[offs_coords + (i*12)]) != 0)
    {

      mesh->texcoord[(exuv[i]*2)+0] = mesh->texcoord[(i*2)+0] + 0.5f;
      mesh->texcoord[(exuv[i]*2)+1] = mesh->texcoord[(i*2)+1];

    }

  }

  /* Read triangle data. */

  for (i = 0; i < num_faces; i++)
  {

    /* Get vertex index. */

    for (k = 0; k < 3; k++)
    {

      mesh->group[0]->tri[i]->v_id[k] =
        binGetUI32_LE (&data[offs_faces + (i*16) + (k*4) + 4]);

    }

    for (k = 0; k < 3; k++)
    {

      /* Determine whether this texture coordinate index needs to be changed. */

      if (binGetUI32_LE (&data[offs_faces + (i*16)]) != 0)
      {

        mesh->group[0]->tri[i]->t_id[k] =
          mesh->group[0]->tri[i]->v_id[k];

      }
      else
      {

        mesh->group[0]->tri[i]->t_id[k] =
          exuv[mesh->group[0]->tri[i]->v_id[k]];

      }

    }

  }

  /* Free the alternative texture coordinate index table. */

  if (exuv) { free (exuv); }

  /* Get vertex scale/transform data. */

  memcpy (&scale, &data[ 8], sizeof(float) * 3);
  memcpy (&trans, &data[20], sizeof(float) * 3);

  /* Read frame data. */

  k = offs_frames;
  j = 0;

  for (i = 0; i < mesh->num_frames; i++)
  {

    if (j == 0)
    {

      /* Check frame flags. */

      j =  binGetUI32_LE (&data[k]);

      k += 4;

      /* Skip some data if this is a "group" frame. */

      if (j != 0) { k += 8 + (mesh->num_frames * 4); }

    }

    /* Skip some unnecessary data. */

    k += 8;

    /* Get frame name. */

    mesh->name[i] = (char*)malloc(17);

    if (mesh->name[i])
    {
      memset(mesh->name[i], 0,        17);
      memcpy(mesh->name[i], &data[k], 16);
    }

    k += 16;

    /* Copy vertex data. */

    for (a = 0; a < num_verts; a++)
    {

      for (b = 0; b < 3; b++)
      {

        mesh->vertex[i][(a*3) + b] = ((float)data[k+b] * scale[b]) + trans[b];

      }

      k += 4;

    }

  }

  /* Return the mesh. */

  return mesh;

}
