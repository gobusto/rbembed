/**
@file
@brief Various functions related to exporting meshes.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "export.h"

/*
[PUBLIC] Export a single frame of a mesh to a file.
*/

int meshSaveFile(const mesh_s *mesh, lgm_index_t frame, const char *file_name, mesh_format_t format)
{
  switch (format)
  {
    case MESH_FORMAT_OBJ: return meshSaveOBJ(mesh, frame, file_name, NULL);
    case MESH_FORMAT_OFF: return meshSaveOFF(mesh, frame, file_name);
    case MESH_FORMAT_PLY: return meshSavePLY(mesh, frame, file_name);
    case MESH_FORMAT_STL: return meshSaveSTL(mesh, frame, file_name);
    default: break;
  }
  return -1;
}
