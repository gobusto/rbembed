/**
@file
@brief Handles Stanford .PLY files.

<http://paulbourke.net/dataformats/ply/>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Export a mesh as a Stanford PLY file.
*/

int meshSavePLY(const mesh_s *mesh, lgm_index_t frame, const char *file_name)
{

  FILE *out = NULL;
  long g, i;

  if      (!mesh || !file_name                   ) { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Get the TOTAL number of triangles in the mesh. */

  for (i = 0, g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g]) { i += mesh->group[g]->num_tris; }
  }

  /* Open the file and output a file header. */

  out = fopen(file_name, "wb");
  if (!out) { return -3; }
  fprintf(out, "ply\nformat ascii 1.0\n");

  fprintf(out, "element vertex %ld\n", mesh->num_verts);
  fprintf(out, "property float x\nproperty float y\nproperty float z\n");

  fprintf(out, "element face %ld\n", i);
  fprintf(out, "property list uint uint vertex_index\nend_header\n");

  /* Output each vertex. */

  for (i = 0; i < mesh->num_verts * 3; i++)
  { fprintf(out, "%f%c", mesh->vertex[frame][i], i % 3 == 2 ? '\n' : ' '); }

  /* Output each triangle. */

  for (g = 0; g < mesh->num_groups; g++)
  {

    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    for (i = 0; i < group->num_tris; i++)
    {
      fprintf(out, "3 %ld %ld %ld\n", group->tri[i]->v_id[2],
                                      group->tri[i]->v_id[1],
                                      group->tri[i]->v_id[0]);
    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}
