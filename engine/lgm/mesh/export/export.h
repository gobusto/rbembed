/**
@file
@brief Functions used internally to save meshes.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_EXPORT_MESH_H__
#define __LGM_EXPORT_MESH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../mesh.h"
#include "../../shared/export.h"

/**
@brief Export a mesh as a Princeton OFF (Object File Format) file.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int meshSaveOFF(const mesh_s *mesh, lgm_index_t frame, const char *file_name);

/**
@brief Export a mesh as a Stanford PLY file.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int meshSavePLY(const mesh_s *mesh, lgm_index_t frame, const char *file_name);

/**
@brief Export a mesh as an STL-format file.

Currently, only binary STL files are supported.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int meshSaveSTL(const mesh_s *mesh, lgm_index_t frame, const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_EXPORT_MESH_H__ */
