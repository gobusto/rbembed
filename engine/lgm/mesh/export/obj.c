/**
@file
@brief Handles Wavefront .OBJ files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Export a mesh as an OBJ.
*/

int meshSaveOBJ(const mesh_s *mesh, lgm_index_t frame, const char *obj_file, const char *mtl_file)
{

  FILE          *out = NULL;
  mesh_group_s *grp = NULL;
  long g, t, i;

  /* Check parameters. */

  if      (!mesh || !obj_file)                     { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Open the file. */

  out = fopen(obj_file, "wb");
  if (!out) { return -3; }

  /* If an MTLLIB has been specified, create the MTL file and reference it. */

  if (mtrlSaveFile(mesh->mtrl, mtl_file, MTRL_FORMAT_MTL) == 0) { fprintf(out, "mtllib %s\n", mtl_file); }

  /* Write vertices. */

  for (i = 0; i < mesh->num_verts; i++)
  {
    fprintf(out, "v %f %f %f\n", mesh->vertex[frame][(i*3)+0],
                                 mesh->vertex[frame][(i*3)+1],
                                 mesh->vertex[frame][(i*3)+2]);
  }

  /* Write normals. */

  for (i = 0; i < mesh->num_norms; i++)
  {
    fprintf(out, "vn %f %f %f\n", mesh->normal[frame][(i*3)+0],
                                  mesh->normal[frame][(i*3)+1],
                                  mesh->normal[frame][(i*3)+2]);
  }

  /* Write texcoords. */

  for (i = 0; i < mesh->num_coords; i++)
  {
    fprintf(out, "vt %f %f\n", mesh->texcoord[(i*2)+0], mesh->texcoord[(i*2)+1]);
  }

  /* Write groups. */

  for (g = 0; g < mesh->num_groups; g++ )
  {

    grp = mesh->group[g];
    if (!grp) { continue; }

    /* Write group and material data. */

    fprintf(out, "g %s\n", grp->name ? grp->name : "");

    if (mesh->mtrl)
    {
      if (grp->mat_id >= 0 && grp->mat_id < mesh->mtrl->num_items)
      { fprintf(out, "usemtl %s\n", mesh->mtrl->item[grp->mat_id]->name); }
      else
      { fprintf(out, "usemtl notexture\n"); }
    }

    /* Write faces. */

    for (t = 0; t < grp->num_tris; t++)
    {

      fputc('f', out);

      for (i = 2; i >= 0; i--)
      {

        fprintf(out, " %ld", grp->tri[t]->v_id[i] + 1);

        if (mesh->texcoord) { fprintf(out, "/%ld", grp->tri[t]->t_id[i] + 1); }

        if (mesh->normal)
        {
          if (!mesh->texcoord) { fputc('/', out); }
          fprintf(out, "/%ld", grp->tri[t]->n_id[i] + 1);
        }

      }

      fputc('\n', out);

    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}
