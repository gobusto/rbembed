/**
@file
@brief Provides an "associative array" structure designed for fast look-up.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "wordtree.h"

/*
[PUBLIC] Create a new "tree" node structure.
*/

wordtree_s *treeCreate(void)
{
  wordtree_s *tree = (wordtree_s*)malloc(sizeof(wordtree_s));
  if (tree) { memset(tree, 0, sizeof(wordtree_s)); }
  return tree;
}

/**
@brief [INTERNAL] Recursively explore every branch of a wordtree structure.

This is called internally by the treeDelete() and treeEachItem() functions.

@param tree The current root-node of the tree being explored.
@param func An optional callback for any data items found in the tree.
@param user An optional user-data pointer passed to the callback for each item.
@param mode If true, delete the tree structures. This is set by treeDelete().
*/

static void treeRecurse(wordtree_s *tree, void *(*func)(const char*, void*, void*), void *user, int mode)
{

  unsigned char i = 0;

  if (!tree) { return; }

  /* If this node has data, and a function is provided, pass the data to it. */

  if (func && tree->data)
  {

    wordtree_s *node = NULL;
    char       *name = NULL;
    size_t      len  = 0;

    /* Allocate a temporary string for storing the "name" of this node. */

    for (len = 0, node = tree->parent; node; node = node->parent, ++len) { }
    name = (char*)malloc(len + 1);

    /* Get the "name" of this node by working back up to the root node. */

    if (name)
    {

      wordtree_s *prev = tree;

      name[len] = 0;  /* <-- NULL terminator. */

      for (node = tree->parent; node; node = node->parent, prev = prev->parent)
      {
        for (--len, i = 0; i < node->length; ++i)
        {
          if (node->branch[i] == prev) { name[len] = node->offset + i; break; }
        }
      }

    }

    /* Call the user-provided function (possibly replaces the data pointer). */

    tree->data = func(name, tree->data, user);
    if (name) { free(name); }

  }

  /* Loop over any child branches, optionally deleting them afterwards. */

  if (tree->branch)
  {
    for (i = 0; i < tree->length; ++i) { treeRecurse(tree->branch[i], func, user, mode); }
    if (mode) { free(tree->branch); }
  }

  if (mode) { free(tree); }

}

/*
[PUBLIC] Delete an existing tree structure, along with any sub-trees.
*/

wordtree_s *treeDelete(wordtree_s *tree, void *(*func)(const char*, void*, void*), void *user)
{ treeRecurse(tree, func, user, 1); return NULL; }

/*
[PUBLIC] Iterate over all entries in a wordtree structure.
*/

void treeEachItem(wordtree_s *tree, void *(*func)(const char*, void*, void*), void *user)
{ treeRecurse(tree, func, user, 0); }

/*
[PUBLIC] Insert a new item into a tree, replacing the old value if necessary.
*/

int treeInsert(wordtree_s *tree, const char *key, void *data, void **old_data)
{

  wordtree_s **new_branch;
  unsigned char new_length, k, i;

  if (!tree || !key) { return 0; }

  k = (unsigned char)key[0];

  /* If this tree node indicates the end of the string, store the data here. */

  if (k == 0)
  {
    if (old_data) { *old_data = tree->data; }
    tree->data = data;
    return 1;
  }

  /* If this node hasn't got a branch array yet, set the starting point. */

  if (tree->offset == 0) { tree->offset = k; }

  /* If the key comes BEFORE THE START of this node's list, extend it LEFT. */

  if (k < tree->offset)
  {

    new_length = tree->length + (tree->offset - k);
    new_branch = (wordtree_s**)realloc(tree->branch, sizeof(wordtree_s*) * new_length);
    if (!new_branch) { return 0; }

    for (i = tree->length; i > 0; --i)
    { new_branch[(i-1) + (new_length - tree->length)] = new_branch[i-1]; }
    memset(new_branch, 0, sizeof(wordtree_s*) * (new_length - tree->length));

    tree->offset = k;
    tree->length = new_length;
    tree->branch = new_branch;

  }

  /* If the key comes AFTER THE END of this node's list, extend it RIGHT. */

  else if (k - tree->offset >= tree->length)
  {

    new_length = (k - tree->offset) + 1;
    new_branch = (wordtree_s**)realloc(tree->branch, sizeof(wordtree_s*) * new_length);
    if (!new_branch) { return 0; }

    memset(new_branch + tree->length, 0, sizeof(wordtree_s*) * (new_length - tree->length));

    tree->length = new_length;
    tree->branch = new_branch;

  }

  /* If required, create a new sub-node structure for this byte of the key. */

  if (!tree->branch[k - tree->offset])
  {
    tree->branch[k - tree->offset] = treeCreate();
    if (!tree->branch[k - tree->offset]) { return 0; }
    tree->branch[k - tree->offset]->parent = tree;
  }

  return treeInsert(tree->branch[k - tree->offset], key + 1, data, old_data);

}

/*
[PUBLIC] Get a given (named) item from a tree structure.
*/

void *treeSearch(wordtree_s *tree, const char *key)
{

  unsigned char k = 0;

  if (!tree || !key) { return NULL; }

  k = (unsigned char)key[0];
  if (k == 0) { return tree->data; }

  if (k < tree->offset || k - tree->offset >= tree->length) { return NULL; }
  return treeSearch(tree->branch[k - tree->offset], key + 1);

}
