/**
@file
@brief Functions used internally to save animations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_EXPORT_ANIM_H__
#define __LGM_EXPORT_ANIM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../anim.h"
#include "../../shared/export.h"

/**
@brief Export a single skeletal animation sequence to a BioVision BVH file.

@param anim The animation sequence to be exported.
@param file_name The name (and path) of the output file.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t animSaveBVH(const anim_s *anim, const char *file_name);

/**
@brief Export a set of morph-based animation sequences to a Quake 3 CFG file.

@param anim_set The collection of animation sequences to be exported.
@param file_name The name (and path) of the output file.
@return The number of animation sequences exported, or -1 on error.
*/

lgm_count_t animSetSaveCFG(wordtree_s *anim_set, const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_EXPORT_ANIM_H__ */
