/**
@file
@brief Handles Quake III .CFG files.

<http://cube.wikispaces.com/Importing+md2+and+md3+files>

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "export.h"

/**
@brief Per-line callback for animSaveCFG() to output individual animations.

@todo Can FPS be a float?

@param name The name of the animation sequence.
@param data The `anim_s` structure representing a single animation.
@param user The `FILE` pointer to output the animation sequence to.
@return Always returns the original value of the `data` parameter.
*/

static void *animSaveCFG_callback(const char *name, void *data, void *user)
{
  const anim_s *anim = (anim_s*)data;
  if (!animIsMorph(anim)) { return data; }

  fprintf((FILE*)user, "%ld %ld 0 %ld // %s\n",
      anim->frame_start, anim->frame_count, (long)anim->fps, name);

  return data;
}

/*
[PUBLIC] Export a set of morph-based animation sequences to a Quake 3 CFG file.
*/

lgm_count_t animSetSaveCFG(wordtree_s *anim_set, const char *file_name)
{
  const lgm_count_t morph_animation_count = animSetMorphCount(anim_set);
  FILE *out = NULL;

  if (!file_name || morph_animation_count < 1) { return -1; }

  out = fopen(file_name, "wb");
  if (!out) { return -1; }
  treeEachItem(anim_set, animSaveCFG_callback, out);
  fclose(out);

  return morph_animation_count;
}
