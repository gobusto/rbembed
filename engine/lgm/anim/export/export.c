/**
@file
@brief Various functions related to exporting animations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "export.h"

/*
[PUBLIC] Export a single animation sequence to a file.
*/

lgm_bool_t animSaveFile(
  const anim_s *anim,
  const char *file_name,
  anim_format_t format
)
{
  switch (format)
  {
    case ANIM_FORMAT_BVH: return animSaveBVH(anim, file_name);
    default: break;
  }
  return LGM_FALSE;
}

/*
[PUBLIC] Export a set of animation sequences to a file.
*/

lgm_count_t animSetSaveFile(
  wordtree_s *anim_set,
  const char *file_name,
  anim_set_format_t format
)
{
  switch (format)
  {
    case ANIM_FORMAT_CFG: return animSetSaveCFG(anim_set, file_name);
    default: break;
  }
  return -1;
}
