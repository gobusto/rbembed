/**
@file
@brief Provides utilities for describing 3D model animations.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_ANIM_H__
#define __LGM_ANIM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "../shared/typedefs.h"
#include "util/wordtree.h"
#include "tag.h"

/**
@brief Find an animation sequence by name.

@param anim_set The collection of animation sequences to search.
@param name The name of the animation sequence to look for.
@return The animation sequence structure, if it exists, or NULL if it doesn't.
*/

#define animSetFind(anim_set,name) ((anim_s*)treeSearch(anim_set, name))

/**
@brief Determine if a particular animation is skeletal or not.

@param anim The animation to check.
@return True if the animation is skeletal, or false if it's morph-based.
*/

#define animIsSkeletal(anim) ((anim)->frame)

/**
@brief Determine if a particular animation is morph-based or not.

@param anim The animation to check.
@return True if the animation is morph-based, or false if it's skeletal.
*/

#define animIsMorph(anim) (!(anim)->frame)

/**
@brief Enumerates possible export formats for INDIVIDUAL animations.
*/

typedef enum
{

  ANIM_FORMAT_BVH /**< @brief Biovision Hierarchy .BVH */

} anim_format_t;

/**
@brief Enumerates possible export formats for GROUPS OF animations.
*/

typedef enum
{

  ANIM_FORMAT_CFG /**< @brief Quake III .CFG */

} anim_set_format_t;

/**
@brief This structure describes a single animation sequence for a mesh.

An animation can be morph-based (i.e. Quake-style "vertex sets" for each frame)
or skeletal. This structure is used to represent both, as follows:

+ For morph-based animations, `frame_start` and `frame_count` refer to the data
  stored directly by the mesh itself (that is, `vertex[frame_number][x_y_or_z]`
  and `normal[frame_number][x_y_or_z]`).
+ For skeletal animations, `frame_count` is the number of tag-sets that `frame`
  contains.

In other words: If `frame` is `NULL`, then it's a morph-based animation, but if
not then it's a skeletal animation.
*/

typedef struct
{

  float fps;                /**< @brief Playback rate in frames-per-second. */
  lgm_count_t frame_count;  /**< @brief The sequence duration in frames.    */

  lgm_index_t frame_start;  /**< @brief Morph animation: Index of 1st frame. */
  mesh_tag_set_s **frame;   /**< @brief Boned animation: Per-frame tag sets. */

} anim_s;

/* ========================================================================= */
/* IMPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Load one or more animation sequences from a text string.

This function **only** attempts to handle plain-text formats, like CFG.

@param anim_set The animation set to add the new animations to.
@param name If no animation "name" is available, use this string instead.
@param text The text buffer to parse.
@return The number of new animations loaded, or -1 on failure.
*/

lgm_count_t animLoadText(
  wordtree_s *anim_set,
  const char *name,
  const char *text
);

/**
@brief Load one or more animation sequences from a binary data buffer.

This function **doesn't** attempt to handle plain-text formats like CFG.

@param anim_set The animation set to add the new animations to.
@param name If no animation "name" is available, use this string instead.
@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return The number of new animations loaded, or -1 on failure.
*/

lgm_count_t animLoadData(
  wordtree_s *anim_set,
  const char *name,
  size_t length,
  const unsigned char *data
);

/**
@brief Load one or more animation sequences from a file.

@param anim_set The animation set to add the new animations to.
@param name If no animation "name" is available, use this string instead.
@param file_name The name (and path) of the file to load.
@return The number of new animations loaded, or -1 on failure.
*/

lgm_count_t animLoadFile(
  wordtree_s *anim_set,
  const char *name,
  const char *file_name
);

/* ========================================================================= */
/* EXPORT FUNCTIONS                                                          */
/* ========================================================================= */

/**
@brief Export a single animation sequence to a file.

@param anim The animation sequence to be exported.
@param file_name The name (and path) of the output file.
@param format The file format to export as.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t animSaveFile(
  const anim_s *anim,
  const char *file_name,
  anim_format_t format
);

/**
@brief Export a set of animation sequences to a file.

@param anim_set The collection of animation sequences to be exported.
@param file_name The name (and path) of the output file.
@param format The file format to export as.
@return The number of animation sequences exported, or -1 on error.
*/

lgm_count_t animSetSaveFile(
  wordtree_s *anim_set,
  const char *file_name,
  anim_set_format_t format
);

/* ========================================================================= */
/* OTHER FUNCTIONS                                                           */
/* ========================================================================= */

/**
@brief Create a new animation sequence structure and add it into a collection.

@param anim_set The collection that the new animation should be added to.
@param name The name of the animation sequence.
@param start The starting frame-index of the animation sequence.
@param num_frames The number of frames that the sequence should last for.
@param num_tags The number of tags for each skeletal animation frame. May be 0.
@param fps The playback rate of the animation in frames-per-second.
@return A new animation structure on success, or NULL on failure.
*/

anim_s *animCreate(
  wordtree_s *anim_set,
  const char *name,
  lgm_index_t start,
  lgm_count_t num_frames,
  lgm_count_t num_tags,
  float fps
);

/**
@brief Create a new collection of animation sequences.
*/

#define animSetCreate treeCreate

/**
@brief Delete a previously-created collection of animation sequences.

@param anim_set The collection of animation sequences to be deleted.
@return Always returns NULL.
*/

wordtree_s *animSetDelete(wordtree_s *anim_set);

/**
@brief Get the number of skeletal animations in an animation set.

@param anim_set The animation set to inspect.
@return The number of skeletal animations in the animation set.
*/

lgm_count_t animSetMorphCount(wordtree_s *anim_set);

/**
@brief Get the number of morph-based animations in an animation set.

@param anim_set The animation set to inspect.
@return The number of morph-based animations in the animation set.
*/

lgm_count_t animSetSkeletalCount(wordtree_s *anim_set);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_ANIM_H__ */
