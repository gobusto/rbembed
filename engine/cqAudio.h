/**
@file
@brief about

writeme
*/

#ifndef __CQ2_AUDIO_H__
#define __CQ2_AUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"

/**
@brief about

writeme

@param file_name about
@param language about
@return writeme
*/

int sndCacheEffectSet(const char *file_name, const char *language);

/**
@brief Initialise the audio subsystem.

Call this function before doing anything else.

@param num_channels The number of audio channels to allocate.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int sndInit(v2d_ui8_t num_channels);

/**
@brief Shut down the audio subsystem.

Call this function when the program shuts down.
*/

void sndQuit(void);

/**
@brief Play (and optionally loop) a sound effect.

Sounds may be either 2D or 3D. 2D sound effects are simply played as they are,
but 3D sound effects can be given a "position" which will affect their volume
and left/right panning relative to the current position of the 3D "listener" (a
"virtual ear", essentially), making them useful for environmental effects.

@param sound The name of the effect to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@param is_3D about
@param x about
@param y about
@param z about
@param radius about
@return writeme
*/

v2d_si32_t sndPlayEffect(const char *sound, int num_loops, v2d_bool_t is_3D,
                        float x, float y, float z, float radius);

/**
@brief Stop a sound from playing.

Does what it says on the tin.

@param guid The GUID of the sound to stop playing.
@return V2D_TRUE on success, or V2D_FALSE if the GUID is no longer valid.
*/

v2d_bool_t sndStopSound(v2d_si32_t guid);

/**
@brief Determine if a sound effect is still playing.

Does what it says on the tin.

@param guid The GUID of the sound to stop playing.
@return V2D_TRUE on success, or V2D_FALSE if the GUID is no longer valid.
*/

v2d_bool_t sndSoundPlaying(v2d_si32_t guid);

/**
@brief Reposition a 3D sound.

Has no effect on 2D sounds.

@param guid The GUID of the sound to stop playing.
@param x about
@param y about
@param z about
@return V2D_TRUE on success, or V2D_FALSE if the GUID is no longer valid.
*/

v2d_bool_t sndSoundSetPosition(v2d_si32_t guid, float x, float y, float z);

/**
@brief Play (and optionally loop) a music file.

Any currently-playing music is stopped first.

@param file_name The name (and path) of the music file to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int sndPlayMusic(const char *file_name, int num_loops);

/**
@brief Stop any currently-playing music.

Does what you'd expect it to do.
*/

void sndStopMusic(void);

/**
@brief Determine if any music is currently being played.

TODO: Use a specific "boolean" type for the return value?

@return Non-zero if music is being currently being played, or zero if not.
*/

int sndMusicPlaying(void);

/**
@brief about

writeme

@param x about
@param y about
@param z about
@param angle about
*/

void sndSetListener(float x, float y, float z, float angle);

/**
@brief about

writeme
*/

void sndUpdate(void);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_AUDIO_H__ */
