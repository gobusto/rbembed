/**
@file
@brief Provides structures and functions for handling 2D "sprite" graphics.

These functions are mainly useful for creating in-game HUDs or menus.
*/

#ifndef __CQ2_SPRITE_H__
#define __CQ2_SPRITE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"
#include "lgm/anim/util/wordtree.h"

/**
@brief This structure represents a single instance of a 2D "sprite" image.

These sprites are used to represent any 2D element displayed on-screen, such as
an icon, a crosshair, or a string of text. The actual image data is specific to
the graphics subsystem, as different APIs (OpenGL, Direct3D, etc.) store images
in different formats internally, so an API-agnostic pointer is stored here.

There are two kinds of sprite: Basic and Text. Basic sprites are simple images,
usually representing HUD icons for things such as health or armor. Text sprites
store UTF-8 strings, and can be used to display score counters or player names.
*/

typedef struct
{

  struct _image_s *image; /**< @brief Pointer to the actual image pixel data. */
  v2d_matrix_t    matrix; /**< @brief Matrix describing position, scale, etc. */

  char       *text;   /**< @brief A UTF8 string, or NULL for "basic" sprites. */
  v2d_real_t xywh[4]; /**< @brief Image subregion. Ignored by "text" sprites. */
  v2d_rgba_t rgba;    /**< @brief Red/Green/Blue color, plus the alpha value. */

} sprite_s;

/**
@brief Load various images into the graphics subsystem so sprites can use them.

It's not currently possible to "uncache" images once they've been loaded.

@param file_name The path to an ImageSet XML file, such as "base/imageset.xml".
@param language If provided, images tagged with a different code are not loaded.
@return Zero on success, or non-zero on failure.
*/

int spriteCacheImageSet(const char *file_name, const char *language);

/**
@brief Initialise the sprite management subsystem.

This function must be called before doing anything else.

@return Zero on success, or non-zero on failure.
*/

int spriteInit(void);

/**
@brief Shut down the sprite management subsystem and free any allocated memory.

This should be called when the program shuts down.
*/

void spriteQuit(void);

/**
@brief Get a pointer to the internal "sprite set" structure.

This should ONLY be called by the graphics subsystem (so that it knows what it
needs to draw for each sprite).

@return The data structure used to store all currently-active sprites.
*/

wordtree_s *spriteGetData(void);

/**
@brief Create a new sprite.

If a sprite with the same name already exists, the old sprite is deleted first.

@param name A unique identifier for the new sprite structure.
@param x The X position of the sprite.
@param y The Y position of the sprite.
@param w The width of the sprite.
@param h The height of the sprite.
@return A pointer to the new sprite structure on success, or NULL on failure.
*/

sprite_s *spriteCreate(const char *name, v2d_real_t x, v2d_real_t y, v2d_real_t w, v2d_real_t h);

/**
@brief Destroy a previously-created sprite.

Note that this function requires a name, rather than a pointer to a structure.
This is so that the sprite ID can be removed from the internal set of sprites.

@param name The name of the sprite to be deleted.
*/

void spriteDelete(const char *name);

/**
@brief Find a specific sprite by name.

@param name The name of the sprite to search for.
@return A pointer to the sprite structure if it is found, or NULL if it isn't.
*/

sprite_s *spriteFind(const char *name);

/**
@brief Set or change the text string of a sprite structure.

Note that a "text" sprite can be converted back into a regular image by passing
in a NULL pointer instead of a text string.

@param sprite The sprite structure to change.
@param text The new text string to display.
@return Zero on success, or non-zero on error.
*/

int spriteSetText(sprite_s *sprite, const char *text);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_SPRITE_H__ */
