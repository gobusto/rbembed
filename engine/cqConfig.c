/**
@file
@brief Provides functions for reading and writing configuration files.

Note that user input configuration is handled separately, by cqInput.c
*/

#include <stdlib.h>
#include <string.h>
#include "cqConfig.h"
#include "xml/xml.h"

#include <stdio.h>  /* For logging debug info to stdout and stderr. */

/*
[PUBLIC] Initialise a configuration structure to hard-coded "default" values.
*/

int cfgInit(config_s *config)
{
  if (!config) { return -1; }

  memset(config, 0, sizeof(config_s));

  config->flags = CFG_FLAG_USEAUDIO;

  config->video_w = 640;
  config->video_h = 480;

  config->audio_channels = 16;

  config->mouse_sensitivity = 0.05;

  return 0;
}

/*
[PUBLIC] Load configuration settings from an external file.
*/

int cfgLoad(config_s *config, const char *file_name)
{
  xml_s *document = NULL;
  xml_s *root_tag = NULL;
  xml_s *this_tag = NULL;

  if (!config) { return -666; }

  document = xmlLoadFile(file_name);
  if (!document) { return -1; }

  root_tag = xmlFindName(document->node, "UserConfig", 0);
  if (!root_tag)
  {
    xmlDelete(document);
    return -2;
  }

  for (this_tag = root_tag->node; this_tag; this_tag = this_tag->next)
  {

    xml_s *attrib = NULL;

    if (this_tag->name[0] == '#') { continue; }

    if (strcmp(this_tag->name, "Locale") == 0)
    {

      attrib = xmlFindName(this_tag->attr, "code", 0);
      if (attrib)
      {
        memset(config->language, 0, CONFIG_MAX_LANGCODE);
        strncpy(config->language, attrib->text, CONFIG_MAX_LANGCODE - 1);
      }

    }
    else if (strcmp(this_tag->name, "Display") == 0)
    {

      attrib = xmlFindName(this_tag->attr, "width", 0);
      if (attrib) { config->video_w = atof(attrib->text); }

      attrib = xmlFindName(this_tag->attr, "height", 0);
      if (attrib) { config->video_h = atof(attrib->text); }

      attrib = xmlFindName(this_tag->attr, "fullscreen", 0);
      if (attrib)
      {
        if      (strcmp(attrib->text, "true") == 0  ) { config->flags |= CFG_FLAG_FULLSCREEN; }
        else if (config->flags & CFG_FLAG_FULLSCREEN) { config->flags -= CFG_FLAG_FULLSCREEN; }
      }

    }
    else if (strcmp(this_tag->name, "Audio") == 0)
    {

      attrib = xmlFindName(this_tag->attr, "channels", 0);
      if (attrib) { config->audio_channels = atoi(attrib->text); }

      attrib = xmlFindName(this_tag->attr, "enabled", 0);
      if (attrib)
      {
        if      (strcmp(attrib->text, "true") == 0) { config->flags |= CFG_FLAG_USEAUDIO; }
        else if (config->flags & CFG_FLAG_USEAUDIO) { config->flags -= CFG_FLAG_USEAUDIO; }
      }

    }
    else if (strcmp(this_tag->name, "Mouse") == 0)
    {

      attrib = xmlFindName(this_tag->attr, "sensitivity", 0);
      if (attrib) { config->mouse_sensitivity = atof(attrib->text); }

      attrib = xmlFindName(this_tag->attr, "enabled", 0);
      if (attrib)
      {
        if      (strcmp(attrib->text, "true") == 0 ) { config->flags |= CFG_FLAG_LOCKMOUSE; }
        else if (config->flags & CFG_FLAG_LOCKMOUSE) { config->flags -= CFG_FLAG_LOCKMOUSE; }
      }

    }
    else { fprintf(stderr, "[WARNING] Unknown tag: %s\n", this_tag->name); }

  }

  xmlDelete(document);

  return 0;
}

/*
[PUBLIC] Save configuration settings to an external file.
*/

int cfgSave(config_s *config, const char *file_name)
{
  if (!config || !file_name) { return -1; }

  /* TODO */

  return 0;
}
