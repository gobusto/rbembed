/**
@file
@brief Provides functions for reading and writing input configuration files.

This file allows "physical" inputs - such as mouse movement, joystick axes, and
keyboard keys - to be mapped to "virtual" input slots. Multiple physical inputs
can be mapped to a single virtual slot; a First Person Shooter game might allow
the player to use either a joypad thumbstick or a mouse to turn the camera, for
example. The game engine itself doesn't care which kind of device is being used
to populate an input slot - it only needs to know the value that the input slot
currently contains, so that it can update the player character's position, etc.

Each "virtual" input slot is expected to store values of zero or more - zero is
used to represent "no button pressed", with values of 0.3 or 1.0 indicating the
total "pressure" being applied by all gamepad axes/keyboard keys/etc. mapped to
the slot. Note that slots aren't limited to a maximum value of 1.0, since quick
mouse movement will often generate higher values. Capping mouse-movement speeds
would cause problems (for example, in an FPS where accurate aiming is required)
and so restrictions on values over 1.0 are something that the game engine needs
to handle itself. As a side effect, this also means that mapping two buttons to
the same slot will result in a value of 2.0 if they are both pressed at once.

As mentioned above, slots cannot contain negative values. They're like buttons,
rather than axes, for two reasons: (A) Keyboard keys and joystick/mouse buttons
cannot report negative values, and (B) a physical axis value of -1.0 can offset
a physical button value of 1.0, resulting in a combined input value of zero. To
avoid this issue, physical axes are split into "positive" and "negative" pairs,
with positive values passed through normally, and negative axes inverted first.

This will commonly result in there being two "virtual" input slots - turn_right
and turn_left, for example - with one "half" of the axis mapped to each, but it
is possible to map the negative half to "shoot" and the positive half to "use",
if required...
*/

#ifndef __CQ2_INPUT_H__
#define __CQ2_INPUT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lgm/anim/util/wordtree.h"
#include "bits/type.h"

/* Input mapping flags - used to tweak input mappings in various ways. */

#define INPUT_FLAG_HATUP    0x01  /**< @brief Joypads: Only if hat is UP.    */
#define INPUT_FLAG_HATDOWN  0x02  /**< @brief Joypads: Only if hat is DOWN.  */
#define INPUT_FLAG_HATLEFT  0x04  /**< @brief Joypads: Only if hat is LEFT.  */
#define INPUT_FLAG_HATRIGHT 0x08  /**< @brief Joypads: Only if hat is RIGHT. */
#define INPUT_FLAG_NEGATIVE 0x10  /**< @brief Inverts the value of an axis.  */
#define INPUT_FLAG_UNUSED_5 0x20  /**< @brief Reserved for future use.       */
#define INPUT_FLAG_UNUSED_6 0x40  /**< @brief Reserved for future use.       */
#define INPUT_FLAG_UNUSED_7 0x80  /**< @brief Reserved for future use.       */

typedef unsigned char input_flag_t; /**< @brief Used to store mapping flags. */

/**
@brief Enumerates possible types of input hardware.

Note that KEYBOARD, for example, includes non-keyboard devices that are handled
in the same way by the operating system, such as PS2 barcode scanners.
*/

typedef enum
{

  INPUT_DEVICE_UNKNOWN,   /**< @brief Unrecognised device type.            */
  INPUT_DEVICE_KEYBOARD,  /**< @brief Keyboards.                           */
  INPUT_DEVICE_MOUSE,     /**< @brief Mice or similar pointing devices.    */
  INPUT_DEVICE_JOYPAD     /**< @brief Joysticks, Joypads, Dance Mats, etc. */

} input_device_t;

/**
@brief Enumerates possible types of input i.e. buttons or axes.

@todo Add INPUT_TYPE_BALL for joypads which support them?
*/

typedef enum
{

  INPUT_TYPE_UNKNOWN, /**< @brief Unrecognised input type.             */
  INPUT_TYPE_HAT,     /**< @brief Flight stick POV hats.               */
  INPUT_TYPE_AXIS,    /**< @brief Joypad or mouse axes.                */
  INPUT_TYPE_BUTTON   /**< @brief Keyboard keys, joypad/mouse buttons. */

} input_type_t;

/**
@brief about

writeme
*/

typedef struct
{

  input_device_t device_type; /**< @brief about */
  int device_id;              /**< @brief about */

  input_type_t input_type;    /**< @brief about */
  int input_id;               /**< @brief about */

  input_flag_t flags;         /**< @brief about */
  float  *slot;               /**< @brief about */

} input_map_s;

/**
@brief about

writeme
*/

typedef struct
{

  wordtree_s *slot; /**< @brief about */

  int num_items;      /**< @brief about */
  input_map_s **item; /**< @brief about */

} input_set_s;

/**
@brief Create a new input mapping set structure from a configuration file.

@param file_name The path of the configuration file to be loaded.
@return A new input mapping set on success, or NULL on failure.
*/

input_set_s *inputLoad(const char *file_name);

/**
@brief Write an input mapping set structure to a configuration file.

@param file_name The path of the configuration file to be saved to.
@return Zero on success or non-zero on failure.
*/

int inputSave(const char *file_name);

/**
@brief Destroy a previously-created input mapping set.

@param input_set The input mapping set to be deleted.
@return Always returns NULL.
*/

input_set_s *inputFree(input_set_s *input_set);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_INPUT_H__ */
