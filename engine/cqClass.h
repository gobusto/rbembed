/**
@file
@brief about

writeme
*/

#ifndef __CQ2_CLASS_H__
#define __CQ2_CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lgm/mesh/mesh.h"
#include "bits/type.h"

/**
@brief about

writeme
*/

typedef struct
{

  char               *name;     /**< @brief about */
  mesh_s             *mesh;     /**< @brief about */
  wordtree_s         *anim;     /**< @brief about */
  struct _material_s *material; /**< @brief Points to internal texture data. */
  struct _physics_s  *physics;  /**< @brief Points to internal physics data. */

} class_s;

/**
@brief about

writeme

@param file_name about
@return writeme
*/

int classLoad(const char *file_name);

/**
@brief about

writeme
*/

void classQuit(void);

/**
@brief about

writeme

@param name about
@return writeme
*/

class_s *classFind(const char *name);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_CLASS_H__ */
