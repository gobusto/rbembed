/**
@file
@brief Handles everything related to graphics, either 2D or 3D.

Note that this file may be referenced either "internally" by the engine itself,
or "externally" by a renderer .DLL loaded into the engine at runtime.
*/

#ifndef __CQ2_GRAPHICS_H__
#define __CQ2_GRAPHICS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cqActor.h"
#include "cqLamp.h"
#include "cqPhysics.h"
#include "cqSprite.h"
#include "bits/type.h"

/* Flags for sprite images - see `gfxCacheImage()` below... */

#define IMAGE_FLAG_NEAREST 0x01 /**< @brief Use "nearest-neighbor" filtering. */
#define IMAGE_FLAG_CLAMP_X 0x02 /**< @brief Clamp the X axis (no repetition). */
#define IMAGE_FLAG_CLAMP_Y 0x04 /**< @brief Clamp the Y axis (no repetition). */
#define IMAGE_FLAG_UNUSED3 0x08 /**< @brief Not currently used. */
#define IMAGE_FLAG_UNUSED4 0x10 /**< @brief Not currently used. */
#define IMAGE_FLAG_UNUSED5 0x20 /**< @brief Not currently used. */
#define IMAGE_FLAG_UNUSED6 0x40 /**< @brief Not currently used. */
#define IMAGE_FLAG_UNUSED7 0x80 /**< @brief Not currently used. */

/**
@brief This structure defines the position and orientation of a virtual camera.

It is used by `gfxDrawGame()` to determine the viewpoint from which to render a
scene.

@todo Maybe use a radians-based FOV for consistency with the rest of the engine?
*/

typedef struct
{

  v2d_real_t xyz[3];  /**< @brief The 3D position of a camera in world space. */
  quaternion_t quat;  /**< @brief The orientation of a camera in world space. */
  v2d_real_t fov; /**< @brief The vertical "field-of-view" angle, in degrees. */

} camera_s;

/**
@brief This structure contains various "set-up" details for a renderer.

Its main purpose is to make various functions from the engine available to .DLL
renderers after they're loaded in at runtime - see `gfxInit()` for details.
*/

typedef struct
{

  int w;  /**< @brief The width of the display area in pixels. */
  int h;  /**< @brief The height of the display area in pixels. */

  void (*swap_func)(void); /**< @brief An optional buffer-swapping function. */

  /* The following methods need to be made available to external renderers: */

  PHYS_ACTOR_IS_RAGDOLL((*ragdoll_func));         /**< @brief See `physActorIsRagdoll()`     */
  PHYS_ACTOR_GET_POSITION((*position_func));      /**< @brief See `physActorgetPosition()`   */
  PHYS_ACTOR_GET_QUATERNION((*quaternion_func));  /**< @brief See `physActorGetQuaternion()` */
  ACTOR_UPDATE_MESH((*mesh_func));                /**< @brief See `actorUpdateMesh()`        */

} render_init_s;

/**
@brief Get the "name" of the renderer being used.

This doesn't really do anything - maybe it should be removed in the future...?

@return The "name" of the renderer used, such as "Basic OpenGL 1.x Renderer".
*/

const char *gfxRendererName(void);

/**
@brief Get the API version of the renderer being used.

This is only really necessary when an external renderer is loaded in from a DLL,
to ensure that the API version used by the .DLL file matches the one expected by
the game engine.

@return A string of text representing the API version used by the renderer.
*/

const char *gfxApiVersion(void);

/**
@brief Initialise the graphics sub-system.

Call this before doing anything else.

@param argc Length of the argv array.
@param argv Array of command-line program arguments.
@param render_init about
@return `NULL` on success, or a string of text describing the error on failure.
*/

const char *gfxInit(int argc, char **argv, const render_init_s *render_init);

/**
@brief Shut down the graphics sub-system and free any allocated memory.

Call this when the program ends.
*/

void gfxQuit(void);

/**
@brief Set up anything related to the game world when a new map file is loaded.

This function loads the world mesh file and associated textures, precalculates
lighting, culling information, etc.

Some maps might not actually have a mesh file (in which case `file_name` should
be `NULL`), since it's possible that they might only contain actors floating in
a "void".

@param file_name The mesh file to be used for the "environment" of the map.
@param lamp A linked list of lights present within the game world.
@return Zero on success, or non-zero on failure.
*/

int gfxWorldCreate(const char *file_name, const lamp_s *lamp);

/**
@brief Free any memory allocated by `gfxWorldCreate()`.
*/

void gfxWorldDelete(void);

/**
@brief Set up various things for a given actor class when it's first loaded.

This will generally include things like loading textures, but not a model file;
that's done beforehand, in the actor-class code itself, since the model is also
used for other purposes (such as physics).

@param klass The actor class to initialise.
@return Zero on success, or non-zero on failure.
*/

int gfxClassCreate(class_s *klass);

/**
@brief Free any memory allocated by `gfxClassCreate()`.

@param klass The actor class whose resources are to be deallocated.
*/

void gfxClassDelete(class_s *klass);

/**
@brief Initialise a new 2D sprite instance.

@param sprite The sprite instance to initialise.
@return Zero on success, or non-zero on failure.
*/

int gfxSpriteCreate(sprite_s *sprite);

/**
@brief Free any memory allocated by `gfxSpriteCreate()`.

@param sprite The sprite instance whose resources are to be deallocated.
@return Zero on success, or non-zero on failure.
*/

void gfxSpriteDelete(sprite_s *sprite);

/**
@brief Load a new image, and assign a name to it.

This is only ever called by `spriteCacheImageSet()` to load new images.

@param name The "name" string that will be used to reference this image later.
@param file_name The name (and path) of the image file to load.
@param flags Various flags - see the `IMAGE_FLAG_*` constants for details.
@return Zero on success, or non-zero on failure.
*/

int gfxCacheImage(const char *name, const char *file_name, v2d_flag_t flags);

/**
@brief Change the image used by a particular sprite instance.

@param sprite The sprite instance to modify.
@param image The name of the cached image to be used by this sprite.
@return Zero on success, or non-zero on failure.
*/

int gfxSpriteSetImage(sprite_s *sprite, const char *image);

/**
@brief Draw everything in the game world.

@param camera Defines the position, orientation, and FOV of the virtual camera.
@param actor A linked list of actor structures present in the game world.
@param sprite_set A (possibly empty) set of sprite instances.
@param lamp A linked list of lamp structures present in the game world.
*/

void gfxDrawGame(const camera_s *camera, actor_s *actor, wordtree_s *sprite_set, const lamp_s *lamp);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_GRAPHICS_H__ */
