/**
@file
@brief Stubs for the functions in `cqGraphics.h`, delegated to a DLL at runtime.

This file can be compiled into the engine in place of `cqGraphics_GL1.c`, making
it possible to load in an "external" renderer from a .dll/.so/.dylib without any
changes to the main game engine executable after compilation.

This is currently implemented with SDL, to avoid faffing around with OS-specific
DLL stuff.
*/

#include <SDL2/SDL.h>
#include <string.h>
#include "cqGraphics.h"

#include <stdio.h>  /* For debugging; could be removed later. */

/* No "ISO C forbids conversion of object pointer to function pointer type": */
#pragma GCC diagnostic ignored "-Wpedantic"

/* Determine which OS-dependent dynamic library file extension to use: */
#ifdef __APPLE__
  #define GFX_DLL_EXT "dylib"
#else
  #ifdef _WIN32
      #define GFX_DLL_EXT "dll"
  #else
    #define GFX_DLL_EXT "so"
  #endif
#endif

/* Function signature macros for convenience - avoids repetition below... */
#define GFX_RENDERER_NAME(x) const char *x(void)
#define GFX_API_VERSION(x) const char *x(void)
#define GFX_INIT(x) const char *x(int argc, char **argv, const render_init_s *render_init)
#define GFX_QUIT(x) void x(void)
#define GFX_WORLD_CREATE(x) int x(const char *file_name, const lamp_s *lamp)
#define GFX_WORLD_DELETE(x) void x(void)
#define GFX_CLASS_CREATE(x) int x(class_s *klass)
#define GFX_CLASS_DELETE(x) void x(class_s *klass)
#define GFX_SPRITE_CREATE(x) int x(sprite_s *sprite)
#define GFX_SPRITE_DELETE(x) void x(sprite_s *sprite)
#define GFX_CACHE_IMAGE(x) int x(const char *name, const char *file_name, v2d_flag_t flags)
#define GFX_SPRITE_SET_IMAGE(x) int x(sprite_s *sprite, const char *image)
#define GFX_DRAW_GAME(x) void x(const camera_s *camera, actor_s *actor, wordtree_s *sprite_set, const lamp_s *lamp)

/* These variables store pointers to various functions within the DLL: */
GFX_RENDERER_NAME((*gfxRendererName_func));
GFX_API_VERSION((*gfxApiVersion_func));
GFX_INIT((*gfxInit_func));
GFX_QUIT((*gfxQuit_func));
GFX_WORLD_CREATE((*gfxWorldCreate_func));
GFX_WORLD_DELETE((*gfxWorldDelete_func));
GFX_CLASS_CREATE((*gfxClassCreate_func));
GFX_CLASS_DELETE((*gfxClassDelete_func));
GFX_SPRITE_CREATE((*gfxSpriteCreate_func));
GFX_SPRITE_DELETE((*gfxSpriteDelete_func));
GFX_CACHE_IMAGE((*gfxCacheImage_func));
GFX_SPRITE_SET_IMAGE((*gfxSpriteSetImage_func));
GFX_DRAW_GAME((*gfxDrawGame_func));

/**
@brief This variable is used to store a pointer to the dynamic library itself.

We load this DLL when `gfxInit()` is called and free it again in `gfxQuit()`. In
most cases, the other function stubs simply "pass through" to the functions from
the DLL, without doing anything themselves.
*/

void *gfx_dll = NULL;

/*
[PUBLIC] Get the "name" of the renderer being used.
*/

GFX_RENDERER_NAME(gfxRendererName)
{
  if (gfxRendererName_func) { return gfxRendererName_func(); }
  return "Dynamic ." GFX_DLL_EXT " file";
}

/*
[PUBLIC] Get the API version of the renderer being used.
*/

GFX_API_VERSION(gfxApiVersion) { return "Conqore2_2017-04-14"; }

/*
[PUBLIC] Initialise the graphics sub-system.
*/

GFX_INIT(gfxInit)
{
  const char *file_name = "./render_basic." GFX_DLL_EXT;
  int i;

  gfxQuit();

  /* If the user has specified some other renderer, use that one instead: */
  for (i = 1; i < argc; ++i)
  {
    if (strcmp(argv[i], "--renderer") == 0) { file_name = argv[++i]; }
  }

  /* Attempt to load the specified DLL file (seems to require a `./` prefix): */
  gfx_dll = SDL_LoadObject(file_name);
  if (!gfx_dll) { return SDL_GetError(); }

  /* Ensure that the DLL file implements the correct version of the API: */
  gfxRendererName_func = (GFX_RENDERER_NAME((*)))SDL_LoadFunction(gfx_dll, "gfxRendererName");
  gfxApiVersion_func = (GFX_API_VERSION((*)))SDL_LoadFunction(gfx_dll, "gfxApiVersion");

  if (!gfxApiVersion_func || strcmp(gfxApiVersion(), gfxApiVersion_func()) != 0)
  { return "Renderer API version mismatch."; }

  /* Set up the rest of the function pointers: */
  gfxInit_func = (GFX_INIT((*)))SDL_LoadFunction(gfx_dll, "gfxInit");
  gfxQuit_func = (GFX_QUIT((*)))SDL_LoadFunction(gfx_dll, "gfxQuit");
  gfxWorldCreate_func = (GFX_WORLD_CREATE((*)))SDL_LoadFunction(gfx_dll, "gfxWorldCreate");
  gfxWorldDelete_func = (GFX_WORLD_DELETE((*)))SDL_LoadFunction(gfx_dll, "gfxWorldDelete");
  gfxClassCreate_func = (GFX_CLASS_CREATE((*)))SDL_LoadFunction(gfx_dll, "gfxClassCreate");
  gfxClassDelete_func = (GFX_CLASS_DELETE((*)))SDL_LoadFunction(gfx_dll, "gfxClassDelete");
  gfxSpriteCreate_func = (GFX_SPRITE_CREATE((*)))SDL_LoadFunction(gfx_dll, "gfxSpriteCreate");
  gfxSpriteDelete_func = (GFX_SPRITE_DELETE((*)))SDL_LoadFunction(gfx_dll, "gfxSpriteDelete");
  gfxCacheImage_func = (GFX_CACHE_IMAGE((*)))SDL_LoadFunction(gfx_dll, "gfxCacheImage");
  gfxSpriteSetImage_func = (GFX_SPRITE_SET_IMAGE((*)))SDL_LoadFunction(gfx_dll, "gfxSpriteSetImage");
  gfxDrawGame_func = (GFX_DRAW_GAME((*)))SDL_LoadFunction(gfx_dll, "gfxDrawGame");

  /* Now that it's loaded, try to call the `gfxInit()` of the DLL renderer: */
  return gfxInit_func ? gfxInit_func(argc, argv, render_init) : "gfxInit() not found.";
}

/*
[PUBLIC] Shut down the graphics sub-system and free any allocated memory.
*/

GFX_QUIT(gfxQuit)
{
  if (gfxQuit_func) { gfxQuit_func(); }

  /* Reset all function pointers to NULL, to ensure they can't be called: */
  gfxRendererName_func = NULL;
  gfxApiVersion_func = NULL;
  gfxInit_func = NULL;
  gfxQuit_func = NULL;
  gfxWorldCreate_func = NULL;
  gfxWorldDelete_func = NULL;
  gfxClassCreate_func = NULL;
  gfxClassDelete_func = NULL;
  gfxSpriteCreate_func = NULL;
  gfxSpriteDelete_func = NULL;
  gfxCacheImage_func = NULL;
  gfxSpriteSetImage_func = NULL;
  gfxDrawGame_func = NULL;

  /* Free the DLL from memory: */
  SDL_UnloadObject(gfx_dll);
  gfx_dll = NULL;
}

/*
[PUBLIC] writeme
*/

GFX_WORLD_CREATE(gfxWorldCreate)
{
  return gfxWorldCreate_func ? gfxWorldCreate_func(file_name, lamp) : 666;
}

/*
[PUBLIC] writeme
*/

GFX_WORLD_DELETE(gfxWorldDelete)
{
  if (gfxWorldDelete_func) gfxWorldDelete_func();
}

/*
[PUBLIC] writeme
*/

GFX_CLASS_CREATE(gfxClassCreate)
{
  return gfxClassCreate_func ? gfxClassCreate_func(klass) : 666;
}

/*
[PUBLIC] writeme
*/

GFX_CLASS_DELETE(gfxClassDelete)
{
  if (gfxClassDelete_func) gfxClassDelete_func(klass);
}

/*
[PUBLIC] writeme
*/

GFX_SPRITE_CREATE(gfxSpriteCreate)
{
  return gfxSpriteCreate_func ? gfxSpriteCreate_func(sprite) : 666;
}

/*
[PUBLIC] writeme
*/

GFX_SPRITE_DELETE(gfxSpriteDelete)
{
  if (gfxSpriteDelete_func) gfxSpriteDelete_func(sprite);
}

/*
[PUBLIC] writeme
*/

GFX_CACHE_IMAGE(gfxCacheImage)
{
  return gfxCacheImage_func ? gfxCacheImage_func(name, file_name, flags) : 666;
}

/*
[PUBLIC] writeme
*/

GFX_SPRITE_SET_IMAGE(gfxSpriteSetImage)
{
  return gfxSpriteSetImage_func ? gfxSpriteSetImage_func(sprite, image) : 666;
}

/*
[PUBLIC] Draw everything in the game world.
*/

GFX_DRAW_GAME(gfxDrawGame)
{
  if (gfxDrawGame_func) gfxDrawGame_func(camera, actor, sprite_set, lamp);
}
