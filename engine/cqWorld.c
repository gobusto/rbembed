/**
@file
@brief Provides functions for loading and managing game map files.

The functions in this file call out to other parts of the engine (such as actor
logic, physics, and scripting), so it serves as a "manager" for the game state.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqAudio.h"
#include "cqGraphics.h"
#include "cqPhysics.h"
#include "cqScript.h"
#include "cqWorld.h"
#include "xml/xml.h"

/* Global objects and variables. */

v2d_chain_s *cq_world = NULL; /**< @brief A linked list of actor instances. */
v2d_chain_s *cq_lamp = NULL;  /**< @brief A linked list of light instances. */

char *map_file_name = NULL;   /**< @brief The file name for the CURRENT map. */
xml_s *map_file_data = NULL;  /**< @brief The XML file data of the NEXT map. */

/**
@brief Clear out any old map data prior to (possibly) loading a new one.

@param force_actor_deletion If true, *force* any current actors to be deleted.
*/

static void worldClearOld(v2d_bool_t force_actor_deletion)
{
  actor_s *a = worldGetActor();
  lamp_s *lamp;

  gfxWorldDelete();
  physWorldDelete();

  while (a)
  {
    int keep = scriptCall(a, EVENT_MAPCHANGE, map_file_name, &force_actor_deletion);
    a = (!keep || force_actor_deletion) ? actorDelete(a) : actorGetNext(a);
  }

  for (lamp = worldGetLamp(); lamp; lamp = lampDelete(lamp)) { }

  if (force_actor_deletion)
  {
    spriteInit();
    v2dChainDelete(cq_world); cq_world = NULL;
    v2dChainDelete(cq_lamp); cq_lamp = NULL;
  }
}

/**
@brief Actually change to a new map; `worldLoadMap()` just "queues" one.

@param load_actors If `false`, actors are not loaded; used when loading saves.
@return `V2D_TRUE` on success, or `V2D_FALSE` on failure.
*/

static v2d_bool_t worldChangeMap(v2d_bool_t load_actors)
{
  xml_s *root_tag = xmlFindName(map_file_data->node, "ConqoreMap", 0);
  xml_s *this_tag = NULL;
  xml_s *attrib   = NULL;

  /* Clear out any old data first. */
  worldClearOld(map_file_data->user ? V2D_TRUE : V2D_FALSE);

  /* If no actor list structure currently exists, create one. */
  if (!cq_world && !(cq_world = v2dChainCreate(0, NULL)))
  {
    fprintf(stderr, "[ERROR] Couldn't allocate memory for actors!\n");
    return V2D_FALSE;
  }

  /* If no lamp list structure currently exists, create one. */
  if (!cq_lamp && !(cq_lamp = v2dChainCreate(0, NULL)))
  {
    fprintf(stderr, "[ERROR] Couldn't allocate memory for lights!\n");
    return V2D_FALSE;
  }

  /* Load lights. */
  attrib = xmlFindName(root_tag->attr, "lights", 0);
  lampLoad(attrib ? attrib->text : NULL);

  /* Initialise graphics. Note that any preprocessing might take a while! */
  attrib = xmlFindName(root_tag->attr, "mesh", 0);
  gfxWorldCreate(attrib ? attrib->text : NULL, worldGetLamp());

  /* Initialise physics. Use the visual mesh if no collision mesh is given. */
  attrib = xmlFindName(root_tag->attr, "collide", 0);
  if (!attrib) { attrib = xmlFindName(root_tag->attr, "mesh", 0); }
  physWorldCreate(attrib ? attrib->text : NULL);

  /* Create actors. Done last, as any 3D sounds they add need updating ASAP: */
  for (this_tag = root_tag->node; load_actors && this_tag; this_tag = this_tag->next)
  {
    actor_s *actor = NULL;
    v2d_real_t x, y, z, a;

    if (this_tag->name[0] == '#') { continue; }

    if (strcmp(this_tag->name, "Actor") != 0)
    {
      fprintf(stderr, "[WARNING] Unknown tag type: %s\n", this_tag->name);
      continue;
    }

    attrib = xmlFindName(this_tag->attr, "x", 0);
    x = attrib ? atof(attrib->text) : 0;

    attrib = xmlFindName(this_tag->attr, "y", 0);
    y = attrib ? atof(attrib->text) : 0;

    attrib = xmlFindName(this_tag->attr, "z", 0);
    z = attrib ? atof(attrib->text) : 0;

    attrib = xmlFindName(this_tag->attr, "angle", 0);
    a = attrib ? atof(attrib->text) : 0;

    attrib = xmlFindName(this_tag->attr, "class", 0);
    actor = actorCreate(attrib ? attrib->text : "", x, y, z, a);

    for (attrib = this_tag->node; attrib; attrib = attrib->next)
    {
      const xml_s *name = xmlFindName(attrib->attr, "name", 0);
      const xml_s *value = xmlFindName(attrib->attr, "value", 0);

      if (attrib->name[0] == '#') { continue; }

      if (strcmp(attrib->name, "Message") != 0)
      {
        fprintf(stderr, "[WARNING] Unknown tag type: %s\n", attrib->name);
        continue;
      }

      scriptCall(actor, EVENT_MESSAGE, name ? name->text : "", value ? value->text : "");
    }
  }

  /* Play music, if required. */
  attrib = xmlFindName(root_tag->attr, "music", 0);
  if (attrib) { sndPlayMusic(attrib->text, -1); }

  fprintf(stderr, "[DEBUG] Map loaded successfully: %s\n", map_file_name);
  return V2D_TRUE;
}

/*
[PUBLIC] Specify a new map file to be loaded at the next possible opportunity.
*/

v2d_bool_t worldLoadMap(const char *file_name, v2d_bool_t force_actor_deletion)
{
  char *new_map_file_name;

  /* If we already have another map "queued" to be loaded, refuse this one: */
  if (map_file_data)
  {
    fprintf(stderr, "[WARNING] A new map is already queued - ignored...\n");
    return V2D_FALSE;
  }

  /* Load the requested file. */
  map_file_data = xmlLoadFile(file_name);
  if (!map_file_data)
  {
    fprintf(stderr, "[ERROR] Couldn't load %s\n", file_name ? file_name : "NULL");
    return V2D_FALSE;
  }

  /* Ensure that it's valid: */
  if (!xmlFindName(map_file_data->node, "ConqoreMap", 0))
  {
    fprintf(stderr, "[ERROR] %s is not a valid Conqore map file\n", file_name);
    xmlDelete(map_file_data);
    map_file_data = NULL;
    return V2D_FALSE;
  }

  /* Allocate memory to store the new file name: */
  new_map_file_name = (char*)malloc(strlen(file_name) + 1);
  if (!new_map_file_name)
  {
    fprintf(stderr, "[ERROR] malloc() failed; couldn't store %s\n", file_name);
    xmlDelete(map_file_data);
    map_file_data = NULL;
    return V2D_FALSE;
  }

  /* Switch the "real" file name to point to the newly-allocated buffer: */
  free(map_file_name);
  map_file_name = new_map_file_name;
  strcpy(map_file_name, file_name);

  /* Store the "force actor deletion" flag as "user data" + report success! */
  map_file_data->user = force_actor_deletion ? map_file_data : NULL;
  return V2D_TRUE;
}

/*
[PUBLIC] Free any currently-existing map data.
*/

void worldDestroy(void)
{
  /* Destroy the current game world itself: */
  worldClearOld(V2D_TRUE);
  /* There will be no new map, so get rid of the "current map file" string: */
  free(map_file_name);
  map_file_name = NULL;
  /* If we were about to switch to a new map, get rid of the XML data too: */
  xmlDelete(map_file_data);
  map_file_data = NULL;
}

/*
[PUBLIC] Save the world state to a file.
*/

v2d_bool_t worldSave(const char *file_name)
{
  FILE *sav;
  actor_s *actor = worldGetActor();

  if (!map_file_name || !file_name) { return V2D_FALSE; }
  sav = fopen(file_name, "wb");
  if (!sav) { return V2D_FALSE; }

  fprintf(sav, "{\n");
  fprintf(sav, "\t\"version\": \"CQSAVEV0\",\n");
  fprintf(sav, "\t\"mapfile\": \"%s\",\n", map_file_name);

  fprintf(sav, "\t\"actors\": [\n");
  for (; actor; actor = actorGetNext(actor))
  {
    int i;

    fprintf(sav, "\t\t{\n");
    fprintf(sav, "\t\t\t\"class\": \"%s\",\n", actor->klass->name);

    fprintf(sav, "\t\t\t\"gravity\": %s,\n", physActorGetGravity(actor) ? "true" : "false");

    fprintf(sav, "\t\t\t\"physics\": [\n");
    for (i = 0; V2D_TRUE; ++i)
    {
      float v[3];
      quaternion_t q;

      if (physActorGetQuaternion(actor, i, q) != 0) { break; }

      fprintf(sav, "\t\t\t\t{\n");
      fprintf(sav, "\t\t\t\t\t\"quaternion\": [%f, %f, %f, %f],\n", q[0], q[1], q[2], q[3]);
      physActorGetPosition(actor, i, v);
      fprintf(sav, "\t\t\t\t\t\"position\": [%f, %f, %f],\n", v[0], v[1], v[2]);
      physActorGetVelocity(actor, i, v);
      fprintf(sav, "\t\t\t\t\t\"velocity\": [%f, %f, %f]\n", v[0], v[1], v[2]);
      fprintf(sav, "\t\t\t\t}%s\n", physActorGetQuaternion(actor, i+1, q) == 0 ? "," : "");
    }
    fprintf(sav, "\t\t\t],\n");

    /* TODO: Include script state data. */

    /* TODO: Include animation state data. */

    fprintf(sav, "\t\t\t\"texture_offset\": [%f, %f]\n", actor->texture_offset[0], actor->texture_offset[1]);
    fprintf(sav, "\t\t}%s\n", actorGetNext(actor) ? "," : "");
  }
  fprintf(sav, "\t]\n");

  /* TODO: Include sprite state data. */

  fprintf(sav, "}\n");
  fclose(sav);
  return V2D_TRUE;
}

/*
[PUBLIC] Load the world state from a file.
*/

v2d_bool_t worldRestore(const char *file_name)
{
  if (!file_name) { return V2D_FALSE; }
  fprintf(stderr, "[TODO] worldRestore() is UNIMPLEMENTED!\n");
  return V2D_FALSE;
}

/*
[PUBLIC] Update the state of all objects in the world.
*/

v2d_bool_t worldUpdate(void)
{
  v2d_bool_t map_has_changed = V2D_FALSE;
  actor_s *a = NULL;

  if (map_file_data)
  {
    map_has_changed = V2D_TRUE;
    if (!worldChangeMap(V2D_TRUE)) { /* TODO: Panic, I guess...? */ }
    xmlDelete(map_file_data);
    map_file_data = NULL;
  }

  for (a = worldGetActor(); a; a = actorUpdate(a) ? actorDelete(a) : actorGetNext(a)) {}
  physUpdate();

  return map_has_changed;
}

/*
[PUBLIC] Get the first actor structure in the linked list of world actors.
*/

actor_s *worldGetActor(void)
{
  if (!cq_world) { return NULL; }
  return (actor_s*)(cq_world->first ? cq_world->first->data : NULL);
}

/*
[PUBLIC] Get the first lamp structure in the linked list of world lamps.
*/

lamp_s *worldGetLamp(void)
{
  if (!cq_lamp) { return NULL; }
  return (lamp_s*)(cq_lamp->first ? cq_lamp->first->data : NULL);
}

/*
[PUBLIC] Get the "chain" structure used to store all actors in the world.
*/

v2d_chain_s *worldGetActorChain(void) { return cq_world; }

/*
[PUBLIC] Get the "chain" structure used to store all lamps in the world.
*/

v2d_chain_s *worldGetLampChain(void) { return cq_lamp; }
