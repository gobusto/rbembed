/**
@file
@brief Provides structures and functions for handling in-game lamp instances.
*/

#ifndef __CQ2_LAMP_H__
#define __CQ2_LAMP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/chain.h"
#include "bits/type.h"

/**
@brief Get the next lamp structure in a linked-list of lamp structures.

This macro is provided for convenience, in order to avoid casting everywhere...

@param item The current lamp in the linked-list.
@return The next lamp in the linked-list, or NULL if there isn't one.
*/

#define lampGetNext(item) ((lamp_s*)(item ? v2dLinkGetNext(item->link) : 0))

/**
@brief A generic "lamp" structure, used to represent in-game lamps.

@todo Support a wider variety of lamps, not just basic "point" lamps...

Note that `lamp_s` is used to avoid conflicting with `light_s` in `mesh/`.
*/

typedef struct
{

  v2d_real_t origin[3]; /**< @brief The XYZ position of the lamp. */
  v2d_real_t radius;    /**< @brief The effective radius of the lamp. */
  v2d_real_t rgb[3];    /**< @brief The RGB colour of the lamp. */

  v2d_link_s *link; /**< @brief Linked list data. */

} lamp_s;

/**
@brief Delete a previously-created lamp structure.

@param lamp The lamp to be deleted.
@return The next lamp in the linked list, if there is one, or NULL if not.
*/

lamp_s *lampDelete(lamp_s *lamp);

/**
@brief Create a new lamp.

@param x The initial X position of the lamp.
@param y The initial Y position of the lamp.
@param z The initial Z position of the lamp.
@param r The radius of the lamp.
@return A new lamp structure on success, or NULL on failure.
*/

lamp_s *lampCreate(v2d_real_t x, v2d_real_t y, v2d_real_t z, v2d_real_t r);

/**
@brief Load a set of lamp definitions from a file.

@param file_name The file containing the lamp definitions.
@return The number of lamps created.
*/

int lampLoad(const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_LAMP_H__ */
