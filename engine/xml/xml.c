/**
@file xml.c

@brief Provides utilities for loading, saving, and manipulating XML data/files.

IMPORTANT: Unless noted otherwise, all text strings are stored in UTF-8 format.

Copyright (C) 2013-2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xml.h"
#include "unicode.h"

/*
[PUBLIC] Create a new XML node/attribute structure.
*/

xml_s *xmlCreate(const char *name, const char *text)
{

  xml_s *xml = NULL;

  xml = (xml_s*)malloc(sizeof(xml_s));
  if (!xml) { return NULL; }
  memset(xml, 0, sizeof(xml_s));

  if (xmlSetName(xml, name) != 0) { return xmlDelete(xml); }
  if (xmlSetText(xml, text) != 0) { return xmlDelete(xml); }

  #ifdef NDEBUG
    fprintf(stderr, "[DEBUG] Created '%s'\n", xml->name);
  #endif

  return xml;

}

/*
[PUBLIC] Delete an XML node/attribute.
*/

xml_s *xmlDelete(xml_s *xml)
{

  xml_s *next = NULL;

  if (!xml) { return NULL; }

  #ifdef NDEBUG
    fprintf(stderr, "[DEBUG] Deleting '%s'\n", xml->name ? xml->name : "");
  #endif

  if (xml->name) { free(xml->name); }
  if (xml->text) { free(xml->text); }

  while (xml->node) { xml->node = xmlDelete(xml->node); }
  while (xml->attr) { xml->attr = xmlDelete(xml->attr); }

  next = xmlUnlink(xml);
  free(xml);
  return next;

}

/*
[PUBLIC] Unlink an XML node/attribute from both its parent and any siblings.
*/

xml_s *xmlUnlink(xml_s *xml)
{

  xml_s *next = NULL;

  if (!xml) { return NULL; }

  if (xml->owner)
  {
    if (xml->owner->node == xml) { xml->owner->node = xml->next; }
    if (xml->owner->attr == xml) { xml->owner->attr = xml->next; }
  }

  if (xml->prev) { xml->prev->next = xml->next; }
  if (xml->next) { xml->next->prev = xml->prev; }

  next = xml->next;
  xml->owner = NULL;
  xml->prev = NULL;
  xml->next = NULL;
  return next;

}

/*
[PUBLIC] Set the name value of an XML node or attribute.
*/

int xmlSetName(xml_s *xml, const char *name)
{

  if (!xml) { return -1; }

  if (name)
  {

    /* TODO: Ensure that this is a valid XML node/attribute name! */

  } else { name = "#document"; }

  if (xml->name) { free(xml->name); }
  xml->name = (char*)malloc(strlen(name) + 1);
  if (!xml->name) { return 1; }
  strcpy(xml->name, name);

  return 0;

}

/*
[PUBLIC] Set the text value of an XML node or attribute.
*/

int xmlSetText(xml_s *xml, const char *text)
{

  if (!xml) { return -1; }

  if (text)
  {

    /* TODO: Ensure that this is valid UTF-8 text! */

  } else { text = ""; }

  free(xml->text);
  xml->text = (char*)malloc(strlen(text) + 1);
  if (!xml->text) { return 1; }
  strcpy(xml->text, text);

  return 0;

}

/*
[PUBLIC] Set the text value of an XML node or attribute.
*/

int xmlAddText(xml_s *xml, const char *text)
{

  char *temp = NULL;
  size_t len = 0;
  int result = 0;

  if (!xml) { return -1; }
  if (!text) { text = ""; }

  len = strlen(xml->text);
  temp = (char*)malloc(len + strlen(text) + 1);
  if (!temp) { return 1; }

  strcpy(temp, xml->text);
  strcpy(&temp[len], text);
  result = xmlSetText(xml, temp);
  free(temp);

  return result;

}

/*
[PUBLIC] Assign a node as the child of another.
*/

xml_s *xmlAddChildNode(xml_s *owner, xml_s *node)
{

  /* If no owner exists, delete the child node and report failure. */

  if (!owner)
  {
    xmlDelete(node);
    return NULL;
  }
  else if (!node) { return NULL; }

  /* Unlink the attribute from any existing owner/siblings. */

  xmlUnlink(node);

  /* Insert the new child node at the very end of the linked list. */

  node->owner = owner;

  for (node->prev = owner->node; node->prev; node->prev = node->prev->next)
  {
    if (!node->prev->next) { node->prev->next = node; break; }
  }

  if (!owner->node) { owner->node = node; }

  /* Success. */

  return node;

}

/*
[PUBLIC] Assign an attribute to a node.
*/

xml_s *xmlAddAttribute(xml_s *owner, xml_s *attr)
{

  xml_s *item = NULL;

  /* If no owner exists, delete the attribute and report failure. */

  if (!owner)
  {
    xmlDelete(attr);
    return NULL;
  }
  else if (!attr) { return NULL; }

  /* Unlink the attribute from any existing owner/siblings. */

  xmlUnlink(attr);

  /* Remove any existing attributes with the same name from the linked list. */

  attr->owner = owner;

  for (item = owner->attr; item; )
  {

    if (strcmp(attr->name, item->name) == 0)
    {
      attr->prev = item->prev;
      item = xmlDelete(item);
    }
    else
    {
      attr->prev = item;
      item = item->next;
    }

  }

  if (attr->prev) { attr->prev->next = attr; }
  else            { owner->attr      = attr; }

  /* Success. */

  return attr;

}

/**
@brief [INTERNAL] Get a single unicode character from a string of XML text.

Most characters are simply passed back as-is, but sequences starting with an
ampersand are converted into the value that they represent.

NOTE: External entity definitions are not handled here. As a special case, they
are reported as a negative byte length, so that the calling function can detect
and handle them if they occur.

@param text A null-terminated string of XML text data.
@param value An optional output variable for storing the unicode character in.
@return The number of bytes read in on success, or zero on error.
*/

static int xmlLoadChar(const char *text, long *value)
{

  long utf = 0;
  int len, base, i;

  if (!text) { return 0; }

  if      (text[0] != '&') { len = utfDecodeUTF8(text, &utf); }
  else if (text[1] != '#')
  {
    if      (strncmp(text, "&lt;",   4) == 0) { len = 4; utf = '<'; }
    else if (strncmp(text, "&gt;",   4) == 0) { len = 4; utf = '>'; }
    else if (strncmp(text, "&amp;",  5) == 0) { len = 5; utf = '&'; }
    else if (strncmp(text, "&quot;", 6) == 0) { len = 6; utf = '"'; }
    else if (strncmp(text, "&apos;", 6) == 0) { len = 6; utf = '\''; }
    else /* Seems like an external entity. */ { return -1;          }
  }
  else
  {

    if (text[2] == 'x') { base = 16; len = 3; }
    else                { base = 10; len = 2; }

    for (i = len; text[i] != ';'; i++)
    {
      if      (text[i] >= '0' && text[i] <= '9'              ) { /* OK! */ }
      else if (text[i] >= 'A' && text[i] <= 'F' && base == 16) { /* OK! */ }
      else if (text[i] >= 'a' && text[i] <= 'f' && base == 16) { /* OK! */ }
      else                                                     { return 0; }
    }

    utf = strtol(&text[len], NULL, base);

    len = i + 1;
    text += len;

  }

  /* Return the result. */

  if (value) { *value = utf; }
  return len;

}

/**
@brief [INTERNAL] Read a string of text from an XML data buffer.

This function replaces any INTERNAL entity references with the character that
they represent. If an unrecognised (external?) entity reference is found, the
function stops parsing and returns a pointer to that point of the XML text.

@param text A null-terminated string of XML text data.
@param eol about
@param num_bytes Optional; Stores the number of bytes needed to store the text.
@param out Optional; If provided, the (converted) text data is written here.
@return A pointer to the end of the text data on success, or NULL on failure.
*/

static const char *xmlLoadText(const char *text, long eol, size_t *num_bytes, char *out)
{

  int len;
  long value;

  if (!text) { return NULL; }

  if (num_bytes) { *num_bytes = 0; }

  /* Keep going until a (terminating) reserved character is found. */

  while (*text != 0 && *text != eol)
  {

    /* Read in a single character (NOTE: Might be &apos; etc.) from the XML. */

    len = xmlLoadChar(text, &value);
    if      (len > 0) { text += len; }  /* A normal, unicode character. */
    else if (len < 0) { return text; }  /* External entity reference..? */
    else              { return NULL; }  /* ERROR: Something went wrong. */

    /* Store the unicode character value as a UTF-8 encoded sequence. */

    len = utfEncodeUTF8(value, out);

    if (len < 1)
    {
      #ifdef NDEBUG
        fprintf(stderr, "[DEBUG] Error: Not a valid UTF-8 sequence: %ld\n", value);
      #endif
      return NULL;
    }

    if (out      ) { out        += len; }
    if (num_bytes) { *num_bytes += len; }

  }

  /* Return a pointer to the END of the parsed text. */

  return text;

}

/**
@brief [INTERNAL] Read a name from XML data buffer.

This could be a tag name, an entity reference name, or an attribute name.

@param text A null-terminated string of XML text data.
@param num_bytes Optional; Stores the number of bytes needed to store the name.
@param out Optional; If provided, the name data is written here.
@return A pointer to the end of the name string on success, or NULL on failure.
*/

static const char *xmlLoadName(const char *text, size_t *num_bytes, char *out)
{

  size_t i = 0;

  if (!text) { return NULL; }

  if (num_bytes) { *num_bytes = 0; }

  /* Copy the character data, ignoring any opening ',' or '&' characters. */

  for (i = (text[0] == '&' || text[0] == '<') ? 1 : 0; !isspace(text[i]); i++)
  {

    if (text[i] == 0) { return NULL; }

    if (text[0] == '&')
    {
      if (text[i] == ';') { break; }
    }
    else if (text[0] == '<')
    {
      if (text[i] == '>'         ) { break; }
      if (text[i] == '/' && i > 1) { break; }
    }
    else
    {
      if (text[i] == '=') { break; }
    }

    if (num_bytes) { *num_bytes += 1; }
    if (out) { *out = text[i]; out++; }

  }

  if (out) { *out = 0; }

  /* Tag and entity names are expected to end with different characters. */

  if (text[0] == '<')
  {
    if (text[i] != '>' && text[i] != '/' && !isspace(text[i])) { return NULL; }
  }
  else if (text[0] == '&')
  {
    if (text[i] != ';') { return NULL; }
  }

  /* Return the result. */

  return &text[i];

}

/**
@brief [INTERNAL] Build an XML node tree from a text string.

This function calls itself recursively.

@param text A string of XML text data.
@param owner The node that contains any nodes found.
@return A pointer to the end of the text read, or NULL if something went wrong.
*/

static const char *xmlLoadNode(const char *text, xml_s *owner)
{

  xml_s *node = NULL;
  xml_s *attr = NULL;
  char *temp = NULL;
  size_t len = 0;

  if (!text || !owner) { return NULL; }

  /* Loop until a NULL terminator byte is found. */

  while (text[0])
  {

    /* Read in any text data before the next tag. */

    if (!xmlLoadText(text, '<', &len, NULL)) { return NULL; }

    if (len > 0)
    {

      temp = (char*)malloc(len + 1);
      if (!temp) { return NULL; }
      text = xmlLoadText(text, '<', NULL, temp);

      node = xmlNewChildNode(owner, "#text", temp);
      free(temp);
      if (!node) { return NULL; }

    }

    /* Only an external entity reference or a new tag can follow text data. */

    if (text[0] == '&')
    {

      if (!xmlLoadName(text, &len, NULL)) { return NULL; }

      temp = (char*)malloc(len + 1);
      if (!temp) { return NULL; }
      text = xmlLoadName(text, NULL, temp) + 1;

      node = xmlNewChildNode(owner, "#entity", temp);
      free(temp);
      if (!node) { return NULL; }

    }
    else if (strncmp(text, "<!--", 4) == 0)
    {
      text = strstr(text, "-->");
      if (!text) { return NULL; }
      text += 3;
    }
    else if (strncmp(text, "<!", 2) == 0)
    {

      if (!xmlLoadName(text, &len, NULL)) { return NULL; }

      temp = (char*)malloc(len + 1);
      if (!temp) { return NULL; }
      text = xmlLoadName(text, NULL, temp);

      node = xmlNewChildNode(owner, temp, NULL);
      free(temp);
      if (!node) { return NULL; }

      while (isspace(text[0])) { text++; }

      for (len = 0; text[len] != '>'; len++)
      {
        if (text[len] == 0) { return NULL; }
      }

      temp = (char*)malloc(len + 1);
      if (!temp) { return NULL; }
      memcpy(temp, text, len); temp[len] = 0;

      xmlSetText(node, temp);

      free(temp);

      text += len + 1;

    }
    else if (text[0] == '<')
    {

      if (text[1] == '/') { return text; }

      /* Create a new tag node. */

      if (!xmlLoadName(text, &len, NULL)) { return NULL; }

      temp = (char*)malloc(len + 1);
      if (!temp) { return NULL; }
      text = xmlLoadName(text, NULL, temp);

      node = xmlNewChildNode(owner, temp, NULL);
      free(temp);
      if (!node) { return NULL; }

      while (1)
      {

        while (isspace(text[0])) { text++; }

        /* Look for any end-of-tag characters. */

        if (text[0] == '?')
        {
          if (node->name[0] != '?' || text[1] != '>') { return NULL; }
          text += 2;
          break;
        }

        if (text[0] == '/')
        {
          if (node->name[0] == '?' || text[1] != '>') { return NULL; }
          text += 2;
          break;
        }

        if (text[0] == '>')
        {

          if (node->name[0] == '?') { return NULL; }

          len = strlen(node->name);

          text = xmlLoadNode(text + 1, node);

          if      (!text                                    ) { return NULL; }
          else if (text[0]                            != '<') { return NULL; }
          else if (text[1]                            != '/') { return NULL; }
          else if (strncmp(&text[2], node->name, len) !=  0 ) { return NULL; }
          else if (text[2 + len]                      != '>') { return NULL; }

          text = &text[2 + len + 1];

          break;

        }

        /* Load attributes. */

        if (!xmlLoadName(text, &len, NULL)) { return NULL; }

        temp = (char*)malloc(len + 1);
        if (!temp) { return NULL; }
        text = xmlLoadName(text, NULL, temp);

        attr = xmlNewAttribute(node, temp, NULL);
        free(temp);
        if (!attr) { return NULL; }

        while (isspace(text[0])) { text++; }
        if (text[0] != '=') { return NULL; }
        text++;

        while (isspace(text[0])) { text++; }
        if (text[0] != '"' && text[0] != '\'') { return NULL; }

        if (!xmlLoadText(text+1, text[0], &len, NULL)) { return NULL; }

        if (len > 0)
        {

          temp = (char*)malloc(len + 1);
          if (!temp) { return NULL; }
          text = xmlLoadText(text+1, text[0], NULL, temp) + 1;

          xmlSetText(attr, temp);
          free(temp);

        } else { text += 2; }

      }

    }
    else if (text[0]) { return NULL; }

  }

  /* Return a pointer to the end of the parsed data. */

  return text;

}

/*
[PUBLIC] Build an XML node tree from a text string.
*/

xml_s *xmlLoadData(const char *text)
{

  const unsigned char *data = (const unsigned char*)text;

  xml_s *root = NULL;

  if (!data) { return NULL; }

  /* If a UTF-8 BOM is present, skip past it. */

  if (data[0] == 0xEF)
  {
    if      (data[1] != 0xBB) { return NULL; }
    else if (data[2] != 0xBF) { return NULL; }
    else                      { data += 3;   }
  }

  /* Attempt to parse the text as XML data and generate a tree of XML nodes. */

  root = xmlCreate(NULL, NULL);
  return xmlLoadNode((const char*)data, root) ? root : xmlDelete(root);

}

/*
[PUBLIC] Load an XML document from a file.
*/

xml_s *xmlLoadFile(const char *path)
{

  FILE *src = NULL;
  long len = 0;
  char *text = NULL;
  xml_s *xml = NULL;

  /* Attempt to open the file. */

  src = fopen(path, "rb");
  if (!src) { return NULL; }

  /* Get the size of the file data in bytes. */

  fseek(src, 0, SEEK_END);
  len = ftell(src);
  fseek(src, 0, SEEK_SET);

  /* Copy the file data into memory as a NULL-terminated C-string. */

  text = (char*)malloc(len + 1);

  if (text)
  {
    memset(text, 0, len + 1);
    fread(text, 1, len, src);
  }

  fclose(src);

  /* Attempt to parse the text as XML data. */

  xml = xmlLoadData(text);
  if (xml) { xmlSetText(xml, path); }

  /* Free the string buffer and return the result. */

  if (text) { free(text); }
  return xml;

}

/**
@brief [INTERNAL] Write XML text to a file, replacing any special characters.

See http://en.wikipedia.org/wiki/List_of_XML_character_entity_references

@param out The file to be written to.
@param text The text to be written.
@param for_attributes If true, replace all characters disallowed in attributes.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

static int xmlSaveText(FILE *out, const char *text, int for_attributes)
{

  size_t i = 0;

  if (!out || !text) { return -1; }

  for (i = 0; text[i]; i++)
  {
    if      (text[i] == '<'                   ) { fprintf(out, "&lt;"  ); }
    else if (text[i] == '&'                   ) { fprintf(out, "&amp;" ); }
    else if (text[i] == '>'  && for_attributes) { fprintf(out, "&gt;"  ); }
    else if (text[i] == '"'  && for_attributes) { fprintf(out, "&quot;"); }
    else if (text[i] == '\'' && for_attributes) { fprintf(out, "&apos;"); }
    else                                        { fputc(text[i], out   ); }
  }

  return 0;

}

/**
@brief [INTERNAL] Write a node to an XML file.

Note that this function does not attempt to validate tag names, as they should
already have been checked by the xmlSetName() function.

@param out The file to be written to.
@param xml The node to be written.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

static int xmlSaveNode(FILE *out, const xml_s *xml)
{

  xml_s *item = NULL;
  size_t i = 0;

  if (!out || !xml) { return -1; }

  /* For comments, write the text value. NOTE: Double-hyphens aren't allowed. */

  if (strcmp(xml->name, "!--") == 0)
  {

    fprintf(out, "<!--");

    for (i = 0; xml->text[i]; i++)
    {
      if (strncmp(&xml->text[i], "--", 2) == 0) { fprintf(out, "&#45;");      }
      else                                      { fputc(xml->text[i], out);   }
    }

    fprintf(out, "-->");

    return 0;

  }

  /* For other ! tags, the text value represents their entire contents. */

  if (xml->name[0] == '!')
  {
    fprintf(out, "<%s %s>", xml->name, xml->text);
    return 0;
  }

  /* #document pseudo-tags are simply used as a "container" for the others. */

  if (strcmp(xml->name, "#document") == 0)
  {
    for (item = xml->node; item; item = item->next) { xmlSaveNode(out, item); }
    return 0;
  }

  /* For #entity pseudo-tags, the text value is the name of the entity. */

  if (strcmp(xml->name, "#entity") == 0)
  {
    fprintf(out, "&%s;", xml->text);
    return 0;
  }

  /* #text pseudo-tags are just text. */

  if (strcmp(xml->name, "#text") == 0)
  {
    xmlSaveText(out, xml->text, 0);
    return 0;
  }

  /* For all other node types, output a tag and a list of attributes. */

  fprintf(out, "<%s", xml->name);

  for (item = xml->attr; item; item = item->next)
  {
    fprintf(out, " %s=\"", item->name);
    xmlSaveText(out, item->text, 1);
    fprintf(out, "\"");
  }

  /* Output any child tags. */

  if      (xml->name[0] == '?') { fprintf(out, "?>"); return 0; }
  else if (!xml->node)          { fprintf(out, "/>"); return 0; }

  fputc('>', out);
  for (item = xml->node; item; item = item->next) { xmlSaveNode(out, item); }
  fprintf(out, "</%s>", xml->name);

  /* Success. */

  return 0;

}

/*
[PUBLIC] Export an XML node tree to a file.
*/

int xmlSaveFile(const xml_s *xml, const char *path)
{

  FILE *out = NULL;
  int status = 0;

  if (!xml || !path) { return -1; }
  out = fopen(path, "wb");
  if (!out) { return -1; }

  status = xmlSaveNode(out, xml);

  fclose(out);
  return status;

}

/*
[PUBLIC] Try to find a node/attribute by name, using a depth-first tree search.
*/

xml_s *xmlFindName(xml_s *xml, const char *name, int depth)
{

  if (!xml || !name) { return NULL; }

  for (; xml; xml = xml->next)
  {

    if (strcmp(xml->name, name) == 0) { return xml; }

    if (depth != 0)
    {
      xml_s *child = xmlFindName(xml->node, name, (depth < 0) ? -1 : depth-1);
      if (child) { return child; }
    }

  }

  return NULL;

}
