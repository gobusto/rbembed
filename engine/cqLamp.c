/**
@file
@brief Provides structures and functions for handling in-game lamp instances.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqLamp.h"
#include "cqWorld.h"
#include "lgm/light/light.h"

/*
[PUBLIC] Free a previously created lamp from memory.
*/

lamp_s *lampDelete(lamp_s *lamp)
{
  lamp_s *next = lampGetNext(lamp);
  if (!lamp) { return NULL; }

  free(v2dLinkDelete(lamp->link));
  return next;
}

/*
[PUBLIC] Create a new lamp.
*/

lamp_s *lampCreate(v2d_real_t x, v2d_real_t y, v2d_real_t z, v2d_real_t r)
{
  lamp_s *lamp = NULL;

  v2d_chain_s *chain = worldGetLampChain();
  if (!chain) { return NULL; }

  /* Allocate memory for the new lamp structure. */
  lamp = (lamp_s*)malloc(sizeof(lamp_s));
  if (!lamp)
  {
    fprintf(stderr, "[WARNING] malloc() failed for lamp\n");
    return NULL;
  }

  /* Initialise the lamp structure. */
  memset(lamp, 0, sizeof(lamp_s));
  lamp->origin[0] = x;
  lamp->origin[1] = y;
  lamp->origin[2] = z;
  lamp->radius = r;
  lamp->rgb[0] = 1.0f;
  lamp->rgb[1] = 1.0f;
  lamp->rgb[2] = 1.0f;

  /* Return the newly created lamp structure. */
  lamp->link = v2dLinkCreate(lamp);
  if (!lamp->link) { return lampDelete(lamp); }
  v2dLinkInsert(chain, NULL, lamp->link);

  return lamp;
}

/*
[PUBLIC] Load a set of lamp definitions from a file.
*/

int lampLoad(const char *file_name)
{
  light_set_s *light_set = lightLoadFile(file_name);
  long i;

  if (!light_set) { return 0; }

  for (i = 0; i < light_set->num_items; ++i)
  {
    const light_s *light = light_set->item[i];

    if (light->flags & LIGHT_FLAG_SUN)
      fprintf(stderr, "[TODO] Sun lamps aren't implemented yet.\n");
    else if (light->flags & LIGHT_FLAG_SPOT)
      fprintf(stderr, "[TODO] Spot lamps aren't implemented yet.\n");
    else
    {
      lamp_s *lamp = lampCreate(light->origin[0], light->origin[1], light->origin[2], light->radius);
      if (lamp)
      {
        lamp->rgb[0] = light->rgb[0];
        lamp->rgb[1] = light->rgb[1];
        lamp->rgb[2] = light->rgb[2];
      }
    }

  }

  lightSetDelete(light_set);
  return (int)i;
}
