/**
@file
@brief A Ruby implementation of the cqScript.h file.

This file has been tested with Ruby versions 1.9 to 2.3.

NOTE: When compiling, remember to set up both the main include path AND the one
that contains the config.h file. For Ruby 2.3 on Debian, these are as follows:

Main directory: /usr/include/ruby-2.3.0

Config directory: /usr/include/x86_64-linux-gnu/ruby-2.3.0

Other operating systems (or even Ruby versions) may put them somewhere else...

@todo Perhaps try using mruby instead of "standard" Ruby?
*/

#include <ruby/ruby.h>
#include <ruby/version.h> /* Used to dump some debug info to stderr. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cqAudio.h"
#include "cqGraphics.h"
#include "cqPhysics.h"
#include "cqScript.h"
#include "cqSprite.h"
#include "cqSystem.h"
#include "cqWorld.h"
#include "lgm/anim/util/wordtree.h"

/**
@brief [INTERNAL] A Ruby-specific implementation of the "_script_s" structure.

For the Ruby, this structure only needs to contain a single VALUE (for storing
the Ruby actor object instance) and nothing else.
*/

struct _script_s { VALUE r_actor; /**< @brief The Ruby actor object. */ };

typedef struct _script_s script_s;  /**< @brief A nicer name for the struct. */

/* Global objects and variables. */

wordtree_s *script_actor_set = NULL;  /**< @brief about */

/**
@brief [RUBY API] End the program at the next possible opportunity.

@param self Ignore. Always passed through to C functions when called from Ruby.
@return Always returns `Qnil`.
*/

static VALUE scriptAPI_worldDestroy(VALUE self)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  sysEndProgram();
  return Qnil;
}

/**
@brief [RUBY API] Switch to a different map file.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param r_filename The path to the map XML file.
@param force If true, all existing actors will be forcibly deleted on change.
@return Qtrue on success, Qfalse on failure, or Qnil for invalid parameters.
*/

static VALUE scriptAPI_changeMap(VALUE self, VALUE r_filename, VALUE force)
{
  char *c_filename = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(r_filename) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_filename = StringValueCStr(r_filename);

  return worldLoadMap(c_filename, RTEST(force)) ? Qtrue : Qfalse;
}

/**
@brief [RUBY] Save the world state to a file.

@param r_filename The name of (and path to) the file to save.
@return Qtrue on success, Qfalse on failure, or Qnil for invalid parameters.
*/

static VALUE scriptAPI_saveGame(VALUE self, VALUE r_filename)
{
  char *c_filename = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(r_filename) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_filename = StringValueCStr(r_filename);

  return worldSave(c_filename) ? Qtrue : Qfalse;
}

/**
@brief [RUBY] Load the world state from a file.

@param r_filename The name of (and path to) the file to load.
@return Qtrue on success, Qfalse on failure, or Qnil for invalid parameters.
*/

static VALUE scriptAPI_loadGame(VALUE self, VALUE r_filename)
{
  char *c_filename = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(r_filename) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_filename = StringValueCStr(r_filename);

  return worldRestore(c_filename) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Returns the locale string defined in the "config.xml" file.

@param self Ignore. Always passed through to C functions when called from Ruby.
@return The current locale code string.
*/

static VALUE scriptAPI_getLocale(VALUE self)
{
  char language[32];
  if (self) { /* Silence "Unused Parameter" warning. */ }
  if (!sysGetLocale(language, 32)) { return Qnil; }
  return rb_str_new_cstr(language);
}

/**
@brief [RUBY API] Get a list of all (scripted) actors within the world.

@param self Ignore. Always passed through to C functions when called from Ruby.
@return An array of all scripted actors within the world.
*/

static VALUE scriptAPI_getActorList(VALUE self)
{

  VALUE r_array = rb_ary_new();
  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  for (actor = worldGetActor(); actor; actor = actorGetNext(actor))
  {
    if (actor->script) { rb_ary_push(r_array, actor->script->r_actor); }
  }

  return r_array;

}

/**
@brief [RUBY API] Perform a "hitscan" collision test for a 3D line.

Given the parameters `x`, `y`, `z`, `dx`, `dy`, `dz`, the physics engine checks
for all collision points with actors (scripted or non-scripted) and environment
and returns an array of contact points. Each contact is stored as an array with
4-items: `x`, `y`, `z`, and `actor`. If the contact point is for a non-scripted
actor or the environment, the `actor` parameter will be set to `nil`.

@todo Unlike the `onCollide()` method in `CoreObject.rb`, it's not possible to
tell the difference between non-scripted actors and the environment here - this
should be changed to include an extra `is_actor` boolean to fix the problem.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param px The X origin of the line.
@param py The Y origin of the line.
@param pz The Z origin of the line.
@param dx The relative X offset to the end of the line.
@param dy The relative Y offset to the end of the line.
@param dz The relative Z offset to the end of the line.
@return An array of all affected actors.
*/

static VALUE scriptAPI_cqHitScan(VALUE self, VALUE px, VALUE py, VALUE pz,
                                             VALUE dx, VALUE dy, VALUE dz)
{

  VALUE r_hitscan = rb_ary_new();
  hitscan_s *c_hitscan;
  size_t i;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a list of hit-scan collision points from the physics sub-system. */

  c_hitscan = physHitscanCreate(NUM2DBL(px), NUM2DBL(py), NUM2DBL(pz),
                                NUM2DBL(dx), NUM2DBL(dy), NUM2DBL(dz));

  if (!c_hitscan) { return r_hitscan; }

  /* Convert the C hitscan structure into a format that Ruby can use. */

  for (i = 0; i < c_hitscan->num_items; ++i)
  {

    VALUE data[4];

    data[0] = rb_float_new(c_hitscan->point[(i*3)+0]);  /* Contact point X. */
    data[1] = rb_float_new(c_hitscan->point[(i*3)+1]);  /* Contact point Y. */
    data[2] = rb_float_new(c_hitscan->point[(i*3)+2]);  /* Contact point Z. */
    data[3] = Qnil; /* Contacts may be with a non-Ruby actor, or the world. */

    if (c_hitscan->actor[i] && c_hitscan->actor[i]->script)
    { data[3] = c_hitscan->actor[i]->script->r_actor; }

    rb_ary_push(r_hitscan, rb_ary_new4(4, data));

  }

  /* Free the C hitscan structure and return the Ruby one. */

  physHitscanDelete(c_hitscan);
  return r_hitscan;

}

/**
@brief [RUBY API] Convert a set of three euler angles into a quaternion.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param x The X angle in radians.
@param y The Y angle in radians.
@param z The Z angle in radians.
@return An array containing the WXYZ quaternion.
*/

static VALUE scriptAPI_quatFromEuler(VALUE self, VALUE x, VALUE y, VALUE z)
{

  VALUE r_quat[4];
  quaternion_t quat;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  quatFromEuler(quat, NUM2DBL(x), NUM2DBL(y), NUM2DBL(z));
  r_quat[0] = rb_float_new(quat[QUAT_W]);
  r_quat[1] = rb_float_new(quat[QUAT_X]);
  r_quat[2] = rb_float_new(quat[QUAT_Y]);
  r_quat[3] = rb_float_new(quat[QUAT_Z]);
  return rb_ary_new4(4, r_quat);

}

/**
@brief [RUBY API] Update the position and angle of the 3D camera.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param x The X position of the camera.
@param y The Y position of the camera.
@param z The Z position of the camera.
@param qw The W component of the rotation quaternion.
@param qx The X component of the rotation quaternion.
@param qy The Y component of the rotation quaternion.
@param qz The Z component of the rotation quaternion.
@param fov The camera field-of-view angle (in DEGREES, rather than radians).
@return Always returns Qnil.
*/

static VALUE scriptAPI_setCamera(VALUE self, VALUE x, VALUE y, VALUE z, VALUE qw, VALUE qx, VALUE qy, VALUE qz, VALUE fov)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  sysSetCamera(NUM2DBL(x), NUM2DBL(y), NUM2DBL(z), NUM2DBL(qw), NUM2DBL(qx), NUM2DBL(qy), NUM2DBL(qz), NUM2DBL(fov));
  return Qnil;
}

/**
@brief [RUBY API] Update the position and angle of the 3D listener.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param x The X position of the "virtual ear" in 3D space.
@param y The Y position of the "virtual ear" in 3D space.
@param z The Z position of the "virtual ear" in 3D space.
@param angle The "turn" (Z-rotation) angle of the ear in radians.
@return Always returns Qnil.
*/

static VALUE scriptAPI_setListener(VALUE self, VALUE x, VALUE y, VALUE z, VALUE angle)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  sndSetListener(NUM2DBL(x), NUM2DBL(y), NUM2DBL(z), NUM2DBL(angle));
  return Qnil;
}

/**
@brief [RUBY API] Create a new actor instance.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param actor_type The type of actor to be created, such as "enemy_tank".
@param x The X position of the new actor.
@param y The Y position of the new actor.
@param z The Z position of the new actor.
@param angle The "turn" (Z-rotation) angle of the actor in radians.
@return The Ruby actor object, if the new actor is script-able; otherwise Qnil.
*/

static VALUE scriptAPI_actorCreate(VALUE self, VALUE actor_type, VALUE x, VALUE y, VALUE z, VALUE angle)
{

  char *actor_type_as_c_string = NULL;
  actor_s *actor = NULL;
  float c_x = 0.0f;
  float c_y = 0.0f;
  float c_z = 0.0f;
  float c_angle = 0.0f;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(actor_type) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  actor_type_as_c_string = StringValueCStr(actor_type);

  /* TODO: Only allow T_FLOAT, T_FIXNUM or T_BIGNUM here. */
  c_x = NUM2DBL(x);
  c_y = NUM2DBL(y);
  c_z = NUM2DBL(z);
  c_angle = NUM2DBL(angle);

  actor = actorCreate(actor_type_as_c_string, c_x, c_y, c_z, c_angle);
  if (!actor) { return Qnil; }

  if (actor->script) { return actor->script->r_actor; }

  return Qnil;

}

/**
@brief [RUBY API] Offset the texture coordinates of an actor's mesh.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to change.
@param u The horizontal offset of the texture coordinates.
@param v The vertical offset of the texture coordinates.
@return Qtrue on success, Qfalse on failure, or Qnil for invalid parameters.
*/

static VALUE scriptAPI_actorSetTextureOffset(VALUE self, VALUE ruby_actor, VALUE u, VALUE v)
{
  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  actor->texture_offset[0] = NUM2DBL(u);
  actor->texture_offset[1] = NUM2DBL(v);

  return Qtrue;
}

/**
@brief [RUBY API] Change the current animation sequence of an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to change.
@param anim_name The name of the animation sequence to play.
@param loop If false, the animation plays once and stops.
@return Qtrue on success, Qfalse on failure, or Qnil for invalid parameters.
*/

static VALUE scriptAPI_actorSetAnimation(VALUE self, VALUE ruby_actor, VALUE anim_name, VALUE loop)
{

  actor_s *actor = NULL;
  char *c_anim_name = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  if (TYPE(anim_name) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_anim_name = StringValueCStr(anim_name);

  return actorSetAnimation(actor, c_anim_name, RTEST(loop)) == 0 ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Change the current animation speed of an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to change.
@param speed A floating point value, with 1.0 representing "normal" speed.
@return Qtrue on success or Qfalse on failure.
*/

static VALUE scriptAPI_actorSetAnimationSpeed(VALUE self, VALUE ruby_actor, VALUE speed)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  return actorSetAnimationSpeed(actor, NUM2DBL(speed)) ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Check if an actor is currently playing an animation sequence.

This is useful for determining when a non-looping animation sequence has ended.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to check.
@return Qtrue if a sequence is being played, Qfalse if not, or Qnil on failure.
*/

static VALUE scriptAPI_actorAnimationIsPlaying(VALUE self, VALUE ruby_actor)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  return actorAnimationIsPlaying(actor) ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Determine if an actor is affected by gravity.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to check.
@return Qtrue if gravity is enabled, Qfalse if not, or Qnil on failure.
*/

static VALUE scriptAPI_actorGetGravity(VALUE self, VALUE ruby_actor)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  return physActorGetGravity(actor) ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Enable/disable gravity for a specific actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to check.
@param enable Qtrue to enable gravity, or Qfalse to disable it.
@return Qtrue on success, or Qfalse on failure.
*/

static VALUE scriptAPI_actorSetGravity(VALUE self, VALUE ruby_actor, VALUE enable)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qfalse; }

  return physActorSetGravity(actor, RTEST(enable)) ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Attempt to convert a non-ragdoll actor into a ragdoll.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to convert into a ragdoll.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorBecomeRagdoll(VALUE self, VALUE ruby_actor)
{
  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */
  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  return physActorBecomeRagdoll(actor) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Get the position of an actor.

This simply converts the position attributes of a C actor structure into VALUE
types so that Ruby can understand them.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to return a position value for.
@return An array containing the XYZ position on success, or Qnil on failure.
*/

static VALUE scriptAPI_actorGetPosition(VALUE self, VALUE ruby_actor)
{

  actor_s *actor = NULL;

  VALUE r_value_array[3];
  float c_value_array[3];

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  physActorGetPosition(actor, 0, c_value_array);
  r_value_array[0] = rb_float_new(c_value_array[0]);
  r_value_array[1] = rb_float_new(c_value_array[1]);
  r_value_array[2] = rb_float_new(c_value_array[2]);
  return rb_ary_new4(3, r_value_array);

}

/**
@brief [RUBY API] Set the position of an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to be modified.
@param x The new X position.
@param y The new Y position.
@param z The new Z position.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorSetPosition(VALUE self, VALUE ruby_actor, VALUE x, VALUE y, VALUE z)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  /* TODO: Only allow T_FLOAT, T_FIXNUM or T_BIGNUM here. */
  physActorSetPosition(actor, NUM2DBL(x), NUM2DBL(y), NUM2DBL(z));

  return Qtrue;

}

/**
@brief [RUBY API] Get the current XYZ velocity of an actor.

This simply converts the velocity attributes of a C actor structure into VALUE
types so that Ruby can understand them.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to return velocity values for.
@return An array containing the XYZ velocity on success, or Qnil on failure.
*/

static VALUE scriptAPI_actorGetVelocity(VALUE self, VALUE ruby_actor)
{

  actor_s *actor = NULL;

  VALUE r_value_array[3];
  float c_value_array[3];

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  physActorGetVelocity(actor, 0, c_value_array);

  r_value_array[0] = rb_float_new(c_value_array[0]);
  r_value_array[1] = rb_float_new(c_value_array[1]);
  r_value_array[2] = rb_float_new(c_value_array[2]);

  return rb_ary_new4(3, r_value_array);

}

/**
@brief [RUBY API] Explicitly set the XYZ velocity of an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to be updated.
@param x The new X axis velocity value.
@param y The new Y axis velocity value.
@param z The new Z axis velocity value.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorSetVelocity(VALUE self, VALUE ruby_actor, VALUE x, VALUE y, VALUE z)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  /* TODO: Only allow T_FLOAT, T_FIXNUM or T_BIGNUM here. */
  physActorSetVelocity(actor, 0, NUM2DBL(x), NUM2DBL(y), NUM2DBL(z));

  return Qtrue;

}

/**
@brief [RUBY API] Apply a physical force to an actor along the X/Y/Z axes.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to be updated.
@param x The force to apply along the X axis.
@param y The force to apply along the Y axis.
@param z The force to apply along the Z axis.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorAddForce(VALUE self, VALUE ruby_actor, VALUE x, VALUE y, VALUE z)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  /* TODO: Only allow T_FLOAT, T_FIXNUM or T_BIGNUM here. */
  physActorAddForce(actor, 0, NUM2DBL(x), NUM2DBL(y), NUM2DBL(z));

  return Qtrue;

}

/**
@brief [RUBY API] Set a simple Z-rotation angle for an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to be updated.
@param angle The new Z rotation value.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorSetAngle(VALUE self, VALUE ruby_actor, VALUE angle)
{
  actor_s *actor = NULL;
  quaternion_t quat;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */
  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  /* Create a quaternion based on the Z-rotation angle provided and use it: */
  quatFromEuler(quat, 0, 0, NUM2DBL(angle));
  physActorSetQuaternion(actor, quat[0], quat[1], quat[2], quat[3]);

  return Qtrue;
}

/**
@brief [RUBY API] Set the quaternion of an actor.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param ruby_actor The actor to be updated.
@param w The new quaternion W value.
@param x The new quaternion X value.
@param y The new quaternion Y value.
@param z The new quaternion Z value.
@return Qtrue on success, Qfalse on failure, or Qnil if something went wrong.
*/

static VALUE scriptAPI_actorSetQuaternion(VALUE self, VALUE ruby_actor, VALUE w, VALUE x, VALUE y, VALUE z)
{

  actor_s *actor = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  /* Get a pointer to the C actor stucture from the ruby_actor VALUE data. */

  if (TYPE(ruby_actor) != T_STRING) { return Qnil; }
  actor = (actor_s*)treeSearch(script_actor_set, StringValueCStr(ruby_actor));
  if (!actor) { return Qnil; }

  physActorSetQuaternion(actor, NUM2DBL(w), NUM2DBL(x), NUM2DBL(y), NUM2DBL(z));

  return Qtrue;

}

/**
@brief [RUBY API] Get information about a particular input slot.

This is an (indirect) way for Ruby to get information about joypads, mice, etc.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param slot The name of the input slot to check.
@param clamp_range If true, The value is restricted to a zero-to-one range.
@return A floating point value. Always either zero or a positive value.
*/

static VALUE scriptAPI_inputGetValue(VALUE self, VALUE slot, VALUE clamp_range)
{

  char *slot_name_as_c_string = NULL;
  float value = 0;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(slot) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  slot_name_as_c_string = StringValueCStr(slot);

  value = sysGetInput(slot_name_as_c_string);
  if (RTEST(clamp_range) && value > 1.0) { value = 1.0; }

  return rb_float_new(value);

}

/**
@brief [RUBY API] Make a joypad vibrate.

@todo Need to know the number of available pads / if they support haptics.

@todo Need to know which pad relates to which player.

@todo Need to allow effects other than a basic rumble.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param pad_id A joypad ID, indexed from zero.
@param strength A magnitude value in the range 0.0 to 1.0.
@param length A time value, measured in milliseconds.
@return Qtrue on success, or Qfalse on failure.
*/

static VALUE scriptAPI_inputPadVibrate(VALUE self, VALUE pad_id, VALUE strength, VALUE length)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  return sysPadRumble(NUM2INT(pad_id), NUM2DBL(strength), NUM2INT(length)) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Play a sound effect.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param sound The name of the effect to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@return An ID on success, -1 on failure, or Qnil if the arguments are invalid.
*/

static VALUE scriptAPI_soundPlay(VALUE self, VALUE sound, VALUE num_loops)
{
  char *c_name = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(sound) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_name = StringValueCStr(sound);

  return rb_int_new(sndPlayEffect(c_name, NUM2INT(num_loops), V2D_FALSE, 0, 0, 0, 0));
}

/**
@brief [RUBY API] Play a 3D "positional" sound effect.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param sound The name of the effect to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@param x The X position of the sound.
@param y The Y position of the sound.
@param z The Z position of the sound.
@param radius The radius of the sound.
@return An ID on success, -1 on failure, or Qnil if the arguments are invalid.
*/

static VALUE scriptAPI_soundPlay3D(VALUE self, VALUE sound, VALUE num_loops,
                        VALUE x, VALUE y, VALUE z, VALUE radius)
{
  char *c_name = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(sound) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  c_name = StringValueCStr(sound);

  return rb_int_new(sndPlayEffect(c_name, NUM2INT(num_loops), V2D_TRUE, NUM2DBL(x), NUM2DBL(y), NUM2DBL(z), NUM2DBL(radius)));
}

/**
@brief [RUBY API] Stop a previously-created sound effect from playing.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param guid The ID that was returned by scriptAPI_soundPlay() earlier.
@return Qtrue on success, or Qfalse on failure.
*/

static VALUE scriptAPI_soundStop(VALUE self, VALUE guid)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  return sndStopSound(NUM2INT(guid)) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Determine if a sound effect is still playing.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param guid The ID that was returned by scriptAPI_soundPlay() earlier.
@return Qtrue if the sound effect is still playing, or Qfalse if not.
*/

static VALUE scriptAPI_soundIsPlaying(VALUE self, VALUE guid)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  return sndSoundPlaying(NUM2INT(guid)) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Reposition a 3D sound.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param guid The ID that was returned by scriptAPI_soundPlay() earlier.
@param x The new X position of the sound.
@param y The new Y position of the sound.
@param z The new Z position of the sound.
@return Qtrue on success, or Qfalse on failure.
*/

static VALUE scriptAPI_soundSetPosition(VALUE self, VALUE guid, VALUE x, VALUE y, VALUE z)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  return sndSoundSetPosition(NUM2INT(guid), NUM2DBL(x), NUM2DBL(y), NUM2DBL(z)) ? Qtrue : Qfalse;
}

/**
@brief [RUBY API] Play (and loop) a music file.

@param self Ignore. Always passed through to C functions when called from Ruby.
@param file_name The name (and path) of the music file to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@return Qtrue on success, Qfalse on failure, or Qnil if something goes wrong.
*/

static VALUE scriptAPI_musicPlay(VALUE self, VALUE file_name, VALUE num_loops)
{

  char *file_name_as_c_string = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(file_name) != T_STRING) { return Qnil; }
  /* FIXME: StringValueCStr() doesn't like text containing embedded NULLs. */
  file_name_as_c_string = StringValueCStr(file_name);

  return sndPlayMusic(file_name_as_c_string, NUM2INT(num_loops)) == 0 ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] Stop any currently-playing music.

@param self Ignore. Always passed through to C functions when called from Ruby.
@return Always returns Qnil.
*/

static VALUE scriptAPI_musicStop(VALUE self)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  sndStopMusic();
  return Qnil;
}

/**
@brief [RUBY API] Determine if any music is currently being played.

@param self Ignore. Always passed through to C functions when called from Ruby.
@return Qtrue if music is currently playing, or Qfalse if not.
*/

static VALUE scriptAPI_musicIsPlaying(VALUE self)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  if (sndMusicPlaying()) { return Qtrue; }
  return Qfalse;
}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param image about
@param x about
@param y about
@param w about
@param h about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteCreate(VALUE self, VALUE id, VALUE image, VALUE x, VALUE y, VALUE w, VALUE h)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;
  char *c_image = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING || TYPE(image) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);
  c_image = StringValueCStr(image);

  sprite = spriteCreate(c_id, NUM2DBL(x), NUM2DBL(y), NUM2DBL(w), NUM2DBL(h));

  if (!sprite) { return Qfalse; }

  gfxSpriteSetImage(sprite, c_image);

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
*/

static VALUE scriptAPI_spriteDestroy(VALUE self, VALUE id)
{
  if (self) { /* Silence "Unused Parameter" warning. */ }
  if (TYPE(id) == T_STRING) { spriteDelete(StringValueCStr(id)); }
  return Qnil;
}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param text about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteSetText(VALUE self, VALUE id, VALUE text)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;
  char *c_text = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING || TYPE(text) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);
  c_text = StringValueCStr(text);

  sprite = spriteFind(c_id);

  if (!sprite) { return Qfalse; }

  return spriteSetText(sprite, c_text) == 0 ? Qtrue : Qfalse;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param r about
@param g about
@param b about
@param a about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteSetRGBA(VALUE self, VALUE id, VALUE r, VALUE g, VALUE b, VALUE a)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);

  sprite = spriteFind(c_id);

  if (!sprite) { return Qfalse; }

  sprite->rgba[0] = (unsigned char)NUM2INT(r);
  sprite->rgba[1] = (unsigned char)NUM2INT(g);
  sprite->rgba[2] = (unsigned char)NUM2INT(b);
  sprite->rgba[3] = (unsigned char)NUM2INT(a);

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param x about
@param y about
@param w about
@param h about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteSetSubImage(VALUE self, VALUE id, VALUE x, VALUE y, VALUE w, VALUE h)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);

  sprite = spriteFind(c_id);

  if (!sprite) { return Qfalse; }

  sprite->xywh[0] = NUM2DBL(x);
  sprite->xywh[1] = NUM2DBL(y);
  sprite->xywh[2] = NUM2DBL(w);
  sprite->xywh[3] = NUM2DBL(h);

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param x about
@param y about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteSetPosition(VALUE self, VALUE id, VALUE x, VALUE y)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);

  if (!sprite) { return Qfalse; }

  v2dMatrixPosition(sprite->matrix, NUM2DBL(x), NUM2DBL(y));

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@return writeme
*/

static VALUE scriptAPI_spriteGetPosition(VALUE self, VALUE id)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  VALUE r_value_array[2];

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }

  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);

  if (!sprite) { return Qfalse; }

  r_value_array[0] = rb_float_new(sprite->matrix[2][0]);
  r_value_array[1] = rb_float_new(sprite->matrix[2][1]);
  return rb_ary_new4(2, r_value_array);

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteMatrixReset(VALUE self, VALUE id)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }
  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);
  if (!sprite) { return Qfalse; }

  v2dMatrixReset(sprite->matrix);

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param angle about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteRotate(VALUE self, VALUE id, VALUE angle)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }
  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);
  if (!sprite) { return Qfalse; }

  v2dMatrixRotate(sprite->matrix, NUM2DBL(angle));

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param x about
@param y about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteScale(VALUE self, VALUE id, VALUE x, VALUE y)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }
  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);
  if (!sprite) { return Qfalse; }

  v2dMatrixScale(sprite->matrix, NUM2DBL(x), NUM2DBL(y));

  return Qtrue;

}

/**
@brief [RUBY API] About

writeme

@param self Ignore. Always passed through to C functions when called from Ruby.
@param id about
@param x about
@param y about
@return true on success, false on failure, or nil if the arguments are invalid.
*/

static VALUE scriptAPI_spriteShear(VALUE self, VALUE id, VALUE x, VALUE y)
{

  sprite_s *sprite = NULL;

  char *c_id = NULL;

  if (self) { /* Silence "Unused Parameter" warning. */ }

  if (TYPE(id) != T_STRING) { return Qnil; }
  c_id = StringValueCStr(id);
  sprite = spriteFind(c_id);
  if (!sprite) { return Qfalse; }

  v2dMatrixShear(sprite->matrix, NUM2DBL(x), NUM2DBL(y));

  return Qtrue;

}

/*
[PUBLIC] Initialise everything.

See the following Stack Overflow question for details on Ruby initialisation:

http://stackoverflow.com/q/6585610/4200092
*/

int scriptInit(int argc, char **argv)
{
  VALUE actor, camera, input, listener, music, sound, sprite, support, world;

  scriptQuit();

  fprintf(stderr, "[INIT] Ruby %s\n", ruby_version);
  fprintf(stderr, "\tBuild: %s\n", ruby_description);

  ruby_sysinit(&argc, &argv);
  { RUBY_INIT_STACK }
  ruby_init();
  ruby_init_loadpath();
  ruby_script("Conqore2");

  actor = rb_define_module("Actor");
  camera = rb_define_module("Camera");
  input = rb_define_module("UserInput");
  listener = rb_define_module("Listener");
  music = rb_define_module("Music");
  sound = rb_define_module("Sound");
  sprite = rb_define_module("Sprite");
  support = rb_define_module("Support");
  world = rb_define_module("World");

  rb_define_module_function(world, "destroy", scriptAPI_worldDestroy, 0);
  rb_define_module_function(world, "change_map", scriptAPI_changeMap, 2);
  rb_define_module_function(world, "load_game", scriptAPI_loadGame, 1);
  rb_define_module_function(world, "save_game", scriptAPI_saveGame, 1);
  rb_define_module_function(world, "actors", scriptAPI_getActorList, 0);
  rb_define_module_function(world, "hitscan", scriptAPI_cqHitScan, 6);
  rb_define_module_function(support, "locale", scriptAPI_getLocale, 0);
  rb_define_module_function(support, "convert_euler", scriptAPI_quatFromEuler, 3);
  rb_define_module_function(camera, "set_position", scriptAPI_setCamera, 8);
  rb_define_module_function(listener, "set_position", scriptAPI_setListener, 4);
  rb_define_module_function(actor, "create", scriptAPI_actorCreate, 5);
  rb_define_module_function(actor, "set_texture_offset", scriptAPI_actorSetTextureOffset, 3);
  rb_define_module_function(actor, "set_animation", scriptAPI_actorSetAnimation, 3);
  rb_define_module_function(actor, "set_animation_speed", scriptAPI_actorSetAnimationSpeed, 2);
  rb_define_module_function(actor, "animating?", scriptAPI_actorAnimationIsPlaying, 1);
  rb_define_module_function(actor, "gravity?", scriptAPI_actorGetGravity, 1);
  rb_define_module_function(actor, "set_gravity", scriptAPI_actorSetGravity, 2);
  rb_define_module_function(actor, "ragdoll!", scriptAPI_actorBecomeRagdoll, 1);
  rb_define_module_function(actor, "get_position", scriptAPI_actorGetPosition, 1);
  rb_define_module_function(actor, "set_position", scriptAPI_actorSetPosition, 4);
  rb_define_module_function(actor, "get_velocity", scriptAPI_actorGetVelocity, 1);
  rb_define_module_function(actor, "set_velocity", scriptAPI_actorSetVelocity, 4);
  rb_define_module_function(actor, "add_force", scriptAPI_actorAddForce, 4);
  rb_define_module_function(actor, "set_angle", scriptAPI_actorSetAngle, 2);
  rb_define_module_function(actor, "set_quaternion", scriptAPI_actorSetQuaternion, 5);
  rb_define_module_function(input, "state", scriptAPI_inputGetValue, 2);
  rb_define_module_function(input, "vibrate", scriptAPI_inputPadVibrate, 3);
  rb_define_module_function(sound, "play", scriptAPI_soundPlay, 2);
  rb_define_module_function(sound, "play3D", scriptAPI_soundPlay3D, 6);
  rb_define_module_function(sound, "stop", scriptAPI_soundStop, 1);
  rb_define_module_function(sound, "playing?", scriptAPI_soundIsPlaying, 1);
  rb_define_module_function(sound, "set_position", scriptAPI_soundSetPosition, 4);
  rb_define_module_function(music, "play", scriptAPI_musicPlay, 2);
  rb_define_module_function(music, "stop", scriptAPI_musicStop, 0);
  rb_define_module_function(music, "playing?", scriptAPI_musicIsPlaying, 0);
  rb_define_module_function(sprite, "create", scriptAPI_spriteCreate, 6);
  rb_define_module_function(sprite, "destroy", scriptAPI_spriteDestroy, 1);
  rb_define_module_function(sprite, "set_text", scriptAPI_spriteSetText, 2);
  rb_define_module_function(sprite, "set_rgba", scriptAPI_spriteSetRGBA, 5);
  rb_define_module_function(sprite, "set_subimage", scriptAPI_spriteSetSubImage, 5);
  rb_define_module_function(sprite, "set_position", scriptAPI_spriteSetPosition, 3);
  rb_define_module_function(sprite, "get_position", scriptAPI_spriteGetPosition, 1);
  rb_define_module_function(sprite, "reset", scriptAPI_spriteMatrixReset, 1);
  rb_define_module_function(sprite, "rotate", scriptAPI_spriteRotate, 2);
  rb_define_module_function(sprite, "scale", scriptAPI_spriteScale, 3);
  rb_define_module_function(sprite, "shear", scriptAPI_spriteShear, 3);

  script_actor_set = treeCreate();
  return script_actor_set ? 0 : 666;
}

/*
[PUBLIC] Shut everything down again.
*/

void scriptQuit(void)
{
  if (!script_actor_set) { return; }
  script_actor_set = treeDelete(script_actor_set, NULL, NULL);
  ruby_finalize();
}

/*
[PUBLIC] Load a script class file.
*/

int scriptClassCreate(const char *file_name)
{

  char  *code = NULL;
  int status = 0;

  if (!script_actor_set) { return -1; }

  if (!file_name) { return 0; /* No script is allowed. */ }

  code = (char*)malloc(12 + strlen(file_name) + 1);
  if (!code) { return -1; }
  sprintf(code, "require './%s'", file_name);

  rb_eval_string_protect(code, &status);
  free(code);

  return status;

}

/*
[PUBLIC] Free a previously created Class from memory.
*/

void scriptClassDelete(const char *class_name)
{

  if (!class_name) { return; }

  /* Classes can't be deleted in Ruby, so this function doesn't do anything. */

}

/**
@brief [INTERNAL] about

writeme

@param args about
@return writeme
*/

static VALUE scriptActorCreateSafe(VALUE args)
{
  VALUE ruby_class = rb_ary_entry(args, 0); /* The Ruby-side class to use. */
  VALUE actor_data = rb_ary_entry(args, 1); /* The C actor data structure. */
  VALUE init_angle = rb_ary_entry(args, 2); /* The initial angle at spawn. */
  return rb_funcall(ruby_class, rb_intern("new"), 2, actor_data, init_angle);
}

/*
[PUBLIC] Initialise script data for a given actor.
*/

int scriptActorCreate(const char *class_name, actor_s *actor, v2d_real_t angle)
{

  char lookup_id[512];
  VALUE argv[3];
  VALUE r_param;
  int status = 0;

  if (!class_name || !actor) { return 0; }

  /* Free any old data and initialise the actor. */
  scriptActorDelete(actor);

  /* Get the Ruby class. TODO: Evaluating unchecked strings isn't very safe! */
  argv[0] = rb_eval_string_protect(class_name, &status);
  if (argv[0] == Qnil || status != 0) { return 0; /* Class does not exist. */ }

  /* Add this actor to the look-up table. */
  sprintf(lookup_id, "%p", (void*)actor);
  if (!treeInsert(script_actor_set, lookup_id, actor, NULL)) { return -1; }
  argv[1] = rb_str_new_cstr(lookup_id);

  /* Create a script structure for this actor. */
  actor->script = (script_s*)malloc(sizeof(script_s));
  if (!actor->script) { return -1; }

  /* Create a Ruby actor object. */
  argv[2] = rb_float_new(angle);
  r_param = rb_ary_new4(3, argv);
  actor->script->r_actor = rb_protect(scriptActorCreateSafe, r_param, &status);
  if (actor->script->r_actor) { rb_gc_register_address(&actor->script->r_actor); }
  rb_ary_free(r_param);

  return status;

}

/*
[PUBLIC] Free any previously created script data for an actor.
*/

void scriptActorDelete(actor_s *actor)
{

  char lookup_id[512];

  if (!actor) { return; }

  sprintf(lookup_id, "%p", (void*)actor);
  treeInsert(script_actor_set, lookup_id, NULL, NULL);

  if (actor->script) { rb_gc_unregister_address(&actor->script->r_actor); }

  free(actor->script);
  actor->script = NULL;

}

/**
@brief [INTERNAL] about

writeme

@param args about
@return writeme
*/

static VALUE scriptCallSafe(VALUE args)
{

  event_t event_type;
  VALUE actor;
  VALUE param_a;
  VALUE param_b;

  event_type = NUM2ULONG(rb_ary_entry(args, 0));
  actor = rb_ary_entry(args, 1);
  param_a = rb_ary_entry(args, 2);
  param_b = rb_ary_entry(args, 3);

  switch (event_type)
  {

    case EVENT_MAPCHANGE:
      return rb_funcall(actor, rb_intern("map_change"), 2, param_a, param_b);

    case EVENT_MESSAGE:
      return rb_funcall(actor, rb_intern("message"), 2, param_a, param_b);

    /* FIXME: Non-scripted actors are currently reported as "not an actor". */
    case EVENT_COLLIDE:
      return rb_funcall(actor, rb_intern("collide"), 3, (param_a == Qnil) ? Qfalse : Qtrue, param_a, param_b);

    case EVENT_THINK:
      return rb_funcall(actor, rb_intern("think"), 0);

    default:
      return Qnil;

  }

  return Qnil;

}

/*
[PUBLIC] Have an actor call a specific script method.
*/

int scriptCall(actor_s *actor, event_t event_type, void *data_a, void *data_b)
{

  VALUE argv[4];
  VALUE r_param;
  VALUE result;
  int status, i;

  if      (!actor        ) { return 0; }
  else if (!actor->script) { return 0; }

  /* Set up the arguments array. */

  argv[0] = INT2NUM(event_type);
  argv[1] = actor->script->r_actor;
  argv[2] = Qnil;
  argv[3] = Qnil;

  /* Some kinds of events include extra parameters: */

  switch (event_type)
  {

    case EVENT_MAPCHANGE:
      argv[2] = data_a ? rb_str_new_cstr((const char*)data_a) : Qnil;
      argv[3] = *((v2d_bool_t*)data_b) ? Qtrue : Qfalse;
    break;

    case EVENT_MESSAGE:
      argv[2] = data_a ? rb_str_new_cstr((const char*)data_a) : Qnil;
      argv[3] = data_b ? rb_str_new_cstr((const char*)data_b) : Qnil;
    break;

    case EVENT_COLLIDE:

      if (data_a)
      {
        actor_s *other = (actor_s*)data_a;
        if (other->script) { argv[2] = other->script->r_actor; }
      }

      if (data_b)
      {

        const v2d_real_t *contact_data = (const v2d_real_t*)data_b;

        argv[3] = rb_ary_new();

        for (i = 0; i < (int)contact_data[0]; i++)
        {
          VALUE xyz = rb_ary_new();
          rb_ary_push(xyz, rb_float_new(contact_data[1+(i*3)+0]));  /* X */
          rb_ary_push(xyz, rb_float_new(contact_data[1+(i*3)+1]));  /* Y */
          rb_ary_push(xyz, rb_float_new(contact_data[1+(i*3)+2]));  /* Z */
          rb_ary_push(argv[3], xyz);
        }

      }

    break;

    case EVENT_THINK: default: break;

  }

  /* Call the actor method via rb_protect() to guard against script errors. */

  r_param = rb_ary_new4(4, argv);
  result = rb_protect(scriptCallSafe, r_param, &status);

  /* Return result. Integers are returned as-is; anything else is boolean. */

  if (status != 0)
  {
    VALUE text = rb_eval_string("$!.to_s + '\n\t' + $!.backtrace.join('\n\t')");
    fprintf(stderr, "[WARNING] Script Error: %s\n", StringValueCStr(text));
    return 0;
  }

  return TYPE(result) == T_FIXNUM ? NUM2INT(result) : RTEST(result);

}
