/*

FILENAME: type.h
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

V2D UTILITIES - THIS FILE IS PART OF THE V2D SWF/SVG PROJECT

Copyright (C) 2012 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CHANGELOG:
----------
TD 09 Dec 2012: Initial version created.
TD 24 Dec 2012: Added missing Doxygen info and a matrix shear function.
TD 06 May 2013: Added v2dMatrixGetScale() function.

*/

/**
@file type.h

@brief Defines various common datatypes and constants.

This file also provides some useful functions for working with matrices.
*/

#ifndef __V2D_TYPE_H__
#define __V2D_TYPE_H__

#ifdef __cplusplus
extern "C" {
#endif

#define V2D_PI 3.14159265358979323846 /**< @brief Pi. */

#define V2D_FALSE 0  /**< @brief Boolean FALSE. */
#define V2D_TRUE  1  /**< @brief Boolean TRUE.  */

typedef unsigned char  v2d_bool_t;  /**< @brief Boolean data type.        */
typedef signed   char  v2d_si8_t;   /**< @brief Signed, 8-bit integer.    */
typedef unsigned char  v2d_ui8_t;   /**< @brief Unsigned, 8-bit integer.  */
typedef signed   short v2d_si16_t;  /**< @brief Signed, 16-bit integer.   */
typedef unsigned short v2d_ui16_t;  /**< @brief Unsigned, 16-bit integer. */
typedef signed   long  v2d_si32_t;  /**< @brief Signed, 32-bit integer.   */
typedef unsigned long  v2d_ui32_t;  /**< @brief Unsigned, 32-bit integer. */
typedef double         v2d_real_t;  /**< @brief Float numeric value type. */

typedef v2d_ui8_t  v2d_flag_t;          /**< @brief Flags bit-field type.   */
typedef v2d_ui8_t  v2d_rgba_t[4];       /**< @brief RGBA colour value type. */
typedef v2d_real_t v2d_matrix_t[3][2];  /**< @brief 2x3 transform matrix.   */

/**
@brief

about
*/

typedef struct
{

  v2d_real_t x1;  /**< @brief about */
  v2d_real_t y1;  /**< @brief about */
  v2d_real_t x2;  /**< @brief about */
  v2d_real_t y2;  /**< @brief about */

} v2d_rect_s;

/**
@brief Convert an angle value from degrees to radians.

@param a An angle in degrees.
@return An angle in radians.
*/

#define v2dAsRadians(a) (((a) * V2D_PI) / 180.0)

/**
@brief Determine if a floating point number represents a NaN value.

This is here because the isnan() function is not part of the ISO-C90 standard,
and so it may not be available for all platforms/compilers. For example using
the GCC "-ansi" flag results in "implicit declaration of function 'isnan'".

A simple test for NaN values is "x != x" (as comparisons with NaN values return
false in all cases), but we use a less-than-or-equal-to in order to prevent the
warnings raised by the GCC "-Wfloat-equal" flag from appearing.

@param x A floating point value.
@return True if the value represents a NaN, or false if not.
*/

#define v2dIsNaN(x) (!((x) <= (x)))

/**
@brief about

writeme

@param rect about
@return about
*/

v2d_bool_t v2dRectRepair(v2d_rect_s *rect);

/**
@brief about

writeme

@param a about
@param b about
@return about
*/

v2d_bool_t v2dCollideAABB(v2d_rect_s *a, v2d_rect_s *b);

/**
@brief about

writeme

@param matrix about
@param rect about
@return about
*/

v2d_bool_t v2dMatrixRect(v2d_matrix_t matrix, v2d_rect_s *rect);

/**
@brief Set a matrix to an initial state, with no scaling, shearing or rotation.

The result isn't exactly an identity matrix, as the X/Y position values are not
overwritten. However, they can be reset with v2dMatrixPosition(matrix, 0, 0) if
a true identity matrix is required.

@param matrix The matrix to modify.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixReset(v2d_matrix_t matrix);

/**
@brief Multiply one matrix by another.

Courtesy of Mathew Carr.

@param matrix The matrix to modify.
@param transform The multiplication matrix.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixMultiply(v2d_matrix_t matrix, v2d_matrix_t transform);

/**
@brief Set the X/Y position values of a matrix.

Note that this will simply overwrite the previous coordinate values. To offset
the matrix relative to the current position, use v2dMatrixTranslate() instead.

@param matrix The matrix to modify.
@param x The new horizontal position.
@param y The new vertical position.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixPosition(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y);

/**
@brief Update the X/Y position of a matrix, relative to the current position.

To specify an exact position, use the v2dMatrixPosition() function instead.

@param matrix The matrix to modify.
@param x The horizontal offset value.
@param y The vertical offset value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixTranslate(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y);

/**
@brief Update the X/Y scale of a matrix, relative to the current size.

A value of 1.0 means 100% of the current size, and will thus have no effect.

@param matrix The matrix to modify.
@param x The horizontal scaling value.
@param y The vertical scaling value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixScale(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y);

/**
@brief Update the X/Y shear of a matrix, relative to the current shear values.

Behaves as you'd expect it to.

@param matrix The matrix to modify.
@param x The horizontal shear value.
@param y The vertical shear value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixShear(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y);

/**
@brief Update the rotation of a matrix, relative to the current orientation.

Angles are specified in radians.

@param matrix The matrix to modify.
@param angle The angle to use.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dMatrixRotate(v2d_matrix_t matrix, v2d_real_t angle);

/**
@brief about

writeme

@param output about
@param source about
@return writeme
*/

v2d_bool_t v2dMatrixCopy(v2d_matrix_t output, v2d_matrix_t source);

/**
@brief about

writeme

@param matrix about
@param y about
@return writeme
*/

v2d_real_t v2dMatrixGetScale(v2d_matrix_t matrix, v2d_bool_t y);

/**
@brief about

writeme

@param matrix about
@param x about
@param y about
@return writeme
*/

v2d_bool_t v2dMatrixVector(v2d_matrix_t matrix, v2d_real_t *x, v2d_real_t *y);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_TYPE_H__ */
