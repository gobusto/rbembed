/**
@file
@brief Provides structures and functions for handling in-game actor instances.

Actors are based on classes, which are defined by the cqClass.h and cqClass.c
files. AI and physics are handled by cqScript.h and cqPhysics.h respectively.
*/

#ifndef __CQ2_ACTOR_H__
#define __CQ2_ACTOR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cqClass.h"
#include "bits/chain.h"
#include "bits/type.h"

/* Function prototypes for sharing with renderer DLLs: */

#define ACTOR_UPDATE_MESH(x) int x(actor_s *actor, v2d_bool_t include_physics_transformations)

/**
@brief Get the next actor structure in a linked-list of actor structures.

This macro is provided for convenience, in order to avoid casting everywhere...

@param item The current actor in the linked-list.
@return The next actor in the linked-list, or NULL if there isn't one.
*/

#define actorGetNext(item) ((actor_s*)(item ? v2dLinkGetNext(item->link) : 0))

/**
@brief A generic "actor" structure, used to represent in-game objects.

Actors are used to represent anything interactive (or script-able) in the game
world. Examples include player characters, AI-driven enemy monsters, and bonus
items which the player can collect.

The exact behaviour of an actor is defined by the "class" it is based on, which
specifies the appearance (3D model and textures), physics, and AI scripts used.
*/

typedef struct
{

  class_s *klass;  /**< @brief The "class" on which this actor is based. */

  struct _script_s  *script;  /**< @brief Points to internal script data.  */
  struct _ragdoll_s *physics; /**< @brief Points to internal physics data. */

  v2d_link_s *link; /**< @brief Linked list data. */

  /* TODO: Move these into their own structure...? */
  const anim_s *animation;  /**< @brief Pointer to the animation data struct. */
  v2d_bool_t   anim_loops;  /**< @brief If true, the animation should repeat. */
  v2d_real_t   anim_speed;  /**< @brief about */
  v2d_si32_t   anim_frame;  /**< @brief about */
  v2d_si32_t   anim_index;  /**< @brief Offset from the starting frame index. */
  v2d_real_t   anim_tween;  /**< @brief Current frame interpolation: 0.0-1.0. */

  /* Graphics-related things that scripts can tweak at runtime: */
  v2d_real_t texture_offset[2]; /**< @brief about */

} actor_s;

/**
@brief Delete a previously-created actor structure.

@param actor The actor to be deleted.
@return The next actor in the linked list, if there is one, or NULL if not.
*/

actor_s *actorDelete(actor_s *actor);

/**
@brief Create a new actor.

@param class_name The type of actor to be created. See cqClass.h for details.
@param x The initial X position of the actor.
@param y The initial Y position of the actor.
@param z The initial Z position of the actor.
@param angle The initial "turning" angle of the actor, measured in radians.
@return A new actor structure on success, or NULL on failure.
*/

actor_s *actorCreate(const char *class_name, v2d_real_t x, v2d_real_t y, v2d_real_t z, v2d_real_t angle);

/**
@brief Update the current AI-state of an actor.

Note that this does not update the actor physics - it's purely for AI purposes.
This is usually called once-per-game-loop by the worldUpdate() function.

@param actor The actor to be updated.
@return NOT YET DOCUMENTED - TO BE DEFINED LATER.
*/

int actorUpdate(actor_s *actor);

/**
@brief about

writeme

@param actor writeme
@param anim_name writeme
@param loop writeme
@return about
*/

int actorSetAnimation(actor_s *actor, const char *anim_name, v2d_bool_t loop);

/**
@brief about

writeme

@param actor about
@param speed about
@return writeme
*/

v2d_bool_t actorSetAnimationSpeed(actor_s *actor, v2d_real_t speed);

/**
@brief about

writeme

@param actor about
@return writeme
*/

v2d_bool_t actorAnimationIsPlaying(actor_s *actor);

/**
@brief about

writeme

@param actor about
@param include_physics_transformations about
@return writeme
*/

ACTOR_UPDATE_MESH(actorUpdateMesh);

#ifdef __cplusplus
}
#endif

#endif /* __CQ2_ACTOR_H__ */
