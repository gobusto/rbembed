# frozen_string_literal: true
require_relative './base_actor.rb'

# Provides some common functionality for players and human-ish AI characters.
class BaseCharacter < BaseActor
  def initialize(internal_id, angle)
    @me = internal_id
    @angle_turn = angle
    @angle_look = 0
    @standing_on_something = false
    @jump_cooldown = 0
  end

  # By default, assume that this actor is NOT standing on a surface...
  def think
    @standing_on_something = false
    @jump_cooldown -= 1 if @jump_cooldown.positive?
    false
  end

  # ...but override this if a suitable collision occurs:
  def collide(_is_actor, other, points)
    return false if other.is_a?(BaseCharacter)

    # Safely handle collision with other actors:
    result = other ? collide_with_actor(other, points) : nil
    return result unless result.nil?

    # Any collision points beneath the character means they're on a surface:
    my_position = Actor.get_position(@me)
    points.each do |point|
      x = point[0] - my_position[0]
      y = point[1] - my_position[1]
      z = point[2] - my_position[2]
      @standing_on_something ||= x.abs < 0.5 && y.abs < 0.5 && z < 0.0
    end

    false
  end

  def on_surface?
    @standing_on_something
  end

  def can_jump?
    @standing_on_something && @jump_cooldown.zero?
  end

  protected

  # Move this actor, relative to their current turning angle.
  def move(forward, strafe = 0)
    x, y = relative_xy(forward, strafe)
    z = Actor.get_velocity(@me)[2]
    Actor.set_velocity(@me, x, y, z)
  end

  # Move this actor WITHOUT overriding their velocity, using some fudging.
  def push(forward, strafe = 0, force: 500, damp: 250)
    x, y = relative_xy(forward, strafe)
    i, j, = Actor.get_velocity(@me)
    Actor.add_force(@me, (x * force) - (i * damp), (y * force) - (j * damp), 0)
  end

  # Jump, if the character is standing on a flat surface.
  def jump(velocity = 5)
    return false unless can_jump? && velocity.positive?
    x, y, = Actor.get_velocity(@me)
    Actor.set_velocity(@me, x, y, velocity)
    @jump_cooldown = 10 # Allow time for the actor to move away from the floor.
    true
  end

  def turn_left(angle)
    @angle_turn += angle
    @angle_turn %= Math::PI * 2
    Actor.set_angle(@me, @angle_turn)
  end

  def turn_right(angle)
    turn_left(-angle)
  end

  def look_up(angle)
    look_down(-angle)
  end

  def look_down(angle)
    @angle_look += angle
    @angle_look = Math::PI * -0.5 if @angle_look < Math::PI * -0.5
    @angle_look = Math::PI * +0.5 if @angle_look > Math::PI * +0.5
  end

  def turn_towards(xyz, turn_speed = 0.018)
    pos = Actor.get_position(@me)
    dif = (0..2).map { |i| xyz[i] - pos[i] }
    ideal_angle = Math.atan2(dif[1], dif[0])
    # Don't turn around several times trying to match the angle:
    @angle_turn += Math::PI * 2 while @angle_turn < ideal_angle - Math::PI
    @angle_turn -= Math::PI * 2 while @angle_turn > ideal_angle + Math::PI
    # Turn to face the specified point:
    if (@angle_turn - ideal_angle).abs < turn_speed
      @angle_turn = ideal_angle
    elsif @angle_turn < ideal_angle
      @angle_turn += turn_speed
    else
      @angle_turn -= turn_speed
    end
    Actor.set_angle(@me, @angle_turn)
    # Report the (squared) distance between this actor and the target point.
    (dif[0]**2) + (dif[1]**2)
  end

  private

  def relative_xy(forward, strafe)
    x = (Math.cos(@angle_turn) * forward) + (Math.sin(@angle_turn) * strafe)
    y = (Math.sin(@angle_turn) * forward) - (Math.cos(@angle_turn) * strafe)
    [x, y]
  end

  # Check whether this actor can collide with another one, avoiding recursion.
  def collide_with_actor(other, points)
    # If the other actor tries to invoke this code again, just say we're solid:
    @this_was_called_recursively ||= false
    return false if @this_was_called_recursively

    # Determine whether the other actor is "solid" or not:
    @this_was_called_recursively = true
    return true if other.collide(true, self, points)
    @this_was_called_recursively = false

    # If all else fails, let the "main" collision method carry on as normal:
    nil
  end
end
