# frozen_string_literal: true
require_relative './base_actor.rb'

# rubocop:disable all
class Player < BaseActor
  def initialize(id, angle)
    @delete_me = World.actors.any? { |actor| actor.instance_of?(Player) }
    return if @delete_me

    @TURN_SPEED = 0.04

    @coreid = id
    @angle_turn = angle
    @angle_look = 0
    @on_surface = false
    @timer = 0

    @item_list = []
    @waiting_to_switch = false
    @current_item = 0
    @next_held = false
    @use_held = false

    Sprite.create('logo', 'logo', 0, 210, 256, 32)
    Sprite.create('demotext', 'font', -(16*8) + 6, 180, 16, 16)
    Sprite.set_text('demotext', 'Engine tech demo')

    Sprite.create('crosshair', 'crosshairs', 0, 0, 32, 32)
    Sprite.set_subimage('crosshair', 0, 0.125, 0.125, 0.125)

    Sprite.create('healthicon', 'health', -296, -200, 32, 32)
    Sprite.set_rgba('healthicon', 200, 222, 255, 255)

    Sprite.create('healthcount', 'font', -264, -200, 32, 32)
    Sprite.set_rgba('healthcount', 200, 222, 255, 255)

    Sprite.create('ammoicon', 'ammo', 200, -200, 32, 32)
    Sprite.set_rgba('ammoicon', 200, 222, 255, 255)

    Sprite.create('ammocount', 'font', 232, -200, 32, 32)
    Sprite.set_rgba('ammocount', 200, 222, 255, 255)

    Sprite.create('targetname', 'font', 32, 32, 12, 16)
    Sprite.create('targetdistance', 'font', 40, 20, 8, 8)

    begin
      Actor.create('SuperShotgun', 0, 0, 0, 0).change_owner(self)
    rescue
    end

    # Initialise the player's ammo/health counters and update the HUD to match.
    @ammo = 0
    give_ammo(32)
    @health = 0
    give_health(100)
  end

  def alive?
    !@delete_me
  end

  # Get the player position. This is needed to position the gun and the camera.
  def get_position
    xyz = Actor.get_position(@coreid)
    xyz[2] += 0.5 # Eye position offset.
    xyz
  end

  # Set the player position. This is needed to change maps.
  def set_position(xyz)
    Actor.set_position(@coreid, xyz[0], xyz[1], xyz[2])
  end

  # Add an item to the player's inventory.
  def give_item(item)
    @item_list.push(item)
  end

  # Update the player's ammo counter, and update the HUD accordingly.
  def give_health(value)
    @health += value
    Sprite.set_text('healthcount', @health.to_s)
  end

  # Update the player's ammo counter, and update the HUD accordingly.
  def give_ammo(value)
    @ammo += value
    Sprite.set_text('ammocount', @ammo.to_s)
  end

  # Called once per game loop, before any dynamics or collision updates happen.
  def think
    return true if @delete_me

    World.destroy if UserInput.state('escape_key', true).positive?

    if UserInput.state('quick_save', true).positive?
      print 'quick saving... '
      puts(World.save_game('quicksave.json') ? 'OK!' : 'FAILED!')
    end

    if UserInput.state('quick_load', true).positive?
      print 'quick loading... '
      puts(World.load_game('quicksave.json') ? 'OK!' : 'FAILED!')
    end

    # Turn left or right.
    input_turn = (UserInput.state('turn_right', false) - UserInput.state('turn_left', false)) * @TURN_SPEED
    @angle_turn = (@angle_turn - input_turn) % (Math::PI * 2)
    quat = Support.convert_euler(0, 0, @angle_turn)
    Actor.set_quaternion(@coreid, quat[0], quat[1], quat[2], quat[3])

    # Look up or down.
    input_look = (UserInput.state('look_up', false) - UserInput.state('look_down', false)) * @TURN_SPEED
    @angle_look -= input_look
    @angle_look = Math::PI * -0.5 if @angle_look < Math::PI * -0.5
    @angle_look = Math::PI * +0.5 if @angle_look > Math::PI * +0.5

    # Get the current velocity of the player; this is needed below...
    ijk = Actor.get_velocity(@coreid)

    # If the player wants to jump (and can) then do so.
    if @on_surface && UserInput.state('jump', true) > 0 && ijk[2].abs < 0.5
      Actor.set_velocity(@coreid, ijk[0], ijk[1], 5)
      Sound.play('malejump', 0)
    end

    # Play a footstep noise.
    if @on_surface && (ijk[0].abs + ijk[1].abs) > 0.5 && (@timer % 25).zero?
      Sound.play('footstep', 0)
    end

    # Move around, relative to the current player angle.
    input_move = (UserInput.state('run', true) - UserInput.state('backpedal', true)) * 1000
    input_strafe = (UserInput.state('strafe_right', true) - UserInput.state('strafe_left', true)) * 1000

    move_x = (Math.cos(@angle_turn) * input_move) + (Math.sin(@angle_turn) * input_strafe)
    move_y = (Math.sin(@angle_turn) * input_move) - (Math.cos(@angle_turn) * input_strafe)

    Actor.add_force(@coreid, move_x - (ijk[0] * 250), move_y - (ijk[1] * 250), 0)

    # Set the camera position. Note that this includes the "eyes" offset.
    xyz = get_position
    quat = Support.convert_euler(0, @angle_look, @angle_turn)

    Camera.set_position(xyz[0], xyz[1], xyz[2], quat[0], quat[1], quat[2], quat[3], 110)
    Listener.set_position(xyz[0], xyz[1], xyz[2], @angle_turn)

    # Select the next weapon (if possible).
    if UserInput.state('nextweapon', true) > 0
      @waiting_for_switch = true unless @next_held && @item_list.length > 1
      @next_held = true
    else
      @next_held = false
    end

    # Work out some stuff used for positioning the HUD weapon and line-of-sight.
    sin_turn = Math.sin(@angle_turn)
    cos_turn = Math.cos(@angle_turn)
    sin_look = Math.sin(@angle_look)
    cos_look = Math.cos(@angle_look)

    # Hitscan test. Each hitscan is an array in [X, Y, Z, actor] format.
    dx = 50 *  cos_look * cos_turn
    dy = 50 *  cos_look * sin_turn
    dz = 50 * -sin_look

    nearest_thing = nil
    distance_to_current_nearest_thing = 0

    for hitscan in World.hitscan(xyz[0], xyz[1], xyz[2], dx, dy, dz)
      next if hitscan[3] == self # Ignore THIS actor for hitscan purposes...

      # Get the relative distance between this actor and the collision point.
      dx1 = hitscan[0] - xyz[0]
      dy1 = hitscan[1] - xyz[1]
      dz1 = hitscan[2] - xyz[2]
      distance = Math.sqrt(dx1 * dx1 + dy1 * dy1 + dz1 * dz1)

      unless nearest_thing || distance < distance_to_current_nearest_thing
        nearest_thing = hitscan
        distance_to_current_nearest_thing = distance
      end
    end

    want_to_do_the_thing = false
    if UserInput.state('use', true) > 0
      want_to_do_the_thing = true unless @use_held
      @use_held = true
    else
      @use_held = false
    end

    name_of_nearest_thing = ''
    if nearest_thing
      if nearest_thing[3].respond_to? :name
        name_of_nearest_thing = nearest_thing[3].name
      elsif nearest_thing[3].respond_to? :react_to_use
        name_of_nearest_thing = nearest_thing[3].class.to_s
      end
    end

    if name_of_nearest_thing != '' && distance_to_current_nearest_thing < 2.5
      Sprite.set_text('targetname', name_of_nearest_thing)
      Sprite.set_text('targetdistance', '%5.02fm' % distance_to_current_nearest_thing)

      if want_to_do_the_thing && nearest_thing[3].respond_to?(:react_to_use)
        nearest_thing[3].react_to_use(self)
      end
    else
      Sprite.set_text('targetname', '')
      Sprite.set_text('targetdistance', '')
    end

    # Update all items being carried.
    sway = [(ijk[0].abs + ijk[1].abs) * 0.3, 1.0].min
    dx = (sway * (Math.sin(@timer * 0.1) * 0.01))
    dy = (Math.sin(@timer * 0.03) * 0.003)

    i = 0
    for item in @item_list
      if i == @current_item
        if @waiting_for_switch
          @item_list[@current_item].put_in_bag
          if item.in_bag?
            @waiting_for_switch = false
            @current_item = (@current_item + 1) % @item_list.length
          end
        else
          @item_list[@current_item].prepare
          if UserInput.state('fire', true) > 0 && @ammo > 0
            if item.fire
              give_ammo(-1)
              begin
                nearest_thing[3].react_to_damage(10)
              rescue
              end
            end
          end
        end
        dz = 0 # Put the current weapon in front of the player.
      else
        dz = -5 # Hide any others behind the camera so they can't be seen.
      end

      item.set_position(xyz, quat, [dx, dy, dz], sin_turn, cos_turn, sin_look, cos_look)

      i += 1
    end

    # Assume that the player isn't on a surface; collide() may override this.
    @timer += 1
    @on_surface = false
    false
  end

  # The player needs to know if they're standing on something in order to jump.
  def collide(_is_actor, other, contact_list)
    # If the other actor is not solid, don't allow the player to jump off it!
    begin
      return true if other.collide(true, self, contact_list)
    rescue
    end

    # Any collision points beneath the player's feet mean they're on a surface.
    my_position = Actor.get_position(@coreid)

    contact_list.each do |contact|
      x = contact[0] - my_position[0]
      y = contact[1] - my_position[1]
      z = contact[2] - my_position[2]

      if x.abs < 0.5 && y.abs < 0.5 && z < 0.0
        @on_surface = true
        return false
      end
    end

    # For any other cases, just collide normally.
    false
  end

  def map_change(_new_map_file, _deletion_whether_you_like_it_or_not)
    true
  end
end
