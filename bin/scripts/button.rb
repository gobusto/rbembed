# frozen_string_literal: true
require_relative './base_button.rb'

# A button that can trigger various effects when pushed.
class Button < BaseButton
  def name
    'Press button'
  end

  def sound_name
    'button'
  end
end
