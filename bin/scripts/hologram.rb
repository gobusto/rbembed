# frozen_string_literal: true
require_relative './base_actor.rb'

# A semi-transparent hologram that spins in place.
class Hologram < BaseActor
  def initialize(id, angle)
    @coreid = id
    @timer = angle
    xyz = Actor.get_position(id)
    @sound_id = Sound.play3D('techpulse', -1, xyz[0], xyz[1], xyz[2], 7)
  end

  def think
    @timer += 1
    quat = Support.convert_euler(0, 0, @timer * 0.01)
    Actor.set_quaternion(@coreid, quat[0], quat[1], quat[2], quat[3])

    false
  end

  def collide(_is_actor, _other, _contact_list)
    true
  end

  def map_change(_filename, _force_deletion)
    Sound.stop(@sound_id)
    false
  end
end
