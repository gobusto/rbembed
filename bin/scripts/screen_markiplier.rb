# frozen_string_literal: true
require_relative './screen.rb'

# "Hello, everybody!"
class ScreenMarkiplier < Screen
  def initialize(_internal_id, _angle)
    super
    @reverse_when_finished = true
  end
end
