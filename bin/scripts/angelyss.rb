# frozen_string_literal: true
require_relative './base_character.rb'

# A simple AI character.
class Angelyss < BaseCharacter
  def initialize(internal_id, _angle)
    super
    @voice_time = 0
    @voice_id = 0
    @dead = false
    Actor.set_animation_speed(internal_id, 0.9)
  end

  def think
    return false if @dead

    @voice_time += Random.rand(4)
    if @voice_time > 600 && !Sound.playing?(@voice_id)
      @voice_time = 0
      xyz = Actor.get_position(@me)
      @voice_id = Sound.play3D('angelyss_idle', 0, xyz[0], xyz[1], xyz[2], 8)
    end

    turn_left(0.02)
    move(1.5)

    unless Actor.animating?(@me)
      animation = rand(2).zero? ? 'TORSO_GESTURE' : 'LEGS_WALK'
      Actor.set_animation(@me, animation, false)
    end

    super
  end

  def react_to_damage(_amount)
    return if @dead
    @dead = true

    xyz = Actor.get_position(@me)
    Sound.stop(@voice_id)
    Sound.play3D('angelyss_death', 0, xyz[0], xyz[1], xyz[2], 8)
    Actor.set_animation(@me, 'BOTH_DEATH1', false)
    Actor.set_velocity(@me, 0, 0, 0) # Stop her sliding around once dead.
  end

  def collide(_is_actor, other, _contact_list)
    return super unless @dead # Always collide if not dead!
    other # Otherwise, don't collide with SCRIPTED actors.
  end
end
