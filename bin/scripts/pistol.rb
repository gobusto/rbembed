# frozen_string_literal: true
require_relative './base_weapon.rb'

# Pistol weapon
class Pistol < BaseWeapon
  def reloadsound
    'null'
  end

  def firesound
    'pistolfire'
  end

  def hud_offs
    [0.10, -0.20, 0.45]
  end
end
