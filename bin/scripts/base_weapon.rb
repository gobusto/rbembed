# frozen_string_literal: true
require_relative './base_actor.rb'

# A handy base class for all weapon-type actors.
class BaseWeapon < BaseActor
  def reloadsound
    'ammoget'
  end

  def firesound
    'shotgunfire'
  end

  def hud_offs
    [0.18, -0.18, 0.45]
  end

  def initialize(id, angle)
    @coreid = id
    @owner = nil
    @timer = angle
    @recoil = 0
    @wanted = false
    @readiness = 0.0
  end

  # This is called by other actors in order to "take ownership" of the weapon.
  def change_owner(owner)
    @owner = owner
    @owner.give_item(self)
  end

  def fire
    return false unless can_fire?
    Sound.play(firesound, 0)
    UserInput.vibrate(0, 0.7, 400)
    @recoil = 60
    true
  end

  # Called once per game-loop.
  def think
    @timer += 1
    @recoil -= 1 if @recoil.positive?

    # If the weapon has no owner, just spin in place.
    unless @owner
      quat = Support.convert_euler(0, 0, @timer * 0.03)
      Actor.set_quaternion(@coreid, quat[0], quat[1], quat[2], quat[3])
    end

    # Get out / put away weapon.
    @readiness += @wanted ? 0.05 : -0.05
    @readiness = [@readiness, 1.0].min
    @readiness = [@readiness, 0.0].max

    false
  end

  # The weapon model is purely decorative, and shouldn't collide with anything...
  def collide(_is_actor, _other, _contact_list)
    true
  end

  def name
    @owner ? '' : "Pick up #{self.class}"
  end

  def react_to_use(user)
    return unless user.is_a? Player
    change_owner(user)
    Sound.play('ammoget', 0)
  end

  # This is called by the "owner" object to re-position the weapon.
  def set_position(xyz, quat, offs, sin_turn, cos_turn, sin_look, cos_look)
    dx = (hud_offs[0] + offs[0]) + ((1.0 - @readiness) * 0.0)
    dy = (hud_offs[1] + offs[1]) - ((1.0 - @readiness) * 0.4)
    dz = (hud_offs[2] + offs[2]) - ((1.0 - @readiness) * 0.4)

    dz -= (@recoil - 50) * 0.01 if @recoil > 50

    x = xyz[0] + ((dz * cos_look * cos_turn) + (dy * sin_look * cos_turn) + (dx * sin_turn))
    y = xyz[1] + ((dz * cos_look * sin_turn) + (dy * sin_look * sin_turn) - (dx * cos_turn))
    z = xyz[2] - ((dz * sin_look) - (dy * cos_look))

    Actor.set_position(@coreid, x, y, z)
    Actor.set_quaternion(@coreid, quat[0], quat[1], quat[2], quat[3])
  end

  def can_fire?
    ready? && @recoil <= 0
  end

  def ready?
    @wanted && @readiness >= 1.0
  end

  def in_bag?
    !@wanted && @readiness <= 0.0
  end

  def prepare
    return if @wanted
    @wanted = true
    Sound.play(reloadsound, 0)
  end

  def put_in_bag
    return unless can_fire? # Not ready to shoot = not ready to put away.
    @wanted = false
  end

  def map_change(_filename, _force_deletion)
    @owner ? true : false
  end
end
