# frozen_string_literal: true
require_relative './base_character.rb'

# "A Big Monster?" "A Big Monster!" "Let's let the Japanese take care of it..."
class Monster < BaseCharacter
  STOP_DISTANCE_SQUARED = 7

  def initialize(_internal_id, _angle)
    super
    @state = nil
    @target = nil
  end

  def think
    # If no current "target" is specified, look for a player.
    unless @target
      @target = World.actors.find { |a| a.is_a?(Player) && a.alive? }
    end

    # Decide what to do next...
    velocity = 0
    case @state
    when :idle
      # TODO: Randomly make some noises.
      # TODO: If the player is visible, switch to "find" mode.
    when :pain
      # Wait until the "pain" animation has finished playing...
      switch_to_find unless Actor.animating?(@me)
    when :find
      distance_squared = turn_towards(@target.get_position)
      if distance_squared < STOP_DISTANCE_SQUARED
        # The target is near enough; try to attack it:
        switch_to_attack
      else
        # The target is too far away; try to move towards it:
        velocity = 1.5
      end
    when :attack
      distance_squared = turn_towards(@target.get_position)
      # Don't move/attack again until the current animation finishes:
      unless Actor.animating?(@me)
        if distance_squared > STOP_DISTANCE_SQUARED
          # The target has moved away; try to move towards it again:
          switch_to_find
        else
          # Pick a new attack animation if the previous one has finished:
          switch_to_attack
        end
      end
    else
      # If we're in an unexpected state, default to being idle.
      switch_to_idle
    end

    # Prevent the player from simply pushing the monster away:
    move(velocity)
    # Don't ever delete this actor; keep their body around if they're dead.
    false
  end

  def react_to_damage(_amount)
    switch_to_pain
  end

  private

  # Some states have two possible animations; pick one randomly:
  def variation
    1 + rand(2)
  end

  def switch_to_idle
    @state = :idle
    Actor.set_animation_speed(@me, 1)
    Actor.set_animation(@me, 'idle1', true) # Don't bother with the variation.
  end

  def switch_to_pain
    @state = :pain
    Actor.set_animation_speed(@me, 2)
    Actor.set_animation(@me, "pain#{variation}", false)
    xyz = Actor.get_position(@me)
    Sound.play3D('monster_short2', 0, xyz[0], xyz[1], xyz[2], 20)
  end

  def switch_to_find
    @state = :find
    Actor.set_animation_speed(@me, 1.5)
    Actor.set_animation(@me, 'walk', true)
  end

  def switch_to_attack
    @state = :attack
    Actor.set_animation_speed(@me, 1.5)
    Actor.set_animation(@me, "attack#{variation}", false)
    xyz = Actor.get_position(@me)
    Sound.play3D('monster_short1', 0, xyz[0], xyz[1], xyz[2], 20)
  end
end
