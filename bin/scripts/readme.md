The Conqore 2 Ruby API
======================

Conqore 2 doesn't hard-code any game logic into the C source code. Instead, the
[Ruby](https://www.ruby-lang.org/) scripting language is used to control almost
every aspect of the actor behaviour, allowing new AI routines or actor types to
be added or amended without recompiling the game engine itself.

API reference
-------------

This section describes the core game engine functions available to Ruby scripts
at runtime. You should also look at the `base_actor.rb` file (in the `scripts/`
sub-directory) to get an idea of how/when the actor script methods are called.

### `World::destroy`

End the program at the next possible opportunity.

Always returns `nil`.

### `World::change_map(filename, clean)`

Specifies a new XML world, to be switched to as soon as possible. If `clean` is
true, all existing actors will be forcibly deleted before the new world file is
loaded.

Returns `true` on success, or `false` if not. `nil` may also be returned if the
`filename` parameter is invalid.

### `World::save_game(filename)`

Save the world state to a file.

Returns `true` on success, or `false` if not. `nil` may also be returned if the
`filename` parameter is invalid.

### `World::load_game(filename)`

Load the world state from a save-game file at the next possible opportunity.

Returns `true` on success, or `false` if not. `nil` may also be returned if the
`filename` parameter is invalid.

### `World::actors()`

Returns an array of all (scripted) actors that currently exist in the world. No
parameters are required.

### `World::hitscan(x, y, z, dx, dy, dz)`

This function performs a "hitscan" collision test for a 3D line.

Given the parameters `x`, `y`, `z`, `dx`, `dy`, `dz`, the physics engine checks
for all collision points with actors (scripted or non-scripted) and environment
and returns an array of contact points. Each contact is stored as an array with
4-items: `x`, `y`, `z`, and `actor`. If the contact point is for a non-scripted
actor or the environment, the `actor` parameter will be set to `nil`.

*TODO: Unlike the `collide()` method in `base_actor.rb`, it's not possible to
tell the difference between non-scripted actors and the environment here - this
should be changed to include an extra `is_actor` boolean to fix the problem.*

### `Support::locale()`

Returns the locale string defined in `config.xml` - no parameters are required.

### `Support::convert_euler(x, y, z)`

Takes three euler angles (`x`, `y`, and `z`) measured in radians, converts them
into a quaternion, and returns the result.

### `Camera::set_position(x, y, z, qw, qx, qy, qz, fov)`

Updates the position and angle of the 3D camera.

+ `x`, `y`, `z` define the position of the camera.
+ `qw`, `qx`, `qy`, `qz` define the rotation quaternion of the camera.
+ `fov` sets the FOV. (*Uses degrees; change to radians for consistency?*)

This function always returns nil.

### `Listener::set_position(x, y, z, angle)`

Updates the position and angle of the 3D listener.

+ `x`, `y`, `z` define the position of the "virtual ear" in 3D space.
+ `angle` defines the "turn" (Z-rotation) angle of the ear in radians.

This function always returns nil.

### `Actor::create(actor_type, x, y, z, angle)`

Creates a new actor instance. This works in the same way as actors "pre-placed"
in an XML game map file.

+ `actor_type` defines the type of actor to be created, such as `enemy_tank`.
+ `x`, `y`, `z` define the position of the new actor.
+ `angle` defines the "turn" (Z-rotation) angle of the actor in radians.

If the new actor is script-able, the Ruby actor object is returned. If not, the
return value will be `nil` instead.

### `Actor::set_texture_offset(actor, u, v)`

Offset the texture coordinates of an actor's mesh.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `u` is the horizontal offset of the texture coordinates.
+ `v` is the vertical offset of the texture coordinates.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::set_animation(actor, anim_name, loop)`

Changes the current animation sequence of an actor.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `anim_name` is the name of the animation sequence to play.
+ `loop` is a boolean value. If `false`, the animation plays once and stops.

Returns `true` on success or `false` on failure.

### `Actor::set_animation_speed(actor, speed)`

Changes the current animation speed of an actor.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `speed` is a floating point number, with `1.00` being normal speed and `0.50`
  being half as fast as usual.

Returns `true` on success or `false` on failure.

### `Actor::animating?(actor)`

Returns a boolean value, with `false` indicating that an actor is not currently
animating. This is useful for determining when a non-looping animation sequence
has finished.

`actor` is the `id` value of the actor (the one passed to `initialize`).

### `Actor::gravity?(actor)`

Determines if an actor is currently affected by gravity.

Returns `true` if the actor is affected by gravity, `false` if not. It may also
return `nil` if the actor parameter is invalid.

### `Actor::set_gravity(actor, enable)`

Enables or disables gravity for a specific actor.

Returns `true` on success or `false` on failure.

### `Actor::ragdoll!(actor)`

Attempts to convert a non-ragdoll actor into a ragdoll.

`actor` is the `id` value of the actor (the one passed to `initialize`).

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::get_position(actor)`

Gets the current XYZ position of an actor. Note that actor types without a mesh
will not have a meaningful position value, as they have no physical presence.

`actor` is the `id` value of the actor (the one passed to `initialize`).

Returns a 3-item array on success, or `nil` on failure.

### `Actor::set_position(actor, x, y, z)`

Sets the position of an actor.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `x`, `y`, `z` define the new position of the actor.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::get_velocity(actor)`

Gets the current XYZ velocity of an actor.

`actor` is the `id` value of the actor (the one passed to `initialize`).

Returns a 3-item array on success, or `nil` on failure.

### `Actor::set_velocity(actor, x, y, z)`

Replaces the current XYZ velocity of an actor.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `x`, `y`, `z` define the new velocity of the actor.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::add_force(actor, x, y, z)`

Applies a physical force to an actor along the X/Y/Z axes.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `x`, `y`, `z` define the force(s) to apply to the actor.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::set_angle(actor, angle)`

Sets the Z-rotation angle of an actor, assuming that the X and Y are zero. This
has the same effect as calling `Support::convert_euler` then passing the result
to `Actor::set_quaternion`.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `angle` defines the new Z-rotation angle of the actor in radians.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Actor::set_quaternion(actor, w, x, y, z)`

Sets the rotation quaternion of an actor.

+ `actor` is the `id` value of the actor (the one passed to `initialize`).
+ `w`, `x`, `y`, `z` define the new quaternion of the actor.

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `UserInput::state(name, clamp_range)`

Gets the state of a particular input "slot" by name. Physical input devices are
mapped to named "slots" in the `input.xml` file, so scripts don't need to worry
about whether the player is using a joypad, keyboard, or mouse.

If `clamp_range` is `true`, input values are restricted to a zero-to-one range.

The return value is a non-negative floating point number, with 0.0 representing
"no input" and a positive value representing button presses/mouse movement/etc.

### `UserInput::vibrate(pad_id, strength, length)`

Applies a rumble effect to a given joypad.

+ `pad_id` is the index of the joypad to vibrate.
+ `strength` should be a floating point value from zero to one.
+ `length` should be a integer number representing duration in milliseconds.

Returns `true` on success, or `false` on failure.

*TODO: This is likely to change in the future, as scripts do not know which pad
is being used by which player; they will be abstracted in the same way as input
mappings are. Additionally, effects other than a basic rumble may be allowed.*

### `Sound::play(sound, num_loops)`

Plays a sound effect.

+ `sound` is the name of the sound to play; see `soundset.xml` in `base/`.
+ `num_loops` gives the repetition count. Negative values mean "loop forever".

Returns a sound ID on success, -1 on failure, or `nil` for invalid parameters.

### `Sound::play3D(sound, num_loops, x, y, z, radius)`

Plays a sound effect with a position (and range-of-effect) in the 3D world.

+ `sound` is the name of the sound to play; see `soundset.xml` in `base/`.
+ `num_loops` gives the repetition count. Negative values mean "loop forever".
+ `x`, `y`, `z` define the "location" of the sound .
+ `radius` defines the effective radius of the sound.

Returns a sound ID on success, -1 on failure, or `nil` for invalid parameters.

### `Sound::stop(sound_id)`

Stops a previously-created sound effect from playing.

`sound_id` should be the ID that was returned by `Sound::play[3D]()` earlier.

Returns `true` on success, or `false` on failure.

### `Sound::playing?(sound_id)`

Determines if a previously-created sound effect is still playing.

`sound_id` should be the ID that was returned by `Sound::play[3D]()` earlier.

Returns `true` on success, or `false` on failure.

### `Sound::set_position(sound_id, x, y, z)`

Repositions a previously-created 3D sound effect.

+ `sound_id` should be the ID that was returned by `Sound::play3D()` earlier.
+ `x`, `y`, `z` define the new "location" of the sound.

Returns `true` on success, or `false` on failure.

### `Music::play(filename, num_loops)`

Plays a music file, optionally repeating it a given number of times.

+ `file_name` specifies the name (and path) of the music file to play.
+ `num_loops` gives the repetition count. Negative values mean "loop forever".

Returns `true` if it works, `false` if not, or `nil` if something goes wrong.

### `Music::stop()`

Stop any currently-playing music. This function doesn't require any parameters,
and always returns `nil`.

### `Music::playing?()`

Returns `true` if music is currently playing, or `false` if not - no parameters
are required.

### `Sprite::create(sprite_id, image_id, x, y, w, h)`

*not yet written*

### `Sprite::destroy(sprite_id)`

*not yet written*

### `Sprite::set_text(sprite_id, text)`

*not yet written*

### `Sprite::set_rgba(sprite_id, r, g, b, a)`

*not yet written*

### `Sprite::set_subimage(sprite_id, x, y, w, h)`

*not yet written*

### `Sprite::set_position(sprite_id, x, y)`

*not yet written*

### `Sprite::get_position(sprite_id)`

*not yet written*

### `Sprite::reset(sprite_id)`

*not yet written*

### `Sprite::rotate(sprite_id, angle)`

*not yet written*

### `Sprite::scale(sprite_id, x, y)`

*not yet written*

### `Sprite::shear(sprite_id, x, y)`

*not yet written*

License
-------

The Ruby source code files in this directory are intended to be used as a
starting point for any projects based on the Conqore2 game engine. Unless
noted otherwise, they are licensed under CC0 i.e. they are public domain:

<http://creativecommons.org/publicdomain/zero/1.0/>

NOTE: You don't have to license the modified scripts as CC0; any modified
versions belong to YOU, and you can do whatever you want with them.
