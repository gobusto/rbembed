# frozen_string_literal: true
require_relative './base_actor.rb'

# A button that can trigger various effects when pushed.
class BaseButton < BaseActor
  def initialize(id, _angle)
    @xyz = Actor.get_position(id)
    @signal = ''
  end

  def name
    'Use' # Subclasses may override this.
  end

  def sound_name
    nil # Subclasses may override this.
  end

  def message(name, value)
    @signal = value if name == 'signal'
  end

  def react_to_use(_user)
    Sound.play3D(sound_name, 0, @xyz[0], @xyz[1], @xyz[2], 5) if sound_name
    World.actors.each do |actor|
      actor.receive_signal(@signal) if actor.respond_to?(:receive_signal)
    end
  end
end
