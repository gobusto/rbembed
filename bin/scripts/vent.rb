# frozen_string_literal: true
require_relative './base_weapon.rb'

# An air vent that makes air vent noises.
class Vent < BaseActor
  def initialize(id, _angle)
    xyz = Actor.get_position(id)
    @sound_id = Sound.play3D('airflow', -1, xyz[0], xyz[1], xyz[2], 6)
  end

  def map_change(_filename, _force_deletion)
    Sound.stop(@sound_id)
    false
  end
end
