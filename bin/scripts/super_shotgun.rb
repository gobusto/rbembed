# frozen_string_literal: true
require_relative './base_weapon.rb'

# Super Shotgun
class SuperShotgun < BaseWeapon
  def firesound
    'supershotgunfire'
  end

  def reloadsound
    'shotgunreload'
  end

  def hud_offs
    [0.14, -0.18, 0.35]
  end
end
