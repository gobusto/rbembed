RSpec tests
===========

This directory is used to store various tests for the files in `scripts/`; none
of these are used by the game engine itself.
