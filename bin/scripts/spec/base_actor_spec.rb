# frozen_string_literal: true
require_relative '../base_actor.rb'

describe BaseActor do
  it 'receives messages' do
    actor = BaseActor.new(nil, 0)
    actor.message('name', 'value')
  end

  it 'thinks it should not be deleted' do
    actor = BaseActor.new(nil, 0)
    expect(actor.think).to be(false)
  end

  it 'collides normally' do
    actor = BaseActor.new(nil, 0)
    expect(actor.collide(false, nil, [])).to be(false)
  end

  it 'is deleted when the map changes' do
    actor = BaseActor.new(nil, 0)
    expect(actor.map_change('maps/start.xml', false)).to be(false)
  end
end
