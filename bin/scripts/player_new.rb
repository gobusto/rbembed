# frozen_string_literal: true
require_relative './base_character.rb'

# The player character. This handles user input and updates the camera/listener.
class Player < BaseCharacter
  def initialize(_id, _angle)
    super
    # Prevent duplicate players from being spawned when a new map loads:
    @delete_me = World.actors.any? { |actor| actor.is_a? Player }
  end

  def think
    handle_input
    update_camera
    super # Reset jump-related stuff.
    @delete_me
  end

  def map_change(_file_name, _force_deletion)
    true # Persist this player actor between levels.
  end

  private

  def handle_input
    World.destroy if UserInput.state('escape_key', true).positive?

    forward = scaled_inputs('run', 'backpedal', true)
    strafe = scaled_inputs('strafe_right', 'strafe_left', true)

    push(forward, strafe)
    turn_left scaled_inputs('turn_left', 'turn_right', false)
    look_down scaled_inputs('look_down', 'look_up', false)
    Sound.play('malejump', 0) if UserInput.state('jump', true).positive? && jump
  end

  def scaled_inputs(pos, neg, movement)
    scale = movement ? 2.0 : 0.04
    (UserInput.state(pos, movement) - UserInput.state(neg, movement)) * scale
  end

  # Reposition the camera/listener + make the actor mesh face the right way.
  def update_camera(eye_offset = 0.5)
    xyz = Actor.get_position(@me)
    xyz[2] += eye_offset
    q = Support.convert_euler(0, @angle_look, @angle_turn)
    Camera.set_position(xyz[0], xyz[1], xyz[2], q[0], q[1], q[2], q[3], 110)
    Listener.set_position(xyz[0], xyz[1], xyz[2], @angle_turn)
  end
end
