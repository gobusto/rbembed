# frozen_string_literal: true
require_relative './base_actor.rb'

# A door that moves directly upwards when it opens.
class BigDoor < BaseActor
  def initialize(id, _angle)
    @coreid = id
    @xyz = Actor.get_position(@coreid)
    @timer = 0
  end

  def think
    offset_z = Actor.get_position(@coreid)[2]

    if @timer.positive? && offset_z < @xyz[2] + 2.6
      Actor.set_position(@coreid, @xyz[0], @xyz[1], offset_z + 0.05)
    elsif !@timer.positive? && offset_z > @xyz[2]
      Actor.set_position(@coreid, @xyz[0], @xyz[1], offset_z - 0.05)
    end

    Sound.play3D('bigdoorclose', 0, @xyz[0], @xyz[1], @xyz[2], 6) if @timer == 1
    @timer -= 1 if @timer.positive?
    false
  end

  def collide(_is_actor, _other, _contact_list)
    if is_actor && !@timer.positive?
      @timer = 200
      Sound.play3D('bigdooropen', 0, @xyz[0], @xyz[1], @xyz[2], 6)
    end
    false
  end
end
