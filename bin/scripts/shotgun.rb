# frozen_string_literal: true
require_relative './base_weapon.rb'

# Shotgun - this uses the default settings from the base weapon class.
class Shotgun < BaseWeapon
end
