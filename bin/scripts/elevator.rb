# frozen_string_literal: true
require_relative './base_actor.rb'

# A platform that moves between two Z-positions when activated.
class Elevator < BaseActor
  def initialize(id, _angle)
    @coreid = id
    @signal = ''
    @xyz = Actor.get_position(@coreid)
    @z_offs = 0
    @speed = 0.02
    @enabled = false
  end

  def message(name, value)
    if name == 'signal'
      @signal = value
    elsif name == 'z'
      @z_offs = value.to_f
    end
  end

  def receive_signal(name)
    return if name != @signal
    @enabled = !@enabled
  end

  def think
    current_z = Actor.get_position(@coreid)[2]
    target_z = @enabled ? @z_offs : @xyz[2]

    if (current_z - target_z).abs < @speed
      Actor.set_position(@coreid, @xyz[0], @xyz[1], target_z)
    elsif current_z < target_z
      Actor.set_position(@coreid, @xyz[0], @xyz[1], current_z + @speed)
    elsif current_z > target_z
      Actor.set_position(@coreid, @xyz[0], @xyz[1], current_z - @speed)
    end

    false
  end
end
