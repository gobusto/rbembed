# frozen_string_literal: true

# This is the base class from which all other types are descended. You don't
# actually NEED to derive sub-classes from this - you could simply make your
# own class which implements the necessary "event response" methods. However,
# using this class as a starting point means that your custom sub-class will
# be equipped with convenient "default" responses from the very start.
class BaseActor
  # This is the constructor method called when an actor is first created. It
  # is passed an actor ID value, which can be used to get actor "state" data
  # back from the game engine, so it's important to store the ID somewhere.
  # The angle parameter is the initial Z-axis rotation of the actor.

  def initialize(internal_id, _angle)
    @coreid = internal_id # Pointer to the internal C structure for the actor.
  end

  # The message method is used to send special intialisation messages to an
  # actor, so that (for example) "generic" button actors can be set to affect
  # specific doors within a given map.

  def message(_name, _value)
    # puts "#{name} = #{value}"
  end

  # The think method is called once per logic-update for each actor. This
  # method can be used to make collectable coins spin, or AI characters move
  # towards the player, etc. It returns true if the actor should be deleted,
  # or false if not.

  def think
    false
  end

  # The collide method is called when an actor collides with something.
  # The contact_list array contains a list of XYZ collision points.
  #
  # * If is_actor is false, then the collision is with the environment.
  # * If is_actor is true, but "other" is nil, the other actor has no script.
  # * If is_actor is true, and "other" ISN'T nil, it's the other (Ruby) actor.
  #
  # If the method returns false (or, more accurately, a "zero" value), then
  # the actor can collide with things. If a non-zero value is returned, the
  # collision is ignored - useful for things like ghosts or holograms!

  def collide(_is_actor, _other, _contact_list)
    false
  end

  # The map_change method is called just before the game engine switches to
  # a new map. It returns true if the actor should be kept around, of false if
  # the actor should be deleted. If the force_deletion parameter is true, then
  # the actor will ALWAYS be deleted, no matter what the return value is. Note
  # that the filename parameter may be nil if there is no new map!

  def map_change(_filename, _force_deletion)
    false
  end
end
