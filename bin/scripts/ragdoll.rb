# frozen_string_literal: true
require_relative './base_character.rb'

# Ragdoll test actor.
class Ragdoll < BaseCharacter
  def message(name, _value)
    become_ragdoll(true) if name == 'startAsRagdoll'
  end

  def think
    turn_left(0.04) unless ragdoll?
    update_sound_position
    false
  end

  def collide(is_actor, _other, _contact_list)
    become_ragdoll if is_actor
    super
  end

  def name
    ragdoll? ? '' : 'Talk to Egbert'
  end

  def react_to_use(_user)
    play_sound('slimespeech') unless ragdoll?
  end

  def react_to_damage(_amount)
    become_ragdoll unless ragdoll?
  end

  private

  def play_sound(name)
    xyz = Actor.get_position(@me)
    Sound.stop(@sound) if sound_playing?
    @sound = Sound.play3D(name, 0, xyz[0], xyz[1], xyz[2], 12)
  end

  def update_sound_position
    return unless sound_playing?
    xyz = Actor.get_position(@me)
    Sound.set_position(@sound, xyz[0], xyz[1], xyz[2])
  end

  # FIXME: Should reset voice_id to `nil` once the sound stops.
  def sound_playing?
    @sound ||= nil
  end

  def ragdoll?
    @ragdoll ||= false
  end

  def become_ragdoll(quietly = false)
    return if ragdoll? || !Actor.ragdoll!(@me)

    @ragdoll = true
    play_sound('maledeath') unless quietly

    xyz = Actor.get_position(@me)
    Actor.create('PlasmaGun', xyz[0], xyz[1], xyz[2], @angle_turn)
  end
end
