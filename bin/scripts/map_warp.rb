# frozen_string_literal: true
require_relative './base_actor.rb'

# An object that changes the current map when interacted with.
class MapWarp < BaseActor
  def initialize(_internal_id, _angle)
    @description = '????'
    @filename = 'maps/start.xml'
    @newxyz = nil
  end

  def message(name, value)
    if name == 'description'
      @description = value
    elsif name == 'filename'
      @filename = value
    else
      axis = { 'newX' => 0, 'newY' => 1, 'newZ' => 2 }[name]
      return unless axis
      @newxyz = [0, 0, 0] unless @newxyz
      @newxyz[axis] = value.to_f
    end
  end

  def name
    "Travel to #{@description}"
  end

  def react_to_use(user)
    Sprite.set_text('targetdistance', "Loading #{@description}")
    World.change_map(@filename, false)
    user.set_position(@newxyz) if @newxyz
  end
end
