# frozen_string_literal: true
require_relative './base_actor.rb'

# A screen surface that displays an animated texture.
class Screen < BaseActor
  GAME_ENGINE_FRAMES_PER_SECOND = 60

  def initialize(internal_id, _angle)
    @me = internal_id
    @current_frame = 0
    @frame_delay = 0
    @reversing = false
    @smashed = false
    # Defaults; these may be overridden in subclasses:
    @total_frames = 10
    @frames_per_second = 15
    @reverse_when_finished = false
  end

  def think
    # If the engine runs at 60Hz and this is at 30Hz, delay the next frame:
    @frame_delay += @frames_per_second
    return false if @frame_delay < GAME_ENGINE_FRAMES_PER_SECOND
    @frame_delay %= GAME_ENGINE_FRAMES_PER_SECOND

    # Offset the texture coordinates vertically to show the next "frame":
    next_texture_animation_frame
    Actor.set_texture_offset(@me, 0, (1.0 / @total_frames) * @current_frame)

    # If this screen is smashed, it needs to be removed from the game world:
    @smashed
  end

  def react_to_damage(_amount)
    xyz = Actor.get_position(@me)
    Sound.play3D('glass-break', 0, xyz[0], xyz[1], xyz[2], 8.0)
    @smashed = true
  end

  private

  def next_texture_animation_frame
    if @current_frame.zero?
      @reversing = false
    elsif @current_frame == @total_frames - 1 && @reverse_when_finished
      @reversing = true
    end
    @current_frame += @reversing ? -1 : 1
    @current_frame %= @total_frames
  end
end
