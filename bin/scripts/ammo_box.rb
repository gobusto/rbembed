# frozen_string_literal: true
require_relative './base_actor.rb'

# A generic box of generic ammo.
class AmmoBox < BaseActor
  def initialize(_id, _angle)
    @kill_me = false
    @ammo_count = 20
  end

  def message(name, value)
    @ammo_count = value.to_i if name == 'setAmmo'
  end

  def think
    @kill_me
  end

  def name
    'Pick up ammo'
  end

  def react_to_use(user)
    Sound.play('ammoget', 0)
    user.give_ammo(@ammo_count)
    @kill_me = true
  end
end
