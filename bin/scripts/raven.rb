# frozen_string_literal: true
require_relative './base_actor.rb'

# birb
class Raven < BaseActor
  def initialize(internal_id, _angle)
    Actor.set_animation(internal_id, 'fly', true)
  end
end
