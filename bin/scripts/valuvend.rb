# frozen_string_literal: true
require_relative './base_weapon.rb'

# A vending machine.
class ValuVend < BaseActor
  def initialize(id, angle)
    @xyz = Actor.get_position(id)
    @angle = angle
  end

  def name
    'Use Val-U-Vend machine'
  end

  def react_to_use(_user)
    return unless Actor.create('AmmoBox', @xyz[0], @xyz[1], @xyz[2], @angle)
    Sound.play3D('venduse', 0, @xyz[0], @xyz[1], @xyz[2], 5)
  end
end
