# frozen_string_literal: true
require_relative './base_character.rb'

# Knight test actor.
class Knight < BaseCharacter
  def name
    'Knight'
  end

  def react_to_damage(_amount)
    Actor.ragdoll!(@me)
  end
end
