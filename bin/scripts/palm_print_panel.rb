# frozen_string_literal: true
require_relative './base_button.rb'

# A palm-print panel that can trigger various effects when pushed.
class PalmPrintPanel < BaseButton
  def initialize(internal_id, _angle)
    @me = internal_id
    @on = false
    super
  end

  def name
    'Scan palm'
  end

  def sound_name
    'scan-palm'
  end

  def react_to_use(_user)
    @on = !@on
    Actor.set_texture_offset(@me, @on ? 0.5 : 0.0, 0)
    super
  end
end
