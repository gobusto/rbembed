# frozen_string_literal: true
require_relative './base_weapon.rb'

# Plasmagun weapon; should really fire visible objects rather than hitscannning.
class PlasmaGun < BaseWeapon
  def reloadsound
    'plasmareload'
  end

  def firesound
    'plasmafire'
  end

  def hud_offs
    [0.14, -0.20, 0.40]
  end
end
