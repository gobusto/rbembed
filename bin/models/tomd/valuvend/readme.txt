ADDRESS: http://commons.wikimedia.org/wiki/File:Lingerie_vending_machine.jpg
AUTHORS: François Rejeté from Paris, moving to Japan, France
LICENCE: CC-BY 2.0

The main image used to create this was taken from Wikimedia Commons; the actual
mesh is public domain.
