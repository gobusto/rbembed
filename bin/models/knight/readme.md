# Animated Knight
## Clement Wu, Nikolaus & Botanic
### CC-BY 3.0 / CC-BY-SA 3.0 / GPL 2.0 / GPL 3.0
<https://opengameart.org/content/animated-knight>

Fully animated and textured :)

Designed for low poly application such as iOS and Android. Animations:

+ Attack
+ Idle
+ Hit
+ Pose
+ Walk
