# Beetle Golem [Animated]
## killyoverdrive
### CC-BY-SA 3.0
<http://opengameart.org/content/beetle-golem-animated>

NOTE: This model is based on a modified version of the one on OGA - It includes
part of this CC-BY-SA 3.0 skull image from Wikimedia Commons:

<https://commons.wikimedia.org/wiki/File:SkullFromTheFront.JPG>

...plus a few other public domain (CC0) images, such as:

+ <https://commons.wikimedia.org/wiki/File:Eye_dilate.gif>
+ <http://opengameart.org/node/50522>
