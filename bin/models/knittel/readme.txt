ADDRESS: http://opengameart.org/content/shuttle-2
AUTHORS: Kenten Fina
LICENCE: CC-BY 3.0

3D Model of a shuttle. I wasn't confident with my old one, so I made a new one.

828 Faces

UV unwrapped

2048x2048 texture included (diffuse, normal, glow: recommended use with the following Shader: color = lightintesity * diffuse + (1 - lightintesity) * glow)

Thanks to yuhgues, his textures were used in this model.
