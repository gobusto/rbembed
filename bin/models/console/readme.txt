ADDRESS: http://opengameart.org/content/scifi-console-mystical-shrine
AUTHORS: Kenten Fina
LICENCE: CC-BY 3.0

3D Model of a Console, but can be used as a Shrine as well (the model, the texture definetly not ;) )

438 Faces, UV unwrapped

1024x1024 sci fi texture for the console [including diffuse and glow map - recommended use with the following Shader: color = (lightintesity * diffuse + (1 - lightintesity) * glow) ]

- Thanks to yuhgues! His textures were used in the model.

