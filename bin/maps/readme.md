How Maps Work
=============

Each map is stored as a UTF-8 XML file, which should look something like this:

    <?xml version="1.0" encoding="utf-8"?>
    <ConqoreMap mesh='maps/start.obj' lights='maps/start.lights'>
      <Actor class='Crate' x='19.5' y='-55.5' z='1.1'>
        <Message name='contains' value='Ammo' />
      </Actor>
      <Actor class='Barrel' x='19.5' y='-20.5' z='1.5' />
      <Actor class='Barrel' x='18' y='-20.5' z='1.5' />
    </ConqoreMap>

General map details are specified as attributes on the main `<ConqoreMap>` tag,
with individual actors defined as `<Actor>` sub-tags. Each actor can optionally
have one or more `<Message>` sub-tags, allowing extra "setup" information to be
passed through to scripts.

The map itself
--------------

The `<ConqoreMap>` tag can have the following attributes:

+ `mesh` - The 3D model used for the "environment" by the graphics engine.
+ `collide` - The 3D model used for physics; `mesh` is used if this is missing.
+ `lights` - The "lighting" file. TODO: Fall back to `mesh` if this is missing.
+ `music` - The music file to play when the map is loaded by the game engine.

Note that none of these _have_ to be present (maps without music would omit the
`music` attribute, for example), and any "unknown" attributes are ignored.

Actors
------

Each `<Actor>` tag can have the following attributes:

+ `class` - Specifies which _type_ of actor this is, based on `classset.xml`.
+ `x` - Specifies the X position of the actor spawn-point.
+ `y` - Specifies the Y position of the actor spawn-point.
+ `z` - Specifies the Z position of the actor spawn-point.
+ `angle` - Specifies the "turn" angle of the actor in radians when spawned.

The `class` attribute _must_ be specified - however, `x`, `y`, `z`, and `angle`
will default to `0` if they're missing.

Actor tags may optionally have one or more `<Message/>` sub-tags, each with two
attributes: `name` and `value`. These are passed to the `message` method of the
Ruby script associated with the actor when the game engine first loads the map.

Miscellaneous
-------------

By default, the engine tries to load `maps/start.xml` when it first starts, but
this can be changed by specifying `--map mymap.xml` as a command-line argument.
