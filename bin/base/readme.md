Resource Definition Files
=========================

This directory contains "definition" files for actor types, sounds, and images.
Each file defines various things for the engine to load in when it starts up.

NOTE: All file paths should be relative to the location of the game executable.

Actor Classes
-------------

This file defines the possible "classes" of actor that can be placed in maps or
spawned by scripts:

    <?xml version="1.0" encoding="utf-8"?>
    <ClassSet>
      <Class name="Player" script="scripts/player.rb" mesh="models/players/default/aabb.obj" shape="sphere" weight="55" nospin="true"/>
      <Class name="Pistol" script="scripts/pistol.rb" mesh="models/weapons/Dm3d/Pistol.obj" scale="0.28" static="true"/>
      <Class name="TrafficCone" mesh="models/traffic-cone/low.obj" shape="trimesh" weight="2"/>
    </ClassSet>

Possible attributes are described below:

+ "name" specifies the name of the class. All 'Class' tags should include this,
  ESPECIALLY if they specify a script file - see below...
+ "script" specifies a Ruby .rb script file to control the behaviour of actors.
  This does not have to be provided; decorative items such as crates or barrels
  won't need scripts (unless you want to be able to break them open, or perhaps
  play a sound effect when something bumps into them). If you want to know more
  about how scripts work, see the CoreObject.rb file in the scripts/ directory.
  Note that the file MUST contain a Ruby class with the same name as the "name"
  attribute of this 'Class' XML tag, though the filename itself doesn't matter.
+ "mesh" specifies a 3D model used to represent actors of this type. Classes do
  not NEED to include this, but be aware that actors without a mesh file do not
  have a position/size/etc. since they don't physically exist. They can be used
  to do other things, though - for example, an "EnemyFactory" class could spawn
  a new enemy every 10 seconds using a script file.
+ "material" overrides the default mesh materials, loading them from a specific
  MTL or SKIN file instead. You won't usually need to specify this; just change
  the mesh file (or associated .MTL file) instead.
+ "animation" specifies the file containing animation data for the 3D model.
+ "shape" specifies the general shape of a class for physics purposes. Possible
  values are 'box', 'sphere', 'capsule', 'cylinder', and 'trimesh'. If missing,
  'box' is assumed.
+ "scale", "xscale", "yscale", and "zscale" resize the 3D model for physics and
  rendering purposes. A value of 0.5 makes the mesh half as big, and 2 makes it
  twice the usual size. "scale" applies first, with "xscale" and co. overriding
  it for a specific axis.
+ "static" causes actors ignore ALL physical forces if set to 'true', which can
  be used to create solid but non-pushable objects, such as floating platforms.
  Note that actors of this type can still be moved about if they are explicitly
  repositioned by scripts.
+ "nospin" prevents actors from rotating if set to 'true'. Useful for humanoids
  modelled physically as capsules or spheres, since they will need to be pushed
  around by physical forces without rolling over on to their sides.
+ "weight" specifies the weight of actors in kilograms. This should be included
  unless the 'static' attribute is set to true.

Sprite Images
-------------

This file defines all images that can be used by "sprite" instances:

    <?xml version="1.0" encoding="utf-8"?>
    <ImageSet>
      <Image name="font">
        <File path="sprites/fonts-liberation/liberation-mono.png" clamp="true"/>
      </Image>
      <Image name="crosshairs">
        <File path="sprites/para/crosshairs64.png" clamp="true"/>
      </Image>
    </ImageSet>

Each Image tag should have a "name" attribute (used by sprites to specify which
image they want to use) and one or more "File" tags with the following attributes:

+ "path" specifies the location of the image file.
+ "locale" can be used to restrict a "File" tag to a specific locale code - see
  config.xml for the current locale setting. If this attribute is not included,
  the file is assumed to apply to ALL locale codes.
+ "smoothing" can be set to "nearest" to disable bilinear texture filtering.
+ "clamp", "clampx", and "clampy" can be used to disable pixel wrapping if they
  are set to 'true'.

Sounds
------

This file defines all possible sound effects that the game engine can play:

    <?xml version="1.0" encoding="utf-8"?>
    <SoundSet>
      <Sound name="button">
        <File path="sounds/tomd/press.wav"/>
      </Sound>
      <Sound name="footstep">
        <File path="sounds/kenney/footstep05_modified.wav"/>
      </Sound>
    </SoundSet>

The "Sound" and "File" tags are explained below:

+ "Sound" tags have just one attribute: "name". This is a unique ID value, used
  by scripts to specify which sound they want to play.
+ Each "Sound" tag should contain one or more "File" tags. "File" tags can have
  one or two attributes: "path" (required) and "locale" (optional), with "path"
  specifying the audio file to be loaded and "locale" restricting it to a given
  language code (see the config.xml file). If no "locale" attribute is defined,
  the sound is assumed to apply to ALL locale codes.

Miscellaneous
-------------

The game engine always tries to load `base/classset.xml`, `base/imageset.xml`,
and `base/soundset.xml` when it first starts up, but it is possible to specify
_additional_ `*set.xml` files as command-line arguments, allowing game mods to
be created without needing to edit the "main" .XML files - see the user manual
for further details.
