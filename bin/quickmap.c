/*
To quickly generate/test a map:

    rm ./quickmap
    gcc quickmap.c -o quickmap -Wall
    ./quickmap start.txt maps/start
    ./conqore2
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define src_file argv[1]
#define map_name argv[2]

#define TILE_W 3.0
#define TILE_H 4.0

#define LIGHT_HEIGHT 3.0
#define LIGHT_RADIUS 10.0

int main(int argc, char **argv)
{
  char tmp[512];
  int chr;
  int t = 0;
  int x = 0;
  int y = 0;

  FILE *src;
  FILE *obj;
  FILE *lit;
  FILE *xml;

  if (argc < 3)
  {
    fprintf(stderr, "Usage: %s path/to/input.txt path/to/map\n", argv[0]);
    return 1;
  }

  /* This is the original plain-text file that we will read from: */
  src = fopen(src_file, "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open %s\n", src_file);
    return 2;
  }

  /* This is the OBJ mesh file that we will use for rendering/physics: */
  sprintf(tmp, "%s.obj", map_name);
  obj = fopen(tmp, "wb");
  if (!obj)
  {
    fclose(src);
    fprintf(stderr, "Could not open %s\n", tmp);
    return 2;
  }

  /* If possible, try to generate a lights definition file too: */
  sprintf(tmp, "%s.lights", map_name);
  lit = fopen(tmp, "wb");
  if (!lit) { fprintf(stderr, "Could not open %s\n", tmp); }

  /* If possible, try to generate the main XML file too: */
  sprintf(tmp, "%s.xml", map_name);
  xml = fopen(tmp, "wb");
  if (xml)
  {
    fprintf(xml, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    fprintf(xml, "<ConqoreMap mesh='%s.obj' lights='%s.lights' music='music/tomd/ambient1.wav'>\n", map_name, map_name);
  }
  else { fprintf(stderr, "Could not open %s\n", tmp); }

  /* Define some basic stuff for the OBJ file: */
  fprintf(obj, "mtllib %s.mtl\n", map_name);
  fprintf(obj, "vt 0 0\nvt 1 0\nvt 1 1\nvt 0 1\n");
  fprintf(obj, "vn  0 -1  0\n");
  fprintf(obj, "vn  0  1  0\n");
  fprintf(obj, "vn -1  0  0\n");
  fprintf(obj, "vn  1  0  0\n");
  fprintf(obj, "vn  0  0 -1\n");
  fprintf(obj, "vn  0  0  1\n");

  /* Each character within the file is treated as a tile within a grid: */
  while (EOF != (chr = fgetc(src)))
  {
    /* Ignore carriage-returns; only line-feeds are used here. */
    if (chr == '\r') { continue; }

    /* Whitespace characters don't generate anything... */
    if (!isspace(chr))
    {
      float ex = (x + 0.5) * TILE_W;
      float ey = (y + 0.5) * TILE_W;
      float ez = 1.1;

      fprintf(obj, "v %f %f %f\n", (x+0)*TILE_W, (y+0)*TILE_W , 0.0);
      fprintf(obj, "v %f %f %f\n", (x+1)*TILE_W, (y+0)*TILE_W , 0.0);
      fprintf(obj, "v %f %f %f\n", (x+1)*TILE_W, (y+1)*TILE_W , 0.0);
      fprintf(obj, "v %f %f %f\n", (x+0)*TILE_W, (y+1)*TILE_W , 0.0);
      fprintf(obj, "v %f %f %f\n", (x+0)*TILE_W, (y+0)*TILE_W , TILE_H);
      fprintf(obj, "v %f %f %f\n", (x+1)*TILE_W, (y+0)*TILE_W , TILE_H);
      fprintf(obj, "v %f %f %f\n", (x+1)*TILE_W, (y+1)*TILE_W , TILE_H);
      fprintf(obj, "v %f %f %f\n", (x+0)*TILE_W, (y+1)*TILE_W , TILE_H);

      if (isalnum(chr))
      {
        /* Alphabetic or numeric characters are treated as walls: */
        fprintf(obj, "g wall%d%d\nusemtl wall_%c\n", x, y, chr);
        fprintf(obj, "f %d/1/1 %d/2/1 %d/3/1 %d/4/1\n", (t*8)+1, (t*8)+2, (t*8)+6, (t*8)+5);
        fprintf(obj, "f %d/1/2 %d/2/2 %d/3/2 %d/4/2\n", (t*8)+3, (t*8)+4, (t*8)+8, (t*8)+7);
        fprintf(obj, "f %d/1/3 %d/2/3 %d/3/3 %d/4/3\n", (t*8)+4, (t*8)+1, (t*8)+5, (t*8)+8);
        fprintf(obj, "f %d/1/4 %d/2/4 %d/3/4 %d/4/4\n", (t*8)+2, (t*8)+3, (t*8)+7, (t*8)+6);
      }
      else
      {
        /* Punctuation characters are treated as empty squares: */
        fprintf(obj, "g ceiling%d%d\nusemtl %s\n", x, y, chr == '@' ? "light" : "ceiling");
        fprintf(obj, "f %d/1/5 %d/2/5 %d/3/5 %d/4/5\n", (t*8)+8, (t*8)+7, (t*8)+6, (t*8)+5);
        fprintf(obj, "g floor%d%d\nusemtl floor\n", x, y);
        fprintf(obj, "f %d/1/6 %d/2/6 %d/3/6 %d/4/6\n", (t*8)+1, (t*8)+2, (t*8)+3, (t*8)+4);
      }

      /* Interpret the @ character as a light source: */
      if (lit && chr == '@')
      {
        fprintf(lit, "point %f %f %f %f 1 1 1\n", ex, ey, LIGHT_HEIGHT, LIGHT_RADIUS);
      }

      /* Interpret punctuation (other than `_`) as entity spawns: */
      if (xml && ispunct(chr) && chr != '@' && chr != '_')
      {
        fprintf(xml, "  <Actor class='");
        switch (chr)
        {
          case '$': fprintf(xml, "Player"); break;
          case '%': fprintf(xml, "Cart"); break;
          case '&': fprintf(xml, "Angelyss"); break;
          case '*': fprintf(xml, "Ragdoll"); break;
          case '^': fprintf(xml, "TrafficCone"); break;
          case '~': fprintf(xml, "Console"); break;
          case ';': fprintf(xml, "AmmoBox"); break;
          case ':': fprintf(xml, "ValuVend"); break;
          case '!': fprintf(xml, "Monster"); ez = 1.5; break;
          default:  fprintf(xml, "Crate"); break;
        }
        fprintf(xml, "' x='%f' y='%f' z='%f'/>\n", ex, ey, ez);
      }

      ++t;
    }

    /* Newlines start a new row; anything else just moves to the next column. */
    if (chr == '\n') { --y; x = 0; }
    else             { ++x;        }
  }

  /* Close any files that we have open: */
  if (xml)
  {
    fprintf(xml, "</ConqoreMap>\n");
    fclose(xml);
  }

  if (lit) { fclose(lit); }
  fclose(obj);
  fclose(src);

  /* Enable this to generate a skeletal materials file: */
  #if 1
    sprintf(tmp, "%s.mtl", map_name);
    obj = fopen(tmp, "wb");
    if (obj)
    {
      fprintf(obj, "newmtl floor\n  map_Kd textures/crimzan/hexagon/HexagonTile_SPEC.png\n\n");
      fprintf(obj, "newmtl ceiling\n  map_Kd textures/crimzan/hexagon/HexagonTile_DIFF.png\n");
      fprintf(obj, "newmtl light\n  map_Kd textures/crimzan/hexagon/HexagonTile_HGT.png\n");
      fprintf(obj, "newmtl wall_0\n  map_Kd textures/p0ss/rusted-metal/metall010-new-tileable-bar_0.png\n");
      fclose(obj);
    }
  #endif

  /* Success! */
  return 0;
}
