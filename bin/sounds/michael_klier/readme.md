# Large Monster
## Michael Klier
### CC-BY-SA 3.0
<http://opengameart.org/content/large-monster>

Growls and grunts of a large Monster / Creature.
