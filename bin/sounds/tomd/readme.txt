ADDRESS: http://opengameart.org/content/ambient-pulse-noise
         http://opengameart.org/content/button-press
AUTHORS: Thomas Glyn Dennis
LICENCE: CC-BY-SA 3.0

A general background "future-tech" noise + a computer-ish button noise.
