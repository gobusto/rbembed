Conqore 2 - User Manual
=======================

This document is intended to cover anything that might be useful to "end-users"
who might want to run games (or create mods) without touching the C source code
for the game engine itself.

More will be added here as time goes by.

Command-line parameters
-----------------------

Some things about the engine can be tweaked via command-line options. These are
described below.

### --renderer

There are currently two possible ways to compile Conqore:

+ As an "all-in-one" executable, with a renderer built-in at compile time.
+ As a renderer-less executable, with a renderer loaded from a .DLL at runtime.

In the latter case, the engine will attempt to load `./render_basic.*` (with `*`
being `dll`, `so`, or `dylib` depending on the operating system) when it starts
up. This can be changed using `--renderer ./someotherfile.dll` via the command
line:

    > ./conqore2-ext-render --renderer ./render_dummy.so

Obviously, this has no effect if the "all-in-one" executable is being used...

### --map

By default, the engine tries to load `maps/start.xml` when it first starts. You
can use `--map mymaps/mymapfile.xml` to have it load `mymapfile.xml` instead.

### --actors

The engine always tries to load `base/classset.xml` when it first starts. Extra
class definition files can be loaded in using `--actors path/to/myactorset.xml`.

### --images

The engine always tries to load `base/imageset.xml` when it first starts. Extra
image definition files can be loaded in using `--actors path/to/myimageset.xml`.

### --sounds

The engine always tries to load `base/soundset.xml` when it first starts. Extra
sound definition files can be loaded in using `--actors path/to/mysoundset.xml`.

### --stderr

Error messages can be redirected to a file via `--stderr filename.txt`.
