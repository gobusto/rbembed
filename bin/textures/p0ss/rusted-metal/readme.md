# Rusted metal texture pack
## p0ss
### CC-BY 3.0 or GPL 2.0+
<http://opengameart.org/node/9086>

This pack contains 3 1024x1024 metal textures with normal maps (alpha as height)
and .xcfs for each.

These textures were created for UFO:AI to replace non GPL versions.
