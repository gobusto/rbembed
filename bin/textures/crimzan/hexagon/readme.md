# Sci-Fi Hexagon Texture
## Dustin 'Crimzan' Ludwig
### CC-BY 3.0
<http://opengameart.org/content/sci-fi-hexagon-texture>

Hello,

I created a high resolution, detailed Sci-Fi texture for use in anything you can
imagine. Since it's actually a small texture, it can be tiled together a lot of
times so that it is sharp even when looking at it at a very close range.

This is my first submission, I'm not a proffesional, and still have a lot to
learn. Many hours of my free time went into this material, feel free to
criticize my work, but please don't hate or insult me ;)  - I only want to be
mentioned like how I wrote in the Copyright-Notice.

It is a Deus Ex (1) inspired Hexagon Texture very similar to the one in the MJ12
labs.

I hope you like it! :)

-Crimi

PS: The LGT texture is a texture with fixed lighting information I made for a
tutorial on this website. You can use this texture if your project doesn't need
or support normal- or specularmaps.  If you want to change the lighting of the
texture yourself, you can go ahead and do so while revisiting my tutorial:

<http://opengameart.org/forumtopic/normal-diffuse-specular-baked-lighting-information-for-old-engines-or-backgrounds>
