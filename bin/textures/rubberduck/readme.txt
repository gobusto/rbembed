ADDRESS: http://opengameart.org/node/27498
         http://opengameart.org/node/19429
         http://opengameart.org/node/19379
AUTHORS: rubberduck
LICENCE: CC0 (Public Domain)

this pack contains 50 free seamless textures

the first 25 textures are from paras cc0 texture resources-pack:

http://opengameart.org/content/avoid-the-germs-hunt-the-gems-3-misc-unso...

the rest comes from myself (texture resources can be found in my texture resource packs)

the quality is higher than my previous pack (http://opengameart.org/content/50-free-textures-4-normalmaps), because there are less visible repeating patterns.

you can compare these textures from both packs to see the difference

included are metal, wood, textile, nature, wall/brick, stone and roof textures
