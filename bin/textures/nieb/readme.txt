ADDRESS: opengameart.org/content/niebs-tesseract-textures
AUTHORS: nieb
LICENCE: CC0 (Public Domain)

PNG textures of various resolutions (up to 512 × 512), with normal, specular and parallax maps.
