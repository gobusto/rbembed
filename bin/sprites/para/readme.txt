ADDRESS: http://opengameart.org/content/64-crosshairs-pack
AUTHORS: para
LICENCE: CC0 (Public Domain)

A pack of 64 FPS style crosshairs, most are intended as helpers for weapoin aiming, some are more general case 'icons' .. 

 

- they are 64x64 in size and laid out sprite-sheet style on a 512x512 texture.

- graysccale, easy to tint or re-color in code

- don't scale very well, sorry for that.

- public domain, no worries, no micro-management of attributions

 

Feedback and action shots more than welcome! 

