ADDRESS: http://opengameart.org/content/forqu3s-vector-icons-as-pngs
AUTHORS: Forqu3
LICENCE: CC0 (Public Domain)

the popular Vector Icons, screenshotted and clipped, then converted to pngs.
